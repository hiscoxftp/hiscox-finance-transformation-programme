<?xml version="1.0" encoding="UTF-8" ?> 
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="no" /> 
  <xsl:output method="xml" /> 
  <xsl:key name="contacts-by-LogicalGroupReference" match="OutboundPayment" use="LogicalGrouping/LogicalGroupReference" /> 
 <xsl:template match="OutboundPaymentInstruction">
 <Document  xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 
<xsl:variable name="instrid" select="PaymentInstructionInfo/InstructionReferenceNumber" /> 
<xsl:variable name="apos">'</xsl:variable>
 <!-- Commented and added below for BER HSBC ACH <CstmrCdtTrfInitn> -->
 <pain.001.001.02>
 <!-- Addition End -->
 <GrpHdr>
 <MsgId>
  <xsl:value-of select="$instrid" /> 
  </MsgId>
  <CreDtTm>
  <xsl:choose>
	<xsl:when test="contains(PaymentInstructionInfo/InstructionCreationDate , '+')">
		<xsl:value-of select="substring(PaymentInstructionInfo/InstructionCreationDate , 1,	string-length(PaymentInstructionInfo/InstructionCreationDate)-6)"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="PaymentInstructionInfo/InstructionCreationDate" />
	</xsl:otherwise>
  </xsl:choose> 
  </CreDtTm>
 <NbOfTxs>
  <xsl:value-of select="InstructionTotals/PaymentCount" /> 
  </NbOfTxs>
 <CtrlSum>
    <xsl:value-of select="format-number(sum(OutboundPayment/PaymentAmount/Value), '##0.00')"/>
  </CtrlSum>
  <!-- Added below for BER HSBC ACH -->
  <Grpg>MIXD</Grpg>
  <!-- Addition End -->
 <InitgPty>
 <!-- Commented below for BER HSBC ACH -->
 <!--<Nm>
  <xsl:choose>
  <xsl:when test="(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) and (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) != ''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="InstructionGrouping/Payer/LegalEntityName" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Nm>
  -->
 <Id>
 <OrgId>
 <!-- Commented and added below for BER HSBC ACH
 <xsl:if test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value) or (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value='')">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value !=''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
  </Othr>
  </xsl:if>-->
  <!--Refer below<BkPtyId>
   <xsl:value-of select="substring(/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber,1,35)" /> 
   </BkPtyId>-->
   <BkPtyId>ABC65688001</BkPtyId>
  </OrgId>
  </Id>
  </InitgPty>
  <FwdgAgt>
  <FinInstnId>
	<PrtryId>
		<Id>HSBC CONNECT</Id>
	</PrtryId>
	</FinInstnId>
  </FwdgAgt>
  </GrpHdr>
 <xsl:for-each select="OutboundPayment[count(. | key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)[1]) = 1]">
  <xsl:sort select="LogicalGrouping/LogicalGroupReference" /> 
<!--Start of payment information block-->
 <PmtInf>
 <xsl:if test="not(LogicalGrouping/LogicalGroupReference='')">
 <PmtInfId>
  <xsl:value-of select="substring(translate(LogicalGrouping/LogicalGroupReference, translate(translate(LogicalGrouping/LogicalGroupReference,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,140)" />
  </PmtInfId>
 <!--Commenting since not required
 <PmtInfId>
  <xsl:value-of select="substring(LogicalGrouping/LogicalGroupReference,1,35)" /> 
  </PmtInfId>-->
 <!--Commenting since not required
 <PmtInfId>
  <xsl:value-of select="substring(LogicalGrouping/LogicalGroupReference,1,35)" /> 
  </PmtInfId>-->
  </xsl:if>
  <PmtMtd>TRF</PmtMtd> 
   <!-- Commenting below for BER HSBC ACH -->
  <!--<xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='')">
 <BtchBookg>
 <xsl:choose>
 <xsl:when test="(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='N')">
  <xsl:text>false</xsl:text> 
  </xsl:when>
 <xsl:otherwise>
  <xsl:text>true</xsl:text> 
  </xsl:otherwise>
  </xsl:choose>
  </BtchBookg>
  </xsl:if>-->
  <!--better to use attributes from new logical grp table -->
  <!-- Commenting below for BER HSBC ACH -->
  <!-- <NbOfTxs>
  <xsl:value-of select="LogicalGrouping/PaymentInformationTotal" /> 
  </NbOfTxs>
 <CtrlSum>
  <xsl:value-of select="format-number(LogicalGrouping/PaymentInformationAmountTotal, '##0.00')" /> 
  </CtrlSum>  -->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') or not
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
<!--Start of payment type information block-->
 <PmtTpInf>
  <!-- Commented and added below for BER HSBC ACH
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='Y')">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if> -->
  <ClrChanl>MPNS</ClrChanl>
    <!-- Addition End -->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
    <!-- Commenting below for BER HSBC ACH 
  <xsl:if test="not(ServiceLevel/Code='')">  
 <SvcLvl>
  <Cd>
  <xsl:value-of select="ServiceLevel/Code" /> 
  </Cd> 
  </SvcLvl>
  </xsl:if>
  -->
  <!-- Commenting this for BER HSBC ACH <xsl:if test="not(DeliveryChannel/Code='')"> -->
  <LclInstrm>
  <Prtry>B</Prtry> 
  </LclInstrm>
   <!-- Addition End -->
 <!-- Commenting the end tag </xsl:if> -->
  </xsl:if>
  </xsl:if>
   <!-- Commenting below for BER HSBC ACH
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
  <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>
  -->
  </PmtTpInf>
<!--End of payment type information block-->
  </xsl:if>
 <ReqdExctnDt>
  <xsl:value-of select="PaymentDate" /> 
  </ReqdExctnDt>
 <Dbtr>
  <Nm>
  <xsl:value-of select="substring(translate(Payer/Name, translate(translate(Payer/Name,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), ''),1,140)" /> 
  </Nm>
  <!--commenting sinnce not required<Nm>
  <xsl:value-of select="substring(Payer/Name,1,140)" /> 
  </Nm>-->
 <PstlAdr>
 <!-- Commenting below for BER HSBC ACH
  <StrtNm>
  <xsl:value-of select="Payer/Address/AddressLine1" /> 
  </StrtNm>
  <PstCd>
  <xsl:value-of select="Payer/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="Payer/Address/City" /> 
  </TwnNm>
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payer/Address/County" /> 
  <xsl:value-of select="Payer/Address/State" /> 
  <xsl:value-of select="Payer/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if>
  -->
  <AdrLine>
  <!--<xsl:value-of select="SupplierorParty/Address/AddressLine1" /> -->
  <xsl:value-of select="substring(translate(Payer/Address/AddressLine1, translate(translate(Payer/Address/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:()., + ',''),$apos,''), ''),1,35)" /> 
  </AdrLine>
 <xsl:if test="not(Payer/Address/AddressLine2='')"> 
 <AdrLine>
  <!-- <xsl:value-of select="SupplierorParty/Address/AddressLine2" /> -->
  <xsl:value-of select="substring(translate(Payer/Address/AddressLine2, translate(translate(Payer/Address/AddressLine2,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:()., + ',''),$apos,''), ''),1,35)" /> 
  </AdrLine>
  </xsl:if>
 <xsl:if test="not(Payer/Address/Country='')">
 <Ctry>
  <xsl:value-of select="Payer/Address/Country" /> 
  </Ctry>
 </xsl:if>
  </PstlAdr>
  <!-- Commenting below for BER HSBC ACH -->
  <!--
 <Id>
 <OrgId>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value) or /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value = ''">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value != ''">
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>

 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value!=''">
  <SchmeNm>
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value"/> 
  </Cd>
  </SchmeNm>
  </xsl:if>
  </Othr>
  </xsl:if>
  </OrgId>
  </Id>
    -->
 </Dbtr>
  <!-- Commented and added below for BER HSBC ACH
 <DbtrAcct>
 <Id>
 <IBAN>
  <xsl:value-of select="BankAccount/IBANNumber" /> 
  </IBAN>
  </Id>
 <Ccy>
  <xsl:value-of select="BankAccount/BankAccountCurrency/Code" /> 
  </Ccy>
  </DbtrAcct>
  -->
  <DbtrAcct>
 <Id>
 <PrtryAcct>
<Id>
<xsl:choose>
	 <xsl:when test="PayerBankAccount/IBANNumber">
		<xsl:choose>
			<xsl:when test="(PayerBankAccount/IBANNumber='')">
				<xsl:value-of select="BankAccount/BankAccountNumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="PayerBankAccount/IBANNumber"/>
			</xsl:otherwise>
		</xsl:choose> 
	</xsl:when>
	<xsl:otherwise>
		 <xsl:value-of select="BankAccount/BankAccountNumber"/>
    </xsl:otherwise>
</xsl:choose> 
</Id>
</PrtryAcct>
<!--commenting as customization done as per mapping
 <PrtryAcct>
 <Id>
 <xsl:value-of select="BankAccount/BankAccountNumber" />
 </Id>
 </PrtryAcct>
 -->
 </Id>
 </DbtrAcct>
 <!-- Addition End -->
 <DbtrAgt>
 <FinInstnId>
  <!-- Added below for BER HSBC ACH -->
 <CmbndId>
 <!-- Addition End -->
 <xsl:if test="not(BankAccount/SwiftCode='')">
 <BIC>BBDABMHM</BIC>
  </xsl:if>
  <xsl:if test="not(BankAccount/BranchNumber='')">
  <ClrSysMmbId>
    <!-- Commented and added below for BER HSBC ACH
	<MmbId><Othr><Id> NOT PROVIDED</Id></Othr></MmbId>  -->
		<Id>
		<xsl:value-of select="BankAccount/BranchNumber" />
		</Id>
	<!-- Addition End -->
  </ClrSysMmbId>
  </xsl:if> 
  <xsl:if test="not(BankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="BankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
  <!-- Added below for BER HSBC ACH -->
 </CmbndId>
 <!-- Addition End -->
 </FinInstnId>
    <!-- Commented below for BER HSBC ACH
 <xsl:if test="not(BankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if>
  -->
  </DbtrAgt>
   <!-- Commented below for BER HSBC ACH
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByUltimateDebtor='N')  and not(InvoicingLegalEntity/LegalEntityId='') and (InvoicingLegalEntity/LegalEntityId != Payer/LegalEntityInternalID)">
  <UltmtDbtr>
  <Nm>
  <xsl:value-of select="InvoicingLegalEntity/Name" /> 
  </Nm>   
  <PstlAdr>
  <PstCd>
  <xsl:value-of select="InvoicingLegalEntity/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="InvoicingLegalEntity/Address/City" /> 
  </TwnNm>
  <CtrySubDvsn>
  <xsl:value-of select="InvoicingLegalEntity/Address/County" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/State" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/Province" /> 
  </CtrySubDvsn>
  <Ctry>
  <xsl:value-of select="InvoicingLegalEntity/Address/Country" /> 
  </Ctry>
  </PstlAdr>
  <Id>
 <OrgId>
 <Othr>
 <Id>
  <xsl:value-of select="InvoicingLegalEntity/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </OrgId>
  </Id>
 </UltmtDbtr>
  </xsl:if>-->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
     <!-- Commented below for BER HSBC ACH
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  -->
  DEBT
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <xsl:variable name="paymentdetails" select="PaymentDetails" /> 
 <xsl:for-each select="key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)">

<!--Start of credit transaction block-->
 <CdtTrfTxInf>
 <PmtId>
  <!-- Commenting below for BER HSBC ACH -->
 <!--<InstrId>
  <xsl:value-of select="PaymentNumber/PaymentReferenceNumber"/> 
  </InstrId>-->
  <!--Payment Number changed to Check Number for Reconciliation-->
 <EndToEndId>
  <!-- <xsl:value-of select="substring(translate(PaymentNumber/PaymentReferenceNumber,'_',''),1,35)" />  -->
  <xsl:value-of select="substring(translate(PaymentNumber/CheckNumber,'_',''),1,35)" /> 
  </EndToEndId>
  </PmtId>

<!--Start of payment type information block-->
 <!--Commenting for BER HSBC ACH
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') and 
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">

 <PmtTpInf>
    <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
 
  <xsl:if test="((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='N') and 
not(SettlementPriority/Code=''))">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>
 
 </xsl:if>
 -->
   <!--Commenting for BER HSBC ACH
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
 <SvcLvl>
  <Cd><xsl:value-of select="ServiceLevel/Code" /> </Cd> 
  </SvcLvl>
  <LclInstrm>
  <Cd>
  <xsl:value-of select="DeliveryChannel/Code" />
  </Cd>
  </LclInstrm>
  </xsl:if>
  </xsl:if>

  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
 <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>
  </PmtTpInf>  

  </xsl:if>-->
 <Amt>
 <InstdAmt>
 <xsl:attribute name="Ccy">
  <xsl:value-of select="PaymentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(PaymentAmount/Value, '##0.00')" /> 
  </InstdAmt>
  </Amt>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <ChrgBr>DEBT</ChrgBr>  <!-- TBD. Need to check bank wants as DEBT -->
  </xsl:if>
  <!--Commenting ChrgBr from the standard template
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  -->
  </xsl:if>
  </xsl:if>
  </xsl:if>
 <CdtrAgt>
 <FinInstnId>
  <!-- Added below for BER HSBC ACH -->
 <CmbndId>
 <!-- Addition end -->
 <xsl:if test="not(PayeeBankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="PayeeBankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
  <!-- commenting since not required 28Jan,2019
  <xsl:if test="(PayeeBankAccount/SwiftCode='')">-->
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <ClrSysMmbId>
  <Id>
  <xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
  </Id>
  </ClrSysMmbId>
  </xsl:if> 
  <!--commenting since not present in the sample output on 28Jan,2019
   <xsl:if test="not(PayeeBankAccount/BankName='')">
  <Nm>
    <xsl:value-of select="PayeeBankAccount/BankName" />
	<xsl:value-of select="substring(translate(PayeeBankAccount/BankName, translate(translate(PayeeBankAccount/BankName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), ''),1,140)" /> 
  </Nm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="PayeeBankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
   Added below for BER HSBC ACH -->
 </CmbndId>
 <!-- Addition end -->
  </FinInstnId>
  <!-- Commented below for BER HSBC ACH 
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if>
  -->
  </CdtrAgt>
 <Cdtr>
  <Nm>
 <xsl:value-of select="substring(translate(SupplierorParty/Name, translate(translate(SupplierorParty/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
<!--commenting since not required
  <xsl:value-of select="substring(SupplierorParty/Name,1,140)" /> 
 -->
  </Nm>
 <PstlAdr>
 <AdrLine>
  <!--<xsl:value-of select="SupplierorParty/Address/AddressLine1" /> -->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine1, translate(translate(SupplierorParty/Address/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:()., + ',''),$apos,''), ''),1,35)" /> 
  </AdrLine>
 <xsl:if test="not(SupplierorParty/Address/AddressLine2='')"> 
 <AdrLine>
  <!-- <xsl:value-of select="SupplierorParty/Address/AddressLine2" /> -->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine2, translate(translate(SupplierorParty/Address/AddressLine2,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:()., + ',''),$apos,''), ''),1,35)" /> 
  </AdrLine>
  </xsl:if>
   <Ctry>
 <xsl:value-of select="substring(translate(SupplierorParty/Address/Country, translate(translate(SupplierorParty/Address/Country,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  <!--<xsl:value-of select="SupplierorParty/Address/Country" /> -->
    </Ctry>
  </PstlAdr>
  <!-- Commented below for BER HSBC ACH -->
  <!--
  <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='N')">
 <Id>
 <OrgId>
 <xsl:if test="not(SupplierorParty/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/TaxRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/SupplierNumber='')">
 <xsl:if test="not(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/FirstPartyReference" /> 
  </Id>
  </Othr>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
  </Id>
  </xsl:if>-->
  <!--employee payments-->
    <!-- Commented below for BER HSBC ACH 
  <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='Y')">
  <Id> 
  <PrvtId>
  <DtAndPlcOfBirth>
  <BirthDt>
  <xsl:value-of select="substring(Payee/PersonInfo/BirthDate, 0, 11)" /> 
  </BirthDt> 
  <CityOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/TownOfBirth" /> 
  </CityOfBirth> 
  <CtryOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/CountryOfBirth" /> 
  </CtryOfBirth> 
  </DtAndPlcOfBirth>-->
  <!-- <SchemeNm>
  <Cd>NIDL</Cd>
  </SchemeNm>-->
 <!-- Commented below for BER HSBC ACH 
 </PrvtId>
 </Id>
  </xsl:if>
  -->
  </Cdtr>
 <CdtrAcct>
 <Id>
 <!-- Commented and added below for HXC BER HSBC ACH -->
	<!--
  <IBAN>
   <xsl:value-of select="PayeeBankAccount/IBANNumber" /> 
  </IBAN> -->
  <PrtryAcct>
<Id>
<xsl:choose>
	 <xsl:when test="PayeeBankAccount/IBANNumber">
		<xsl:choose>
			<xsl:when test="(PayeeBankAccount/IBANNumber='')">
				<xsl:value-of select="PayeeBankAccount/BankAccountNumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="PayeeBankAccount/IBANNumber"/>
			</xsl:otherwise>
		</xsl:choose> 
	</xsl:when>
	<xsl:otherwise>
		 <xsl:value-of select="PayeeBankAccount/BankAccountNumber"/>
    </xsl:otherwise>
</xsl:choose> 
</Id>
</PrtryAcct>
  <!--Addition end -->
 </Id>
  <!-- Commented below for HXC BER HSBC ACH
  <xsl:if test="not(PayeeBankAccount/BankAccountName='')">
  <Nm>
  <xsl:value-of select="PayeeBankAccount/BankAccountName" /> 
  </Nm>
  </xsl:if>
  -->
  </CdtrAcct>
  <!--commenting as not present in sample output
<xsl:if test="(Payee/PartyInternalID!= SupplierorParty/PartyInternalID)">
 <UltmtCdtr>
 <Nm>
 <xsl:value-of select="substring(translate(Payee/Name, translate(translate(Payee/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
  </Nm>
 <Id>
 <OrgId>
 <xsl:if test="not(Payee/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
 </Id>
 </Othr>
 </xsl:if>
 <xsl:if test="(Payee/TaxRegistrationNumber='')">
 <xsl:if test="not(Payee/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(Payee/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/SupplierNumber='')">
 <xsl:if test="not(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/FirstPartyReference" /> 
  </Id>
  </Othr>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
	
  </Id>
  </UltmtCdtr>
 </xsl:if>
 -->
 <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and (not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='') or not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails=''))">
  <InstrForCdtrAgt>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='')">
  <Cd>
   <xsl:value-of select="translate(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning,translate(translate(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" />
  </Cd>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails='')">
  <InstrInf>
   <xsl:value-of select="translate(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails,translate(translate(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" />
  </InstrInf>
  </xsl:if>
  </InstrForCdtrAgt>
  </xsl:if>
  <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning='')">
  <InstrForDbtrAgt>
    <xsl:value-of select="translate(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning,translate(translate(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" />
  </InstrForDbtrAgt>
 </xsl:if>
  <!-- Commented below for HXC BER HSBC ACH
 <xsl:if test="not(PaymentReason/Code = '')">
 <Purp>
  <Cd>
  <xsl:value-of select="PaymentReason/Code" /> 
  </Cd> 
  </Purp>
  </xsl:if>
  -->
  <!-- Commented below for HXC BER HSBC ACH
  <xsl:if test="not(Payee/TaxRegistrationNumber='') or not(Payer/TaxRegistrationNumber='')">
  <Tax>
  <xsl:if test="not(Payee/TaxRegistrationNumber='')">
  <Cdtr>
  <TaxId>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </TaxId>
  </Cdtr>
  </xsl:if>
  <xsl:if test="not(Payer/TaxRegistrationNumber='')">
  <Dbtr>
  <TaxId>
  <xsl:value-of select="Payer/TaxRegistrationNumber" /> 
  </TaxId>
  </Dbtr>
  </xsl:if>
  </Tax>
  </xsl:if>
  -->
  <!--Commenting since not required on 28Jan,2019
 <RmtInf>
<xsl:for-each select="DocumentPayable">
 <Strd>
  <RfrdDocInf>
  <Tp>
  <CdOrPrtry>
  <Cd>
      <xsl:choose>
      <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">
      <xsl:text>CINV</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='CREDIT')">
      <xsl:text>CREN</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='DEBIT')">
      <xsl:text>DEBN</xsl:text>
      </xsl:when>
      </xsl:choose>
  </Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
    </Issr>
  </Tp>
  <Nb>
 <xsl:value-of select="substring(translate(DocumentNumber/ReferenceNumber, translate(translate(DocumentNumber/ReferenceNumber,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />
  <xsl:value-of select="DocumentNumber/ReferenceNumber" />
  </Nb>
  <RltdDt>
  <xsl:value-of select="DocumentDate" />
  </RltdDt>
  </RfrdDocInf>
  <RfrdDocAmt>
  <xsl:if test="not(TotalDocumentAmount/Value = 0)">
  <DuePyblAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
  </DuePyblAmt>
  </xsl:if>
  <xsl:if test="not(DiscountTaken/Amount/Value = 0)">
  <DscntApldAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="DiscountTaken/Amount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(DiscountTaken/Amount/Value, '##0.00')" />
  </DscntApldAmt>
  </xsl:if>
  <xsl:if test="not(TotalDocumentTaxAmount/Value = 0)">
  <TaxAmt> 
   <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentTaxAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentTaxAmount/Value, '##0.00')" />
  </TaxAmt> 
  </xsl:if>
  </RfrdDocAmt>
  <xsl:if test="not(DocumentNumber/UniqueRemittanceIdentifier/Number='')">  
  <CdtrRefInf>
  <Tp>
  <CdOrPrtry>
  <Cd>SCOR</Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
  </Tp>
    <Ref>
     	  <xsl:value-of select="translate(DocumentNumber/UniqueRemittanceIdentifier/Number, translate(translate(DocumentNumber/UniqueRemittanceIdentifier/Number,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' ')" /> 
    </Ref>
  </CdtrRefInf>
  </xsl:if>
 </Strd>
</xsl:for-each>
 </RmtInf>-->
  </CdtTrfTxInf>
  </xsl:for-each>
  </PmtInf>
  </xsl:for-each>
    <!-- Commented and added below for BER HSBC ACH <CstmrCdtTrfInitn> -->
  </pain.001.001.02>
  <!-- Addition End -->
  </Document>
  </xsl:template>
  </xsl:stylesheet>

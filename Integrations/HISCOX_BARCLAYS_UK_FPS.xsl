<?xml version='1.0' encoding='utf-8'?>
 <!--   $Header: fusionapps/fin/iby/bipub/shared/runFormat/reports/DisbursementPaymentFileFormats/ISO20022CGI.xsl /st_fusionapps_pt-v2mib/3 2017/12/20 18:35:42 jswirsky Exp $   
  --> 
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="no" /> 
  <xsl:output method="xml" /> 
  <xsl:key name="contacts-by-LogicalGroupReference" match="OutboundPayment" use="LogicalGrouping/LogicalGroupReference" /> 
 <xsl:template match="OutboundPaymentInstruction">
 <Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:variable name="instrid" select="PaymentInstructionInfo/InstructionReferenceNumber" /> 
<xsl:variable name="apos">'</xsl:variable>
 <CstmrCdtTrfInitn>
 <GrpHdr>
 <MsgId>
  <xsl:value-of select="substring($instrid,1,35)" /> 
  </MsgId>
  <CreDtTm>
  <xsl:choose>
	<xsl:when test="contains(PaymentInstructionInfo/InstructionCreationDate , '+')">
		<xsl:value-of select="substring(PaymentInstructionInfo/InstructionCreationDate , 1,	string-length(PaymentInstructionInfo/InstructionCreationDate)-6)"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="PaymentInstructionInfo/InstructionCreationDate" />
	</xsl:otherwise>
  </xsl:choose> 
  </CreDtTm>
 <NbOfTxs>
  <xsl:value-of select="substring(InstructionTotals/PaymentCount,1,15)" /> 
  </NbOfTxs>
 <CtrlSum>
    <xsl:value-of select="format-number(sum(OutboundPayment/PaymentAmount/Value), '##0.00')"/>
  </CtrlSum>
 <InitgPty>
 <Nm>
  <xsl:choose>
  <xsl:when test="(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) and (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) != ''">
  <xsl:value-of select="substring(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value,1,140)"/> 
  </xsl:when>
  <xsl:otherwise>
 <!--commenting since not required<xsl:value-of select="substring(InstructionGrouping/Payer/LegalEntityName,1,140)" /> -->
  <xsl:value-of select="substring(translate(InstructionGrouping/Payer/LegalEntityName, translate(translate(InstructionGrouping/Payer/LegalEntityName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" />
   </xsl:otherwise>
  </xsl:choose>
  </Nm>
 <Id>
 <OrgId>
 <xsl:if test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value) or (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value='')">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value !=''">
  <xsl:value-of select="substring(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value,1,35)"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="substring(/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber,1,35)" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
  </Othr>
  </xsl:if>
  </OrgId>
  </Id>
  </InitgPty>
  </GrpHdr>
 <xsl:for-each select="OutboundPayment[count(. | key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)[1]) = 1]">
  <xsl:sort select="LogicalGrouping/LogicalGroupReference" /> 
<!--Start of payment information block-->
 <PmtInf>
 <xsl:if test="not(LogicalGrouping/LogicalGroupReference='')">
 <PmtInfId>
  <xsl:value-of select="substring(translate(LogicalGrouping/LogicalGroupReference,'_',' '),1,35)" /> 
  </PmtInfId>
 <!--Commenting since not required
 <PmtInfId>
  <xsl:value-of select="substring(LogicalGrouping/LogicalGroupReference,1,35)" /> 
  </PmtInfId>-->
  </xsl:if>
  <PmtMtd>TRF</PmtMtd> 
 <!--  <xsl:value-of select="PaymentMethod/PaymentMethodFormatValue" />  -->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='')">
  <BtchBookg>
  <xsl:choose>
  <xsl:when test="(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='N')">
  <xsl:text>false</xsl:text> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:text>true</xsl:text> 
  </xsl:otherwise>
  </xsl:choose>
  </BtchBookg>
 </xsl:if>
<!-- get from new logical table instead -->
 <NbOfTxs>
   <xsl:value-of select="substring(LogicalGrouping/PaymentInformationTotal,1,15)" />
  </NbOfTxs>
 <CtrlSum>
  <xsl:value-of select="format-number(LogicalGrouping/PaymentInformationAmountTotal, '##0.00')" /> 
  </CtrlSum>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') or not
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
<!--Start of payment type information block-->
 <PmtTpInf>
<!--commenting since this is not required-->
<!--   <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='Y')">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if> -->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
<!--   <xsl:if test="not(ServiceLevel/Code='')"> -->
 <SvcLvl>
  <Cd>URNS</Cd>
<!--   <xsl:value-of select="ServiceLevel/Code" />  -->
  </SvcLvl>
<!--   </xsl:if> -->
  <xsl:if test="not(DeliveryChannel/Code='')">
  <LclInstrm>
  <Cd><xsl:value-of select="substring(DeliveryChannel/Code,1,35)" />
  </Cd> 
  </LclInstrm>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
  <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>
  </PmtTpInf>
<!--End of payment type information block-->
  </xsl:if>
 <ReqdExctnDt>
  <xsl:value-of select="PaymentDate" /> 
  </ReqdExctnDt>
 <Dbtr>
 <Nm>
  <xsl:value-of select="substring(translate(Payer/Name, translate(translate(Payer/Name,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
  </Nm>
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='') or not(Payer/Address/AddressLine1='') or not(Payer/Address/PostalCode='')or not(Payer/Address/Country='') or not(Payer/Address/City='')"> 
 <PstlAdr>
 <!--  <xsl:if test="not(Payer/Address/AddressLine1='')">
  <StrtNm>
  <xsl:value-of select="substring(Payer/Address/AddressLine1,1,70)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="substring(Payer/Address/PostalCode,1,16)" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(Payer/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(Payer/Address/City,1,35)" /> 
  </TwnNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payer/Address/County" /> 
  <xsl:value-of select="Payer/Address/State" /> 
  <xsl:value-of select="Payer/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(Payer/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(Payer/Address/Country,1,2)" /> 
  </Ctry>
  </xsl:if> -->
   <xsl:if test="not(Payer/Address/AddressLine1='')">
  <StrtNm>
   <xsl:value-of select="substring(translate(Payer/Address/AddressLine1, translate(translate(Payer/Address/AddressLine1,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,70)" />
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/PostalCode='')">
  <PstCd>
   <xsl:value-of select="substring(translate(Payer/Address/PostalCode, translate(translate(Payer/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" />
  </PstCd>
  </xsl:if>
  <xsl:if test="not(Payer/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(translate(Payer/Address/City, translate(translate(Payer/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />
   </TwnNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(Payer/Address/County, translate(translate(Payer/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payer/Address/State, translate(translate(Payer/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payer/Address/Province, translate(translate(Payer/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(Payer/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(Payer/Address/Country, translate(translate(Payer/Address/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
    </PstlAdr>
  </xsl:if>

 <Id>
 <OrgId>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value) or /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value = ''">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value != ''">
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
  <!--Commented since not required in Barclays FPS-->
<!--  <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value!=''">
   <SchmeNm>
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value"/> 
  </Cd>
  </SchmeNm>
  </xsl:if> -->
  </Othr>
  </xsl:if>
  </OrgId>
  </Id>
 </Dbtr>
 <DbtrAcct>
 <Id>
  <xsl:if test="not(BankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="BankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>
  <!-- if no IBAN, use bank account number, bank account number  - last 8 digits to be picked-->
  <xsl:if test="(BankAccount/IBANNumber='')">
  <Othr>
    <Id>
      <xsl:value-of select="substring(BankAccount/BankAccountNumber,-8)" />
    </Id>
  </Othr>
  </xsl:if>
 </Id>
 <xsl:if test="not(BankAccount/BankAccountType/Code='')">
 <Tp>
   <!-- <Cd>
     <xsl:value-of select="BankAccount/BankAccountType/Code" />
   </Cd> -->
   <Cd>
      <xsl:choose>
      <xsl:when test="(PayeeBankAccount/BankAccountType/Meaning='Savings')">
      <xsl:text>SVGS</xsl:text>
      </xsl:when>
 <xsl:otherwise>CACC</xsl:otherwise> 
      </xsl:choose>
  </Cd> 
 </Tp>
 </xsl:if>
 <Ccy>
   <xsl:value-of select="BankAccount/BankAccountCurrency/Code" /> 
 </Ccy>
 </DbtrAcct>
 <DbtrAgt>
 <FinInstnId>
 <!--SwiftCode replaced with BankNumber to align to Bank Setup Changes ,old logic commented and aligned to Wire logic-->
<!--  <xsl:if test="not(BankAccount/SwiftCode='')"> -->
<!--  <xsl:if test="not(BankAccount/BankNumber='')"> -->
 <!-- <BIC>BARCGB22</BIC> -->
<!--  <BIC> -->
  <!-- <xsl:value-of select="BankAccount/SwiftCode"/>  -->
<!--   <xsl:value-of select="BankAccount/BankNumber"/> 
  </BIC>
  </xsl:if> -->
<!--  <xsl:if test="(BankAccount/SwiftCode='')"> -->
 <!--  <xsl:if test="(BankAccount/BankNumber='')">
  <xsl:if test="not(BankAccount/BranchNumber='')">
  <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="substring(BankAccount/BranchNumber,1,35)" /> 
    </MmbId>
  </ClrSysMmbId>
   </xsl:if> 
  </xsl:if>  -->
   <!--SwiftCode replaced with AlternateBranchNumber in UAT to align to Bank Setup Changes ,old logic commented and aligned to Wire logic-->
  <xsl:choose>
 <xsl:when test="not(BankAccount/AlternateBranchName='') and not(BankAccount/IBANNumber='')">
  <BIC>
  <!-- <xsl:value-of select="BankAccount/BankNumber" />  -->
     <xsl:value-of select="translate(BankAccount/AlternateBranchName, translate(translate(BankAccount/AlternateBranchName,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </BIC>
 </xsl:when>
 <xsl:when test="(BankAccount/IBANNumber='') or (BankAccount/AlternateBranchName='') ">
  <xsl:if test="not(BankAccount/BranchNumber='') and not(BankAccount/BankAccountNumber='') ">
  <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="substring(BankAccount/BranchNumber,1,35)" /> 
    </MmbId>
  </ClrSysMmbId>
   </xsl:if> 
  </xsl:when> 
  <xsl:otherwise>
   <xsl:if test="not(BankAccount/AlternateBranchName='')">
   <BIC>
  <!-- <xsl:value-of select="BankAccount/BankNumber" />  -->
     <xsl:value-of select="translate(BankAccount/AlternateBranchName, translate(translate(BankAccount/AlternateBranchName,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </BIC>
   </xsl:if>
   <xsl:if test="(BankAccount/AlternateBranchName='')">
   <xsl:if test="not(BankAccount/BranchNumber='')">
    <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="substring(BankAccount/BranchNumber,1,35)" /> 
    </MmbId>
  </ClrSysMmbId> 
   </xsl:if> 
   </xsl:if>    
   </xsl:otherwise>
  </xsl:choose>
<!--   <xsl:if test="not(BankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:choose>
      <xsl:when test="(BankAccount/SwiftCode='BARCGB22')">
      <xsl:text>GG</xsl:text>
      </xsl:when>
 <xsl:otherwise>
 <xsl:value-of select="BankAccount/BankAddress/Country"/>
 </xsl:otherwise> 
      </xsl:choose> 
  </Ctry>
  </PstlAdr>
  </xsl:if> -->
  <!--Hardcoding as per SIT4 Comments-->
   <PstlAdr>
  <!--Uncommenting to align to the Bank Setup change GG Hardcoding Removed-->
  <Ctry>
  <xsl:value-of select="BankAccount/BankAddress/Country"/>
  </Ctry>
<!--   <xsl:choose>
   <xsl:when test="not(BankAccount/BankNumber='') and not(BankAccount/IBANNumber='') ">
  <Ctry>GB</Ctry>
  </xsl:when>
  <xsl:otherwise>
   <xsl:if test="not(BankAccount/BranchNumber='') and not(BankAccount/BankAccountNumber='') ">
  <Ctry>GG</Ctry>
  </xsl:if> 
  </xsl:otherwise>
   </xsl:choose> -->
  </PstlAdr>
 </FinInstnId>
<!--  <xsl:if test="not(BankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if> -->
  </DbtrAgt>
  <!-- <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByUltimateDebtor='N')  and not(InvoicingLegalEntity/LegalEntityId='') and (InvoicingLegalEntity/LegalEntityId != Payer/LegalEntityInternalID)">
   <UltmtDbtr>
  <Nm>
  <xsl:value-of select="substring(InvoicingLegalEntity/Name,1,140)" /> 
  </Nm>
  <PstlAdr>
  <xsl:if test="not(InvoicingLegalEntity/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="substring(InvoicingLegalEntity/Address/PostalCode,1,16)" /> 
  </PstCd>
    </xsl:if>
	  <xsl:if test="not(InvoicingLegalEntity/Address/City='')">
  <TwnNm>
  <xsl:value-of select="InvoicingLegalEntity/Address/City" /> 
  </TwnNm>
  </xsl:if>
  <xsl:if test="not(InvoicingLegalEntity/Address/County='') or not(InvoicingLegalEntity/Address/State='') or not(InvoicingLegalEntity/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="InvoicingLegalEntity/Address/County" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/State" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/Province" /> 
  </CtrySubDvsn>
   </xsl:if>
  <Ctry>
  <xsl:value-of select="substring(InvoicingLegalEntity/Address/Country,1,2)" /> 
  </Ctry>
  </PstlAdr>
  <Id>
 <OrgId>
 <Othr>
 <Id>
  <xsl:value-of select="InvoicingLegalEntity/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </OrgId>
  </Id>
 </UltmtDbtr> 
  </xsl:if> -->
  <!--Commenting since this is already present at header level-->
<!--   <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if> -->
  <xsl:variable name="paymentdetails" select="PaymentDetails" /> 
 <xsl:for-each select="key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)">
<!--Start of credit transaction block-->
 <CdtTrfTxInf>
 <PmtId>
 <InstrId>
  <xsl:value-of select="substring(PaymentNumber/PaymentReferenceNumber,1,35)" /> 
  </InstrId>
   <!--Payment Number changed to Check Number for Reconciliation-->
 <EndToEndId>
  <!-- <xsl:value-of select="substring(PaymentNumber/PaymentReferenceNumber,1,35)" />  -->
  <xsl:value-of select="substring(PaymentNumber/CheckNumber,1,35)" /> 
  </EndToEndId>
  </PmtId>
<!--Start of payment type information block-->
<!-- Commenting since this needs to appear only at header level-->
 <!--  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') and 
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
 <PmtTpInf>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='N') and 
not(SettlementPriority/Code=''))">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>
 </xsl:if>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
 <SvcLvl>
  <Cd><xsl:value-of select="ServiceLevel/Code" /> </Cd> 
  </SvcLvl>
  <LclInstrm>
  <Cd>
 <xsl:value-of select="DeliveryChannel/Code" /> 
</Cd>
  </LclInstrm>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
 <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>
  </PmtTpInf>
  </xsl:if> -->
  <!--End of payment type information block-->
 <Amt>
 <InstdAmt>
 <xsl:attribute name="Ccy">
  <xsl:value-of select="PaymentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(PaymentAmount/Value, '##0.00')" /> 
  </InstdAmt>
  </Amt>
<!--   <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if> -->
 <CdtrAgt>
 <FinInstnId>
 <xsl:if test="not(PayeeBankAccount/SwiftCode='')">
 <BIC>
<xsl:value-of select="PayeeBankAccount/SwiftCode" />
  </BIC>
  </xsl:if>
 <xsl:if test="(PayeeBankAccount/SwiftCode='')">
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <ClrSysMmbId>
  <MmbId>
    <xsl:value-of select="substring(PayeeBankAccount/BranchNumber,1,35)" /> 
  </MmbId>
  </ClrSysMmbId>
  </xsl:if>
  </xsl:if>
  <!--commenting as per SIT4 feedback-->
<!--   <xsl:if test="not(PayeeBankAccount/BankName='')">
  <Nm>
    <xsl:value-of select="substring(PayeeBankAccount/BankName,1,140)" />
  </Nm>
  </xsl:if> -->
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="PayeeBankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
  </FinInstnId>
 <!--  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if> -->
  </CdtrAgt>
 <Cdtr>
<Nm>
 <xsl:value-of select="substring(translate(SupplierorParty/Name, translate(translate(SupplierorParty/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
<!--commenting since not required
  <xsl:value-of select="substring(SupplierorParty/Name,1,140)" /> 
 -->
  </Nm>
  <PstlAdr>
    <!-- <xsl:if test="not(SupplierorParty/Address/AddressLine1='')">
  <StrtNm>
  <xsl:value-of select="substring(SupplierorParty/Address/AddressLine1,1,70)" /> 
  </StrtNm>
  </xsl:if>
     <xsl:if test="not(SupplierorParty/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="substring(SupplierorParty/Address/PostalCode,1,16)" /> 
  </PstCd>
    </xsl:if>
     <xsl:if test="not(SupplierorParty/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(SupplierorParty/Address/City,1,35)" /> 
  </TwnNm>
    </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/County='') or not(SupplierorParty/Address/State='') or not(SupplierorParty/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="SupplierorParty/Address/County" /> 
  <xsl:value-of select="SupplierorParty/Address/State" /> 
  <xsl:value-of select="SupplierorParty/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if> -->
    <xsl:if test="not(SupplierorParty/Address/AddressLine1='')">
  <StrtNm>
   <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine1, translate(translate(SupplierorParty/Address/AddressLine1,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,70)" />
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/PostalCode='')">
  <PstCd>
   <xsl:value-of select="substring(translate(SupplierorParty/Address/PostalCode, translate(translate(SupplierorParty/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" />
  </PstCd>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(translate(SupplierorParty/Address/City, translate(translate(SupplierorParty/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />
   </TwnNm>
  </xsl:if>
 <!--  Province commented as per SIT4 comments -->
  <xsl:if test="not(SupplierorParty/Address/County='') or not(SupplierorParty/Address/State='')"> <!-- or not(SupplierorParty/Address/Province='') -->
  <CtrySubDvsn>
  <xsl:value-of select="translate(SupplierorParty/Address/County, translate(translate(SupplierorParty/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), '')" /> 
  <xsl:value-of select="translate(SupplierorParty/Address/State, translate(translate(SupplierorParty/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), '')" /> 
<!--   <xsl:value-of select="translate(SupplierorParty/Address/Province, translate(translate(SupplierorParty/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), '')" />  -->
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="substring(SupplierorParty/Address/Country,1,2)" /> 
  </Ctry>
  </PstlAdr>
  <!-- <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='N')">
 <Id>
 <OrgId>
 <xsl:if test="not(SupplierorParty/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/TaxRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/SupplierNumber='')">
 <xsl:if test="not(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/FirstPartyReference" /> 
  </Id>
  </Othr>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
  </Id>
  </xsl:if> -->
  <!--employee payments-->
<!--   <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='Y')">
  <Id> 
  <PrvtId>
  <DtAndPlcOfBirth>
  <BirthDt>
  <xsl:value-of select="substring(Payee/PersonInfo/BirthDate, 0, 11)" /> 
  </BirthDt> 
  <CityOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/TownOfBirth" /> 
  </CityOfBirth> 
  <CtryOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/CountryOfBirth" /> 
  </CtryOfBirth> 
  </DtAndPlcOfBirth>
 </PrvtId>
 </Id>
  </xsl:if> -->
  </Cdtr>
 <CdtrAcct>
 <Id>
  <xsl:if test="not(PayeeBankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="PayeeBankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>
  <!-- if no IBAN, use bank account number-->
  <xsl:if test="(PayeeBankAccount/IBANNumber='')">
  <Othr>
   <Id>
     <xsl:value-of select="substring(PayeeBankAccount/BankAccountNumber,1,34)" />
   </Id>
  </Othr>
  </xsl:if>
  </Id>
<!--   <xsl:if test="not(PayeeBankAccount/BankAccountName='')">
  <Nm>
  <xsl:value-of select="substring(PayeeBankAccount/BankAccountName,1,70)" /> 
  </Nm>
  </xsl:if> -->
  </CdtrAcct>
 <xsl:if test="(Payee/PartyInternalID!= SupplierorParty/PartyInternalID)">
 <UltmtCdtr>
 <Nm>
 <!--  <xsl:value-of select="substring(Payee/Name,1,140)" />  -->
  <xsl:value-of select="substring(translate(Payee/Name, translate(translate(Payee/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
  </Nm>
  <PstlAdr>
  <!--  <xsl:if test="not(Payee/Address/AddressLine1='')">
  <StrtNm>
  <xsl:value-of select="substring(Payee/Address/AddressLine1,1,70)" /> 
  </StrtNm>
   </xsl:if>
      <xsl:if test="not(Payee/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="substring(Payee/Address/PostalCode,1,16)" /> 
  </PstCd>
     </xsl:if>
	  <xsl:if test="not(Payee/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(Payee/Address/City,1,35)" /> 
  </TwnNm>
     </xsl:if>
  <xsl:if test="not(Payee/Address/County='') or not(Payee/Address/State='') or not(Payee/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payee/Address/County" /> 
  <xsl:value-of select="Payee/Address/State" /> 
  <xsl:value-of select="Payee/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if> -->
   <xsl:if test="not(Payee/Address/AddressLine1='')">
  <StrtNm>
   <xsl:value-of select="substring(translate(Payee/Address/AddressLine1, translate(translate(Payee/Address/AddressLine1,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,70)" />
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(Payee/Address/PostalCode='')">
  <PstCd>
   <xsl:value-of select="substring(translate(Payee/Address/PostalCode, translate(translate(Payee/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" />
  </PstCd>
  </xsl:if>
  <xsl:if test="not(Payee/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(translate(Payee/Address/City, translate(translate(Payee/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />
   </TwnNm>
  </xsl:if>
  <xsl:if test="not(Payee/Address/County='') or not(Payee/Address/State='') or not(Payee/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(Payee/Address/County, translate(translate(Payee/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payee/Address/State, translate(translate(Payee/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payee/Address/Province, translate(translate(Payee/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="substring(Payee/Address/Country,1,2)" /> 
  </Ctry>
  </PstlAdr>
<!--  <Id>
 <OrgId>
 <xsl:if test="not(Payee/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/TaxRegistrationNumber='')">
 <xsl:if test="not(Payee/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(Payee/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/SupplierNumber='')">
 <xsl:if test="not(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/FirstPartyReference" /> 
  </Id>
  </Othr>
 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
	
  </Id> -->
  </UltmtCdtr>
  </xsl:if>
<!-- <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and (not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='') or not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails=''))">
  <InstrForCdtrAgt>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='')">
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning" />
  </Cd>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails='')">
  <InstrInf>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails"/>
  </InstrInf>
  </xsl:if>
  </InstrForCdtrAgt>
  </xsl:if> -->
<!--   <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning='')">
  <InstrForDbtrAgt>
    <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning" />
  </InstrForDbtrAgt>
 </xsl:if> -->
<!--  <xsl:if test="not(PaymentReason/Code = '')">
 <Purp>
  <Cd>
  <xsl:value-of select="PaymentReason/Code" /> 
  </Cd> 
  </Purp>
  </xsl:if> -->
<!--   <xsl:if test="not(Payee/TaxRegistrationNumber='') or not(Payer/TaxRegistrationNumber='')">
  <Tax>
  <xsl:if test="not(Payee/TaxRegistrationNumber='')">
  <Cdtr>
  <TaxId>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </TaxId>
  </Cdtr>
  </xsl:if>
  <xsl:if test="not(Payer/TaxRegistrationNumber='')">
  <Dbtr>
<TaxId>
  <xsl:value-of select="Payer/TaxRegistrationNumber" /> 
  </TaxId>
  </Dbtr>
  </xsl:if>
  </Tax>
  </xsl:if> -->
 <RmtInf>
<xsl:for-each select="DocumentPayable">
 <Strd>
  <RfrdDocInf>
 <!--  <Tp>
  <CdOrPrtry>
  <Cd>
      <xsl:choose>
      <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">
      <xsl:text>CINV</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='CREDIT')">
      <xsl:text>CREN</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='DEBIT')">
      <xsl:text>DEBN</xsl:text>
      </xsl:when>
      </xsl:choose>
  </Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
  </Tp> -->
  <Nb>
  <!-- <xsl:value-of select="substring(DocumentNumber/ReferenceNumber,1,35)" /> -->
  <xsl:value-of select="substring(translate(DocumentNumber/ReferenceNumber, translate(translate(DocumentNumber/ReferenceNumber,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,35)" /> 
  </Nb>
<!--   <RltdDt>
  <xsl:value-of select="DocumentDate" />
  </RltdDt> -->
  </RfrdDocInf>
  <!-- <RfrdDocAmt> -->
  <!-- <xsl:if test="not(TotalDocumentAmount/Value = 0)">
  <DuePyblAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
  </DuePyblAmt>
  </xsl:if> -->
 <!--  <xsl:if test="not(DiscountTaken/Amount/Value = 0)">
  <DscntApldAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="DiscountTaken/Amount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(DiscountTaken/Amount/Value, '##0.00')" />
  </DscntApldAmt>
  </xsl:if> -->
<!--   <xsl:if test="not(TotalDocumentTaxAmount/Value = 0)">
  <TaxAmt> 
   <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentTaxAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentTaxAmount/Value, '##0.00')" />
  </TaxAmt> 
  </xsl:if> -->
<!--   </RfrdDocAmt> -->
  <xsl:if test="not(DocumentNumber/UniqueRemittanceIdentifier/Number='')">  
  <CdtrRefInf>
 <!--  <Tp>
  <CdOrPrtry>
  <Cd>SCOR</Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
  </Tp> -->
    <Ref>
      <xsl:value-of select="translate(DocumentNumber/UniqueRemittanceIdentifier/Number, translate(translate(DocumentNumber/UniqueRemittanceIdentifier/Number,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' ')" /> 
	</Ref>
  </CdtrRefInf>
  </xsl:if>
 </Strd>
</xsl:for-each>
 </RmtInf>
  </CdtTrfTxInf>
  </xsl:for-each>
  </PmtInf>
  </xsl:for-each>
  </CstmrCdtTrfInitn>
  </Document>
  </xsl:template>
  </xsl:stylesheet>

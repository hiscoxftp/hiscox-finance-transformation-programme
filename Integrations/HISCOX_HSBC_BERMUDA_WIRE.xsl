<?xml version='1.0' encoding='utf-8'?>
 <!--   $Header: fusionapps/fin/iby/bipub/shared/runFormat/reports/DisbursementPaymentFileFormats/ISO20022CGI.xsl /st_fusionapps_pt-v2mib/3 2017/12/20 18:35:42 jswirsky Exp $   
  --> 
 <!--Customized for Bermuda HSBC Wire-->
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="no" /> 
  <xsl:output method="xml" /> 
  <xsl:key name="contacts-by-LogicalGroupReference" match="OutboundPayment" use="LogicalGrouping/LogicalGroupReference" /> 
 <xsl:template match="OutboundPaymentInstruction">
 <Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.02" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:variable name="instrid" select="PaymentInstructionInfo/InstructionReferenceNumber" />
  <xsl:variable name="apos">'</xsl:variable>  
 <pain.001.001.02>
 <GrpHdr>
 <MsgId>
  <xsl:value-of select="$instrid" /> 
  </MsgId>
  <CreDtTm>
  <xsl:choose>
	<xsl:when test="contains(PaymentInstructionInfo/InstructionCreationDate , '+')">
		<xsl:value-of select="substring(PaymentInstructionInfo/InstructionCreationDate , 1,	string-length(PaymentInstructionInfo/InstructionCreationDate)-6)"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="PaymentInstructionInfo/InstructionCreationDate" />
	</xsl:otherwise>
  </xsl:choose> 
  </CreDtTm>
 <NbOfTxs>
  <xsl:value-of select="substring(InstructionTotals/PaymentCount,1,15)" /> 
  </NbOfTxs>
 <CtrlSum>
    <xsl:value-of select="format-number(sum(OutboundPayment/PaymentAmount/Value), '##0.00')"/>
  </CtrlSum>
<Grpg>MIXD</Grpg> <!--Added for HSBC Bermuda Wire-->
 <InitgPty>
  <!--commenting since not required<xsl:value-of select="substring(InstructionGrouping/Payer/LegalEntityName,1,140)" /> -->
 <!-- <Nm>
  <xsl:choose>
  <xsl:when test="(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) and (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) != ''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="substring(translate(InstructionGrouping/Payer/LegalEntityName, translate(translate(InstructionGrouping/Payer/LegalEntityName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" />
   </xsl:otherwise>
  </xsl:choose>
  </Nm> -->
 <Id>
 <OrgId>
 <!--Commenting this since BkPtyId is Required by Hiscox Bermuda Wire-->
 <!--<xsl:if test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>-->
 <!--<xsl:if test="not(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value) or (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value='')">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value !=''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
  </Othr>
  </xsl:if>-->
  <!-- Adding New tag similar to Othr-->
  <!--  <xsl:value-of select="substring(/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber,1,35)" />  -->
  <BkPtyId>ABC65688001</BkPtyId>
  </OrgId>
  </Id>
  </InitgPty>
<FwdgAgt>
<FinInstnId>
<PrtryId>
<Id>HSBC CONNECT</Id>
</PrtryId>
</FinInstnId>
</FwdgAgt>
  </GrpHdr>
 <xsl:for-each select="OutboundPayment[count(. | key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)[1]) = 1]">
  <xsl:sort select="LogicalGrouping/LogicalGroupReference" /> 
<!--Start of payment information block-->
 <PmtInf>
 <xsl:if test="not(LogicalGrouping/LogicalGroupReference='')">
 <PmtInfId>
  <xsl:value-of select="substring(translate(LogicalGrouping/LogicalGroupReference,'_',' '),1,35)" /> 
  </PmtInfId>
 <!--Commenting since not required
 <PmtInfId>
  <xsl:value-of select="substring(LogicalGrouping/LogicalGroupReference,1,35)" /> 
  </PmtInfId>-->
  </xsl:if>
  <!--Hardcoding as per FUT Comments-->
    <!-- <xsl:value-of select="substring(PaymentMethod/PaymentMethodFormatValue,1,3)" />  -->
  <PmtMtd>TRF</PmtMtd>
<!--Not Needed for HSBC Bermuda Wire-->  
  <!--<xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='')">
  <BtchBookg>
  <xsl:choose>
  <xsl:when test="(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='N')">
  <xsl:text>false</xsl:text> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:text>true</xsl:text> 
  </xsl:otherwise>
  </xsl:choose>
  </BtchBookg>
 </xsl:if>-->
<!-- get from new logical table instead -->
<!-- Number of Transactions and Control Sum is present above as well, not needed here -->
 <!--<NbOfTxs>
  <xsl:value-of select="LogicalGrouping/PaymentInformationTotal" /> 
  </NbOfTxs>
 <CtrlSum>
  <xsl:value-of select="format-number(LogicalGrouping/PaymentInformationAmountTotal, '##0.00')" /> 
  </CtrlSum>-->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') or not
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
<!--Start of payment type information block-->
 <PmtTpInf>
 <!--Commenting Tag InstrPrty since it is mentioned that this is not required, also commenting Servcechnl since ClrChanl is used-->
  <!--<xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='Y')">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>-->
  <!--<xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="not(ServiceLevel/Code='')">
 <SvcLvl>
  <Cd>
  <xsl:value-of select="ServiceLevel/Code" /> 
  </Cd> 
  </SvcLvl>
  </xsl:if>
  <xsl:if test="not(DeliveryChannel/Code='')">
  <LclInstrm>
  <Cd>
  <xsl:value-of select="DeliveryChannel/Code" /> 
  </Cd> 
  </LclInstrm>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
  <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>-->
  <!--Added for HSBC Bermuda WIRE-->
  <!-- <SvcLv><Cd>SDVA</Cd></SvcLv> -->
  <ClrChanl>RTGS</ClrChanl>
  </PmtTpInf>
<!--End of payment type information block-->
  </xsl:if>
 <ReqdExctnDt>
  <xsl:value-of select="PaymentDate" /> 
  </ReqdExctnDt>
 <Dbtr>
 <Nm>
  <xsl:value-of select="substring(translate(Payer/Name, translate(translate(Payer/Name,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
  </Nm>
  <!--commenting sinnce not required<Nm>
  <xsl:value-of select="substring(Payer/Name,1,140)" /> 
  </Nm>-->
 <PstlAdr>
 <!--Commenting other tags as discussed-->
  <!--<StrtNm>
  <xsl:value-of select="substring(Payer/Address/AddressLine1,1,35)" /> 
  </StrtNm>
  <PstCd>
  <xsl:value-of select="Payer/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="Payer/Address/City" /> 
  </TwnNm>-->
  <!--<xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="substring(Payer/Address/County,1,35)" /> 
  <xsl:value-of select="substring(Payer/Address/State,1,35)" /> 
  <xsl:value-of select="substring(Payer/Address/Province,1,35)" /> 
  </CtrySubDvsn>
  </xsl:if>
 <xsl:if test="not(Payer/Address/Country='')">
 <Ctry>
  <xsl:value-of select="substring(Payer/Address/Country,1,35)" /> 
  </Ctry>
 </xsl:if>-->
 <AdrLine>
  <!--<xsl:value-of select="SupplierorParty/Address/AddressLine1" /> -->
  <xsl:value-of select="substring(translate(Payer/Address/AddressLine1, translate(translate(Payer/Address/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:()., + ',''),$apos,''), ''),1,35)" /> 
  </AdrLine>
 <xsl:if test="not(Payer/Address/AddressLine2='')"> 
 <AdrLine>
  <!-- <xsl:value-of select="SupplierorParty/Address/AddressLine2" /> -->
  <xsl:value-of select="substring(translate(Payer/Address/AddressLine2, translate(translate(Payer/Address/AddressLine2,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:()., + ',''),$apos,''), ''),1,35)" /> 
  </AdrLine>
  </xsl:if>
 <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(Payer/Address/County, translate(translate(Payer/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payer/Address/State, translate(translate(Payer/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payer/Address/Province, translate(translate(Payer/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(Payer/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(Payer/Address/Country, translate(translate(Payer/Address/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
<!--  <Id>
 <OrgId>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value) or /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value = ''">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value != ''">
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value!=''">
  <SchmeNm>
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value"/> 
  </Cd>
  </SchmeNm>
  </xsl:if>
  </Othr>
  </xsl:if>
  </OrgId>
  </Id> -->
<!--    <xsl:if test="not(BankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="BankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(BankAccount/SwiftCode='')">
  <xsl:if test="not(BankAccount/BranchNumber='')">
  <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </MmbId>
  </ClrSysMmbId>
   </xsl:if> 
    </xsl:if> --> 
 </Dbtr>
<!--  <DbtrAcct>
 <Id>
  <xsl:if test="not(BankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="BankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>-->
  <!-- if no IBAN, use bank account number-->
 <!-- <xsl:if test="(BankAccount/IBANNumber='')">
  <Othr>
    <Id>
      <xsl:value-of select="BankAccount/BankAccountNumber" />
    </Id>
  </Othr>
  </xsl:if>
 </Id>
 <xsl:if test="not(BankAccount/BankAccountType/Code='')">
 <Tp>
   <Cd>
     <xsl:value-of select="BankAccount/BankAccountType/Code" />
   </Cd>
 </Tp>
 </xsl:if>
 <Ccy>
   <xsl:value-of select="BankAccount/BankAccountCurrency/Code" /> 
 </Ccy>
 </DbtrAcct> -->
 <!--Rewriting Debtor account with a new Hierarchy for HSBC-->
 <DbtrAcct>
 <Id>
 <PrtryAcct>
<Id>
<xsl:choose>
	 <xsl:when test="PayerBankAccount/IBANNumber">
		<xsl:choose>
			<xsl:when test="(PayerBankAccount/IBANNumber='')">
				<xsl:value-of select="BankAccount/BankAccountNumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="PayerBankAccount/IBANNumber"/>
			</xsl:otherwise>
		</xsl:choose> 
	</xsl:when>
	<xsl:otherwise>
		 <xsl:value-of select="BankAccount/BankAccountNumber"/>
    </xsl:otherwise>
</xsl:choose> 
</Id>
</PrtryAcct>
<!--commenting since customization is as per mapping
 <PrtryAcct>
 <Id>
 <xsl:value-of select="BankAccount/BankAccountNumber" />
 </Id>
 </PrtryAcct>
 -->
 </Id>
 </DbtrAcct>
 <DbtrAgt>
 <FinInstnId>
 <CmbndId>
 <xsl:if test="not(BankAccount/SwiftCode='')">
<!--  <BIC>
  <xsl:value-of select="BankAccount/SwiftCode" /> 
  </BIC> -->
  <BIC>BBDABMHM</BIC>
  </xsl:if>
 <xsl:if test="(BankAccount/SwiftCode='')">
  <xsl:if test="not(BankAccount/BranchNumber='')">
  <ClrSysMmbId>
    <Id>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </Id>
  </ClrSysMmbId>
   </xsl:if> 
  </xsl:if> 
  <xsl:if test="not(BankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="BankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
   </CmbndId>
 </FinInstnId>
<!--  <xsl:if test="not(BankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if> -->
  </DbtrAgt>
  <!--commenting since not required
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByUltimateDebtor='N')  and not(InvoicingLegalEntity/LegalEntityId='') and (InvoicingLegalEntity/LegalEntityId != Payer/LegalEntityInternalID)">
  <UltmtDbtr>
  <Nm>
  <xsl:value-of select="InvoicingLegalEntity/Name" /> 
  </Nm>
  <PstlAdr>
  <PstCd>
  <xsl:value-of select="InvoicingLegalEntity/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="InvoicingLegalEntity/Address/City" /> 
  </TwnNm>
  <CtrySubDvsn>
  <xsl:value-of select="InvoicingLegalEntity/Address/County" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/State" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/Province" /> 
  </CtrySubDvsn>
  <Ctry>
  <xsl:value-of select="InvoicingLegalEntity/Address/Country" /> 
  </Ctry>
  </PstlAdr>
  <Id>
 <OrgId>
 <Othr>
 <Id>
  <xsl:value-of select="InvoicingLegalEntity/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </OrgId>
  </Id>
 </UltmtDbtr>
  </xsl:if>-->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <xsl:variable name="paymentdetails" select="PaymentDetails" /> 
 <xsl:for-each select="key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)">

<!--Start of credit transaction block-->
 <CdtTrfTxInf>
 <PmtId>
 <!--Commenting since not needed for HSBC WIRE BERMUDA-->
 <!--<InstrId>
  <xsl:value-of select="PaymentNumber/PaymentReferenceNumber" /> 
  </InstrId>-->
   <!--Payment Number changed to Check Number for Reconciliation-->
 <EndToEndId>
 <!--  <xsl:value-of select="substring(translate(PaymentNumber/CheckNumber,'_',''),1,35)" />  -->
  <xsl:value-of select="substring(translate(PaymentNumber/CheckNumber,'_',''),1,35)" /> 
  </EndToEndId>
  </PmtId>

<!--Start of payment type information block-->
  <!--<xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') and 
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
 <PmtTpInf>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='N') and 
not(SettlementPriority/Code=''))">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>
 </xsl:if>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
 <SvcLvl>
  <Cd><xsl:value-of select="ServiceLevel/Code" /> </Cd> 
  </SvcLvl>
  <LclInstrm>
  <Cd>
  <xsl:value-of select="DeliveryChannel/Code" />
  </Cd>
  </LclInstrm>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
 <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>
  </PmtTpInf>
  </xsl:if>-->
  <!--End of payment type information block-->
 <Amt>
 <InstdAmt>
 <xsl:attribute name="Ccy">
  <xsl:value-of select="PaymentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(PaymentAmount/Value, '##0.00')" /> 
  </InstdAmt>
  </Amt>
  <!--<xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
   <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr>  
  </xsl:if>
  </xsl:if>
  </xsl:if>-->
    <!--Adding Intermediary Details as per change Request-->
 <xsl:if test="(PayeeBankAccount/IntermediaryBankAccount)">														   
 <xsl:if test="not(PayeeBankAccount/IntermediaryBankAccount/SwiftCode='') or not(PayeeBankAccount/IntermediaryBankAccount/BranchNumber='')">
  <IntrmyAgt1>
  <FinInstnId>
  <CmbndId>
   <xsl:if test="not(PayeeBankAccount/IntermediaryBankAccount/SwiftCode='')">
   <BIC>
  <xsl:value-of select="PayeeBankAccount/IntermediaryBankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/IntermediaryBankAccount/BankName='')">
  <Nm>
    <!--<xsl:value-of select="PayeeBankAccount/BankName" />-->
	<xsl:value-of select="substring(translate(PayeeBankAccount/IntermediaryBankAccount/BankName, translate(translate(PayeeBankAccount/IntermediaryBankAccount/BankName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,140)" /> 
  </Nm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/IntermediaryBankAccount/Country='')"> 
  <PstlAdr>
   <Ctry>
  <xsl:value-of select="PayeeBankAccount/IntermediaryBankAccount/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
  </CmbndId>
  </FinInstnId>
  </IntrmyAgt1>
  </xsl:if>
  </xsl:if>	   
  <!--Creditor , CreditorAgent or CreditorAccount – Any one of this is required.Commenting CdtrAgt Adding back as per FuT comments-->
 <CdtrAgt>
 <!--commenting since not required
 <FinInstnId>
  <CmbndId>
 <xsl:if test="not(PayeeBankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="PayeeBankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(PayeeBankAccount/SwiftCode='')">
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <ClrSysMmbId>
  <Id>
  <xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
  </Id>
  </ClrSysMmbId>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankName='')">
  <Nm>
    <xsl:value-of select="PayeeBankAccount/BankName" />
  </Nm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="PayeeBankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
   </CmbndId>
   </FinInstnId>-->
   <FinInstnId>
   <CmbndId> 
 <xsl:if test="not(PayeeBankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="PayeeBankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(PayeeBankAccount/SwiftCode='')">
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <ClrSysMmbId>
  <Id>
  <xsl:value-of select="concat('XXXXX',PayeeBankAccount/BranchNumber)"/> 
  <!--<xsl:value-of select="PayeeBankAccount/BranchNumber" /> -->
  </Id>
  </ClrSysMmbId>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankName='')">
  <Nm>
    <!--<xsl:value-of select="PayeeBankAccount/BankName" />-->
	<xsl:value-of select="substring(translate(PayeeBankAccount/BankName, translate(translate(PayeeBankAccount/BankName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
  </Nm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')"> 
  <PstlAdr>
  <xsl:if test="not(PayeeBankAccount/BankAddress/AddressLine1='')">
  <StrtNm>
  <!--<xsl:value-of select="substring(PayeeBankAccount/BankAddress/AddressLine1,1,70)" /> -->
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/AddressLine1, translate(translate(PayeeBankAccount/BankAddress/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,70)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/PostalCode='')">
  <PstCd>
  <!--<xsl:value-of select="substring(PayeeBankAccount/BankAddress/PostalCode,1,16)" /> -->
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/PostalCode, translate(translate(PayeeBankAccount/BankAddress/PostalCode,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/City='')">
  <TwnNm>
  <!--<xsl:value-of select="substring(PayeeBankAccount/BankAddress/City,1,35)" />--> 
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/City, translate(translate(PayeeBankAccount/BankAddress/City,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  </TwnNm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/County='') or not(PayeeBankAccount/BankAddress/State='') or not(PayeeBankAccount/BankAddress/Province='')">
  <CtrySubDvsn>
  <!--
  <xsl:value-of select="substring(PayeeBankAccount/BankAddress/County,1,35)" /> 
  <xsl:value-of select="substring(PayeeBankAccount/BankAddress/State,1,35)" /> 
  <xsl:value-of select="substring(PayeeBankAccount/BankAddress/Province,1,35)" /> -->
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/County, translate(translate(PayeeBankAccount/BankAddress/County,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/State, translate(translate(PayeeBankAccount/BankAddress/State,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/Province, translate(translate(PayeeBankAccount/BankAddress/Province,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="PayeeBankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
  </CmbndId>
  </FinInstnId>
<!-- <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if> -->
  </CdtrAgt>
 <Cdtr>
 <Nm>
 <xsl:value-of select="substring(translate(SupplierorParty/Name, translate(translate(SupplierorParty/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
<!--commenting since not required
  <xsl:value-of select="substring(SupplierorParty/Name,1,140)" /> 
 -->
  </Nm>
  <PstlAdr>
  <xsl:if test="not(SupplierorParty/Address/AddressLine1='')">
  <StrtNm>
  <!--<xsl:value-of select="substring(SupplierorParty/Address/AddressLine1,1,70)" /> -->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine1, translate(translate(SupplierorParty/Address/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,170)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/PostalCode='')">
  <PstCd>
  <!--<xsl:value-of select="substring(SupplierorParty/Address/PostalCode,1,16)" />-->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/PostalCode, translate(translate(SupplierorParty/Address/PostalCode,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" /> 
  </PstCd>
  </xsl:if>
 <!-- <xsl:if test="not(SupplierorParty/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(translate(SupplierorParty/Address/City, translate(translate(SupplierorParty/Address/City,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+',''),$apos,''), ''),1,35)" /> 
  </TwnNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/County='') or not(SupplierorParty/Address/State='') or not(SupplierorParty/Address/Province='')">
  <CtrySubDvsn>
 
  <xsl:value-of select="substring(translate(SupplierorParty/Address/County, translate(translate(SupplierorParty/Address/County,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(SupplierorParty/Address/State, translate(translate(SupplierorParty/Address/State,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(SupplierorParty/Address/Province, translate(translate(SupplierorParty/Address/Province,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+',''),$apos,''), ''),1,35)" /> 
  </CtrySubDvsn>
  </xsl:if>-->
  <Ctry>
  <xsl:value-of select="SupplierorParty/Address/Country" /> 
  </Ctry>
  </PstlAdr>
  <!-- <xsl:if test="not(BankAccount/SwiftCode='')">
  <BIC>
  <xsl:value-of select="BankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if> -->
  <!--Added as per FUT COmments-->
 <!-- <xsl:if test="(BankAccount/SwiftCode='')">
  <xsl:if test="not(BankAccount/BranchNumber='')">
  <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </MmbId>
  </ClrSysMmbId>
   </xsl:if> 
   </xsl:if>  -->
  <!-- <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='N')">
 <Id>
 <OrgId>
 <xsl:if test="not(SupplierorParty/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/TaxRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/SupplierNumber='')">
 <xsl:if test="not(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/FirstPartyReference" /> 
  </Id>
  </Othr>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
  </Id>
  </xsl:if> -->
  </Cdtr>
     <!--Commenting since Different Hieerarchy is needed-->
 <!-- <CdtrAcct>
 <Id>
  <xsl:if test="not(PayeeBankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="PayeeBankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>
  <xsl:if test="(PayeeBankAccount/IBANNumber='')">
  <Othr>
   <Id>
     <xsl:value-of select="PayeeBankAccount/BankAccountNumber" />
   </Id>
  </Othr>
  </xsl:if>
  </Id>
  <xsl:if test="not(PayeeBankAccount/BankAccountName='')">
  <Nm>
  <xsl:value-of select="PayeeBankAccount/BankAccountName" /> 
  </Nm>
  </xsl:if>
  </CdtrAcct> -->
  <!-- Commenting as per SIT4 comments on IBAN Requirements-->
<!--   <CdtrAcct>
  <Id>
     <PrtryAcct>
<Id>
<xsl:choose>
	 <xsl:when test="PayeeBankAccount/IBANNumber">
		<xsl:choose>
			<xsl:when test="(PayeeBankAccount/IBANNumber='')">
				<xsl:value-of select="PayeeBankAccount/BankAccountNumber"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="PayeeBankAccount/IBANNumber"/>
			</xsl:otherwise>
		</xsl:choose> 
	</xsl:when>
	<xsl:otherwise>
		 <xsl:value-of select="PayeeBankAccount/BankAccountNumber"/>
    </xsl:otherwise>
</xsl:choose> 
</Id>
</PrtryAcct>
</Id>
  </CdtrAcct> -->
  <CdtrAcct>
  <Id>
  <xsl:if test="not(PayeeBankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="PayeeBankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>
  <xsl:if test="(PayeeBankAccount/IBANNumber='')">
  <xsl:if test="not(PayeeBankAccount/BankAccountNumber='')">  
  <PrtryAcct>
  <Id>
	<xsl:value-of select="PayeeBankAccount/BankAccountNumber"/>
  </Id>
  </PrtryAcct>
  </xsl:if>
  </xsl:if>
  </Id>
  </CdtrAcct>
<!--commenting since customization required
<PrtryAcct>
<Id><xsl:value-of select="PayeeBankAccount/IBANNumber" />
</Id>
</PrtryAcct>
-->
<!--  <xsl:if test="(Payee/PartyInternalID!= SupplierorParty/PartyInternalID)">
 <UltmtCdtr>
 <Nm>
  <xsl:value-of select="Payee/Name" /> 
  </Nm>
  <PstlAdr>
  <StrtNm>
  <xsl:value-of select="Payee/Address/AddressLine1" /> 
  </StrtNm>
  <PstCd>
  <xsl:value-of select="Payee/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="Payee/Address/City" /> 
  </TwnNm>
  <xsl:if test="not(Payee/Address/County='') or not(Payee/Address/State='') or not(Payee/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payee/Address/County" /> 
  <xsl:value-of select="Payee/Address/State" /> 
  <xsl:value-of select="Payee/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="Payee/Address/Country" /> 
  </Ctry>
  </PstlAdr>
 <Id>
 <OrgId>
 <xsl:if test="not(Payee/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/TaxRegistrationNumber='')">
 <xsl:if test="not(Payee/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(Payee/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/SupplierNumber='')">
 <xsl:if test="not(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/FirstPartyReference" /> 
  </Id>
  </Othr>
   </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
	  </Id>
  </UltmtCdtr>
  </xsl:if> -->
 <!--<xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and (not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='') or not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails=''))">
   <InstrForCdtrAgt>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='')">
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning" />
  </Cd>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails='')">
  <InstrInf>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails"/>
  </InstrInf>
  </xsl:if>
  </InstrForCdtrAgt>
  </xsl:if>
  <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning='')">
  <InstrForDbtrAgt>
    <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning" />
  </InstrForDbtrAgt>
 </xsl:if>
 <xsl:if test="not(PaymentReason/Code = '')">
 <Purp>
  <Cd>
  <xsl:value-of select="PaymentReason/Code" /> 
  </Cd> 
  </Purp>
  </xsl:if>
  <xsl:if test="not(Payee/TaxRegistrationNumber='') or not(Payer/TaxRegistrationNumber='')">
  <Tax>
  <xsl:if test="not(Payee/TaxRegistrationNumber='')">
  <Cdtr>
  <TaxId>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </TaxId>
  </Cdtr>
  </xsl:if>
  <xsl:if test="not(Payer/TaxRegistrationNumber='')">
  <Dbtr>
  <TaxId>
  <xsl:value-of select="Payer/TaxRegistrationNumber" /> 
  </TaxId>
  </Dbtr>
  </xsl:if>
  </Tax>
  </xsl:if> -->
 <RmtInf>
<!-- <xsl:for-each select="DocumentPayable"> -->
<!--  <Strd>
  <RfrdDocInf>
  <Tp>
  <CdOrPrtry>
  <Cd>
      <xsl:choose>
      <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">
      <xsl:text>CINV</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='CREDIT')">
      <xsl:text>CREN</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='DEBIT')">
      <xsl:text>DEBN</xsl:text>
      </xsl:when>
      </xsl:choose>
  </Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
  </Tp>
  <Nb>
  <xsl:value-of select="DocumentNumber/ReferenceNumber" />
  </Nb>
  <RltdDt>
  <xsl:value-of select="DocumentDate" />
  </RltdDt>
  </RfrdDocInf>
  <RfrdDocAmt>
  <xsl:if test="not(TotalDocumentAmount/Value = 0)">
  <DuePyblAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
  </DuePyblAmt>
  </xsl:if>
  <xsl:if test="not(DiscountTaken/Amount/Value = 0)">
  <DscntApldAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="DiscountTaken/Amount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(DiscountTaken/Amount/Value, '##0.00')" />
  </DscntApldAmt>
  </xsl:if>
  <xsl:if test="not(TotalDocumentTaxAmount/Value = 0)">
  <TaxAmt> 
   <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentTaxAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentTaxAmount/Value, '##0.00')" />
  </TaxAmt> 
  </xsl:if>
  </RfrdDocAmt>
  <xsl:if test="not(DocumentNumber/UniqueRemittanceIdentifier/Number='')">  
  <CdtrRefInf>
  <Tp>
  <CdOrPrtry>
  <Cd>SCOR</Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
  </Tp>
    <Ref>
      <xsl:value-of select="DocumentNumber/UniqueRemittanceIdentifier/Number" />
    </Ref>
  </CdtrRefInf>
  </xsl:if>
 </Strd> -->
<xsl:variable name="MyConcatReferenceNumberVar">
    <xsl:for-each select="DocumentPayable/DocumentNumber/ReferenceNumber">
         <xsl:value-of select="translate(., translate(translate(.,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" />
         <xsl:text>;</xsl:text>
    </xsl:for-each>
</xsl:variable>
<Ustrd>
    <xsl:value-of select="substring($MyConcatReferenceNumberVar,1,35)" />
<!--<xsl:value-of select="DocumentNumber/ReferenceNumber" />-->
</Ustrd>
<!-- Custom Logic added -->
<xsl:variable name="MyConcatPayTextVar">
    <xsl:for-each select="/OutboundPaymentInstruction/OutboundPayment/PaymentTextMessage">
         <xsl:value-of select="translate(., translate(translate(.,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" />
    <!-- <xsl:text>&#160;</xsl:text> -->
    </xsl:for-each>
</xsl:variable>
<xsl:if test="(string-length($MyConcatPayTextVar) &gt; 0) and (string-length($MyConcatPayTextVar) &lt; 36)">
    <Ustrd>
	      <xsl:value-of select="$MyConcatPayTextVar" />
	</Ustrd>
</xsl:if>
<xsl:if test="string-length($MyConcatPayTextVar) &gt; 35">
    <Ustrd>
	      <xsl:value-of select="substring($MyConcatPayTextVar,1,35)" />
	</Ustrd>
    <Ustrd>
	      <xsl:value-of select="substring($MyConcatPayTextVar,36,35)" />
	</Ustrd>	
</xsl:if>
<!-- End of custom Logic -->
<!-- </xsl:for-each> -->
 </RmtInf>
  </CdtTrfTxInf>
  </xsl:for-each>
  </PmtInf>
  </xsl:for-each>
  </pain.001.001.02>
  </Document>
  </xsl:template>
  </xsl:stylesheet>

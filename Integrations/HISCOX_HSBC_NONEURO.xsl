<?xml version='1.0' encoding='utf-8'?>
 <!--   $Header: fusionapps/fin/iby/bipub/shared/runFormat/reports/DisbursementPaymentFileFormats/ISO20022CGI.xsl /st_fusionapps_pt-v2mib/4 2018/04/04 07:35:17 jswirsky Exp $   
  --> 
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="no" /> 
  <xsl:output method="xml" /> 
  <xsl:key name="contacts-by-LogicalGroupReference" match="OutboundPayment" use="LogicalGrouping/LogicalGroupReference" /> 
 <xsl:template match="OutboundPaymentInstruction">
 <Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:variable name="instrid" select="PaymentInstructionInfo/InstructionReferenceNumber" /> 
  <xsl:variable name="apos">'</xsl:variable>
 <CstmrCdtTrfInitn>
 <GrpHdr>
 <MsgId>
  <xsl:value-of select="$instrid" /> 
  </MsgId>
  <CreDtTm>
  <xsl:choose>
	<xsl:when test="contains(PaymentInstructionInfo/InstructionCreationDate , '+')">
		<xsl:value-of select="substring(PaymentInstructionInfo/InstructionCreationDate , 1,	string-length(PaymentInstructionInfo/InstructionCreationDate)-6)"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="PaymentInstructionInfo/InstructionCreationDate" />
	</xsl:otherwise>
  </xsl:choose> 
  </CreDtTm>
  <Authstn>
  <Cd>FDET</Cd>
  </Authstn>
   <NbOfTxs>
  <xsl:value-of select="InstructionTotals/PaymentCount" /> 
  </NbOfTxs>
 <CtrlSum>
    <xsl:value-of select="format-number(sum(OutboundPayment/PaymentAmount/Value), '##0.00')"/>
  </CtrlSum>
 <InitgPty>
 <Nm>
  <xsl:choose>
  <xsl:when test="(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) and (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) != ''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <!--commenting since not required<xsl:value-of select="substring(InstructionGrouping/Payer/LegalEntityName,1,140)" /> -->
  <xsl:value-of select="substring(translate(InstructionGrouping/Payer/LegalEntityName, translate(translate(InstructionGrouping/Payer/LegalEntityName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,140)" />
   </xsl:otherwise>
  </xsl:choose>
  </Nm>
  <!--uncommenting as per SIT4 comments. Was commented during FUT-->
 <Id>
 <OrgId>
 <!--Commenting since not needed as per mapping sheet-->
<!--  <xsl:if test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if> -->
 <xsl:if test="not(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value) or (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value='')">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value !=''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="substring(/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber,1,35)" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
  </Othr>
  </xsl:if>
  </OrgId>
  </Id>
  </InitgPty>
  </GrpHdr>
 <xsl:for-each select="OutboundPayment[count(. | key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)[1]) = 1]">
  <xsl:sort select="LogicalGrouping/LogicalGroupReference" /> 
<!--Start of payment information block-->
 <PmtInf>
 <xsl:if test="not(LogicalGrouping/LogicalGroupReference='')">
  <PmtInfId>
  <xsl:value-of select="substring(translate(LogicalGrouping/LogicalGroupReference, translate(translate(LogicalGrouping/LogicalGroupReference,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,35)" />
  </PmtInfId>
 <!--Commenting since not required
 <PmtInfId>
  <xsl:value-of select="substring(LogicalGrouping/LogicalGroupReference,1,35)" /> 
  </PmtInfId>-->
  </xsl:if>
  <!-- Refer Below<PmtMtd>
  <xsl:value-of select="PaymentMethod/PaymentMethodFormatValue" /> 
  </PmtMtd> --> 
  <PmtMtd>TRF</PmtMtd>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='')">
  <BtchBookg>
  <xsl:choose>
  <xsl:when test="(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='N')">
  <xsl:text>false</xsl:text> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:text>true</xsl:text> 
  </xsl:otherwise>
  </xsl:choose>
  </BtchBookg>
 </xsl:if>
<!-- get from new logical table instead -->
 <NbOfTxs>
  <xsl:value-of select="LogicalGrouping/PaymentInformationTotal" /> 
  </NbOfTxs>
 <CtrlSum>
  <xsl:value-of select="format-number(LogicalGrouping/PaymentInformationAmountTotal, '##0.00')" /> 
  </CtrlSum>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') or not
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
<!--Start of payment type information block-->
 <PmtTpInf>
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='Y')">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <!--commenting since not required
  <xsl:if test="not(ServiceLevel/Code='')">
 <SvcLvl>
  <Cd>
  <xsl:value-of select="ServiceLevel/Code" /> 
  </Cd> 
  </SvcLvl>
  </xsl:if>
  -->
  <!--Adding as per SIT4 Comments-->
  <SvcLvl>
  <Cd>URGP</Cd>
  </SvcLvl>
  <xsl:if test="not(DeliveryChannel/Code='')">
  <LclInstrm>
  <Cd>
  <xsl:value-of select="substring(DeliveryChannel/Code,1,35)" /> 
  </Cd> 
  </LclInstrm>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <CtgyPurp>
  <Cd>TREA</Cd> 
  </CtgyPurp>
  </PmtTpInf>
  </xsl:if>
<!--End of payment type information block-->
   <ReqdExctnDt>
  <xsl:value-of select="PaymentDate" /> 
  </ReqdExctnDt>
 <Dbtr>
 <Nm>
  <xsl:value-of select="substring(translate(Payer/Name, translate(translate(Payer/Name,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,140)" /> 
  </Nm>
  <!--commenting since not required<Nm>
  <xsl:value-of select="substring(Payer/Name,1,140)" /> 
  </Nm>-->

 <!--Commenting other tags as discussed-->
  <!--<StrtNm>
  <xsl:value-of select="substring(Payer/Address/AddressLine1,1,35)" /> 
  </StrtNm>
  <PstCd>
  <xsl:value-of select="Payer/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="Payer/Address/City" /> 
  </TwnNm>-->
  <!--<xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="substring(Payer/Address/County,1,35)" /> 
  <xsl:value-of select="substring(Payer/Address/State,1,35)" /> 
  <xsl:value-of select="substring(Payer/Address/Province,1,35)" /> 
  </CtrySubDvsn>
  </xsl:if>
 <xsl:if test="not(Payer/Address/Country='')">
 <Ctry>
  <xsl:value-of select="substring(Payer/Address/Country,1,35)" /> 
  </Ctry>
 </xsl:if>-->
 <!--commenting since not required as per FUT comments , uncommenting since required as per SIT4-->
  <PstlAdr>
 <xsl:if test="not(Payer/Address/AddressLine1='')">
 <StrtNm>
  <xsl:value-of select="translate(Payer/Address/AddressLine1, translate(translate(Payer/Address/AddressLine1
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="translate(Payer/Address/PostalCode, translate(translate(Payer/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(Payer/Address/TwnNm='')">
  <TwnNm>
  <xsl:value-of select="translate(Payer/Address/City, translate(translate(Payer/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </TwnNm>
  </xsl:if>
 <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(Payer/Address/County, translate(translate(Payer/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(Payer/Address/State, translate(translate(Payer/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <!-- <xsl:value-of select="translate(Payer/Address/Province, translate(translate(Payer/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> --> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(Payer/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(Payer/Address/Country, translate(translate(Payer/Address/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
 
  <!--commenting since not required as per FUT comments
 <Id>
 <OrgId>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value) or /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value = ''">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value != ''">
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="substring(/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber,1,35)" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
   <CtryOfRes>	
  <xsl:value-of select="substring(translate(Payer/Address/Country, translate(translate(Payer/Address/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </CtryOfRes>	
  <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value!=''">
  <SchmeNm>
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value"/> 
  </Cd>
  </SchmeNm>
  </xsl:if>  
  </Othr>
  </xsl:if>
  </OrgId>
  </Id>-->
 </Dbtr>
 <DbtrAcct>
 <Id>
 <!--Removing commented code for IBAN tag as per SIT comments-->
  <xsl:if test="not(BankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="BankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>

  <!-- if no IBAN, use bank account number-->
  <xsl:if test="(BankAccount/IBANNumber='')">
  <Othr>
    <Id>
      <xsl:value-of select="substring(BankAccount/BankAccountNumber,1,34)" />
    </Id>
  </Othr>
  </xsl:if>
 </Id>
 <!--Commenting since not needed as per UAT comments -->
<!--  <xsl:if test="not(BankAccount/BankAccountType/Code='')">
 <Tp>
   <Cd>
     <xsl:value-of select="BankAccount/BankAccountType/Code" />
   </Cd>
 </Tp>
 </xsl:if> -->
 <!--commenting since not required
 <Ccy>
   <xsl:value-of select="BankAccount/BankAccountCurrency/Code" /> 
 </Ccy>
 -->
 <Nm>
 <xsl:value-of select="Payer/LegalEntityName" />
 </Nm>
 </DbtrAcct>
 <DbtrAgt>
 <FinInstnId>
 <xsl:if test="not(BankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="BankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(BankAccount/SwiftCode='')">
  <xsl:if test="not(BankAccount/BranchNumber='')">
 <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="substring(BankAccount/BranchNumber,1,35)" /> 
    </MmbId>
  </ClrSysMmbId>
   </xsl:if> 
  </xsl:if> 
  <Nm>
  <!--  <xsl:value-of select="BankAccount/BankAccountName"/> -->
    <xsl:value-of select="BankAccount/BankName"/>
  </Nm>
  <xsl:if test="not(BankAccount/BankAddress/Country='')">  
  <!--<PstlAdr>
  <Ctry>
  <xsl:value-of select="BankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>-->
   <PstlAdr>
  <xsl:if test="not(BankAccount/BankAddress/AddressLine1='')">
 <StrtNm>
  <xsl:value-of select="translate(BankAccount/BankAddress/AddressLine1, translate(translate(BankAccount/BankAddress/AddressLine1,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(BankAccount/BankAddress/PostalCode='')">
  <PstCd>
  <xsl:value-of select="translate(BankAccount/BankAddress/PostalCode, translate(translate(BankAccount/BankAddress/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(BankAccount/BankAddress/City='')">
  <TwnNm>
  <xsl:value-of select="translate(BankAccount/BankAddress/City, translate(translate(BankAccount/BankAddress/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </TwnNm>
  </xsl:if>
 <xsl:if test="not(BankAccount/BankAddress/County='') or not(BankAccount/BankAddress/State='') or not(BankAccount/BankAddress/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(BankAccount/BankAddress/County, translate(translate(BankAccount/BankAddress/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(BankAccount/BankAddress/State, translate(translate(BankAccount/BankAddress/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(BankAccount/BankAddress/Province, translate(translate(BankAccount/BankAddress/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(BankAccount/BankAddress/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(BankAccount/BankAddress/Country, translate(translate(BankAccount/BankAddress/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
  </xsl:if>
 </FinInstnId>
 <!--coment
 <xsl:if test="not(BankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if>
  -->
  </DbtrAgt>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByUltimateDebtor='N')  and not(InvoicingLegalEntity/LegalEntityId='') and (InvoicingLegalEntity/LegalEntityId != Payer/LegalEntityInternalID)">
  <UltmtDbtr>
  <Nm>
  <xsl:value-of select="substring(translate(InvoicingLegalEntity/Name, translate(translate(InvoicingLegalEntity/Name,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,140)" /> 
  </Nm>
  <PstlAdr>
  <xsl:if test="not(InvoicingLegalEntity/Address/AddressLine1='')">
 <StrtNm>
  <xsl:value-of select="translate(InvoicingLegalEntity/Address/AddressLine1, translate(translate(InvoicingLegalEntity/Address/AddressLine1,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(InvoicingLegalEntity/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="translate(InvoicingLegalEntity/Address/PostalCode, translate(translate(InvoicingLegalEntity/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(InvoicingLegalEntity/Address/TwnNm='')">
  <TwnNm>
  <xsl:value-of select="translate(InvoicingLegalEntity/Address/City, translate(translate(InvoicingLegalEntity/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </TwnNm>
  </xsl:if>
 <xsl:if test="not(InvoicingLegalEntity/Address/County='') or not(InvoicingLegalEntity/Address/State='') or not(InvoicingLegalEntity/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(InvoicingLegalEntity/Address/County, translate(translate(InvoicingLegalEntity/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(InvoicingLegalEntity/Address/State, translate(translate(InvoicingLegalEntity/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(InvoicingLegalEntity/Address/Province, translate(translate(InvoicingLegalEntity/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(InvoicingLegalEntity/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(InvoicingLegalEntity/Address/Country, translate(translate(InvoicingLegalEntity/Address/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
  <Id>
 <OrgId>
 <Othr>
 <Id>
  <xsl:value-of select="substring(InvoicingLegalEntity/LegalEntityRegistrationNumber,1,35)" /> 
  </Id>
  </Othr>
  </OrgId>
  </Id>

 </UltmtDbtr>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <xsl:variable name="paymentdetails" select="PaymentDetails" /> 
 <xsl:for-each select="key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)">

<!--Start of credit transaction block-->
 <CdtTrfTxInf>
 <PmtId>
 <InstrId>
  <xsl:value-of select="substring(PaymentNumber/PaymentReferenceNumber,1,35)" /> 
  </InstrId>
 <EndToEndId>
 <!--  <xsl:value-of select="substring(PaymentNumber/PaymentReferenceNumber,1,35)" />  -->
   <xsl:value-of select="substring(PaymentNumber/CheckNumber,1,35)" /> 
  </EndToEndId>
  </PmtId>

<!--Start of payment type information block-->
  <xsl:if test="(((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') and 
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N'))
and
( ((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='N') and 
not(SettlementPriority/Code='')) or (/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N') 
or (/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='N')))">
 <PmtTpInf>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='N') and 
not(SettlementPriority/Code=''))">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>
 </xsl:if>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
   
 <SvcLvl>
  <Cd><xsl:value-of select="ServiceLevel/Code" /></Cd> 
  </SvcLvl>
  <LclInstrm>
  <Cd>
  <xsl:value-of select="substring(DeliveryChannel/Code,1,35)" />
  </Cd>
  </LclInstrm>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
 <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if>
  </PmtTpInf>
<!--End of payment type information block-->
  </xsl:if>
 <Amt>
 <InstdAmt>
 <xsl:attribute name="Ccy">
  <xsl:value-of select="PaymentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(PaymentAmount/Value, '##0.00')" /> 
  </InstdAmt>
  </Amt>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if>
 <CdtrAgt>
 <FinInstnId>
 <xsl:if test="not(PayeeBankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="PayeeBankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(PayeeBankAccount/SwiftCode='')">
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <ClrSysMmbId>
  <MmbId>
  <xsl:value-of select="substring(PayeeBankAccount/BranchNumber,1,35)" /> 
  </MmbId>
  </ClrSysMmbId>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankName='')">
  <Nm>
    <xsl:value-of select="substring(PayeeBankAccount/BankName,1,140)" />
  </Nm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')">  
    <PstlAdr>
  <xsl:if test="not(PayeeBankAccount/BankAddress/AddressLine1='')">
 <StrtNm>
  <xsl:value-of select="translate(PayeeBankAccount/BankAddress/AddressLine1, translate(translate(PayeeBankAccount/BankAddress/AddressLine1,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/PostalCode='')">
  <PstCd>
  <xsl:value-of select="translate(PayeeBankAccount/BankAddress/PostalCode, translate(translate(PayeeBankAccount/BankAddress/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/City='')">
  <TwnNm>
  <xsl:value-of select="translate(PayeeBankAccount/BankAddress/City, translate(translate(PayeeBankAccount/BankAddress/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </TwnNm>
  </xsl:if>
 <xsl:if test="not(PayeeBankAccount/BankAddress/County='') or not(PayeeBankAccount/BankAddress/State='') or not(PayeeBankAccount/BankAddress/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(PayeeBankAccount/BankAddress/County, translate(translate(PayeeBankAccount/BankAddress/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(PayeeBankAccount/BankAddress/State, translate(translate(PayeeBankAccount/BankAddress/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  <xsl:value-of select="translate(PayeeBankAccount/BankAddress/Province, translate(translate(PayeeBankAccount/BankAddress/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), '')" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(PayeeBankAccount/BankAddress/Country, translate(translate(PayeeBankAccount/BankAddress/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
  </xsl:if>
  </FinInstnId>
  <!--commenting since not required
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="substring(PayeeBankAccount/BranchNumber,1,35)" /> 
    </Id>
  </BrnchId>
  </xsl:if>
  -->
  </CdtrAgt>
 <Cdtr>
<Nm>
 <xsl:value-of select="substring(translate(SupplierorParty/Name, translate(translate(SupplierorParty/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
<!--commenting since not required
  <xsl:value-of select="substring(SupplierorParty/Name,1,140)" /> 
 -->
  </Nm>
  <!--commenting since not required as per FUT comments
  <PstlAdr>
  <xsl:if test="not(SupplierorParty/Address/AddressLine1='')">
  <StrtNm>
  <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine1, translate(translate(SupplierorParty/Address/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,170)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/PostalCode='')">
  <PstCd>
 
  <xsl:value-of select="substring(translate(SupplierorParty/Address/PostalCode, translate(translate(SupplierorParty/Address/PostalCode,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" /> 
  </PstCd>
  </xsl:if>
   <Ctry>
  <xsl:value-of select="SupplierorParty/Address/Country" /> 
  </Ctry>
  </PstlAdr>-->
  <PstlAdr>
        <xsl:if test="not(SupplierorParty/Address/AddressLine1='')">
  <StrtNm>
   <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine1, translate(translate(SupplierorParty/Address/AddressLine1,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,70)" />
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/PostalCode='')">
  <PstCd>
   <xsl:value-of select="substring(translate(SupplierorParty/Address/PostalCode, translate(translate(SupplierorParty/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" />
  </PstCd>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(translate(SupplierorParty/Address/City, translate(translate(SupplierorParty/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />
   </TwnNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/County='') or not(SupplierorParty/Address/State='')">
  <CtrySubDvsn>
  <xsl:value-of select="translate(SupplierorParty/Address/County, translate(translate(SupplierorParty/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), '')" /> 
  <xsl:value-of select="translate(SupplierorParty/Address/State, translate(translate(SupplierorParty/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), '')" /> 
 <!--  <xsl:value-of select="translate(SupplierorParty/Address/Province, translate(translate(SupplierorParty/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',' '),$apos,''), '')" />  -->
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="substring(SupplierorParty/Address/Country,1,2)" /> 
  </Ctry>
  </PstlAdr>
  <!--commenting since not required as per FUT comments
  <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='N')">
 <Id>
 <OrgId>
 <xsl:if test="not(SupplierorParty/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/TaxRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/SupplierNumber='')">
 <xsl:if test="not(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="SupplierorParty/FirstPartyReference" /> 
  </Id>
  </Othr>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
  </Id>
  </xsl:if>-->
  <!--employee payments, commented since not required
  <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='Y')">
  <Id> 
  <PrvtId>
  <DtAndPlcOfBirth>
  <BirthDt>
  <xsl:value-of select="substring(Payee/PersonInfo/BirthDate, 0, 11)" /> 
  </BirthDt> 
  <CityOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/TownOfBirth" /> 
  </CityOfBirth> 
  <CtryOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/CountryOfBirth" /> 
  </CtryOfBirth> 
  </DtAndPlcOfBirth>
  </PrvtId>
 </Id>
  </xsl:if>-->
  </Cdtr>
 <CdtrAcct>
 <Id>
  <xsl:if test="not(PayeeBankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="PayeeBankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>
  <!-- if no IBAN, use bank account number-->
  <xsl:if test="(PayeeBankAccount/IBANNumber='')">
  <Othr>
   <Id>
     <xsl:value-of select="substring(PayeeBankAccount/BankAccountNumber,1,34)" />
   </Id>
  </Othr>
  </xsl:if>
  </Id>
  <xsl:if test="not(PayeeBankAccount/BankAccountName='')">
  <Nm>
  <xsl:value-of select="PayeeBankAccount/BankAccountName" /> 
  </Nm>
  </xsl:if>
  </CdtrAcct>
  <!--commenting since not required
 <xsl:if test="(Payee/PartyInternalID!= SupplierorParty/PartyInternalID)">
 <UltmtCdtr>
 <Nm>
  <xsl:value-of select="Payee/Name" /> 
  </Nm>
  <PstlAdr>
  <StrtNm>
  <xsl:value-of select="Payee/Address/AddressLine1" /> 
  </StrtNm>
  <PstCd>
  <xsl:value-of select="Payee/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="Payee/Address/City" /> 
  </TwnNm>
  <xsl:if test="not(Payee/Address/County='') or not(Payee/Address/State='') or not(Payee/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payee/Address/County" /> 
  <xsl:value-of select="Payee/Address/State" /> 
  <xsl:value-of select="Payee/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="Payee/Address/Country" /> 
  </Ctry>
  </PstlAdr>
 <Id>
 <OrgId>
 <xsl:if test="not(Payee/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/TaxRegistrationNumber='')">
 <xsl:if test="not(Payee/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/LegalEntityRegistrationNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(Payee/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/SupplierNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/SupplierNumber='')">
 <xsl:if test="not(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/PartyNumber" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="Payee/FirstPartyReference" /> 
  </Id>
  </Othr>
 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
	
  </Id>
  </UltmtCdtr>
  </xsl:if>-->
  <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and (not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='') or not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails=''))">
  <InstrForCdtrAgt>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='')">
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning" />
  </Cd>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails='')">
  <InstrInf>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails"/>
  </InstrInf>
  </xsl:if>
  </InstrForCdtrAgt>
  </xsl:if>
  <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning='')">
  <InstrForDbtrAgt>
    <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning" />
  </InstrForDbtrAgt>
 </xsl:if>
 <!--commenting since not required
 <xsl:if test="not(PaymentReason/Code = '')">
 <Purp>
  <Cd>
  <xsl:value-of select="PaymentReason/Code" /> 
  </Cd> 
  </Purp>
  </xsl:if>
  -->
  <xsl:if test="not(Payee/TaxRegistrationNumber='') or not(Payer/TaxRegistrationNumber='')">
  <Tax>
  <xsl:if test="not(Payee/TaxRegistrationNumber='')">
  <Cdtr>
  <TaxId>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </TaxId>
  </Cdtr>
  </xsl:if>
  <xsl:if test="not(Payer/TaxRegistrationNumber='')">
  <Dbtr>
  <TaxId>
  <xsl:value-of select="Payer/TaxRegistrationNumber" /> 
  </TaxId>
  </Dbtr>
  </xsl:if>
  </Tax>
  </xsl:if>
  <!--Commenting since would be ignored by the Bank as per SIT4 comments-->
 <!-- <RmtInf>
<xsl:for-each select="DocumentPayable">
 <Strd>
  <RfrdDocInf>
  <Tp>
  <CdOrPrtry>
  <Cd>
      <xsl:choose>
      <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">
      <xsl:text>CINV</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='CREDIT')">
      <xsl:text>CREN</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='DEBIT')">
      <xsl:text>DEBN</xsl:text>
      </xsl:when>
      </xsl:choose>
  </Cd>
  </CdOrPrtry>
  commenting since not required
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
 
  </Tp>
  commenting since not required
  <Nb>
  <xsl:value-of select="DocumentNumber/ReferenceNumber" />
  </Nb>
  <RltdDt>
  <xsl:value-of select="DocumentDate" />
  </RltdDt>
 
  </RfrdDocInf>
  <RfrdDocAmt>
  <xsl:if test="not(TotalDocumentAmount/Value = 0)">
  <DuePyblAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
  </DuePyblAmt>
  </xsl:if>
  <xsl:if test="not(DiscountTaken/Amount/Value = 0)">
  <DscntApldAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="DiscountTaken/Amount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(DiscountTaken/Amount/Value, '##0.00')" />
  </DscntApldAmt>
  </xsl:if>
  <xsl:if test="not(TotalDocumentTaxAmount/Value = 0)">
  <TaxAmt> 
   <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentTaxAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentTaxAmount/Value, '##0.00')" />
  </TaxAmt> 
  </xsl:if>
   <xsl:if test="(DocumentType/Code='CREDIT')">
  <CdtNoteAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" />
  </CdtNoteAmt>
  </xsl:if>
  </RfrdDocAmt>
  <xsl:if test="not(DocumentNumber/UniqueRemittanceIdentifier/Number='')">  
  <CdtrRefInf>
  <Tp>
  <CdOrPrtry>
  <Cd>SCOR</Cd>
  </CdOrPrtry>
  commenting since ot required
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
 
  </Tp>
  commenting since not required
    <Ref>
      <xsl:value-of select="DocumentNumber/UniqueRemittanceIdentifier/Number" />
    </Ref>
	
  </CdtrRefInf>
  </xsl:if>
 </Strd>
</xsl:for-each>
 </RmtInf> -->
  </CdtTrfTxInf>
  </xsl:for-each>
  </PmtInf>
  </xsl:for-each>
  </CstmrCdtTrfInitn>
  </Document>
  </xsl:template>
  </xsl:stylesheet>

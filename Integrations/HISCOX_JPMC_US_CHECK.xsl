<?xml version='1.0' encoding='utf-8'?>
 <!--   $Header: fusionapps/fin/iby/bipub/shared/runFormat/reports/DisbursementPaymentFileFormats/ISO20022CGI.xsl /st_fusionapps_pt-v2mib/3 2017/12/20 18:35:42 jswirsky Exp $   
  --> 
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="no" /> 
  <xsl:output method="xml" /> 
  <xsl:key name="contacts-by-LogicalGroupReference" match="OutboundPayment" use="LogicalGrouping/LogicalGroupReference" /> 
 <xsl:template match="OutboundPaymentInstruction">
 <Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:variable name="instrid" select="PaymentInstructionInfo/InstructionReferenceNumber" /> 
  <xsl:variable name="apos">'</xsl:variable>
 <CstmrCdtTrfInitn>
 <GrpHdr>
 <MsgId>
  <xsl:value-of select="$instrid" /> 
  </MsgId>
  <!--<CreDtTm>
  <xsl:choose>
	<xsl:when test="contains(PaymentInstructionInfo/InstructionCreationDate , '+')">
		<xsl:value-of select="substring(PaymentInstructionInfo/InstructionCreationDate , 1,	string-length(PaymentInstructionInfo/InstructionCreationDate)-6)"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="PaymentInstructionInfo/InstructionCreationDate" />
	</xsl:otherwise>
  </xsl:choose> 
  </CreDtTm>-->
   <CreDtTm>
  <xsl:value-of select="substring(PaymentInstructionInfo/InstructionCreationDate,1,19)" /> 
  </CreDtTm>
 <NbOfTxs>
  <xsl:value-of select="InstructionTotals/PaymentCount" /> 
  </NbOfTxs>
 <CtrlSum>
    <xsl:value-of select="format-number(sum(OutboundPayment/PaymentAmount/Value), '##0.00')"/>
  </CtrlSum>
 <InitgPty>
 <Nm>
  <xsl:choose>
  <xsl:when test="(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) and (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value) != ''">
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_NAME')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <!--commenting since not required<xsl:value-of select="substring(InstructionGrouping/Payer/LegalEntityName,1,140)" /> -->
  <xsl:value-of select="substring(translate(InstructionGrouping/Payer/LegalEntityName, translate(translate(InstructionGrouping/Payer/LegalEntityName,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" />
   </xsl:otherwise>
  </xsl:choose>
  </Nm>
  <!--Check if Rest can be commented since not present in Output-->
   <!--Commented as per FUT Comments-->
<!--  <Id>
 <OrgId>
 <xsl:if test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value) or (PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_BICORBEI')]/Value='')">
 <Othr>
  <xsl:choose>
  <xsl:when test="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value and PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value !=''">
   <Id><xsl:value-of select="PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_IP_OTHERID')]/Value"/></Id>
  </xsl:when>
 <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise> 
  </xsl:choose>
  </Othr>
  </xsl:if>
  </OrgId>
  </Id> -->
  </InitgPty>
  </GrpHdr>
 <xsl:for-each select="OutboundPayment[count(. | key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)[1]) = 1]">
  <xsl:sort select="LogicalGrouping/LogicalGroupReference" /> 
<!--Start of payment information block-->
 <PmtInf>
 <xsl:if test="not(LogicalGrouping/LogicalGroupReference='')">
 <PmtInfId>
  <xsl:value-of select="substring(translate(LogicalGrouping/LogicalGroupReference,'_',' '),1,35)" /> 
  </PmtInfId>
 <!--Commenting since not required
 <PmtInfId>
  <xsl:value-of select="substring(LogicalGrouping/LogicalGroupReference,1,35)" /> 
  </PmtInfId>-->
  </xsl:if>
    <!-- Hardcoding value to CHK as per FUT comments-->
<!--   <xsl:value-of select="PaymentMethod/PaymentMethodFormatValue" />  -->
  <PmtMtd>CHK</PmtMtd> 
  <!--<xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='')">
  <BtchBookg>
  <xsl:choose>
  <xsl:when test="(/OutboundPaymentInstruction/PaymentProcessProfile/BatchBookingFlag='N')">
  <xsl:text>false</xsl:text> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:text>true</xsl:text> 
  </xsl:otherwise>
  </xsl:choose>
  </BtchBookg>
 </xsl:if>-->
<!-- get from new logical table instead -->
 <NbOfTxs>
  <xsl:value-of select="LogicalGrouping/PaymentInformationTotal" /> 
  </NbOfTxs>
 <CtrlSum>
  <xsl:value-of select="format-number(LogicalGrouping/PaymentInformationAmountTotal, '##0.00')" /> 
  </CtrlSum>
  <!-- <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') or not
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')"> -->
<!--Start of payment type information block-->
<!--  <PmtTpInf> -->
  <!--<xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='Y')">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>-->
 <!--Check if these are required, not present in Sample Output-->
<!--   <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')"> -->
 <!--  <xsl:if test="not(ServiceLevel/Code='')">
 <SvcLvl>
  <Cd>
  <xsl:value-of select="ServiceLevel/Code" /> 
  </Cd> 
  </SvcLvl>
  </xsl:if> -->
   <!--Modified as Required for JPMC Check-->
 <!--  <xsl:if test="not(DeliveryChannel/Code='')"> -->
  <!--  <Cd>
  <xsl:value-of select="DeliveryChannel/Code" /> 
  </Cd> --> 
<!--     <LclInstrm>
	<Prtry>CII</Prtry>
  </LclInstrm> -->
 <!--  </xsl:if> -->
 <!--  </xsl:if>
  </xsl:if> -->
<!--   <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
  <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if> -->
 <!--  </PmtTpInf> -->
<!--End of payment type information block-->
  <!-- </xsl:if> -->
 <ReqdExctnDt>
  <xsl:value-of select="substring(PaymentDate,1,19)" />  
  </ReqdExctnDt>
 <Dbtr>
  <Nm>
  <xsl:value-of select="substring(translate(Payer/Name, translate(translate(Payer/Name,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
  </Nm>
  <!--commenting sinnce not required<Nm>
  <xsl:value-of select="substring(Payer/Name,1,140)" /> 
  </Nm>-->
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='') or not(Payer/Address/AddressLine1='') or not(Payer/Address/PostalCode='')or not(Payer/Address/Country='') or not(Payer/Address/City='')"> 
 <PstlAdr>
 <!--commenting since not required
  <xsl:if test="not(Payer/Address/AddressLine1='')">
  <StrtNm>
  <xsl:value-of select="substring(Payer/Address/AddressLine1,1,70)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="substring(Payer/Address/PostalCode,1,16)" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(Payer/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(Payer/Address/City,1,35)" /> 
  </TwnNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payer/Address/County" /> 
  <xsl:value-of select="Payer/Address/State" /> 
  <xsl:value-of select="Payer/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(Payer/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(Payer/Address/Country,1,2)" /> 
  </Ctry>
  </xsl:if>
  -->
  <xsl:if test="not(Payer/Address/AddressLine1='')">
  <StrtNm>
   <xsl:value-of select="substring(translate(Payer/Address/AddressLine1, translate(translate(Payer/Address/AddressLine1,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,70)" />
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/PostalCode='')">
  <PstCd>
   <xsl:value-of select="substring(translate(Payer/Address/PostalCode, translate(translate(Payer/Address/PostalCode,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" />
  </PstCd>
  </xsl:if>
  <xsl:if test="not(Payer/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(translate(Payer/Address/City, translate(translate(Payer/Address/City,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />
   </TwnNm>
  </xsl:if>
  <xsl:if test="not(Payer/Address/County='') or not(Payer/Address/State='') or not(Payer/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="substring(translate(Payer/Address/County, translate(translate(Payer/Address/County,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(Payer/Address/State, translate(translate(Payer/Address/State,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(Payer/Address/Province, translate(translate(Payer/Address/Province,
 '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  </CtrySubDvsn>
  </xsl:if>
  <xsl:if test="not(Payer/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(translate(Payer/Address/Country, translate(translate(Payer/Address/Country,
'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
  </xsl:if>
<!--  <Id>
 <OrgId>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value != ''">
 <BICOrBEI>
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value"/> 
 </BICOrBEI>
 </xsl:if>
 <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value) or /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_BICORBEI')]/Value = ''">
 <Othr>
 <Id>
  <xsl:choose>
  <xsl:when test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value != ''">
  <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_OTHERID')]/Value"/> 
  </xsl:when>
  <xsl:otherwise>
  <xsl:value-of select="/OutboundPaymentInstruction/InstructionGrouping/Payer/LegalEntityRegistrationNumber" /> 
  </xsl:otherwise>
  </xsl:choose>
  </Id>
 <xsl:if test="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value and /OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value!=''">
  <SchmeNm>
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/PaymentSystemAccount/AccountSettings[contains(Name,'ISO20022_DR_ID_SCHME_NAME')]/Value"/> 
  </Cd>
  </SchmeNm>
  </xsl:if>
  </Othr>
  </xsl:if>
  </OrgId>
  </Id> -->
 </Dbtr>
 <DbtrAcct>
 <Id>
<!--   <xsl:if test="not(BankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="BankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if> -->
  <!-- if no IBAN, use bank account number removed leading zeros as per JPMC Feedback-->
  <xsl:if test="(BankAccount/IBANNumber='')">
  <Othr>
    <Id>
      <xsl:value-of select="format-number(substring(BankAccount/BankAccountNumber,1,34),'#')" />
    </Id>
  </Othr>
  </xsl:if>
 </Id>
<!--  <xsl:if test="not(BankAccount/BankAccountType/Code='')">
 <Tp>
   <Cd>
     <xsl:value-of select="BankAccount/BankAccountType/Code" />
   </Cd>
 </Tp>
 </xsl:if> -->
 <Ccy>
   <xsl:value-of select="BankAccount/BankAccountCurrency/Code" /> 
 </Ccy>
 </DbtrAcct>
 <DbtrAgt>
 <FinInstnId>
 <xsl:if test="not(BankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="BankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(BankAccount/SwiftCode='')">
  <xsl:if test="not(BankAccount/BranchNumber='')">
  <ClrSysMmbId>
    <MmbId>
	<xsl:value-of select="substring(BankAccount/BranchNumber,1,35)" /> 
    </MmbId>
  </ClrSysMmbId>
   </xsl:if> 
  </xsl:if> 
  <xsl:if test="not(BankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="substring(BankAccount/BankAddress/Country,1,2)" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
 </FinInstnId>
<!--  <xsl:if test="not(BankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="BankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if> -->
  </DbtrAgt>
 <!-- <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByUltimateDebtor='N')  and not(InvoicingLegalEntity/LegalEntityId='') and (InvoicingLegalEntity/LegalEntityId != Payer/LegalEntityInternalID)">
 <UltmtDbtr>
  <Nm>
  <xsl:value-of select="substring(InvoicingLegalEntity/Name,1,140)" /> 
  </Nm>
	  <xsl:if test="not(InvoicingLegalEntity/Address/County='') or not(InvoicingLegalEntity/Address/State='') or not(InvoicingLegalEntity/Address/Province='') or not(InvoicingLegalEntity/Address/PostalCode='') or not(InvoicingLegalEntity/Address/City='') or not(InvoicingLegalEntity/Address/Country='')"> 
  <PstlAdr>
    <xsl:if test="not(InvoicingLegalEntity/Address/AddressLine1='')">
  <StrtNm>
  <xsl:value-of select="substring(InvoicingLegalEntity/Address/AddressLine1,1,70)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(InvoicingLegalEntity/Address/PostalCode='')">
  <PstCd>
  <xsl:value-of select="substring(InvoicingLegalEntity/Address/PostalCode,1,16)" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(InvoicingLegalEntity/Address/City='')">
  <TwnNm>
  <xsl:value-of select="substring(InvoicingLegalEntity/Address/City,1,35)" /> 
  </TwnNm>
  </xsl:if>
  <CtrySubDvsn>
  <xsl:value-of select="InvoicingLegalEntity/Address/County" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/State" /> 
  <xsl:value-of select="InvoicingLegalEntity/Address/Province" /> 
  </CtrySubDvsn>
  <xsl:if test="not(InvoicingLegalEntity/Address/Country='')">
  <Ctry>
  <xsl:value-of select="substring(InvoicingLegalEntity/Address/Country,1,2)" /> 
  </Ctry>
  </xsl:if>
  </PstlAdr>
  </xsl:if>
  <Id>
 <OrgId>
 <Othr>
 <Id>
  <xsl:value-of select="substring(InvoicingLegalEntity/LegalEntityRegistrationNumber,1,35)" /> 
  </Id>
  </Othr>
  </OrgId>
  </Id>
 </UltmtDbtr> 
  </xsl:if>-->
  <!--Charge Bearer Tag is not present in Sample Output. Should this be removed?-->
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  <xsl:variable name="paymentdetails" select="PaymentDetails" /> 
 <xsl:for-each select="key('contacts-by-LogicalGroupReference', LogicalGrouping/LogicalGroupReference)">

<!--Start of credit transaction block-->
 <CdtTrfTxInf>
 <PmtId>
 <InstrId>
  <xsl:value-of select="substring(PaymentNumber/PaymentReferenceNumber,1,35)" /> 
  </InstrId>
   <!--Payment Number changed to Check Number for Reconciliation-->
 <EndToEndId>
  <!-- <xsl:value-of select="substring(PaymentNumber/PaymentReferenceNumber,1,35)" />  -->
  <xsl:value-of select="substring(PaymentNumber/CheckNumber,1,35)" /> 
  </EndToEndId>
  </PmtId>
<!-- Comment Payment type Information Block since not needed as per sample Output-->
<!--Start of payment type information block-->
<!--   <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByPaymentDateLogical='N') and 
(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankAccountLogical='N')">
 <PmtTpInf>
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="((/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupBySettlementPriority='N') and 
not(SettlementPriority/Code=''))">
 <xsl:choose>
  <xsl:when test="(SettlementPriority/Code='NORMAL')">
    <InstrPrty>NORM</InstrPrty>
  </xsl:when>
  <xsl:when test="(SettlementPriority/Code='EXPRESS')">
    <InstrPrty>HIGH</InstrPrty>
  </xsl:when>
 </xsl:choose>
 </xsl:if>
 </xsl:if> 
  <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
  <SvcLvl>
  <Cd><xsl:value-of select="ServiceLevel/Code" /> </Cd> 
  </SvcLvl> 
  <LclInstrm>
  <Cd>
  <xsl:value-of select="DeliveryChannel/Code" />
  </Cd>
  </LclInstrm>
   <Prtry>CII</Prtry>
  </xsl:if>
  </xsl:if>
   <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByCategoryPurpose='Y')">
 <CtgyPurp>
  <Cd>SUPP</Cd> 
  </CtgyPurp>
  </xsl:if> 
  </PmtTpInf>

  </xsl:if> -->
  <!--End of payment type information block-->
 <Amt>
 <InstdAmt>
 <xsl:attribute name="Ccy">
  <xsl:value-of select="PaymentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(PaymentAmount/Value, '##0.00')" /> 
  </InstdAmt>
  </Amt>
  <!--Commented since Sample Output does not have this-->
<!--   <xsl:if test="not(PaymentMethod/PaymentMethodFormatValue='CHK')">
  <xsl:if test="(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByBankChargeBearer='N')">
  <xsl:if test="not(BankCharges/BankChargeBearer/Code='')">
  <ChrgBr>
  <xsl:choose>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='BEN') or (BankCharges/BankChargeBearer/Code='PAYEE_PAYS_EXPRESS')">
   <xsl:text>CRED</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='OUR')">
   <xsl:text>DEBT</xsl:text> 
   </xsl:when>
   <xsl:when test="(BankCharges/BankChargeBearer/Code='SHA')">
   <xsl:text>SHAR</xsl:text> 
   </xsl:when>
   <xsl:otherwise>SLEV</xsl:otherwise> 
  </xsl:choose>
  </ChrgBr> 
  </xsl:if>
  </xsl:if>
  </xsl:if> -->
  <!--Added for JPMC US Check-->
   <ChqInstr>
  <ChqNb>
  <xsl:value-of select="substring(PaymentNumber/CheckNumber,1,35)" />
  </ChqNb>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentProcessProfile/LogicalGrouping/GroupByServiceLevel='N')">
	<!-- <xsl:if test="not(DeliveryChannel/Code='')"> -->
	<xsl:if test="not(SettlementPriority/Code='')">
	  <DlvryMtd>
	<Prtry>
		<!-- xsl:value-of select="substring(DeliveryChannel/Code,1,35)" /> -->
        <xsl:value-of select="substring(SettlementPriority/Code,1,35)" /> 		
	</Prtry>
	  </DlvryMtd>
	</xsl:if>
  </xsl:if>
  <FrmsCd>A1</FrmsCd>
  </ChqInstr>
<!--  <CdtrAgt>
 <FinInstnId>
 <xsl:if test="not(PayeeBankAccount/SwiftCode='')">
 <BIC>
  <xsl:value-of select="PayeeBankAccount/SwiftCode" /> 
  </BIC>
  </xsl:if>
 <xsl:if test="(PayeeBankAccount/SwiftCode='')">
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <ClrSysMmbId>
  <MmbId>
  <xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
  </MmbId>
  </ClrSysMmbId>
  </xsl:if>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankName='')">
  <Nm>
    <xsl:value-of select="PayeeBankAccount/BankName" />
  </Nm>
  </xsl:if>
  <xsl:if test="not(PayeeBankAccount/BankAddress/Country='')">  
  <PstlAdr>
  <Ctry>
  <xsl:value-of select="PayeeBankAccount/BankAddress/Country" /> 
  </Ctry>
  </PstlAdr>
  </xsl:if>
  </FinInstnId>
  <xsl:if test="not(PayeeBankAccount/BranchNumber='')">
  <BrnchId>
    <Id>
	<xsl:value-of select="PayeeBankAccount/BranchNumber" /> 
    </Id>
  </BrnchId>
  </xsl:if>
  </CdtrAgt> -->
 <Cdtr>
 <Nm>
 <xsl:value-of select="substring(translate(SupplierorParty/Name, translate(translate(SupplierorParty/Name,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,140)" /> 
<!--commenting since not required
  <xsl:value-of select="substring(SupplierorParty/Name,1,140)" /> 
 -->
  </Nm>
  <PstlAdr>
  <xsl:if test="not(SupplierorParty/Address/AddressLine1='')">
  <StrtNm>
  <!--<xsl:value-of select="substring(SupplierorParty/Address/AddressLine1,1,70)" /> -->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/AddressLine1, translate(translate(SupplierorParty/Address/AddressLine1,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,170)" /> 
  </StrtNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/PostalCode='')">
  <PstCd>
  <!--<xsl:value-of select="substring(SupplierorParty/Address/PostalCode,1,16)" />-->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/PostalCode, translate(translate(SupplierorParty/Address/PostalCode,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,16)" /> 
  </PstCd>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/City='')">
  <TwnNm>
  <!--<xsl:value-of select="substring(SupplierorParty/Address/City,1,35)" /> -->
  <xsl:value-of select="substring(translate(SupplierorParty/Address/City, translate(translate(SupplierorParty/Address/City,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  </TwnNm>
  </xsl:if>
  <xsl:if test="not(SupplierorParty/Address/County='') or not(SupplierorParty/Address/State='') "> <!---or not(SupplierorParty/Address/Province='') -->
  <CtrySubDvsn>
  <!--
  <xsl:value-of select="substring(SupplierorParty/Address/County,1,35)" /> 
  <xsl:value-of select="substring(SupplierorParty/Address/State,1,35)" /> 
  <xsl:value-of select="substring(SupplierorParty/Address/Province,1,35)" /> -->  
  <xsl:value-of select="substring(translate(SupplierorParty/Address/County, translate(translate(SupplierorParty/Address/County,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
  <xsl:value-of select="substring(translate(SupplierorParty/Address/State, translate(translate(SupplierorParty/Address/State,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" /> 
<!--   <xsl:value-of select="substring(translate(SupplierorParty/Address/Province, translate(translate(SupplierorParty/Address/Province,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ''),1,35)" />  -->
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="SupplierorParty/Address/Country" /> 
  </Ctry>
  </PstlAdr>
  
  <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='N')">
 <Id>
 <OrgId>
 <xsl:if test="not(SupplierorParty/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(SupplierorParty/TaxRegistrationNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/TaxRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(SupplierorParty/LegalEntityRegistrationNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(SupplierorParty/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(SupplierorParty/SupplierNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/SupplierNumber='')">
 <xsl:if test="not(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(SupplierorParty/PartyNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(SupplierorParty/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(SupplierorParty/FirstPartyReference,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
  </Id>
  </xsl:if>
  <!--employee payments-->
<!--   <xsl:if test="(PaymentSourceInfo/EmployeePaymentFlag='Y')">
  <Id> 
  <PrvtId>
  <DtAndPlcOfBirth>
  <BirthDt>
  <xsl:value-of select="substring(Payee/PersonInfo/BirthDate, 0, 11)" /> 
  </BirthDt> 
  <CityOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/TownOfBirth" /> 
  </CityOfBirth> 
  <CtryOfBirth> 
  <xsl:value-of select="Payee/PersonInfo/CountryOfBirth" /> 
  </CtryOfBirth> 
  </DtAndPlcOfBirth> -->
  <!-- <SchemeNm>
  <Cd>NIDL</Cd>
  </SchemeNm>-->
<!--  </PrvtId>
 </Id>
  </xsl:if> -->
  </Cdtr>
 <!-- <CdtrAcct>
 <Id>
  <xsl:if test="not(PayeeBankAccount/IBANNumber='')">
  <IBAN>
  <xsl:value-of select="PayeeBankAccount/IBANNumber" /> 
  </IBAN>
  </xsl:if>-->
  <!-- if no IBAN, use bank account number-->
<!--   <xsl:if test="(PayeeBankAccount/IBANNumber='')">
  <Othr>
   <Id>
     <xsl:value-of select="PayeeBankAccount/BankAccountNumber" />
   </Id>
  </Othr>
  </xsl:if>
  </Id>
  <xsl:if test="not(PayeeBankAccount/BankAccountName='')">
  <Nm>
  <xsl:value-of select="PayeeBankAccount/BankAccountName" /> 
  </Nm>
  </xsl:if>
  </CdtrAcct> --> 
 <!-- <xsl:if test="(Payee/PartyInternalID!= SupplierorParty/PartyInternalID)">
 <UltmtCdtr>
 <Nm>
  <xsl:value-of select="substring(Payee/Name,1,140)" /> 
  </Nm>
 <PstlAdr>
  <StrtNm>
  <xsl:value-of select="Payee/Address/AddressLine1" /> 
  </StrtNm>
  <PstCd>
  <xsl:value-of select="Payee/Address/PostalCode" /> 
  </PstCd>
  <TwnNm>
  <xsl:value-of select="Payee/Address/City" /> 
  </TwnNm>
  <xsl:if test="not(Payee/Address/County='') or not(Payee/Address/State='') or not(Payee/Address/Province='')">
  <CtrySubDvsn>
  <xsl:value-of select="Payee/Address/County" /> 
  <xsl:value-of select="Payee/Address/State" /> 
  <xsl:value-of select="Payee/Address/Province" /> 
  </CtrySubDvsn>
  </xsl:if>
  <Ctry>
  <xsl:value-of select="Payee/Address/Country" /> 
  </Ctry>
  </PstlAdr> 
 <Id>
 <OrgId>
 <xsl:if test="not(Payee/TaxRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(Payee/TaxRegistrationNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/TaxRegistrationNumber='')">
 <xsl:if test="not(Payee/LegalEntityRegistrationNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(Payee/LegalEntityRegistrationNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/LegalEntityRegistrationNumber='')">
 <xsl:if test="not(Payee/SupplierNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(Payee/SupplierNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/SupplierNumber='')">
 <xsl:if test="not(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(Payee/PartyNumber,1,35)" /> 
  </Id>
  </Othr>
  </xsl:if>
 <xsl:if test="(Payee/PartyNumber='')">
 <Othr>
 <Id>
  <xsl:value-of select="substring(Payee/FirstPartyReference,1,35)" /> 
  </Id>
  </Othr>
 
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </xsl:if>
  </OrgId>
	
  </Id>
  </UltmtCdtr>
  </xsl:if> -->
<!--  <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and (not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='') or not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails=''))">
  <InstrForCdtrAgt>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning='')">
  <Cd>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[2]/Meaning" />
  </Cd>
  </xsl:if>
  <xsl:if test="not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails='')">
  <InstrInf>
   <xsl:value-of select="/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstructionDetails"/>
  </InstrInf>
  </xsl:if>
  </InstrForCdtrAgt>
  </xsl:if> -->
  <!--Commenting since this is not in sample Output-->
  <!-- <xsl:if test="(PaymentMethod/PaymentMethodFormatValue='TRF') and not(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning='')">
  <InstrForDbtrAgt>
    <xsl:value-of select="substring(/OutboundPaymentInstruction/PaymentInstructionInfo/BankInstruction[1]/Meaning,1,140)" />
  </InstrForDbtrAgt>
 </xsl:if> -->
 <!-- <xsl:if test="not(PaymentReason/Code = '')">
 <Purp>
  <Cd>
  <xsl:value-of select="PaymentReason/Code" /> 
  </Cd> 
  </Purp>
  </xsl:if>
  <xsl:if test="not(Payee/TaxRegistrationNumber='') or not(Payer/TaxRegistrationNumber='')">
  <Tax>
  <xsl:if test="not(Payee/TaxRegistrationNumber='')">
  <Cdtr>
  <TaxId>
  <xsl:value-of select="Payee/TaxRegistrationNumber" /> 
  </TaxId>
  </Cdtr>
  </xsl:if>
  <xsl:if test="not(Payer/TaxRegistrationNumber='')">
  <Dbtr>
  <TaxId>
  <xsl:value-of select="Payer/TaxRegistrationNumber" /> 
  </TaxId>
  </Dbtr>
  </xsl:if>
  </Tax>
  </xsl:if> -->
<RmtInf>
<xsl:for-each select="DocumentPayable">
 <Strd>
  <RfrdDocInf>
  <Tp>
  <CdOrPrtry>
  <Cd>
      <xsl:choose>
      <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">
      <xsl:text>CINV</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='CREDIT')">
      <xsl:text>CREN</xsl:text>
      </xsl:when>
      <xsl:when test="(DocumentType/Code='DEBIT')">
      <xsl:text>DEBN</xsl:text>
      </xsl:when>
      </xsl:choose>
  </Cd>
  </CdOrPrtry>
  <!-- <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr> -->
  </Tp>
  <Nb>
 <!--  <xsl:value-of select="DocumentNumber/ReferenceNumber" /> -->
  <xsl:value-of select="(translate(DocumentNumber/ReferenceNumber,'_',''))" />
  </Nb>
  <RltdDt>
  <xsl:value-of select="substring(DocumentDate,1,19)" />
  </RltdDt>
  </RfrdDocInf>
  <RfrdDocAmt>
  <xsl:if test="not(TotalDocumentAmount/Value = 0)">
  <xsl:choose>
  <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">  
  <xsl:if test="not(DiscountTaken/Amount/Value = '')"> 
  <DuePyblAmt>
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
  </DuePyblAmt>
  </xsl:if>
  </xsl:when>
  </xsl:choose> 
      <xsl:choose>
          <xsl:when test="//DocumentPayableLine/LineType/Code[text()='AWT']">
            <xsl:for-each select="DocumentPayableLine">
              <xsl:if test="LineType/Code = 'AWT' ">	    
                <DscntApldAmt>
                  <xsl:attribute name="Ccy">
                     <xsl:value-of select="LineGrossAmount/Currency/Code" /> 
                  </xsl:attribute>
                  <xsl:value-of select="format-number( abs(LineGrossAmount/Value), '##0.00')" />
          	    </DscntApldAmt> 
                <RmtdAmt> 
                   <xsl:attribute name="Ccy">
                   <xsl:value-of select="../TotalDocumentAmount/Currency/Code" /> 
                   </xsl:attribute>
                   <xsl:value-of select="format-number((../TotalDocumentAmount/Value) + (LineGrossAmount/Value), '##0.00')" /> 
                </RmtdAmt>				
              </xsl:if>
            </xsl:for-each>		
  		  </xsl:when>
          <xsl:otherwise>
  		    <RmtdAmt> 
               <xsl:attribute name="Ccy">
               <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
               </xsl:attribute>
               <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
            </RmtdAmt>		
  		  </xsl:otherwise>
      </xsl:choose>
  </xsl:if>

  <!-- <xsl:if test="not(DiscountTaken/Amount/Value = 0)">
  <DscntApldAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="DiscountTaken/Amount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(DiscountTaken/Amount/Value, '##0.00')" />
  </DscntApldAmt>
  </xsl:if>
  <xsl:if test="not(TotalDocumentTaxAmount/Value = 0)">
  <TaxAmt> 
   <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentTaxAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentTaxAmount/Value, '##0.00')" />
  </TaxAmt> 
  </xsl:if>
    <xsl:choose>
  <xsl:when test="(DocumentType/Code='CREDIT')"> 
  <xsl:if test="not(TotalDocumentAmount/Value = 0)">
  <CdtNoteAmt> 
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
   <xsl:value-of select="format-number(translate(PaymentAmount/Value,'-',''), '##0.00')" />
  </CdtNoteAmt> 
  </xsl:if>
  </xsl:when>
  </xsl:choose>  -->
 
<!--   <xsl:choose>
  <xsl:when test="(DocumentType/Code='STANDARD') or (DocumentType/Code='INTEREST')">  
  <xsl:if test="not(DiscountTaken/Amount/Value = '')"> 
  <DuePyblAmt>
  <xsl:attribute name="Ccy">
  <xsl:value-of select="TotalDocumentAmount/Currency/Code" /> 
  </xsl:attribute>
  <xsl:value-of select="format-number(TotalDocumentAmount/Value, '##0.00')" /> 
  </DuePyblAmt>
  </xsl:if>
  </xsl:when>
  </xsl:choose>  -->
  </RfrdDocAmt>
  <!--The Contract PO is not in scope for the checks, so the <CdtrRefInf> group should be suppressed. 14-May-19 SIT4 comments-->
<!--  <xsl:if test="not(DocumentNumber/ReferenceNumber='')">  
  <CdtrRefInf>
    <Ref>
      <xsl:value-of select="substring(DocumentNumber/ReferenceNumber,1,35)" />
    </Ref>
  </CdtrRefInf>
  </xsl:if>  -->
  <xsl:if test="not(DocumentDescription='')"> 
  <AddtlRmtInf>
 <!--  <xsl:value-of select="substring(DocumentDescription,1,140)" /> -->
    <xsl:value-of select="substring(translate(DocumentDescription, translate(translate(DocumentDescription,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-?:().,+ ',''),$apos,''), ' '),1,140)" /> 
  </AddtlRmtInf>
  </xsl:if>
 <!--  <xsl:if test="not(DocumentNumber/UniqueRemittanceIdentifier/Number='')">  
  <CdtrRefInf>
  <Tp>
  <CdOrPrtry>
  <Cd>SCOR</Cd>
  </CdOrPrtry>
  <Issr>
  <xsl:value-of select="../Payee/Name" />
  </Issr>
  </Tp>
    <Ref>
      <xsl:value-of select="DocumentNumber/UniqueRemittanceIdentifier/Number" />
    </Ref>
  </CdtrRefInf> 
  </xsl:if>  -->
 </Strd>
</xsl:for-each>
 </RmtInf>
  </CdtTrfTxInf>
  </xsl:for-each>
  </PmtInf>
  </xsl:for-each>
  </CstmrCdtTrfInitn>
  </Document>
  </xsl:template>
  </xsl:stylesheet>

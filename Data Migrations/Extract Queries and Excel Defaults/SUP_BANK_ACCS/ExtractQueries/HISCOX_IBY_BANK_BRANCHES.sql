SELECT 'HISCOX_IBY_BANK_BRANCHES' LT,
bank_name||'-'||bank_home_country||'-'||bank_branch_name LC,
branch_party_number D,
bank_branch_name M,
NULL SD,
NULL ED,
branch_number A1,
bank_home_country A2,
branch_party_id A3,
branch_institution_type A4,
eft_swift_code A5,
NULL A6,
NULL A7,
NULL A8,
NULL A9,
NULL A10
FROM ce_bank_branches_v
WHERE 1=1
AND TRUNC(SYSDATE) BETWEEN TRUNC(START_DATE) AND TRUNC(END_DATE)
SELECT 'HISCOX_IBY_BANKS' LT,
bank_name||'-'||home_country LC,
bank_party_number D,
bank_name M,
NULL SD,
NULL ED,
bank_number A1,
home_country A2,
bank_party_id A3,
bank_institution_type A4,
NULL A5,
NULL A6,
NULL A7,
NULL A8,
NULL A9,
NULL A10
FROM ce_banks_v
WHERE 1=1
AND TRUNC(SYSDATE) BETWEEN TRUNC(START_DATE) AND TRUNC(END_DATE)
SELECT 'HISCOX_POZ_VENDOR_TYPE' LT,
lookup_code LC,
NULL D,
meaning M,
NULL SD,
NULL ED,
NULL A1,
NULL A2,
NULL A3,
NULL A4,
NULL A5,
NULL A6,
NULL A7,
NULL A8,
NULL A9,
NULL A10
FROM fnd_lookup_values
WHERE 1=1
AND lookup_type = 'POZ_VENDOR_TYPE'
AND enabled_flag ='Y'
AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE,SYSDATE)) AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE+1))
SELECT 'HISCOX_POZ_SUPPLIER_ADD' LT,
ps.segment1||'-'||hps.party_site_name LC,
hl.country D,
hps.party_site_number M,
NULL SD,
NULL ED,
NULL A1,
NULL A2,
NULL A3,
NULL A4,
NULL A5,
NULL A6,
NULL A7,
NULL A8,
NULL A9,
NULL A10
FROM poz_suppliers ps,
hz_parties hp,
hz_party_sites hps,
hz_locations hl
WHERE 1=1
AND ps.party_id=hp.party_id
AND hp.party_id=hps.party_id
AND hl.location_id=hps.location_id
AND hps.party_site_name IS NOT NULL
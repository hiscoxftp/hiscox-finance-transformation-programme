SELECT 'HISCOX_POZ_SUPPLIER_SITE_ASS' LT,
pssm.vendor_site_id||'-'||billbu.bu_name LC,
ps.segment1||'-'||hps.party_site_number D,
hp.party_name||'-'||hps.party_site_name M,
NULL SD,
NULL ED,
bu.bu_name A1,
NULL A2,
NULL A3,
NULL A4,
NULL A5,
NULL A6,
NULL A7,
NULL A8,
NULL A9,
NULL A10
FROM poz_site_Assignments_all_m pssam,
poz_supplier_sites_all_m pssm,
poz_suppliers ps,
hz_parties hp,
hz_party_sites hps,
fun_all_business_units_v bu,
--fun_all_business_units_v agntbu,
fun_all_business_units_v billbu
WHERE pssam.bu_id =bu.bu_id
--AND pssam.agent_bu_id =agntbu.bu_id(+)
AND pssam.bill_to_bu_id =billbu.bu_id
AND pssm.vendor_site_id=pssam.vendor_site_id
AND pssm.prc_bu_id=pssam.bu_id
AND hps.party_id=ps.party_id
AND ps.vendor_id=pssm.vendor_id
AND hps.party_site_id=pssm.party_site_id
AND hps.party_id=hp.party_id
AND ps.party_id=hp.party_id
AND TRUNC(SYSDATE) BETWEEN TRUNC(pssam.effective_start_date) AND TRUNC(pssam.effective_end_date)
AND TRUNC(SYSDATE)>TRUNC(NVL(pssam.inactive_date,SYSDATE-1))
AND TRUNC(SYSDATE)>TRUNC(NVL(pssm.inactive_date,SYSDATE-1))
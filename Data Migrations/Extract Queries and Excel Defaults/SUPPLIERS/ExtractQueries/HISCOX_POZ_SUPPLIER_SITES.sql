SELECT 'HISCOX_POZ_SUPPLIER_SITES' LT,
ps.segment1||'-'||pssm.vendor_site_code||'-'||bu_name LC,
hps.party_site_name D,
pssm.vendor_site_id M,
NULL SD,
NULL ED,
hp.party_name A1,
hps.party_site_number A2,
primary_pay_site_flag A3,
pssm.vendor_site_code A4,
eppm.payment_method_code A5,
NULL A6,
NULL A7,
NULL A8,
NULL A9,
NULL A10
FROM poz_supplier_sites_all_m pssm,
hz_party_sites hps,
hz_parties hp,
fun_all_business_units_v fbu,
hz_locations hl,
iby_external_payees_all epa,
iby_ext_party_pmt_mthds eppm,
poz_suppliers ps
WHERE 1=1
AND hp.party_id=hps.party_id
AND hps.party_site_id=pssm.party_site_id
AND fbu.bu_id=pssm.prc_bu_id
AND pssm.vendor_id =ps.vendor_id
AND ps.party_id=hp.party_id
AND hl.location_id=hps.location_id
AND pssm.location_id=hl.location_id
AND epa.supplier_site_id(+)=pssm.vendor_site_id
AND eppm.ext_pmt_party_id(+)=epa.ext_payee_id
AND eppm.primary_flag(+)='Y'
AND TRUNC(SYSDATE)>TRUNC(NVL(pssm.inactive_date,SYSDATE-1))
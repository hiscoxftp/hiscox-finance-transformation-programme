#region Help:  Introduction to the script task
/* The Script Task allows you to perform virtually any operation that can be accomplished in
 * a .Net application within the context of an Integration Services control flow. 
 * 
 * Expand the other regions which have "Help" prefixes for examples of specific ways to use
 * Integration Services features within this script task. */
#endregion


#region Namespaces
using System;
using System.Data;
using Microsoft.SqlServer.Dts.Runtime;
using System.Windows.Forms;
using System.ServiceModel;
using ST_f74b2f65c22a49db9ea1896762ca908a.ServiceReference1;
using System.Threading;
using System.Text;
using System.IO;
#endregion

namespace ST_f74b2f65c22a49db9ea1896762ca908a
{
    /// <summary>
    /// ScriptMain is the entry point class of the script.  Do not change the name, attributes,
    /// or parent of this class.
    /// </summary>
	[Microsoft.SqlServer.Dts.Tasks.ScriptTask.SSISScriptTaskEntryPointAttribute]
	public partial class ScriptMain : Microsoft.SqlServer.Dts.Tasks.ScriptTask.VSTARTScriptObjectModelBase
	{
        #region Help:  Using Integration Services variables and parameters in a script
        /* To use a variable in this script, first ensure that the variable has been added to 
         * either the list contained in the ReadOnlyVariables property or the list contained in 
         * the ReadWriteVariables property of this script task, according to whether or not your
         * code needs to write to the variable.  To add the variable, save this script, close this instance of
         * Visual Studio, and update the ReadOnlyVariables and 
         * ReadWriteVariables properties in the Script Transformation Editor window.
         * To use a parameter in this script, follow the same steps. Parameters are always read-only.
         * 
         * Example of reading from a variable:
         *  DateTime startTime = (DateTime) Dts.Variables["System::StartTime"].Value;
         * 
         * Example of writing to a variable:
         *  Dts.Variables["User::myStringVariable"].Value = "new value";
         * 
         * Example of reading from a package parameter:
         *  int batchId = (int) Dts.Variables["$Package::batchId"].Value;
         *  
         * Example of reading from a project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].Value;
         * 
         * Example of reading from a sensitive project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].GetSensitiveValue();
         * */

        #endregion

        #region Help:  Firing Integration Services events from a script
        /* This script task can fire events for logging purposes.
         * 
         * Example of firing an error event:
         *  Dts.Events.FireError(18, "Process Values", "Bad value", "", 0);
         * 
         * Example of firing an information event:
         *  Dts.Events.FireInformation(3, "Process Values", "Processing has started", "", 0, ref fireAgain)
         * 
         * Example of firing a warning event:
         *  Dts.Events.FireWarning(14, "Process Values", "No values received for input", "", 0);
         * */
        #endregion

        #region Help:  Using Integration Services connection managers in a script
        /* Some types of connection managers can be used in this script task.  See the topic 
         * "Working with Connection Managers Programatically" for details.
         * 
         * Example of using an ADO.Net connection manager:
         *  object rawConnection = Dts.Connections["Sales DB"].AcquireConnection(Dts.Transaction);
         *  SqlConnection myADONETConnection = (SqlConnection)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Sales DB"].ReleaseConnection(rawConnection);
         *
         * Example of using a File connection manager
         *  object rawConnection = Dts.Connections["Prices.zip"].AcquireConnection(Dts.Transaction);
         *  string filePath = (string)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Prices.zip"].ReleaseConnection(rawConnection);
         * */
        #endregion


		/// <summary>
        /// This method is called when this script task executes in the control flow.
        /// Before returning from this method, set the value of Dts.TaskResult to indicate success or failure.
        /// To open Help, press F1.
        /// </summary>
		public void Main()
		{
            try
            {
                Dts.Log("HISCOX: Starting", 0, new byte[0]);
                String userID = (String)Dts.Variables["$Package::UserId"].Value;
                String password = (String)Dts.Variables["$Package::Password"].Value;
                String FilePath = (String)Dts.Variables["$Package::FilePath"].Value;
                String UriWsdl = (String)Dts.Variables["$Package::Uri"].Value;
                String DocNameSeq = (String)Dts.Variables["$Package::DocNameSeq"].Value;

                Dts.Log("userID is: " + userID, 0, new byte[0]);
                Dts.Log("password is: " + password, 0, new byte[0]);
                Dts.Log("FilePath is: " + FilePath, 0, new byte[0]);
                Dts.Log("UriWsdl is: " + UriWsdl, 0, new byte[0]);
                Dts.Log("DocNameSeq is: " + DocNameSeq, 0, new byte[0]);

                string FileName = FilePath.Substring(FilePath.LastIndexOf('/') + 1);

                Dts.Log("FileName is: " + FileName, 0, new byte[0]);
                Dts.Log("HISCOX: Starting 0", 0, new byte[0]);
                //Create Binding
                BasicHttpsBinding binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
                //binding.Security.Mode = SecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                binding.Security.Transport.Realm = "owsb";
                binding.MaxBufferPoolSize = 2147483647;
                binding.MaxReceivedMessageSize = 2147483647;
                binding.TextEncoding = System.Text.Encoding.UTF8;
                binding.MessageEncoding = WSMessageEncoding.Mtom;

                //byte[] content = System.Text.Encoding.UTF8.GetBytes(System.Convert.ToBase64String(System.IO.File.ReadAllBytes(FilePath)));
                //byte[] content = System.Text.Encoding.ASCII.GetBytes(System.Convert.ToByte(System.IO.File.ReadAllBytes(FilePath)));

                //string contentString = System.Text.Encoding.ASCII.GetString(content);
                //string contentString = "UEsDBBQAAgAIACdjZk1YncsnVQAAAAICAAATAAAAUG96U3VwcGxpZXJzSW50LmNzdlNyDnJ1DHFV0lEKDg0IUAgLcQ0OMQPy4MjZPyjAP8gxxNPfD6SotKAgJzO1CCIXHODq5xLvGBri4R/kGeXqAhSiFgr3DHKl3JRRhBUZm5kbQjm8XABQSwECFAAUAAIACAAnY2ZNWJ3LJ1UAAAACAgAAEwAkAAAAAAABACAAAAAAAAAAUG96U3VwcGxpZXJzSW50LmNzdgoAIAAAAAAAAQAYAAABhKqdddQBAAGEqp111AEA4gZecHLUAVBLBQYAAAAAAQABAGUAAACGAAAAAAA=";
                //byte[] content = System.Text.Encoding.UTF8.GetBytes(contentString);

                Byte[] content = File.ReadAllBytes(FilePath);

                Uri epUri = new Uri(UriWsdl);
                EndpointAddress endPoint = new EndpointAddress(epUri);                
                ErpIntegrationServiceClient eis = new ErpIntegrationServiceClient(binding, endPoint);

                eis.ClientCredentials.UserName.UserName = userID;
                eis.ClientCredentials.UserName.Password = password;

                DocumentDetails documentDetails = new DocumentDetails();

                documentDetails.DocumentTitle = FileName;
                documentDetails.DocumentAuthor = userID;
                documentDetails.DocumentSecurityGroup = "FAFusionImportExport";
                documentDetails.DocumentAccount = "prc$/supplier$/import$";
                documentDetails.DocumentName = "SUPP" +DocNameSeq+".zip";
                documentDetails.Content = content;
                documentDetails.FileName = FileName;
                documentDetails.ContentType = "zip";
                //documentDetails.DocumentId = "UCMSUPTEST001";

                Dts.Log("HISCOX: Sending request for document name: " + documentDetails.DocumentName, 0, new byte[0]);

                string ucmUploadResult = eis.uploadFileToUcm(documentDetails);

                Thread.Sleep(10000);
                Dts.Log("HISCOX: Sent request: "+ ucmUploadResult, 0, new byte[0]);

                Dts.Variables["User::UcmDocumentId"].Value = ucmUploadResult;                

                Dts.TaskResult = (int)ScriptResults.Success;
            }
            catch (Exception ex)
            {
                Dts.Log("HISCOX: Exception: " + ex.StackTrace, 0, new byte[0]);
                String errorMessage = ex.Message;
                Dts.Log("HISCOX: Exception2: " + errorMessage, 0, new byte[0]);

                if (errorMessage.Contains("but expected 'text/xml'"))
                {
                    Dts.TaskResult = (int)ScriptResults.Success;
                }
                else

                {
                    Dts.TaskResult = (int)ScriptResults.Failure;
                }

            }
        }

        #region ScriptResults declaration
        /// <summary>
        /// This enum provides a convenient shorthand within the scope of this class for setting the
        /// result of the script.
        /// 
        /// This code was generated automatically.
        /// </summary>
        enum ScriptResults
        {
            Success = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success,
            Failure = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure
        };
        #endregion

	}
}
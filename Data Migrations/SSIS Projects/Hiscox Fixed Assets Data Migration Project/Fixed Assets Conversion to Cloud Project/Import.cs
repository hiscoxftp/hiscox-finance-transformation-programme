#region Help:  Introduction to the script task
/* The Script Task allows you to perform virtually any operation that can be accomplished in
 * a .Net application within the context of an Integration Services control flow. 
 * 
 * Expand the other regions which have "Help" prefixes for examples of specific ways to use
 * Integration Services features within this script task. */
#endregion


#region Namespaces
using System;
using System.Data;
using Microsoft.SqlServer.Dts.Runtime;
using System.Windows.Forms;
using System.ServiceModel;
using ST_b8888d68f7ea4fe7a970ce072c772b6b.LoadInterfaceServiceReference;
using System.Threading;
#endregion

namespace ST_b8888d68f7ea4fe7a970ce072c772b6b
{
    /// <summary>
    /// ScriptMain is the entry point class of the script.  Do not change the name, attributes,
    /// or parent of this class.
    /// </summary>
	[Microsoft.SqlServer.Dts.Tasks.ScriptTask.SSISScriptTaskEntryPointAttribute]
	public partial class ScriptMain : Microsoft.SqlServer.Dts.Tasks.ScriptTask.VSTARTScriptObjectModelBase
	{
        #region Help:  Using Integration Services variables and parameters in a script
        /* To use a variable in this script, first ensure that the variable has been added to 
         * either the list contained in the ReadOnlyVariables property or the list contained in 
         * the ReadWriteVariables property of this script task, according to whether or not your
         * code needs to write to the variable.  To add the variable, save this script, close this instance of
         * Visual Studio, and update the ReadOnlyVariables and 
         * ReadWriteVariables properties in the Script Transformation Editor window.
         * To use a parameter in this script, follow the same steps. Parameters are always read-only.
         * 
         * Example of reading from a variable:
         *  DateTime startTime = (DateTime) Dts.Variables["System::StartTime"].Value;
         * 
         * Example of writing to a variable:
         *  Dts.Variables["User::myStringVariable"].Value = "new value";
         * 
         * Example of reading from a package parameter:
         *  int batchId = (int) Dts.Variables["$Package::batchId"].Value;
         *  
         * Example of reading from a project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].Value;
         * 
         * Example of reading from a sensitive project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].GetSensitiveValue();
         * */

        #endregion

        #region Help:  Firing Integration Services events from a script
        /* This script task can fire events for logging purposes.
         * 
         * Example of firing an error event:
         *  Dts.Events.FireError(18, "Process Values", "Bad value", "", 0);
         * 
         * Example of firing an information event:
         *  Dts.Events.FireInformation(3, "Process Values", "Processing has started", "", 0, ref fireAgain)
         * 
         * Example of firing a warning event:
         *  Dts.Events.FireWarning(14, "Process Values", "No values received for input", "", 0);
         * */
        #endregion

        #region Help:  Using Integration Services connection managers in a script
        /* Some types of connection managers can be used in this script task.  See the topic 
         * "Working with Connection Managers Programatically" for details.
         * 
         * Example of using an ADO.Net connection manager:
         *  object rawConnection = Dts.Connections["Sales DB"].AcquireConnection(Dts.Transaction);
         *  SqlConnection myADONETConnection = (SqlConnection)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Sales DB"].ReleaseConnection(rawConnection);
         *
         * Example of using a File connection manager
         *  object rawConnection = Dts.Connections["Prices.zip"].AcquireConnection(Dts.Transaction);
         *  string filePath = (string)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Prices.zip"].ReleaseConnection(rawConnection);
         * */
        #endregion


		/// <summary>
        /// This method is called when this script task executes in the control flow.
        /// Before returning from this method, set the value of Dts.TaskResult to indicate success or failure.
        /// To open Help, press F1.
        /// </summary>
		public void Main()
		{
            try
            {
                Dts.Log("HISCOX: Starting Interface Load", 0, new byte[0]);
                String intfJobStatus = "";
                String impJobStatus = "";
                String userID = (String)Dts.Variables["$Package::UserId"].Value;
                String password = (String)Dts.Variables["$Package::Password"].Value;
                String FilePath = (String)Dts.Variables["$Package::FilePath"].Value;
                String UriWsdl = (String)Dts.Variables["$Package::Uri"].Value;
                String UcmDocumentId = (String)Dts.Variables["User::UcmDocumentId"].Value;
                String BatchId = (String)Dts.Variables["$Package::BatchId"].Value;

                Dts.Log("userID is: " + userID, 0, new byte[0]);
                Dts.Log("password is: " + password, 0, new byte[0]);
                //Dts.Log("FilePath is: " + FilePath, 0, new byte[0]);
                Dts.Log("UriWsdl is: " + UriWsdl, 0, new byte[0]);
                Dts.Log("UcmDocumentId is: " + UcmDocumentId, 0, new byte[0]);

                string FileName = FilePath.Substring(FilePath.LastIndexOf('/') + 1);

                Dts.Log("FileName is: " + FileName, 0, new byte[0]);

                //Interface Table Load Parameters
                String jobPackageName = "oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader";
                String jobDefinitionName = "InterfaceLoaderController";
                string[] paramList = new string[] { "24", UcmDocumentId, "N", "N" };

                //Create Binding
                BasicHttpsBinding binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
                //binding.Security.Mode = SecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                binding.Security.Transport.Realm = "owsb";
                binding.MaxBufferPoolSize = 2147483647;
                binding.MaxReceivedMessageSize = 2147483647;
                binding.TextEncoding = System.Text.Encoding.UTF8;
                binding.MessageEncoding = WSMessageEncoding.Mtom;

                Uri epUri = new Uri(UriWsdl);
                EndpointAddress endPoint = new EndpointAddress(epUri);
                Dts.Log("HISCOX: Starting 11", 0, new byte[0]);

                ErpIntegrationServiceClient eis = new ErpIntegrationServiceClient(binding, endPoint);

                eis.ClientCredentials.UserName.UserName = userID;
                eis.ClientCredentials.UserName.Password = password;

                Dts.Log("HISCOX: Sending request ", 0, new byte[0]);

                long intfRequestId = eis.submitESSJobRequest(jobPackageName, jobDefinitionName, paramList);
                
                Dts.Log("Interface job submitted with id: "+ intfRequestId.ToString(), 0, new byte[0]);
                //Dts.Log("uploadResult is: " + uploadResult, 0, new byte[0]);

                //Wait for the interface program to complete
                if (intfRequestId != 0)
                {
                    while (intfJobStatus != "SUCCEEDED")
                    {
                        intfJobStatus = eis.getESSJobStatus(intfRequestId);
                        Dts.Log("HISCOX: intfJobStatus: " + intfJobStatus, 0, new byte[0]);
                        Thread.Sleep(5000);
                        if (intfJobStatus == "ERROR")
                            break;
                    }

                    //Submit Import Job
                    jobPackageName = "/oracle/apps/ess/prc/poz/supplierImport/";
                    jobDefinitionName = "ImportSuppliers";
                    string[] paramListImp = new string[] { "NEW", "N", BatchId };
                    Dts.Log("Submitting import job... ", 0, new byte[0]);

                    long impRequestId = eis.submitESSJobRequest(jobPackageName, jobDefinitionName, paramListImp);

                    Dts.Log("Import job submitted with Request id: " + impRequestId.ToString(), 0, new byte[0]);

                    if (impRequestId != 0)
                    {
                        while (impJobStatus != "SUCCEEDED")
                        {
                            impJobStatus = eis.getESSJobStatus(impRequestId);
                            Dts.Log("HISCOX: impJobStatus: " + impJobStatus, 0, new byte[0]);
                            Thread.Sleep(5000);
                            if (impJobStatus == "ERROR")
                                break;
                        }
                    }
                    else
                    {
                        Dts.Log("Could not trigger Import Job", 0, new byte[0]);
                    }
                }
                else
                {
                    Dts.Log("HISCOX: Could not trigger Load Interface File for Import process", 0, new byte[0]);
                }

                Dts.TaskResult = (int)ScriptResults.Success;
            }
            catch (Exception ex)
            {
                Dts.Log("HISCOX: Exception: " + ex.StackTrace, 0, new byte[0]);
                String errorMessage = ex.Message;
                Dts.Log("HISCOX: Exception2: " + errorMessage, 0, new byte[0]);

                if (errorMessage.Contains("but expected 'text/xml'"))
                {
                    Dts.TaskResult = (int)ScriptResults.Success;
                }
                else

                {
                    Dts.TaskResult = (int)ScriptResults.Failure;
                }

            }
        }

        #region ScriptResults declaration
        /// <summary>
        /// This enum provides a convenient shorthand within the scope of this class for setting the
        /// result of the script.
        /// 
        /// This code was generated automatically.
        /// </summary>
        enum ScriptResults
        {
            Success = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success,
            Failure = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure
        };
        #endregion

	}
}
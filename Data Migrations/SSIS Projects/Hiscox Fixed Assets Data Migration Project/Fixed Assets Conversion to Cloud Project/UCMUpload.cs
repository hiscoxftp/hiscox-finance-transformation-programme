#region Help:  Introduction to the script task
/* The Script Task allows you to perform virtually any operation that can be accomplished in
 * a .Net application within the context of an Integration Services control flow. 
 * 
 * Expand the other regions which have "Help" prefixes for examples of specific ways to use
 * Integration Services features within this script task. */
#endregion


#region Namespaces
using System;
using System.Data;
using Microsoft.SqlServer.Dts.Runtime;
using System.Windows.Forms;
using System.Xml;
using ST_53b4020838204e32b79f5e54bea263f7.UCMServiceReference;
using System.ServiceModel;
using System.Text;
#endregion

namespace ST_53b4020838204e32b79f5e54bea263f7
{
    /// <summary>
    /// ScriptMain is the entry point class of the script.  Do not change the name, attributes,
    /// or parent of this class.
    /// </summary>
	[Microsoft.SqlServer.Dts.Tasks.ScriptTask.SSISScriptTaskEntryPointAttribute]
	public partial class ScriptMain : Microsoft.SqlServer.Dts.Tasks.ScriptTask.VSTARTScriptObjectModelBase
	{
        #region Help:  Using Integration Services variables and parameters in a script
        /* To use a variable in this script, first ensure that the variable has been added to 
         * either the list contained in the ReadOnlyVariables property or the list contained in 
         * the ReadWriteVariables property of this script task, according to whether or not your
         * code needs to write to the variable.  To add the variable, save this script, close this instance of
         * Visual Studio, and update the ReadOnlyVariables and 
         * ReadWriteVariables properties in the Script Transformation Editor window.
         * To use a parameter in this script, follow the same steps. Parameters are always read-only.
         * 
         * Example of reading from a variable:
         *  DateTime startTime = (DateTime) Dts.Variables["System::StartTime"].Value;
         * 
         * Example of writing to a variable:
         *  Dts.Variables["User::myStringVariable"].Value = "new value";
         * 
         * Example of reading from a package parameter:
         *  int batchId = (int) Dts.Variables["$Package::batchId"].Value;
         *  
         * Example of reading from a project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].Value;
         * 
         * Example of reading from a sensitive project parameter:
         *  int batchId = (int) Dts.Variables["$Project::batchId"].GetSensitiveValue();
         * */

        #endregion

        #region Help:  Firing Integration Services events from a script
        /* This script task can fire events for logging purposes.
         * 
         * Example of firing an error event:
         *  Dts.Events.FireError(18, "Process Values", "Bad value", "", 0);
         * 
         * Example of firing an information event:
         *  Dts.Events.FireInformation(3, "Process Values", "Processing has started", "", 0, ref fireAgain)
         * 
         * Example of firing a warning event:
         *  Dts.Events.FireWarning(14, "Process Values", "No values received for input", "", 0);
         * */
        #endregion

        #region Help:  Using Integration Services connection managers in a script
        /* Some types of connection managers can be used in this script task.  See the topic 
         * "Working with Connection Managers Programatically" for details.
         * 
         * Example of using an ADO.Net connection manager:
         *  object rawConnection = Dts.Connections["Sales DB"].AcquireConnection(Dts.Transaction);
         *  SqlConnection myADONETConnection = (SqlConnection)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Sales DB"].ReleaseConnection(rawConnection);
         *
         * Example of using a File connection manager
         *  object rawConnection = Dts.Connections["Prices.zip"].AcquireConnection(Dts.Transaction);
         *  string filePath = (string)rawConnection;
         *  //Use the connection in some code here, then release the connection
         *  Dts.Connections["Prices.zip"].ReleaseConnection(rawConnection);
         * */
        #endregion


		/// <summary>
        /// This method is called when this script task executes in the control flow.
        /// Before returning from this method, set the value of Dts.TaskResult to indicate success or failure.
        /// To open Help, press F1.
        /// </summary>
		public void Main()
		{
            try
            {
                // TODO: Add your code here
                Dts.Log("HISCOX: Starting", 0, new byte[0]);
                String userID = "Vasanthagokul.Chinnaswamy@HISCOX.com";
                String password = "HDeloitte01";
                Dts.Log("HISCOX: Starting 0", 0, new byte[0]);
                //Create Binding
                BasicHttpsBinding binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
                //binding.Security.Mode = SecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                binding.Security.Transport.Realm = "owsb";
                binding.MaxBufferPoolSize = 2147483647;
                binding.MaxReceivedMessageSize = 2147483647;
                binding.TextEncoding = System.Text.Encoding.UTF8;
                binding.MessageEncoding = WSMessageEncoding.Mtom;

                Dts.Log("HISCOX: Starting 1", 0, new byte[0]);
                Uri epUri = new Uri("https://ejen-dev1.fa.em2.oraclecloud.com/idcws/GenericSoapPort");
                EndpointAddress endPoint = new EndpointAddress(epUri);
                Dts.Log("HISCOX: Starting 11", 0, new byte[0]);
                GenericSoapPortTypeClient gss = new GenericSoapPortTypeClient(binding, endPoint);

                gss.ClientCredentials.UserName.UserName = userID;
                gss.ClientCredentials.UserName.Password = password;

                Dts.Log("HISCOX: Starting 2", 0, new byte[0]);



                Dts.Log("HISCOX: Starting 3", 0, new byte[0]);

                Dts.Log("HISCOX: Creating service", 0, new byte[0]);
                Service service = new Service();
                service.IdcService = "CHECKIN_UNIVERSAL";

                Dts.Log("HISCOX: Creating service document", 0, new byte[0]);
                ServiceDocument serviceDocument = new ServiceDocument();
                Field docTitleField = new Field();
                docTitleField.name = "dDocTitle";
                docTitleField.Value = "Supplier File for Upload";
                Field docTypeField = new Field();
                docTypeField.name = "dDocType";
                docTypeField.Value = "Document";
                Field docAuthorField = new Field();
                docAuthorField.name = "dDocAuthor";
                docAuthorField.Value = "Vasanthagokul.Chinnaswamy@HISCOX.com";
                Field docSecurityGroupField = new Field();
                docSecurityGroupField.name = "dSecurityGroup";
                docSecurityGroupField.Value = "FAFusionImportExport";
                Field docAccountField = new Field();
                docAccountField.name = "dDocAccount";
                docAccountField.Value = "prc$/supplier$/import$";
                Field docPrimaryFileField = new Field();
                docPrimaryFileField.name = "primaryFile";
                docPrimaryFileField.Value = "SuppTest3.zip";

                Dts.Log("Debug: Attaching fields to service document", 0, new byte[0]);
                serviceDocument.Field = new Field[] { docTitleField, docTypeField, docAuthorField, docSecurityGroupField, docAccountField, docPrimaryFileField };
                File file = new File();

                String inFile = "C:/Users/vivpal/Documents/HISCOX/Objects/Supplier/SuppTest3.zip";
                

                //byte[] content = Encoding.ASCII.GetBytes("VTest UCM Upload");
                byte[] content = System.Text.Encoding.UTF8.GetBytes(System.Convert.ToBase64String(System.IO.File.ReadAllBytes(inFile)));
                file.href = "SuppTest3.zip";
                file.name = "primaryFile";
                file.Contents = content;
                Dts.Log("Debug: Attaching file to service document", 0, new byte[0]);
                serviceDocument.File = new File[] { file };
                Dts.Log("Debug: Attaching service document to service", 0, new byte[0]);
                service.Document = serviceDocument;
                Dts.Log("Debug: Attaching service to request", 0, new byte[0]);

                Generic request = new Generic();
                request.webKey = "cs";

                request.Service = service;

                Dts.Log("Debug: Sending request", 0, new byte[0]);
                Generic response = gss.GenericSoapOperation(request);

                Dts.Log("Debug: Request Sent", 0, new byte[0]);
                //Dts.Log(response.GetType, 0, new byte[0]);

                Dts.TaskResult = (int)ScriptResults.Success;
            }
            catch (Exception ex)
            {
                Dts.Log("HISCOX: Exception: " + ex.StackTrace, 0, new byte[0]);
                String errorMessage = ex.Message;
                Dts.Log("HISCOX: Exception2: " + errorMessage, 0, new byte[0]);
                
                if (errorMessage.Contains("but expected 'text/xml'"))
                {
                    Dts.TaskResult = (int)ScriptResults.Success;
                }
                else
                {
                    Dts.TaskResult = (int)ScriptResults.Failure;
                }

            }
        }

        #region ScriptResults declaration
        /// <summary>
        /// This enum provides a convenient shorthand within the scope of this class for setting the
        /// result of the script.
        /// 
        /// This code was generated automatically.
        /// </summary>
        enum ScriptResults
        {
            Success = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success,
            Failure = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure
        };
        #endregion

	}
}
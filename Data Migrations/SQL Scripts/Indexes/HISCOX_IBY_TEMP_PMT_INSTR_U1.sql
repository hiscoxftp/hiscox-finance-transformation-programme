/*#****************************************************************
OBJECT NAME: HISCOX_IBY_TEMP_PMT_INSTR_U1
DESCRIPTION: Index on the table HISCOX_IBY_TEMP_PMT_INSTR_USES
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
CREATE UNIQUE INDEX HISCOX_IBY_TEMP_PMT_INSTR_U1 ON HISCOX_IBY_TEMP_PMT_INSTR_USES(RECORD_ID)
GO

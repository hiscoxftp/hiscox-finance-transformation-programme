/*#****************************************************************
OBJECT NAME: HISCOX_POZ_SUP_U1
DESCRIPTION: Index on the table HISCOX_POZ_SUPPLIERS_INT_STG
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
CREATE UNIQUE INDEX HISCOX_POZ_SUP_U1 ON HISCOX_POZ_SUPPLIERS_INT_STG(RECORD_ID)
GO

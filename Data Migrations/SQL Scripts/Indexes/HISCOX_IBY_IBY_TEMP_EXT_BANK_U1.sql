/*#****************************************************************
OBJECT NAME: HISCOX_IBY_IBY_TEMP_EXT_BANK_U1
DESCRIPTION: Index on the table HISCOX_IBY_TEMP_EXT_BANK_ACCTS
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
CREATE UNIQUE INDEX HISCOX_IBY_IBY_TEMP_EXT_BANK_U1 ON HISCOX_IBY_TEMP_EXT_BANK_ACCTS(RECORD_ID)
GO

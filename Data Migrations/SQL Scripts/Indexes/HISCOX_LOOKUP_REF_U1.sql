/*#****************************************************************
OBJECT NAME: HISCOX_LOOKUP_REF_U1
DESCRIPTION: Index on the table HISCOX_LOOKUP_REF_TBL
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
CREATE UNIQUE NONCLUSTERED INDEX HISCOX_LOOKUP_REF_U1 ON HISCOX_LOOKUP_REF_TBL (LOOKUP_TYPE,LOOKUP_CODE)
GO

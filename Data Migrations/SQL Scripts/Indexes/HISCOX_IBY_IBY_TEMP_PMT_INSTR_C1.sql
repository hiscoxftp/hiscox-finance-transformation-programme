/*#****************************************************************
OBJECT NAME: HISCOX_IBY_IBY_TEMP_PMT_INSTR_C1
DESCRIPTION: Index on the table HISCOX_IBY_TEMP_PMT_INSTR_USES
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
CREATE INDEX HISCOX_IBY_IBY_TEMP_PMT_INSTR_C1 ON HISCOX_IBY_TEMP_PMT_INSTR_USES(BATCH_ID,PROCESS_FLAG,PAYEE_BANK_ACCOUNT_IDENTIFIER)
GO

/*#****************************************************************
OBJECT NAME: HISCOX_IBY_IBY_TEMP_EXT_BANK_C1
DESCRIPTION: Index on the table HISCOX_IBY_TEMP_EXT_BANK_ACCTS
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
CREATE INDEX HISCOX_IBY_IBY_TEMP_EXT_BANK_C1 ON HISCOX_IBY_TEMP_EXT_BANK_ACCTS(BATCH_ID,PROCESS_FLAG,PAYEE_IDENTIFIER)
GO

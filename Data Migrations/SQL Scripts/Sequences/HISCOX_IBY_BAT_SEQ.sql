/*#****************************************************************
OBJECT NAME: HISCOX_IBY_TEMP_EXT_SEQ
DESCRIPTION: Sequence HISCOX_IBY_TEMP_EXT_SEQ
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
DROP SEQUENCE  HISCOX_IBY_BAT_SEQ
GO

CREATE SEQUENCE [dbo].[HISCOX_IBY_BAT_SEQ] AS INT
 START WITH 1
 INCREMENT BY 1
GO

/*#****************************************************************
OBJECT NAME: HISCOX_IBY_SOURCE
DESCRIPTION: Source table to 
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
DROP TABLE HISCOX_IBY_SOURCE
GO

CREATE TABLE [dbo].HISCOX_IBY_SOURCE(
    [Record_ID][INT] NOT NULL,
	[Process_Flag] [NVARCHAR](10) NOT NULL,
	[Error_Message] [NVARCHAR](max) NULL,
	[Batch_ID] [INT] NULL,
	[Load_Batch_ID] [INT] NULL,
	[code] [NVARCHAR](max) NULL,
	[supplier_code] [NVARCHAR](max) NULL,
	[name] [NVARCHAR](max) NULL,
	[business_unit] [NVARCHAR](max) NULL,
	[address_name] [NVARCHAR](max) NULL,
	[site_name] [NVARCHAR](max) NULL,
	[payment_method] [NVARCHAR](max) NULL,
	[delivery_channel_code] [NVARCHAR](max) NULL,
	[settlement_priority] [NVARCHAR](max) NULL,
	[remit_delivery_method] [NVARCHAR](max) NULL,
	[remit_advice_email] [NVARCHAR](max) NULL,
	[remit_advice_fax] [NVARCHAR](max) NULL,
	[bank_instructions_1] [NVARCHAR](max) NULL,
	[bank_instructions_2] [NVARCHAR](max) NULL,
	[bank_instruction_details] [NVARCHAR](max) NULL,
	[bank_name] [NVARCHAR](max) NULL,
	[bank_branch] [NVARCHAR](max) NULL,
	[bank_sorting_code] [NVARCHAR](max) NULL,
	[bank_account_name] [NVARCHAR](max) NULL,
	[iban_bank_account_number] [NVARCHAR](max) NULL,
	[iban] [NVARCHAR](max) NULL,
	[bank_account_number] [NVARCHAR](max) NULL,
	[country_code] [NVARCHAR](max) NULL,
	[account_currency_code] [NVARCHAR](max) NULL,
	[allow_international_payments] [NVARCHAR](max) NULL,
	[account_start_date] [NVARCHAR](max) NULL,
	[account_end_date] [NVARCHAR](max) NULL,
	[account_alternate_name] [NVARCHAR](max) NULL,
	[account_type_code] [NVARCHAR](max) NULL,
	[primary_flag] [NVARCHAR](max) NULL,
	[account_assignment_start_date] [NVARCHAR](max) NULL,
	[account_assignment_end_date] [NVARCHAR](max) NULL,
	[comment] [NVARCHAR](max) NULL,
	[status] [NVARCHAR](max) NULL,
	[migrate] [NVARCHAR](max) NULL,
	[attribute1] [NVARCHAR](max) NULL,
	[attribute2] [NVARCHAR](max) NULL,
	[attribute3] [NVARCHAR](max) NULL,
	[attribute4] [NVARCHAR](max) NULL,
	[attribute5] [NVARCHAR](max) NULL,
	[attribute6] [NVARCHAR](max) NULL,
	[attribute7] [NVARCHAR](max) NULL,
	[attribute8] [NVARCHAR](max) NULL,
	[attribute9] [NVARCHAR](max) NULL,
	[attribute10] [NVARCHAR](max) NULL,
	[attribute11] [NVARCHAR](max) NULL,
	[attribute12] [NVARCHAR](max) NULL,
	[attribute13] [NVARCHAR](max) NULL,
	[attribute14] [NVARCHAR](max) NULL,
	[attribute15] [NVARCHAR](max) NULL,
	[attribute16] [NVARCHAR](max) NULL,
	[attribute17] [NVARCHAR](max) NULL,
	[attribute18] [NVARCHAR](max) NULL,
	[attribute19] [NVARCHAR](max) NULL,
	[attribute20] [NVARCHAR](max) NULL,
	[attribute21] [NVARCHAR](max) NULL,
	[attribute22] [NVARCHAR](max) NULL,
	[attribute23] [NVARCHAR](max) NULL,
	[attribute24] [NVARCHAR](max) NULL,
	[attribute25] [NVARCHAR](max) NULL,
	[attribute26] [NVARCHAR](max) NULL,
	[attribute27] [NVARCHAR](max) NULL,
	[attribute28] [NVARCHAR](max) NULL,
	[attribute29] [NVARCHAR](max) NULL,
	[attribute30] [NVARCHAR](max) NULL,
	[attribute31] [NVARCHAR](max) NULL,
	[attribute32] [NVARCHAR](max) NULL,
	[attribute33] [NVARCHAR](max) NULL,
	[attribute34] [NVARCHAR](max) NULL,
	[attribute35] [NVARCHAR](max) NULL,
	[attribute36] [NVARCHAR](max) NULL,
	[attribute37] [NVARCHAR](max) NULL,
	[attribute38] [NVARCHAR](max) NULL,
	[attribute39] [NVARCHAR](max) NULL,
	[attribute40] [NVARCHAR](max) NULL,
	[attribute41] [NVARCHAR](max) NULL,
	[attribute42] [NVARCHAR](max) NULL,
	[attribute43] [NVARCHAR](max) NULL,
	[attribute44] [NVARCHAR](max) NULL,
	[attribute45] [NVARCHAR](max) NULL,
	[attribute46] [NVARCHAR](max) NULL,
	[attribute47] [NVARCHAR](max) NULL,
	[attribute48] [NVARCHAR](max) NULL,
	[attribute49] [NVARCHAR](max) NULL,
	[attribute50] [NVARCHAR](max) NULL,
	[attribute51] [NVARCHAR](max) NULL,
	[attribute52] [NVARCHAR](max) NULL,
	[attribute53] [NVARCHAR](max) NULL,
	[attribute54] [NVARCHAR](max) NULL,
	[attribute55] [NVARCHAR](max) NULL,
	[attribute56] [NVARCHAR](max) NULL,
	[attribute57] [NVARCHAR](max) NULL,
	[attribute58] [NVARCHAR](max) NULL,
	[attribute59] [NVARCHAR](max) NULL,
	[attribute60] [NVARCHAR](max) NULL,
	[attribute61] [NVARCHAR](max) NULL,
	[attribute62] [NVARCHAR](max) NULL,
	[attribute63] [NVARCHAR](max) NULL,
	[attribute64] [NVARCHAR](max) NULL,
	[attribute65] [NVARCHAR](max) NULL,
	[attribute66] [NVARCHAR](max) NULL,
	[attribute67] [NVARCHAR](max) NULL,
	[attribute68] [NVARCHAR](max) NULL,
	[attribute69] [NVARCHAR](max) NULL,
	[attribute70] [NVARCHAR](max) NULL,
	[attribute71] [NVARCHAR](max) NULL,
	[attribute72] [NVARCHAR](max) NULL,
	[attribute73] [NVARCHAR](max) NULL,
	[attribute74] [NVARCHAR](max) NULL,
	[attribute75] [NVARCHAR](max) NULL,
	[attribute76] [NVARCHAR](max) NULL,
	[attribute77] [NVARCHAR](max) NULL,
	[attribute78] [NVARCHAR](max) NULL,
	[attribute79] [NVARCHAR](max) NULL,
	[attribute80] [NVARCHAR](max) NULL,
	[attribute81] [NVARCHAR](max) NULL,
	[attribute82] [NVARCHAR](max) NULL,
	[attribute83] [NVARCHAR](max) NULL,
	[attribute84] [NVARCHAR](max) NULL,
	[attribute85] [NVARCHAR](max) NULL,
	[attribute86] [NVARCHAR](max) NULL,
	[attribute87] [NVARCHAR](max) NULL,
	[attribute88] [NVARCHAR](max) NULL,
	[attribute89] [NVARCHAR](max) NULL,
	[attribute90] [NVARCHAR](max) NULL,
	[attribute91] [NVARCHAR](max) NULL,
	[attribute92] [NVARCHAR](max) NULL,
	[attribute93] [NVARCHAR](max) NULL,
	[attribute94] [NVARCHAR](max) NULL,
	[attribute95] [NVARCHAR](max) NULL,
	[attribute96] [NVARCHAR](max) NULL,
	[attribute97] [NVARCHAR](max) NULL,
	[attribute98] [NVARCHAR](max) NULL,
	[attribute99] [NVARCHAR](max) NULL,
	[attribute100] [NVARCHAR](max) NULL,
	[file_geo] [NVARCHAR](1000) NULL,
	[Filename] [NVARCHAR](max) NULL,
	[Creation_Date] [DATETIME] NOT NULL,
	[Last_Update_Date] [DATETIME] NOT NULL,
	[Created_By] [NVARCHAR] (max) NOT NULL,
	[Updated_By] [NVARCHAR] (max) NOT NULL
)ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE HISCOX_IBY_SOURCE ADD CONSTRAINT HISCOX_IBY_SOURCE_REC_SEQ DEFAULT NEXT VALUE FOR HISCOX_IBY_TEMP_EXT_SEQ FOR Record_ID
GO

ALTER TABLE HISCOX_IBY_SOURCE ADD  DEFAULT ('N') FOR [Process_flag]
GO

ALTER TABLE HISCOX_IBY_SOURCE ADD  DEFAULT (getdate()) FOR [Creation_date]
GO

ALTER TABLE HISCOX_IBY_SOURCE ADD  DEFAULT (getdate()) FOR [Last_update_date]
GO

ALTER TABLE HISCOX_IBY_SOURCE ADD  DEFAULT (user_name()) FOR [Created_by]
GO

ALTER TABLE HISCOX_IBY_SOURCE ADD  DEFAULT (user_name()) FOR [Updated_by]
GO

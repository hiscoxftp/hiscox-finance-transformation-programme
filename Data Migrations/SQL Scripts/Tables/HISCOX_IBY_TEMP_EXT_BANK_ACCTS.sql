/*#****************************************************************
OBJECT NAME: HISCOX_IBY_TEMP_EXT_BANK_ACCTS 
DESCRIPTION: FBDI Similar Table for IBY_TEMP_EXT_BANK_ACCTS 
Version 		Name              		Date           		Description
----------------------------------------------------------------------------
1.0				Srinivasan Sridhar		04-MAR-2019 		Initial Version
**************************************************************** */
DROP TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS
GO

CREATE TABLE [dbo].HISCOX_IBY_TEMP_EXT_BANK_ACCTS(
    [Record_ID][INT] NOT NULL,
	[Process_Flag] [NVARCHAR](10) NOT NULL,
	[Error_Message] [NVARCHAR](max) NULL,
	[Batch_ID] [INT] NULL,
	[Load_Batch_ID] [INT] NULL,
	[Source_Record_ID] [INT] NULL,
	[Import_Batch_Identifier] [INT] NULL, --10
	[Payee_Identifier] [INT] NULL,
	[Payee_Bank_Account_Identifier] [INT] NULL,
	[Bank_Name] [NVARCHAR](200),  --80
	[Branch_Name] [NVARCHAR](200) , --80
	[Account_Country_Code] [NVARCHAR](100) ,  --2
	[Account_Name] [NVARCHAR](400) ,  --80
	[Account_Number] [NVARCHAR](400) ,  --100
	[Account_Currency_Code] [NVARCHAR](100) , 
	[Allow_International_Payments] [NVARCHAR](100),  --Y/N
	[Account_Start_Date] [NVARCHAR](100) ,  --YYYY/MM/DD
	[Account_End_Date] [NVARCHAR](100) ,  --YYYY/MM/DD
	[IBAN] [NVARCHAR](400) ,  --50
	[Check_Digits] [NVARCHAR](300) ,  --30
	[Account_Alternate_Name] [NVARCHAR](500) ,  --320
	[Account_Type_Code] [NVARCHAR](100) ,  --25
	[Account_Suffix] [NVARCHAR](300) ,  --30
	[Account_Description] [NVARCHAR](500) ,  --240
	[Agency_Location_Code] [NVARCHAR](100) ,  --30
	[Exchange_Rate_Agreement_Number] [NVARCHAR](500) ,  --80
	[Exchange_Rate_Agreement_Type] [NVARCHAR](500) ,  --80
	[Exchange_Rate] [INT] ,
	[Secondary_Account_Reference] [NVARCHAR](30) ,
	[Attribute_Category] [NVARCHAR](150) ,
	[Attribute_1] [NVARCHAR](150),
	[Attribute_2] [NVARCHAR](150),
	[Attribute_3] [NVARCHAR](150),
	[Attribute_4] [NVARCHAR](150),
	[Attribute_5] [NVARCHAR](150),
	[Attribute_6] [NVARCHAR](150),
	[Attribute_7] [NVARCHAR](150),
	[Attribute_8] [NVARCHAR](150),
	[Attribute_9] [NVARCHAR](150),
	[Attribute_10] [NVARCHAR](150),
	[Attribute_11] [NVARCHAR](150),
	[Attribute_12] [NVARCHAR](150),
	[Attribute_13] [NVARCHAR](150),
	[Attribute_14] [NVARCHAR](150),
	[Attribute_15] [NVARCHAR](150),
	[cnv_attribute1] [NVARCHAR](1000) NULL,
	[cnv_attribute2] [NVARCHAR](1000) NULL,
	[cnv_attribute3] [NVARCHAR](1000) NULL,
	[cnv_attribute4] [NVARCHAR](1000) NULL,
	[cnv_attribute5] [NVARCHAR](1000) NULL,
	[cnv_attribute6] [NVARCHAR](1000) NULL,
	[cnv_attribute7] [NVARCHAR](1000) NULL,
	[cnv_attribute8] [NVARCHAR](1000) NULL,
	[cnv_attribute9] [NVARCHAR](1000) NULL,
	[cnv_attribute10] [NVARCHAR](1000) NULL,
	[cnv_attribute11] [NVARCHAR](1000) NULL,
	[cnv_attribute12] [NVARCHAR](1000) NULL,
	[cnv_attribute13] [NVARCHAR](1000) NULL,
	[cnv_attribute14] [NVARCHAR](1000) NULL,
	[cnv_attribute15] [NVARCHAR](1000) NULL,
	[cnv_attribute16] [NVARCHAR](1000) NULL,
	[cnv_attribute17] [NVARCHAR](1000) NULL,
	[cnv_attribute18] [NVARCHAR](1000) NULL,
	[cnv_attribute19] [NVARCHAR](1000) NULL,
	[cnv_attribute20] [NVARCHAR](1000) NULL,
	[cnv_attribute21] [NVARCHAR](1000) NULL,
	[cnv_attribute22] [NVARCHAR](1000) NULL,
	[cnv_attribute23] [NVARCHAR](1000) NULL,
	[cnv_attribute24] [NVARCHAR](1000) NULL,
	[cnv_attribute25] [NVARCHAR](1000) NULL,
	[cnv_attribute26] [NVARCHAR](1000) NULL,
	[cnv_attribute27] [NVARCHAR](1000) NULL,
	[cnv_attribute28] [NVARCHAR](1000) NULL,
	[cnv_attribute29] [NVARCHAR](1000) NULL,
	[cnv_attribute30] [NVARCHAR](1000) NULL,
	[cnv_attribute31] [NVARCHAR](1000) NULL,
	[cnv_attribute32] [NVARCHAR](1000) NULL,
	[cnv_attribute33] [NVARCHAR](1000) NULL,
	[cnv_attribute34] [NVARCHAR](1000) NULL,
	[cnv_attribute35] [NVARCHAR](1000) NULL,
	[cnv_attribute36] [NVARCHAR](1000) NULL,
	[cnv_attribute37] [NVARCHAR](1000) NULL,
	[cnv_attribute38] [NVARCHAR](1000) NULL,
	[cnv_attribute39] [NVARCHAR](1000) NULL,
	[cnv_attribute40] [NVARCHAR](1000) NULL,
	[cnv_attribute41] [NVARCHAR](1000) NULL,
	[cnv_attribute42] [NVARCHAR](1000) NULL,
	[cnv_attribute43] [NVARCHAR](1000) NULL,
	[cnv_attribute44] [NVARCHAR](1000) NULL,
	[cnv_attribute45] [NVARCHAR](1000) NULL,
	[cnv_attribute46] [NVARCHAR](1000) NULL,
	[cnv_attribute47] [NVARCHAR](1000) NULL,
	[cnv_attribute48] [NVARCHAR](1000) NULL,
	[cnv_attribute49] [NVARCHAR](1000) NULL,
	[cnv_attribute50] [NVARCHAR](1000) NULL,
	[file_geo] [NVARCHAR](1000) NULL,
	[Filename] [NVARCHAR](max) NULL,
	[Creation_Date] [DATETIME] NOT NULL,
	[Last_Update_Date] [DATETIME] NOT NULL,
	[Created_By] [NVARCHAR] (max) NOT NULL,
	[Updated_By] [NVARCHAR] (max) NOT NULL
)ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS ADD CONSTRAINT HISCOX_IBY_TEMP_EXT_BANK_ACCTS_REC_SEQ DEFAULT NEXT VALUE FOR HISCOX_IBY_TEMP_EXT_SEQ FOR Record_ID
GO

ALTER TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS ADD  DEFAULT ('N') FOR [Process_flag]
GO

ALTER TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS ADD  DEFAULT (getdate()) FOR [Creation_date]
GO

ALTER TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS ADD  DEFAULT (getdate()) FOR [Last_update_date]
GO

ALTER TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS ADD  DEFAULT (user_name()) FOR [Created_by]
GO

ALTER TABLE HISCOX_IBY_TEMP_EXT_BANK_ACCTS ADD  DEFAULT (user_name()) FOR [Updated_by]
GO

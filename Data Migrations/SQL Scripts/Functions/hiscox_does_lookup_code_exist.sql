CREATE OR ALTER FUNCTION [dbo].hiscox_does_lookup_code_exist(@p_lookup_type  NVARCHAR(100)
                                                   ,@p_lookup_code  NVARCHAR(100))    
RETURNS INT
/*****************************************************************
OBJECT NAME: HISCOX_DOES_LOOKUP_CODE_EXIST
DESCRIPTION: Function to check if the lookup code exists
Version     Name                Date                Description
----------------------------------------------------------------------------
1.0         Vivek Pal          30-OCT-2018         Initial version
*****************************************************************/
BEGIN 
   DECLARE
   @ln_count   INT = 0;
	

	SET @ln_count = (SELECT COUNT(1)
							 FROM hiscox_lookup_ref_tbl
							WHERE lookup_type = @p_lookup_type
							  AND lookup_code = @p_lookup_code COLLATE Latin1_General_CS_AS );
	RETURN @ln_count;
END
GO
   
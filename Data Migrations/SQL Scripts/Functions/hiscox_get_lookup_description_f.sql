CREATE OR ALTER FUNCTION [dbo].hiscox_get_lookup_description_f(@p_lookup_type  NVARCHAR(1000)
                                                     ,@p_lookup_code  NVARCHAR(1000))    
RETURNS NVARCHAR(1000) 
/*****************************************************************
OBJECT NAME: HISCOX_GET_LOOKUP_DESCRIPTION_F
DESCRIPTION: Function to return the description for the given lookup type and code
Version     Name                Date                Description
----------------------------------------------------------------------------
1.0         Vivek Pal          30-OCT-2018         Initial version
*****************************************************************/
BEGIN 
   DECLARE
   @lv_meaning NVARCHAR(1000), 
   @ln_count   NVARCHAR(1000)
   
   SET @lv_meaning = (SELECT description
                        FROM hiscox_lookup_ref_tbl
                       WHERE lookup_type = @p_lookup_type
                         AND lookup_code = @p_lookup_code COLLATE Latin1_General_CS_AS );
   RETURN @lv_meaning;
END
GO
   
CREATE OR ALTER FUNCTION [dbo].hiscox_get_lookup_meaning_f(@p_lookup_type  NVARCHAR(1000)
                                                 ,@p_lookup_code  NVARCHAR(1000))    
RETURNS nvarchar(1000) 
/*****************************************************************
OBJECT NAME: HISCOX_GET_LOOKUP_MEANING_F
DESCRIPTION: Function to return the meaning for the given lookup type and code
Version     Name                Date                Description
----------------------------------------------------------------------------
1.0         Vivek Pal          30-OCT-2018         Initial version
*****************************************************************/
BEGIN 
   DECLARE
   @lv_meaning NVARCHAR(300), 
   @ln_count   NVARCHAR(300)
	
	SET @lv_meaning = (SELECT meaning
							   FROM hiscox_lookup_ref_tbl
							  WHERE lookup_type = @p_lookup_type
							    AND lookup_code = @p_lookup_code COLLATE Latin1_General_CS_AS );
	RETURN @lv_meaning;
END
GO
   
CREATE OR ALTER PROCEDURE [dbo].HISCOX_POZ_SUPPLIER_TRANS_P
                                @p_batch NVARCHAR(100)
                                ,@p_file_geo NVARCHAR(100)
                                ,@x_error_code INT OUTPUT
                                ,@x_error_msg NVARCHAR(4000) OUTPUT
AS

/*****************************************************************
OBJECT NAME: HISCOX_POZ_SUPPLIER_TRANS_P
DESCRIPTION: Procedure to transform the source data from Suppliers staging table to the target FBDI table further validation
Version     Name                Date                Description
----------------------------------------------------------------------------
1.0         Srinivasan          30-OCT-2018         Initial version
1.1			Srinivasan			20-MAR-2019			Update Liability distribution account to be derived from Business Unit
1.2			Srinivasan		 	22-APR-2019			Updated DataType for NVARCHAR for String Variables
1.3			Srinivasan			14-MAY-2019			Modifications done for Tax Requirements
1.4			Srinivasan			21-MAY-2019			Modifications done for France Tax Requirements
1.5         Paramjeet           06-JUN-2019         BU Based on Dynamic Settlement_Priority based on LOOKUP MAP
1.6			Srinivasan			11-JUL-2019			Modified code for Tax changes
*****************************************************************/

BEGIN
    BEGIN TRY
-- Load FBDI Table with source table data and perform Transformations
        
		-- Declare Source Supplier Table Variables
        BEGIN
        DECLARE @ln_Record_Id INT , @lc_Batch_Id NVARCHAR(4000) , @lc_Filename NVARCHAR(4000) , @lc_File_geo NVARCHAR(4000) , @lc_Process_flag NVARCHAR(1) , @lc_Error_Message NVARCHAR(4000) , @lc_Supplier_Name NVARCHAR(4000) , @lc_Supplier_Code NVARCHAR(4000) , @lc_Supplier_desc1 NVARCHAR(4000) , @lc_Supplier_desc2 NVARCHAR(4000) , @lc_Supplie_Type NVARCHAR(4000) , @lc_Business_Relationship NVARCHAR(4000) , @lc_Payment_Method NVARCHAR(4000) , @lc_Add1_line1 NVARCHAR(4000) , @lc_Add1_line2 NVARCHAR(4000) , @lc_Add1_line3 NVARCHAR(4000) , @lc_Add1_line4 NVARCHAR(4000) , @lc_Add1_line5 NVARCHAR(4000) , @lc_Add1_city NVARCHAR(4000) , @lc_Add1_state NVARCHAR(4000) , @lc_Add1_province NVARCHAR(4000) , @lc_Add1_country NVARCHAR(4000) , @lc_Add1_postal_code NVARCHAR(4000) , @lc_Add1_postal_plus4code NVARCHAR(4000) , @lc_Add1_phone NVARCHAR(4000) , @lc_Add1_phoneext NVARCHAR(4000) , @lc_Add1_phonearea_cd NVARCHAR(4000) , @lc_Add1_fax NVARCHAR(4000) , @lc_Add1_faxext NVARCHAR(4000) , @lc_Add1_faxarea_cd NVARCHAR(4000) , @lc_Add2_line1 NVARCHAR(4000) , @lc_Add2_line2 NVARCHAR(4000) , @lc_Add2_line3 NVARCHAR(4000) , @lc_Add2_line4 NVARCHAR(4000) , @lc_Add2_line5 NVARCHAR(4000) , @lc_Add2_city NVARCHAR(4000) , @lc_Add2_state NVARCHAR(4000) , @lc_Add2_province NVARCHAR(4000) , @lc_Add2_country NVARCHAR(4000) , @lc_Add2_postal_code NVARCHAR(4000) , @lc_Add2_postal_plus4code NVARCHAR(4000) , @lc_Add2_phone NVARCHAR(4000) , @lc_Add2_phoneext NVARCHAR(4000) , @lc_Add2_phonearea_cd NVARCHAR(4000) , @lc_Add2_fax NVARCHAR(4000) , @lc_Add2_faxext NVARCHAR(4000) , @lc_Add2_faxarea_cd NVARCHAR(4000) , @lc_email1 NVARCHAR(4000) , @lc_email2 NVARCHAR(4000) , @lc_email3 NVARCHAR(4000) , @lc_prc_bu NVARCHAR(4000) , @lc_communication_method NVARCHAR(4000) , @lc_inv_curr NVARCHAR(4000) , @lc_payment_currency NVARCHAR(4000) , @lc_payment_terms NVARCHAR(4000) , @lc_term_date_basis NVARCHAR(4000) , @lc_always_take_discount NVARCHAR(4000) , @lc_vat_code NVARCHAR(4000) , @lc_tax_reg_number NVARCHAR(4000) , @lc_delivery_channel NVARCHAR(4000) , @lc_bank_instruction1 NVARCHAR(4000) , @lc_bank_instruction2 NVARCHAR(4000) , @lc_bank_instruction NVARCHAR(4000) , @lc_settlement_priority NVARCHAR(4000) , @lc_bank_charge_bearer NVARCHAR(4000) , @lc_payment_reason NVARCHAR(4000) , @lc_delivery_method NVARCHAR(4000) , @lc_remittance_email NVARCHAR(4000) , @lc_pay_each_doc_alone NVARCHAR(4000) , @lc_client_bu NVARCHAR(4000) , @lc_bill_bu NVARCHAR(4000) , @lc_ship_to_loc NVARCHAR(4000) , @lc_bill_to_loc NVARCHAR(4000) , @lc_liability_distribution NVARCHAR(4000) , @lc_bills_payable_distribution NVARCHAR(4000) , @lc_name_prefix NVARCHAR(4000) , @lc_first_name NVARCHAR(4000) , @lc_middle_name NVARCHAR(4000) , @lc_last_name NVARCHAR(4000) , @lc_job_title NVARCHAR(4000) , @lc_Admin_contact NVARCHAR(4000) , @lc_email_contact NVARCHAR(4000) , @lc_phone_country_code NVARCHAR(4000) , @lc_phone_area_code NVARCHAR(4000) , @lc_phone_extension NVARCHAR(4000) , @lc_phone NVARCHAR(4000) , @lc_mobile_country_code NVARCHAR(4000) , @lc_mobile_area_code NVARCHAR(4000) , @lc_mobile NVARCHAR(4000) , @lc_attribute1 NVARCHAR(4000) , @lc_attribute2 NVARCHAR(4000) , @lc_attribute3 NVARCHAR(4000) , @lc_attribute4 NVARCHAR(4000) , @lc_attribute5 NVARCHAR(4000) , @lc_attribute6 NVARCHAR(4000) , @lc_attribute7 NVARCHAR(4000) , @lc_attribute8 NVARCHAR(4000) , @lc_attribute9 NVARCHAR(4000) , @lc_attribute10 NVARCHAR(4000) , @lc_attribute11 NVARCHAR(4000) , @lc_attribute12 NVARCHAR(4000) , @lc_attribute13 NVARCHAR(4000) , @lc_attribute14 NVARCHAR(4000) , @lc_attribute15 NVARCHAR(4000) , @lc_attribute16 NVARCHAR(4000) , @lc_attribute17 NVARCHAR(4000) , @lc_attribute18 NVARCHAR(4000) , @lc_attribute19 NVARCHAR(4000) , @lc_attribute20 NVARCHAR(4000) , @lc_attribute21 NVARCHAR(4000) , @lc_attribute22 NVARCHAR(4000) , @lc_attribute23 NVARCHAR(4000) , @lc_attribute24 NVARCHAR(4000) , @lc_attribute25 NVARCHAR(4000) , @lc_attribute26 NVARCHAR(4000) , @lc_attribute27 NVARCHAR(4000) , @lc_attribute28 NVARCHAR(4000) , @lc_attribute29 NVARCHAR(4000) , @lc_attribute30 NVARCHAR(4000) , @lc_attribute31 NVARCHAR(4000) , @lc_attribute32 NVARCHAR(4000) , @lc_attribute33 NVARCHAR(4000) , @lc_attribute34 NVARCHAR(4000) , @lc_attribute35 NVARCHAR(4000) , @lc_attribute36 NVARCHAR(4000) , @lc_attribute37 NVARCHAR(4000) , @lc_attribute38 NVARCHAR(4000) , @lc_attribute39 NVARCHAR(4000) , @lc_attribute40 NVARCHAR(4000) , @lc_attribute41 NVARCHAR(4000) , @lc_attribute42 NVARCHAR(4000) , @lc_attribute43 NVARCHAR(4000) , @lc_attribute44 NVARCHAR(4000) , @lc_attribute45 NVARCHAR(4000) , @lc_attribute46 NVARCHAR(4000) , @lc_attribute47 NVARCHAR(4000) , @lc_attribute48 NVARCHAR(4000) , @lc_attribute49 NVARCHAR(4000) , @lc_attribute50 NVARCHAR(4000) , @lc_attribute51 NVARCHAR(4000) , @lc_attribute52 NVARCHAR(4000) , @lc_attribute53 NVARCHAR(4000) , @lc_attribute54 NVARCHAR(4000) , @lc_attribute55 NVARCHAR(4000) , @lc_attribute56 NVARCHAR(4000) , @lc_attribute57 NVARCHAR(4000) , @lc_attribute58 NVARCHAR(4000) , @lc_attribute59 NVARCHAR(4000) , @lc_attribute60 NVARCHAR(4000) , @lc_attribute61 NVARCHAR(4000) , @lc_attribute62 NVARCHAR(4000) , @lc_attribute63 NVARCHAR(4000) , @lc_attribute64 NVARCHAR(4000) , @lc_attribute65 NVARCHAR(4000) , @lc_attribute66 NVARCHAR(4000) , @lc_attribute67 NVARCHAR(4000) , @lc_attribute68 NVARCHAR(4000) , @lc_attribute69 NVARCHAR(4000) , @lc_attribute70 NVARCHAR(4000) , @lc_attribute71 NVARCHAR(4000) , @lc_attribute72 NVARCHAR(4000) , @lc_attribute73 NVARCHAR(4000) , @lc_attribute74 NVARCHAR(4000) , @lc_attribute75 NVARCHAR(4000) , @lc_attribute76 NVARCHAR(4000) , @lc_attribute77 NVARCHAR(4000) , @lc_attribute78 NVARCHAR(4000) , @lc_attribute79 NVARCHAR(4000) , @lc_attribute80 NVARCHAR(4000) , @lc_attribute81 NVARCHAR(4000) , @lc_attribute82 NVARCHAR(4000) , @lc_attribute83 NVARCHAR(4000) , @lc_attribute84 NVARCHAR(4000) , @lc_attribute85 NVARCHAR(4000) , @lc_attribute86 NVARCHAR(4000) , @lc_attribute87 NVARCHAR(4000) , @lc_attribute88 NVARCHAR(4000) , @lc_attribute89 NVARCHAR(4000) , @lc_attribute90 NVARCHAR(4000) , @lc_attribute91 NVARCHAR(4000) , @lc_attribute92 NVARCHAR(4000) , @lc_attribute93 NVARCHAR(4000) , @lc_attribute94 NVARCHAR(4000) , @lc_attribute95 NVARCHAR(4000) , @lc_attribute96 NVARCHAR(4000) , @lc_attribute97 NVARCHAR(4000) , @lc_attribute98 NVARCHAR(4000) , @lc_attribute99 NVARCHAR(4000) , @lc_attribute100 NVARCHAR(4000) , @lc_attribute101 NVARCHAR(4000) , @lc_attribute102 NVARCHAR(4000) , @lc_attribute103 NVARCHAR(4000) , @lc_attribute104 NVARCHAR(4000) , @lc_attribute105 NVARCHAR(4000) , @lc_attribute106 NVARCHAR(4000) , @lc_attribute107 NVARCHAR(4000) , @lc_attribute108 NVARCHAR(4000) , @lc_attribute109 NVARCHAR(4000) , @lc_attribute110 NVARCHAR(4000) , @lc_attribute111 NVARCHAR(4000) , @lc_attribute112 NVARCHAR(4000) , @lc_attribute113 NVARCHAR(4000) , @lc_attribute114 NVARCHAR(4000) , @lc_attribute115 NVARCHAR(4000) , @lc_attribute116 NVARCHAR(4000) , @lc_attribute117 NVARCHAR(4000) , @lc_attribute118 NVARCHAR(4000) , @lc_attribute119 NVARCHAR(4000) , @lc_attribute120 NVARCHAR(4000) , @lc_attribute121 NVARCHAR(4000) , @lc_attribute122 NVARCHAR(4000) , @lc_attribute123 NVARCHAR(4000) , @lc_attribute124 NVARCHAR(4000) , @lc_attribute125 NVARCHAR(4000) , @lc_attribute126 NVARCHAR(4000) , @lc_attribute127 NVARCHAR(4000) , @lc_attribute128 NVARCHAR(4000) , @lc_attribute129 NVARCHAR(4000) , @lc_attribute130 NVARCHAR(4000) , @lc_attribute131 NVARCHAR(4000) , @lc_attribute132 NVARCHAR(4000) , @lc_attribute133 NVARCHAR(4000) , @lc_attribute134 NVARCHAR(4000) , @lc_attribute135 NVARCHAR(4000) , @lc_attribute136 NVARCHAR(4000) , @lc_attribute137 NVARCHAR(4000) , @lc_attribute138 NVARCHAR(4000) , @lc_attribute139 NVARCHAR(4000) , @lc_attribute140 NVARCHAR(4000) , @lc_attribute141 NVARCHAR(4000) , @lc_attribute142 NVARCHAR(4000) , @lc_attribute143 NVARCHAR(4000) , @lc_attribute144 NVARCHAR(4000) , @lc_attribute145 NVARCHAR(4000) , @lc_attribute146 NVARCHAR(4000) , @lc_attribute147 NVARCHAR(4000) , @lc_attribute148 NVARCHAR(4000) , @lc_attribute149 NVARCHAR(4000) , @lc_attribute150 NVARCHAR(4000), @ln_override_mail INT,@lc_override_mail NVARCHAR(100), @lc_bu_country NVARCHAR(100),@lc_bu NVARCHAR(100)
        
		
		SET @lc_Batch_Id = @p_batch
        --Declare common variables
        DECLARE @lc_error_msg  NVARCHAR(4000), @ln_count_rec INT, @lc_lookup_type NVARCHAR(500)
        
        --Declare Variables for Supplier Header
        DECLARE @lc_sup_import_action NVARCHAR(4000) 
        ,@lc_sup_process_flag NVARCHAR(4000) 
        ,@lc_sup_supplier_name NVARCHAR(4000) 
        ,@lc_sup_supplier_number NVARCHAR(4000) 
        ,@lc_sup_tax_organization_type NVARCHAR(4000) 
        ,@lc_sup_supplier_type NVARCHAR(4000) 
        ,@lc_sup_business_relationship NVARCHAR(4000) 
        ,@lc_sup_alias NVARCHAR(4000) 
        ,@lc_sup_tax_pay_country NVARCHAR(4000) 
        ,@lc_sup_tax_pay_id    NVARCHAR(4000) 
        ,@lc_sup_fed_report    NVARCHAR(4000) 
        ,@lc_sup_fed_inc_tax NVARCHAR(4000) 
		,@lc_sup_use_withhold_tax NVARCHAR(4000)
        ,@lc_sup_payment_method    NVARCHAR(4000) 
        ,@lc_sup_settlement_priority NVARCHAR(4000) 
        ,@lc_sup_reg_id    NVARCHAR(4000) 
        ,@lc_sup_pay_service_lvl NVARCHAR(4000) 
        ,@lc_sup_pay_each_doc NVARCHAR(4000)
		,@lc_bu_supp NVARCHAR(4000)
		,@lc_supp_con NVARCHAR(4000)
        
        -- Declare Variables for Supplier Address Interface
        DECLARE @lc_sa_import_action NVARCHAR(4000)
        ,@lc_sa_process_flag NVARCHAR(4000)
        ,@lc_sa_address_name NVARCHAR(4000)
        ,@lc_sa_supplier_name NVARCHAR(4000)
        ,@lc_sa_address_line1 NVARCHAR(4000)
        ,@lc_sa_address_line2 NVARCHAR(4000)
        ,@lc_sa_address_line3 NVARCHAR(4000)
        ,@lc_sa_address_line4 NVARCHAR(4000)
        ,@lc_sa_country NVARCHAR(4000)
        ,@lc_sa_city NVARCHAR(4000)
        ,@lc_sa_state NVARCHAR(4000)
        ,@lc_sa_province NVARCHAR(4000)
        ,@lc_sa_county NVARCHAR(4000)
        ,@lc_sa_postal_code NVARCHAR(4000)
        ,@lc_sa_postal_plus4code NVARCHAR(4000)
        ,@lc_sa_addressee NVARCHAR(4000)
        ,@lc_sa_phone NVARCHAR(4000)
        ,@lc_sa_rfq_bidding NVARCHAR(4000)
        ,@lc_sa_ordering NVARCHAR(4000)
        ,@lc_sa_pay NVARCHAR(4000)
        ,@lc_sa_pay_each_doc_alone NVARCHAR(4000)
		,@lc_comp_country NVARCHAR(4000)
        
        -- Declare Variables for Supplier Site Interface                                        
        DECLARE @lc_ss_import_action NVARCHAR(4000)
        ,@lc_ss_process_flag NVARCHAR(4000)
        ,@lc_ss_supplier_name NVARCHAR(4000)
        ,@lc_ss_procurement_bu NVARCHAR(4000)
        ,@lc_ss_address_name NVARCHAR(4000)
        ,@lc_ss_supplier_site_name NVARCHAR(4000)
        ,@lc_ss_sourcing_only  NVARCHAR(4000)
        ,@lc_ss_pay NVARCHAR(4000)
        ,@lc_ss_primary_pay NVARCHAR(4000)
        ,@lc_ss_income_tax_rep_site NVARCHAR(4000)
        ,@lc_ss_communication_method NVARCHAR(4000)
        ,@lc_ss_email NVARCHAR(4000)
        ,@lc_ss_inv_currency NVARCHAR(4000)
        ,@lc_ss_payment_currency NVARCHAR(4000)
        ,@lc_ss_payment_terms NVARCHAR(4000)
        ,@lc_ss_terms_date_basis NVARCHAR(4000)
        ,@lc_ss_always_tak_discount NVARCHAR(4000)
        ,@lc_ss_vat_code NVARCHAR(4000)
        ,@lc_ss_tax_reg_num NVARCHAR(4000)
        ,@lc_ss_payment_method NVARCHAR(4000)
        ,@lc_ss_delivery_channel NVARCHAR(4000)
        ,@lc_ss_bank_instruction1 NVARCHAR(4000)
        ,@lc_ss_bank_instruction2 NVARCHAR(4000)
        ,@lc_ss_bank_instruction NVARCHAR(4000)
        ,@lc_ss_settlement_priority NVARCHAR(4000)
        ,@lc_ss_bank_charge_bearer NVARCHAR(4000)
        ,@lc_ss_payment_reason NVARCHAR(4000)
        ,@lc_ss_delivery_method NVARCHAR(4000)
        ,@lc_ss_remit_email NVARCHAR(4000)
        ,@lc_ss_pay_each_doc_alone NVARCHAR(4000)
		,@lc_ss_global_attribute_category NVARCHAR(4000)
		,@lc_ss_global_attribute4 NVARCHAR(4000)
        
        -- Declare Variables for Supplier Site Assignments Interface
        DECLARE @lc_ssa_import_action NVARCHAR(4000)
        ,@lc_ssa_process_flag NVARCHAR(4000)
        ,@lc_ssa_supplier_name NVARCHAR(4000)
        ,@lc_ssa_supplier_site NVARCHAR(4000)
        ,@lc_ssa_procurement_bu NVARCHAR(4000)
        ,@lc_ssa_client_bu NVARCHAR(4000)
        ,@lc_ssa_billto_bu NVARCHAR(4000)
        ,@lc_ssa_ship_to_loc NVARCHAR(4000)
        ,@lc_ssa_bill_to_loc NVARCHAR(4000)
        ,@lc_ssa_withhold_tax NVARCHAR(4000)
        ,@lc_ssa_withhold_tax_grp NVARCHAR(4000)
        ,@lc_ssa_lia_dist NVARCHAR(4000)
        ,@lc_ssa_prepay_dist NVARCHAR(4000)
        ,@lc_ssa_bills_pay_dist NVARCHAR(4000)
        ,@lc_ssa_distribution_set NVARCHAR(4000)
        ,@lc_ssa_inv_date NVARCHAR(4000)
                                                
        -- Declare Variables for Supplier Contacts INTERFACE
                                                
        DECLARE @lc_sc_import_action NVARCHAR(4000)
        ,@lc_sc_process_flag NVARCHAR(4000)
        ,@lc_sc_supplier_name NVARCHAR(4000)
        ,@lc_sc_prefix NVARCHAR(4000)
        ,@lc_sc_first_name NVARCHAR(4000)
        ,@lc_sc_middle_name NVARCHAR(4000)
        ,@lc_sc_last_name NVARCHAR(4000)
        ,@lc_sc_job_title NVARCHAR(4000)
        ,@lc_sc_admin_contact NVARCHAR(4000)
        ,@lc_sc_email NVARCHAR(4000)
        ,@lc_sc_phone_country_code NVARCHAR(4000)
        ,@lc_sc_phone_area_code NVARCHAR(4000)
        ,@lc_sc_phone NVARCHAR(4000)
        ,@lc_sc_phone_extension NVARCHAR(4000)
        ,@lc_sc_fax_country_code NVARCHAR(4000)
        ,@lc_sc_fax_area_code NVARCHAR(4000)
        ,@lc_sc_fax NVARCHAR(4000)
        ,@lc_sc_mobile_country_code NVARCHAR(4000)
        ,@lc_sc_mobile_area_code NVARCHAR(4000)
        ,@lc_sc_mobile NVARCHAR(4000)
                                                
                                                
		-- Declare Variables for Supplier Contact Address INTERFACE
        DECLARE @lc_sca_import_action NVARCHAR(4000)
        ,@lc_sca_process_flag NVARCHAR(4000)
        ,@lc_sca_supplier_name NVARCHAR(4000)
        ,@lc_sca_address_name NVARCHAR(4000)
        ,@lc_sca_first_name NVARCHAR(4000)
        ,@lc_sca_last_name NVARCHAR(4000)
        ,@lc_sca_email NVARCHAR(4000)
        
		
		DECLARE @lc_rice_id NVARCHAR(100) = 'P2P-SP-CNV003,005,006,007'
		,@lc_source NVARCHAR(100) = 'HISCOX_POZ_SUPPLIER_TRANS_P'
		,@lc_calling_object NVARCHAR(100) = 'HISCOX Supplier Data Migration SSIS Package'
                
                    BEGIN
					
						-- DELETE the header Row
						DELETE FROM hiscox_poz_suppliers_source WHERE Supplier_Name ='Name' AND Supplier_Code ='Supplier Code' AND add1_postal_code='Postal Code' AND process_flag='N' AND batch_id=@p_batch
						
                        DECLARE cur_suppliers_source CURSOR LOCAL FOR 
                        SELECT Record_Id, Batch_Id, Filename, File_geo, Process_flag, Error_Message, Supplier_Name, Supplier_Code, Supplier_desc1, Supplier_desc2, Supplie_Type, Business_Relationship, Payment_Method, Add1_line1, Add1_line2, Add1_line3, Add1_line4, Add1_line5, Add1_city, Add1_state, Add1_province, Add1_country, Add1_postal_code, Add1_postal_plus4code, Add1_phone, Add1_phoneext, Add1_phonearea_cd, Add1_fax, Add1_faxext, Add1_faxarea_cd, Add2_line1, Add2_line2, Add2_line3, Add2_line4, Add2_line5, Add2_city, Add2_state, Add2_province, Add2_country, Add2_postal_code, Add2_postal_plus4code, Add2_phone, Add2_phoneext, Add2_phonearea_cd, Add2_fax, Add2_faxext, Add2_faxarea_cd, email1, email2, email3, prc_bu, communication_method, inv_curr, payment_currency, payment_terms, term_date_basis, always_take_discount, vat_code, tax_reg_number, delivery_channel, bank_instruction1, bank_instruction2, bank_instruction, settlement_priority, bank_charge_bearer, payment_reason, delivery_method, remittance_email, pay_each_doc_alone, client_bu, bill_bu, ship_to_loc, bill_to_loc, liability_distribution, bills_payable_distribution, name_prefix, first_name, middle_name, last_name, job_title, Admin_contact, email_contact, phone_country_code, phone_area_code, phone_extension, phone, mobile_country_code, mobile_area_code, mobile, attribute1, attribute2, attribute3, attribute4, attribute5, attribute6, attribute7, attribute8, attribute9, attribute10, attribute11, attribute12, attribute13, attribute14, attribute15, attribute16, attribute17, attribute18, attribute19, attribute20, attribute21, attribute22, attribute23, attribute24, attribute25, attribute26, attribute27, attribute28, attribute29, attribute30, attribute31, attribute32, attribute33, attribute34, attribute35, attribute36, attribute37, attribute38, attribute39, attribute40, attribute41, attribute42, attribute43, attribute44, attribute45, attribute46, attribute47, attribute48, attribute49, attribute50, attribute51, attribute52, attribute53, attribute54, attribute55, attribute56, attribute57, attribute58, attribute59, attribute60, attribute61, attribute62, attribute63, attribute64, attribute65, attribute66, attribute67, attribute68, attribute69, attribute70, attribute71, attribute72, attribute73, attribute74, attribute75, attribute76, attribute77, attribute78, attribute79, attribute80, attribute81, attribute82, attribute83, attribute84, attribute85, attribute86, attribute87, attribute88, attribute89, attribute90, attribute91, attribute92, attribute93, attribute94, attribute95, attribute96, attribute97, attribute98, attribute99, attribute100, attribute101, attribute102, attribute103, attribute104, attribute105, attribute106, attribute107, attribute108, attribute109, attribute110, attribute111, attribute112, attribute113, attribute114, attribute115, attribute116, attribute117, attribute118, attribute119, attribute120, attribute121, attribute122, attribute123, attribute124, attribute125, attribute126, attribute127, attribute128, attribute129, attribute130, attribute131, attribute132, attribute133, attribute134, attribute135, attribute136, attribute137, attribute138, attribute139, attribute140, attribute141, attribute142, attribute143, attribute144, attribute145, attribute146, attribute147, attribute148, attribute149, attribute150
                        FROM hiscox_poz_suppliers_source
                        WHERE process_flag = 'N'
                        AND batch_id=@p_batch
						
						
						EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'DEBUG'
													,@p_err_column     = 'DEBUG'  
													,@p_err_value      = 'DEBUG'  
													,@p_err_desc       = 'SUP_TRANS_P Procedure Start'   
													,@p_rect_action    = NULL; 
						
						IF @p_file_geo IN ('US','GGHIG','GGHISL','EU','BERMUDA')
						BEGIN
							/*IF @p_file_geo = 'GGHIG' 
								SET @lc_lookup_type = 'HISCOX_POZ_GGHIG_CONV_DEFAULT_MAP'
							ELSE IF @p_file_geo  = 'GGHISL'
								SET @lc_lookup_type =  'HISCOX_POZ_GGHISL_CONV_DEFAULT_MAP'
							ELSE IF @p_file_geo  = 'US'
								SET @lc_lookup_type =  'HISCOX_POZ_US_CONV_DEFAULT_MAP'
							ELSE IF @p_file_geo  = 'EU'
								SET @lc_lookup_type =  'HISCOX_POZ_EU_CONV_DEFAULT_MAP'
							ELSE IF @p_file_geo  = 'BERMUDA'
								SET @lc_lookup_type =  'HISCOX_POZ_BERMUDA_CONV_DEFAULT_MAP'

							*/
							SET @lc_lookup_type =  'HISCOX_POZ_CONV_DEFAULT_MAP'
							
							SET @ln_override_mail = (	SELECT count(*)
														FROM hiscox_lookup_ref_tbl
														WHERE 1=1
														AND lookup_type	= 'HISCOX_POZ_OVERRIDE'
														AND lookup_code = 'EMAIL'
														AND meaning		= 'Y'
													)
							IF @ln_override_mail = 1
								BEGIN
										SET @lc_override_mail	= (	SELECT description
														FROM hiscox_lookup_ref_tbl
														WHERE 1=1
														AND lookup_type	= 'HISCOX_POZ_OVERRIDE'
														AND lookup_code = 'EMAIL'
														AND meaning		= 'Y'
													)
								END
							
						

                        OPEN cur_suppliers_source  
                        FETCH NEXT FROM cur_suppliers_source INTO 
                        @ln_Record_Id , @lc_batch_Id , @lc_Filename , @lc_File_geo , @lc_Process_flag , @lc_Error_Message , @lc_Supplier_Name , @lc_Supplier_Code , @lc_Supplier_desc1 , @lc_Supplier_desc2 , @lc_Supplie_Type , @lc_Business_Relationship , @lc_Payment_Method , @lc_Add1_line1 , @lc_Add1_line2 , @lc_Add1_line3 , @lc_Add1_line4 , @lc_Add1_line5 , @lc_Add1_city , @lc_Add1_state , @lc_Add1_province , @lc_Add1_country , @lc_Add1_postal_code , @lc_Add1_postal_plus4code , @lc_Add1_phone , @lc_Add1_phoneext , @lc_Add1_phonearea_cd , @lc_Add1_fax , @lc_Add1_faxext , @lc_Add1_faxarea_cd , @lc_Add2_line1 , @lc_Add2_line2 , @lc_Add2_line3 , @lc_Add2_line4 , @lc_Add2_line5 , @lc_Add2_city , @lc_Add2_state , @lc_Add2_province , @lc_Add2_country , @lc_Add2_postal_code , @lc_Add2_postal_plus4code , @lc_Add2_phone , @lc_Add2_phoneext , @lc_Add2_phonearea_cd , @lc_Add2_fax , @lc_Add2_faxext , @lc_Add2_faxarea_cd , @lc_email1 , @lc_email2 , @lc_email3 , @lc_prc_bu , @lc_communication_method , @lc_inv_curr , @lc_payment_currency , @lc_payment_terms , @lc_term_date_basis , @lc_always_take_discount , @lc_vat_code , @lc_tax_reg_number , @lc_delivery_channel , @lc_bank_instruction1 , @lc_bank_instruction2 , @lc_bank_instruction , @lc_settlement_priority , @lc_bank_charge_bearer , @lc_payment_reason , @lc_delivery_method , @lc_remittance_email , @lc_pay_each_doc_alone , @lc_client_bu , @lc_bill_bu , @lc_ship_to_loc , @lc_bill_to_loc , @lc_liability_distribution , @lc_bills_payable_distribution , @lc_name_prefix , @lc_first_name , @lc_middle_name , @lc_last_name , @lc_job_title , @lc_Admin_contact , @lc_email_contact , @lc_phone_country_code , @lc_phone_area_code , @lc_phone_extension , @lc_phone , @lc_mobile_country_code , @lc_mobile_area_code , @lc_mobile , @lc_attribute1 , @lc_attribute2 , @lc_attribute3 , @lc_attribute4 , @lc_attribute5 , @lc_attribute6 , @lc_attribute7 , @lc_attribute8 , @lc_attribute9 , @lc_attribute10 , @lc_attribute11 , @lc_attribute12 , @lc_attribute13 , @lc_attribute14 , @lc_attribute15 , @lc_attribute16 , @lc_attribute17 , @lc_attribute18 , @lc_attribute19 , @lc_attribute20 , @lc_attribute21 , @lc_attribute22 , @lc_attribute23 , @lc_attribute24 , @lc_attribute25 , @lc_attribute26 , @lc_attribute27 , @lc_attribute28 , @lc_attribute29 , @lc_attribute30 , @lc_attribute31 , @lc_attribute32 , @lc_attribute33 , @lc_attribute34 , @lc_attribute35 , @lc_attribute36 , @lc_attribute37 , @lc_attribute38 , @lc_attribute39 , @lc_attribute40 , @lc_attribute41 , @lc_attribute42 , @lc_attribute43 , @lc_attribute44 , @lc_attribute45 , @lc_attribute46 , @lc_attribute47 , @lc_attribute48 , @lc_attribute49 , @lc_attribute50 , @lc_attribute51 , @lc_attribute52 , @lc_attribute53 , @lc_attribute54 , @lc_attribute55 , @lc_attribute56 , @lc_attribute57 , @lc_attribute58 , @lc_attribute59 , @lc_attribute60 , @lc_attribute61 , @lc_attribute62 , @lc_attribute63 , @lc_attribute64 , @lc_attribute65 , @lc_attribute66 , @lc_attribute67 , @lc_attribute68 , @lc_attribute69 , @lc_attribute70 , @lc_attribute71 , @lc_attribute72 , @lc_attribute73 , @lc_attribute74 , @lc_attribute75 , @lc_attribute76 , @lc_attribute77 , @lc_attribute78 , @lc_attribute79 , @lc_attribute80 , @lc_attribute81 , @lc_attribute82 , @lc_attribute83 , @lc_attribute84 , @lc_attribute85 , @lc_attribute86 , @lc_attribute87 , @lc_attribute88 , @lc_attribute89 , @lc_attribute90 , @lc_attribute91 , @lc_attribute92 , @lc_attribute93 , @lc_attribute94 , @lc_attribute95 , @lc_attribute96 , @lc_attribute97 , @lc_attribute98 , @lc_attribute99 , @lc_attribute100 , @lc_attribute101 , @lc_attribute102 , @lc_attribute103 , @lc_attribute104 , @lc_attribute105 , @lc_attribute106 , @lc_attribute107 , @lc_attribute108 , @lc_attribute109 , @lc_attribute110 , @lc_attribute111 , @lc_attribute112 , @lc_attribute113 , @lc_attribute114 , @lc_attribute115 , @lc_attribute116 , @lc_attribute117 , @lc_attribute118 , @lc_attribute119 , @lc_attribute120 , @lc_attribute121 , @lc_attribute122 , @lc_attribute123 , @lc_attribute124 , @lc_attribute125 , @lc_attribute126 , @lc_attribute127 , @lc_attribute128 , @lc_attribute129 , @lc_attribute130 , @lc_attribute131 , @lc_attribute132 , @lc_attribute133 , @lc_attribute134 , @lc_attribute135 , @lc_attribute136 , @lc_attribute137 , @lc_attribute138 , @lc_attribute139 , @lc_attribute140 , @lc_attribute141 , @lc_attribute142 , @lc_attribute143 , @lc_attribute144 , @lc_attribute145 , @lc_attribute146 , @lc_attribute147 , @lc_attribute148 , @lc_attribute149 , @lc_attribute150
                        
                        WHILE @@FETCH_STATUS = 0  
                            BEGIN 
								BEGIN TRY	
										SET @lc_error_msg ='';
										SET @lc_bu_country	='';
                                        -- Set values for Supplier Header Interface 
                                        SET @lc_sup_import_action              = 'CREATE'
                                        SET @lc_sup_process_flag               = 'N'
                                        SET @lc_sup_supplier_name              = LTRIM(RTRIM(@lc_Supplier_Name))
                                        SET @lc_sup_supplier_number            = NULL
                                        SET @lc_sup_tax_organization_type      = 'Corporation'
                                        SET @lc_sup_supplier_type              = 'Supplier'
                                        SET @lc_sup_business_relationship      = 'SPEND_AUTHORIZED'
                                        SET @lc_sup_alias                      = LTRIM(RTRIM(@lc_Supplier_Code))
										SET @lc_bu							   = LTRIM(RTRIM(@lc_attribute14))
										EXEC @lc_bu_country					   = hiscox_get_lookup_meaning_f 'HISCOX_POZ_BU_COUNTRY', @lc_bu
                                        --SET @lc_sup_tax_pay_country            = (CASE WHEN (LTRIM(RTRIM(@lc_tax_reg_number)) IS NOT NULL AND LTRIM(RTRIM(@lc_tax_reg_number)) <>'') THEN LTRIM(RTRIM(@lc_Add1_country)) ELSE NULL END)
										SET @lc_sup_tax_pay_country            = (CASE WHEN (LTRIM(RTRIM(@lc_tax_reg_number)) IS NOT NULL AND LTRIM(RTRIM(@lc_tax_reg_number)) <>'') THEN LTRIM(RTRIM(@lc_bu_country)) ELSE NULL END)
                                        SET @lc_sup_tax_pay_id                 = (CASE WHEN (LTRIM(RTRIM(@lc_tax_reg_number)) IS NOT NULL AND LTRIM(RTRIM(@lc_tax_reg_number)) <>'') THEN LTRIM(RTRIM(@lc_tax_reg_number)) ELSE NULL END)
                                        SET @lc_sup_fed_report                 = 'N'
                                        SET @lc_sup_fed_inc_tax                = NULL
										SET @lc_sup_use_withhold_tax		   = 'N'
                                        SET @lc_sup_payment_method             = LTRIM(RTRIM(@lc_Payment_Method))
										SET @lc_bu_supp                        = LTRIM(RTRIM(@lc_attribute14)) + '-' + 'SETTLEMENT_PRIORITY'
										SET @lc_supp_con                       = LTRIM(RTRIM(@lc_Add1_country))
										--EXEC @lc_sup_settlement_priority        = hiscox_get_lookup_meaning_f @lc_lookup_type, 'SETTLEMENT_PRIORITY'
										
										print 'taxpayer country:'+@lc_sup_tax_pay_country
										
										/* Updated Code for US BU where Settlement Priority is based on BU if Country is US and payment method is CHECK Date:06-JUN-2019 */
										IF @lc_sup_payment_method ='CHECK' AND @lc_supp_con ='US'
											BEGIN
												EXEC @lc_sup_settlement_priority        =hiscox_get_lookup_meaning_f @lc_lookup_type,@lc_bu_supp
												IF @lc_sup_settlement_priority = '' OR @lc_sup_settlement_priority IS NULL
													BEGIN
														EXEC @lc_sup_settlement_priority        = hiscox_get_lookup_meaning_f @lc_lookup_type,'SETTLEMENT_PRIORITY'										
													END
												
											END	
										ELSE
											BEGIN
												
												EXEC @lc_sup_settlement_priority        = hiscox_get_lookup_meaning_f @lc_lookup_type, 'SETTLEMENT_PRIORITY'
											END
											
										SET @lc_sup_reg_id                     = NULL
                                        SET @lc_sup_pay_service_lvl            = NULL
                                        SET @lc_sup_pay_each_doc               = 'Y'
										
										--Added below on 14-MAY-2019
										IF @lc_sup_tax_pay_country 	= 'US' AND LTRIM(RTRIM(@lc_attribute14)) ='US BU'
											BEGIN
											   SET @lc_sup_fed_report		= 'Y'
												SET @lc_sup_fed_inc_tax		= 'MISC7'
											END
										--Added below on 21-MAY-2019										
										ELSE IF @lc_sup_tax_pay_country 	= 'FR' AND LTRIM(RTRIM(@lc_attribute14)) ='FR BU' AND ISNULL(LTRIM(RTRIM(@lc_attribute1)),'') <> ''
											BEGIN
												SET @lc_sup_fed_report		= 'Y'
												SET @lc_sup_fed_inc_tax		= LTRIM(RTRIM(@lc_attribute1))
											END
										ELSE
											BEGIN
											   SET @lc_sup_fed_report		= NULL
												SET @lc_sup_fed_inc_tax		= NULL
											END
										
										--Added below on 14-MAY-2019
										IF LTRIM(RTRIM(@lc_attribute14))  IN ('ES BU','US BU','PT BU') -- Added PT BU on 11-JUL-2019
											BEGIN
												SET @lc_sup_use_withhold_tax			= 'Y'
											END
										ELSE
											BEGIN
												SET @lc_sup_use_withhold_tax			= NULL
											END
											
                                        PRINT '2';
                                        -- Set values for Supplier Address Interface
                                        SET @lc_sa_import_action               = 'CREATE'
                                        SET @lc_sa_process_flag                = 'N'
                                        SET @lc_sa_address_name                = UPPER(LTRIM(RTRIM(@lc_Add1_city)))
                                        SET @lc_sa_supplier_name               = LTRIM(RTRIM(@lc_Supplier_Name))
                                        SET @lc_sa_address_line1               = LTRIM(RTRIM(REPLACE(REPLACE(@lc_Add1_line1,CHAR(10),''),CHAR(13),'')))
                                        SET @lc_sa_address_line2               = LTRIM(RTRIM(REPLACE(REPLACE(@lc_Add1_line2,CHAR(10),''),CHAR(13),'')))
                                        SET @lc_sa_address_line3               = LTRIM(RTRIM(REPLACE(REPLACE(@lc_Add1_line3,CHAR(10),''),CHAR(13),'')))
                                        SET @lc_sa_address_line4               = LTRIM(RTRIM(REPLACE(REPLACE(@lc_Add1_line4,CHAR(10),''),CHAR(13),'')))
										
										SET @lc_comp_country				   = ''
										SET @lc_sa_country 				   	   = LTRIM(RTRIM(@lc_Add1_country))
                                        SET @lc_sa_city                        = LTRIM(RTRIM(@lc_Add1_city))
										SET @lc_sa_state					   = LTRIM(RTRIM(@lc_Add1_state))
                                        SET @lc_sa_province                    = NULL
                                        SET @lc_sa_county                      = LTRIM(RTRIM(@lc_attribute11))
                                        SET @lc_sa_postal_code                 = LTRIM(RTRIM(@lc_Add1_postal_code))
                                        SET @lc_sa_postal_plus4code            = NULL
                                        SET @lc_sa_addressee                   = NULL
                                        SET @lc_sa_phone                       = LTRIM(RTRIM(@lc_Add1_phone))
                                        SET @lc_sa_rfq_bidding                 = 'N'
                                        SET @lc_sa_ordering                    = 'Y'
                                        SET @lc_sa_pay                         = 'Y'
                                        SET @lc_sa_pay_each_doc_alone          = 'N'
                                        
                                        -- Set values for Supplier Site Interface
                                        PRINT '3';
                                        SET @lc_ss_import_action               = 'CREATE'
                                        SET @lc_ss_process_flag                = 'N'
                                        SET @lc_ss_supplier_name               = LTRIM(RTRIM(@lc_Supplier_Name))
                                        SET @lc_ss_procurement_bu              = LTRIM(RTRIM(@lc_attribute14))
                                        SET @lc_ss_address_name                = UPPER(LTRIM(RTRIM(@lc_Add1_city)))
                                        SET @lc_ss_supplier_site_name          = UPPER(LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(@lc_Add1_city)),1,15))))
                                        SET @lc_ss_sourcing_only               = 'N'
                                        SET @lc_ss_pay                         = 'Y'
                                        SET @lc_ss_primary_pay                 = 'Y'
                                        ---  SET @lc_ss_income_tax_rep_site         = 'N'   --Commented on 14-MAY-2019
                                        SET @lc_ss_communication_method        = (CASE WHEN (LTRIM(RTRIM(@lc_email1)) IS NOT NULL AND LTRIM(RTRIM(@lc_email1)) <>'' AND LTRIM(RTRIM(@lc_email1)) <>'N/A') THEN 'EMAIL' ELSE NULL END)
                                        SET @lc_ss_email                       = (CASE WHEN (LTRIM(RTRIM(@lc_email1)) IS NOT NULL AND LTRIM(RTRIM(@lc_email1)) <>'' AND LTRIM(RTRIM(@lc_email1)) <>'N/A') THEN (CASE WHEN @ln_override_mail=1 THEN @lc_override_mail ELSE @lc_email1 END) ELSE NULL END)
										SET @lc_ss_inv_currency						= (CASE 
																						WHEN ISNULL(@lc_inv_curr,'')='' THEN 
																							(	SELECT attribute1
																								FROM hiscox_lookup_ref_tbl
																								WHERE lookup_type='HISCOX_POZ_BU_INVOICE_OPTIONS'
																								AND lookup_code = @lc_ss_procurement_bu
																							)
																						ELSE
																							@lc_inv_curr
																						END
																						)
										SET @lc_ss_payment_currency				 = (CASE 
																						WHEN ISNULL(@lc_payment_currency,'')='' THEN 
																							(	SELECT attribute2
																								FROM hiscox_lookup_ref_tbl
																								WHERE lookup_type='HISCOX_POZ_BU_INVOICE_OPTIONS'
																								AND lookup_code = @lc_ss_procurement_bu
																							)
																						ELSE
																							@lc_payment_currency
																						END
																					)
										SET @lc_ss_payment_terms				= @lc_payment_terms
										SET @lc_ss_terms_date_basis				 = (SELECT attribute4 FROM hiscox_lookup_ref_tbl
																					WHERE lookup_type='HISCOX_POZ_BU_INVOICE_OPTIONS'
																					AND lookup_code = @lc_ss_procurement_bu
																					)
                                        SET @lc_ss_always_tak_discount         = 'Y'
                                        SET @lc_ss_vat_code                    = NULL
                                        SET @lc_ss_tax_reg_num                 = NULL
                                        SET @lc_ss_payment_method              = LTRIM(RTRIM(@lc_Payment_Method))
                                        SET @lc_ss_delivery_channel            = NULL
                                        SET @lc_ss_bank_instruction1           = NULL
                                        SET @lc_ss_bank_instruction2           = NULL
                                        SET @lc_ss_bank_instruction            = NULL
                                        SET @lc_ss_settlement_priority         = @lc_sup_settlement_priority--Modify 1.5 hiscox_get_lookup_meaning_f @lc_lookup_type, 'SETTLEMENT_PRIORITY'
                                        EXEC @lc_ss_bank_charge_bearer          = hiscox_get_lookup_meaning_f @lc_lookup_type, 'BANK_CHARGE_BEARER'
                                        SET @lc_ss_payment_reason              = NULL
                                        SET @lc_ss_delivery_method             = (CASE WHEN (LTRIM(RTRIM(@lc_email1)) IS NOT NULL AND LTRIM(RTRIM(@lc_email1)) <>'' AND LTRIM(RTRIM(@lc_email1)) <>'N/A') THEN 'EMAIL' ELSE NULL END)
                                        SET @lc_ss_remit_email                 = (CASE WHEN (LTRIM(RTRIM(@lc_email1)) IS NOT NULL AND LTRIM(RTRIM(@lc_email1)) <>'' AND LTRIM(RTRIM(@lc_email1)) <>'N/A') THEN (CASE WHEN @ln_override_mail=1 THEN @lc_override_mail ELSE @lc_email1 END) ELSE NULL END)
                                        SET @lc_ss_pay_each_doc_alone          = NULL
										
										--Added below on 14-MAY-2019
										IF @lc_sup_tax_pay_country 	= 'US' AND LTRIM(RTRIM(@lc_attribute14)) ='US BU'
											BEGIN
												SET @lc_ss_income_tax_rep_site			= 'Y'
											END
										--Added below on 21-MAY-2019
										ELSE IF @lc_sup_tax_pay_country 	= 'FR' AND LTRIM(RTRIM(@lc_attribute14)) ='FR BU'
											BEGIN
												SET @lc_ss_income_tax_rep_site			= 'Y'
											END
										ELSE
											BEGIN
												SET @lc_ss_income_tax_rep_site			= NULL
											END										
										
										--Added below on 14-MAY-2019
										IF LTRIM(RTRIM(@lc_attribute14)) ='ES BU'
											BEGIN
												SET @lc_ss_global_attribute_category 		= 'JExESOnlineVatReporting'
												SET @lc_ss_global_attribute4				= 'ORA_01'											
											END
										ELSE
											BEGIN
												SET @lc_ss_global_attribute_category 		= NULL
												SET @lc_ss_global_attribute4				= NULL
											END
                                        
                                        -- Set Values for Supplier Site Assignments FBDI
                                        PRINT '4';
                                        SET @lc_ssa_import_action              = 'CREATE'
                                        SET @lc_ssa_process_flag               = 'N'
                                        SET @lc_ssa_supplier_name              = LTRIM(RTRIM(@lc_supplier_name))
                                        SET @lc_ssa_supplier_site              = UPPER(LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(@lc_Add1_city)),1,15))))
                                        SET @lc_ssa_procurement_bu             = LTRIM(RTRIM(@lc_attribute14))
                                        SET @lc_ssa_client_bu                  = LTRIM(RTRIM(@lc_attribute14))
                                        SET @lc_ssa_billto_bu                  = LTRIM(RTRIM(@lc_attribute14))					
                                        SET @lc_ssa_ship_to_loc                = NULL
                                        SET @lc_ssa_bill_to_loc                = NULL
                                        SET @lc_ssa_withhold_tax               = NULL
                                        SET @lc_ssa_withhold_tax_grp           = NULL
										--EXEC @lc_ssa_lia_dist                  = hiscox_get_lookup_meaning_f @lc_lookup_type, 'LIABILITY_DIST'
										EXEC @lc_ssa_lia_dist                  = hiscox_get_lookup_meaning_f @lc_lookup_type, @lc_ssa_procurement_bu
                                        SET @lc_ssa_prepay_dist                = NULL
                                        SET @lc_ssa_bills_pay_dist             = NULL
                                        SET @lc_ssa_distribution_set           = NULL
                                        SET @lc_ssa_inv_date                   = NULL
										
										--Added below on 14-MAY-2019
										IF LTRIM(RTRIM(@lc_attribute14))  = 'ES BU'
											BEGIN
												SET @lc_ssa_withhold_tax				= 'Y'
												--SET @lc_ssa_withhold_tax_grp			= 'ES INITIAL WHT'  -- Commented on 11-JUL-2019
											END
										/*ELSE
											BEGIN
												SET @lc_ssa_withhold_tax				= NULL
												SET @lc_ssa_withhold_tax_grp			= NULL
											END*/ -- Commented and added below(ELSE) on 01-JUL-2019
                                        
										ELSE IF LTRIM(RTRIM(@lc_attribute14))  = 'US BU'
											BEGIN
												SET @lc_ssa_withhold_tax				= 'Y'
												--SET @lc_ssa_withhold_tax_grp			= 'US BACKUP WHT'  -- Commented on 11-JUL-2019
											END
										ELSE IF LTRIM(RTRIM(@lc_attribute14))  = 'PT BU'-- Added PT BU on 11-JUL-2019
											BEGIN
												SET @lc_ssa_withhold_tax				= 'Y'
											END
										ELSE
											BEGIN
												SET @lc_ssa_withhold_tax				= NULL
												SET @lc_ssa_withhold_tax_grp			= NULL
											END
                                        -- Set Values for Supplier Contacts INTERFACE
                                        PRINT '5';
                                        SET @lc_sc_import_action           = 'CREATE'
                                        SET @lc_sc_process_flag            = 'N'
                                        SET @lc_sc_supplier_name           = LTRIM(RTRIM(@lc_supplier_name))
                                        SET @lc_sc_prefix                  = NULL
                                        SET @lc_sc_first_name              = LTRIM(RTRIM(@lc_supplier_name))        --@first_name
                                        SET @lc_sc_middle_name             = NULL
                                        SET @lc_sc_last_name               = LTRIM(RTRIM(@lc_supplier_name))        --@last_name
                                        SET @lc_sc_job_title               = NULL
                                        SET @lc_sc_admin_contact           = NULL
                                        SET @lc_sc_email                   = NULL
                                        SET @lc_sc_phone_country_code      = NULL
                                        SET @lc_sc_phone_area_code         = NULL
                                        SET @lc_sc_phone                   = NULL
                                        SET @lc_sc_phone_extension         = NULL
                                        SET @lc_sc_fax_country_code        = NULL
                                        SET @lc_sc_fax_area_code           = NULL
                                        SET @lc_sc_fax                     = NULL
                                        SET @lc_sc_mobile_country_code     = NULL
                                        SET @lc_sc_mobile_area_code        = NULL
                                        SET @lc_sc_mobile                  = NULL
                                        PRINT '6';
                                        
										
										-- Set Values for Supplier Contact Address INTERFACE
                                        SET @lc_sca_import_action          = 'CREATE'
                                        SET @lc_sca_process_flag           = 'N'
                                        SET @lc_sca_supplier_name          = UPPER(LTRIM(RTRIM(@lc_supplier_name)))
                                        SET @lc_sca_address_name           = UPPER(LTRIM(RTRIM(@lc_Add1_city)))
                                        SET @lc_sca_first_name             = LTRIM(RTRIM(@lc_supplier_name))
                                        SET @lc_sca_last_name              = LTRIM(RTRIM(@lc_supplier_name))
                                        SET @lc_sca_email                  = NULL 
                                        PRINT '7';
                                        
									
										
                                    
								
								
                            
                                -- Insert into Supplier Interface Table 
                                BEGIN TRY
                                PRINT '8';
										  
                                    INSERT INTO hiscox_poz_suppliers_int_stg
                                    (batch_id
									,load_batch_id
                                    ,source_record_id
                                    ,process_flag
                                    ,import_action
                                    ,supplier_name
                                    ,supplier_number
                                    ,tax_organization_type
                                    ,supplier_type
                                    ,business_relationship
                                    ,alias
                                    ,taxpayer_country
                                    ,taxpayer_id
                                    ,federal_reportable
                                    ,federal_income_tax_type
                                    ,payment_method
                                    ,settlement_priority
                                    ,registry_id
                                    ,payee_service_level 
                                    ,pay_each_document_alone
									,use_withholding_tax
									,file_geo
									,filename
                                    )
                                    VALUES
                                    (@lc_Batch_Id
									,@lc_Batch_Id
                                    ,@ln_Record_Id
                                    ,@lc_sup_process_flag
                                    ,@lc_sup_import_action
                                    ,@lc_sup_supplier_name
                                    ,@lc_sup_supplier_number
                                    ,@lc_sup_tax_organization_type
                                    ,@lc_sup_supplier_type
                                    ,@lc_sup_business_relationship
                                    ,@lc_sup_alias
                                    ,@lc_sup_tax_pay_country
                                    ,@lc_sup_tax_pay_id
                                    ,@lc_sup_fed_report
                                    ,@lc_sup_fed_inc_tax
                                    ,@lc_sup_payment_method
                                    ,@lc_sup_settlement_priority
                                    ,@lc_sup_reg_id
                                    ,@lc_sup_pay_service_lvl
                                    ,@lc_sup_pay_each_doc
									,@lc_sup_use_withhold_tax
									,@p_file_geo
									,@lc_Filename
                                    )
                                    
                                END TRY
                                
                                BEGIN CATCH
                                    SET @lc_error_msg      = CONCAT('Error cccured  while inserting data into Suppliers Interface- ',Error_Message())
                                END CATCH
                                PRINT '9';
                            -- Insert into Supplier Address Interface Table 
                                BEGIN TRY
                                    INSERT INTO hiscox_poz_supplier_addresses_int_stg
                                    (batch_id
									,load_batch_id
                                    ,source_record_id
                                    ,process_flag
                                    ,import_action
                                    ,address_name
                                    ,supplier_name
                                    ,address_line_1
                                    ,address_line_2
                                    ,address_line_3
                                    ,address_line_4
                                    ,country
                                    ,city
                                    ,state
                                    ,province
                                    ,county
                                    ,postal_code
                                    ,postal_plus_4_code
                                    ,addressee
                                    ,phone
                                    ,rfq_or_bidding
                                    ,ordering
                                    ,pay
                                    ,Pay_Each_Document_Alone
									,file_geo
									,filename
                                    )
                                    VALUES
                                    (@lc_Batch_Id
									,@lc_Batch_Id
                                    ,@ln_Record_Id
                                    ,@lc_sa_process_flag
                                    ,@lc_sa_import_action
                                    ,@lc_sa_address_name
                                    ,@lc_sa_supplier_name
                                    ,@lc_sa_address_line1
                                    ,@lc_sa_address_line2
                                    ,@lc_sa_address_line3
                                    ,@lc_sa_address_line4
                                    ,@lc_sa_country
                                    ,@lc_sa_city
                                    ,@lc_sa_state
                                    ,@lc_sa_province
                                    ,@lc_sa_county
                                    ,@lc_sa_postal_code
                                    ,@lc_sa_postal_plus4code
                                    ,@lc_sa_addressee
                                    ,@lc_sa_phone
                                    ,@lc_sa_rfq_bidding
                                    ,@lc_sa_ordering
                                    ,@lc_sa_pay
                                    ,@lc_sa_pay_each_doc_alone
									,@p_file_geo
									,@lc_Filename
									
                                    )
                                    
                                END TRY
                                
                                BEGIN CATCH
                                    SET @lc_error_msg      = CONCAT('Error cccured  while inserting data into Suppliers Addresss Interface- ',Error_Message())
                                END CATCH
                                PRINT '10';
                            -- Insert into Supplier Sites Interface Table   
                                BEGIN TRY
                                    INSERT INTO hiscox_poz_supplier_sites_int_stg
                                    (batch_id
									,load_batch_id
                                    ,source_record_id
                                    ,import_action
                                    ,process_flag
                                    ,supplier_name
                                    ,procurement_bu
                                    ,address_name
                                    ,supplier_site
                                    ,sourcing_only
                                    ,pay
                                    ,primary_pay
                                    ,income_tax_reporting_site
                                    ,communication_method
                                    ,e_mail
                                    ,Invoice_Currency
                                    ,Payment_Currency
                                    ,Payment_Terms
                                    ,Terms_Date_Basis
                                    ,Always_Take_Discount
                                    ,Vat_Code
                                    ,Tax_Registration_Number
                                    ,Payment_Method
                                    ,Delivery_Channel
                                    ,Bank_Instruction_1
                                    ,Bank_Instruction_2
                                    ,Bank_Instruction
                                    ,Settlement_Priority
                                    ,Bank_Charge_Bearer
                                    ,Payment_Reason
                                    ,Delivery_Method
                                    ,Remittance_E_Mail
                                    ,Pay_Each_Document_Alone
									,file_geo
									,filename
									,global_attribute_category
									,global_attribute4
                                    )
                                    VALUES
                                    (@lc_Batch_Id
									,@lc_Batch_Id
                                    ,@ln_Record_Id
                                    ,@lc_ss_import_action
                                    ,@lc_ss_process_flag
                                    ,@lc_ss_supplier_name
                                    ,@lc_ss_procurement_bu
                                    ,@lc_ss_address_name
                                    ,@lc_ss_supplier_site_name
                                    ,@lc_ss_sourcing_only
                                    ,@lc_ss_pay
                                    ,@lc_ss_primary_pay
                                    ,@lc_ss_income_tax_rep_site
                                    ,@lc_ss_communication_method
                                    ,@lc_ss_email
                                    ,@lc_ss_inv_currency
                                    ,@lc_ss_payment_currency
                                    ,@lc_ss_payment_terms
                                    ,@lc_ss_terms_date_basis
                                    ,@lc_ss_always_tak_discount
                                    ,@lc_ss_vat_code
                                    ,@lc_ss_tax_reg_num
                                    ,@lc_ss_payment_method
                                    ,@lc_ss_delivery_channel
                                    ,@lc_ss_bank_instruction1
                                    ,@lc_ss_bank_instruction2
                                    ,@lc_ss_bank_instruction
                                    ,@lc_ss_settlement_priority
                                    ,@lc_ss_bank_charge_bearer
                                    ,@lc_ss_payment_reason
                                    ,@lc_ss_delivery_method
                                    ,@lc_ss_remit_email
                                    ,@lc_ss_pay_each_doc_alone
									,@p_file_geo
									,@lc_Filename
									,@lc_ss_global_attribute_category
									,@lc_ss_global_attribute4									
                                    )
                                    
                                END TRY
                                
                                BEGIN CATCH
                                    SET @lc_error_msg      = CONCAT('Error cccured  while inserting data into Suppliers Sites Interface- ',Error_Message())
                                END CATCH
                                PRINT '11';
                                -- Insert into Supplier Site Assignments Interface Table    
                                BEGIN TRY
                                    INSERT INTO hiscox_poz_site_assignments_int_stg
                                    (batch_id
									,load_batch_id
                                    ,source_record_id
                                    ,import_action
                                    ,supplier_name
                                    ,supplier_site
                                    ,procurement_bu
                                    ,client_bu
                                    ,bill_to_bu
                                    ,ship_to_location
                                    ,bill_to_location
                                    ,use_withholding_tax
                                    ,withholding_tax_group
                                    ,liability_distribution
                                    ,prepayment_distribution
                                    ,bills_payable_distribution
                                    ,distribution_set
                                    ,inactive_date
									,file_geo
									,filename
                                    )
                                    VALUES
                                    (
                                    @lc_Batch_Id
									,@lc_Batch_Id
                                    ,@ln_Record_Id
                                    ,@lc_ssa_import_action
                                    ,@lc_ssa_supplier_name
                                    ,@lc_ssa_supplier_site
                                    ,@lc_ssa_procurement_bu
                                    ,@lc_ssa_client_bu
                                    ,@lc_ssa_billto_bu
                                    ,@lc_ssa_ship_to_loc
                                    ,@lc_ssa_bill_to_loc
                                    ,@lc_ssa_withhold_tax
                                    ,@lc_ssa_withhold_tax_grp
                                    ,@lc_ssa_lia_dist
                                    ,@lc_ssa_prepay_dist
                                    ,@lc_ssa_bills_pay_dist
                                    ,@lc_ssa_distribution_set
                                    ,@lc_ssa_inv_date
									,@p_file_geo
									,@lc_Filename
                                    )
                                    
                                END TRY
                                
                                BEGIN CATCH
                                    SET @lc_error_msg      = CONCAT('Error cccured  while inserting data into Suppliers Sites Assignments Interface- ',Error_Message())
                                END CATCH
                                
								/*
                                -- Insert into Supplier Contacts Interface Table    
                                BEGIN TRY
                                    INSERT INTO hiscox_poz_sup_contacts_int_stg
                                    (batch_id
									,load_batch_id
                                    ,source_record_id
                                    ,import_action
                                    ,process_flag
                                    ,supplier_name
                                    ,prefix
                                    ,first_name
                                    ,middle_name
                                    ,last_name
                                    ,job_title
                                    ,administrative_contact
                                    ,e_mail
                                    ,phone_country_code
                                    ,phone_area_code
                                    ,phone
                                    ,phone_extension
                                    ,fax_country_code
                                    ,fax_area_code
                                    ,fax
                                    ,mobile_country_code
                                    ,mobile_area_code
                                    ,mobile
									,file_geo
									,filename
                                    )
                                    VALUES
                                    (
                                    @lc_Batch_Id
									,@lc_Batch_Id
                                    ,@ln_Record_Id
                                    ,@lc_sc_import_action
                                    ,@lc_sc_process_flag
                                    ,@lc_sc_supplier_name
                                    ,@lc_sc_prefix
                                    ,@lc_sc_first_name
                                    ,@lc_sc_middle_name
                                    ,@lc_sc_last_name
                                    ,@lc_sc_job_title
                                    ,@lc_sc_admin_contact
                                    ,@lc_sc_email
                                    ,@lc_sc_phone_country_code
                                    ,@lc_sc_phone_area_code
                                    ,@lc_sc_phone
                                    ,@lc_sc_phone_extension
                                    ,@lc_sc_fax_country_code
                                    ,@lc_sc_fax_area_code
                                    ,@lc_sc_fax
                                    ,@lc_sc_mobile_country_code
                                    ,@lc_sc_mobile_area_code
                                    ,@lc_sc_mobile
									,@p_file_geo
									,@lc_Filename
                                    )
                                    
                                END TRY
                                
                                BEGIN CATCH
                                    SET @lc_error_msg      = CONCAT('Error cccured  while inserting data into Supplier Contacts Interface- ',Error_Message())
                                END CATCH
                                
                                -- Insert into Supplier Contact Addresses Interface Table   
                                BEGIN TRY
                                    INSERT INTO hiscox_poz_supp_contact_addresses_int_Stg
                                    (batch_id
									,load_batch_id
                                    ,source_record_id
                                    ,import_action
                                    ,process_flag
                                    ,supplier_name
                                    ,address_name
                                    ,first_name
                                    ,last_name
                                    ,e_mail
									,file_geo
									,filename
                                    )
                                    VALUES
                                    (
                                    @lc_Batch_Id
									,@lc_Batch_Id
                                    ,@ln_Record_Id
                                    ,@lc_sca_import_action
                                    ,@lc_sca_process_flag
                                    ,@lc_sca_supplier_name
                                    ,@lc_sca_address_name
                                    ,@lc_sca_first_name
                                    ,@lc_sca_last_name
                                    ,@lc_sca_email
									,@p_file_geo
									,@lc_Filename
                                    )
                                    
                                END TRY
                                
                                BEGIN CATCH
                                    SET @lc_error_msg      = CONCAT('Error cccured  while inserting data into Supplier Contacts Address Interface- ',Error_Message())
                                END CATCH
								*/
                                
                                IF @lc_error_msg = '' 
                                    BEGIN
                                        UPDATE hiscox_poz_suppliers_source
                                        SET process_flag = 'S'
                                        WHERE record_id = @ln_Record_Id
                                    END
                                ELSE
                                    BEGIN
                                        
										THROW 60000,@lc_Error_Msg,5
                                    END
                            
							
					END TRY
					BEGIN CATCH
						SET @lc_error_msg = Error_Message()
						UPDATE hiscox_poz_suppliers_source
                        SET process_flag = 'E'
                        ,error_message=@lc_error_msg
                        WHERE record_id = @ln_Record_Id
					END CATCH
					FETCH NEXT FROM cur_suppliers_source INTO 
                        @ln_Record_Id , @lc_batch_Id , @lc_Filename , @lc_File_geo , @lc_Process_flag , @lc_Error_Message , @lc_Supplier_Name , @lc_Supplier_Code , @lc_Supplier_desc1 , @lc_Supplier_desc2 , @lc_Supplie_Type , @lc_Business_Relationship , @lc_Payment_Method , @lc_Add1_line1 , @lc_Add1_line2 , @lc_Add1_line3 , @lc_Add1_line4 , @lc_Add1_line5 , @lc_Add1_city , @lc_Add1_state , @lc_Add1_province , @lc_Add1_country , @lc_Add1_postal_code , @lc_Add1_postal_plus4code , @lc_Add1_phone , @lc_Add1_phoneext , @lc_Add1_phonearea_cd , @lc_Add1_fax , @lc_Add1_faxext , @lc_Add1_faxarea_cd , @lc_Add2_line1 , @lc_Add2_line2 , @lc_Add2_line3 , @lc_Add2_line4 , @lc_Add2_line5 , @lc_Add2_city , @lc_Add2_state , @lc_Add2_province , @lc_Add2_country , @lc_Add2_postal_code , @lc_Add2_postal_plus4code , @lc_Add2_phone , @lc_Add2_phoneext , @lc_Add2_phonearea_cd , @lc_Add2_fax , @lc_Add2_faxext , @lc_Add2_faxarea_cd , @lc_email1 , @lc_email2 , @lc_email3 , @lc_prc_bu , @lc_communication_method , @lc_inv_curr , @lc_payment_currency , @lc_payment_terms , @lc_term_date_basis , @lc_always_take_discount , @lc_vat_code , @lc_tax_reg_number , @lc_delivery_channel , @lc_bank_instruction1 , @lc_bank_instruction2 , @lc_bank_instruction , @lc_settlement_priority , @lc_bank_charge_bearer , @lc_payment_reason , @lc_delivery_method , @lc_remittance_email , @lc_pay_each_doc_alone , @lc_client_bu , @lc_bill_bu , @lc_ship_to_loc , @lc_bill_to_loc , @lc_liability_distribution , @lc_bills_payable_distribution , @lc_name_prefix , @lc_first_name , @lc_middle_name , @lc_last_name , @lc_job_title , @lc_Admin_contact , @lc_email_contact , @lc_phone_country_code , @lc_phone_area_code , @lc_phone_extension , @lc_phone , @lc_mobile_country_code , @lc_mobile_area_code , @lc_mobile , @lc_attribute1 , @lc_attribute2 , @lc_attribute3 , @lc_attribute4 , @lc_attribute5 , @lc_attribute6 , @lc_attribute7 , @lc_attribute8 , @lc_attribute9 , @lc_attribute10 , @lc_attribute11 , @lc_attribute12 , @lc_attribute13 , @lc_attribute14 , @lc_attribute15 , @lc_attribute16 , @lc_attribute17 , @lc_attribute18 , @lc_attribute19 , @lc_attribute20 , @lc_attribute21 , @lc_attribute22 , @lc_attribute23 , @lc_attribute24 , @lc_attribute25 , @lc_attribute26 , @lc_attribute27 , @lc_attribute28 , @lc_attribute29 , @lc_attribute30 , @lc_attribute31 , @lc_attribute32 , @lc_attribute33 , @lc_attribute34 , @lc_attribute35 , @lc_attribute36 , @lc_attribute37 , @lc_attribute38 , @lc_attribute39 , @lc_attribute40 , @lc_attribute41 , @lc_attribute42 , @lc_attribute43 , @lc_attribute44 , @lc_attribute45 , @lc_attribute46 , @lc_attribute47 , @lc_attribute48 , @lc_attribute49 , @lc_attribute50 , @lc_attribute51 , @lc_attribute52 , @lc_attribute53 , @lc_attribute54 , @lc_attribute55 , @lc_attribute56 , @lc_attribute57 , @lc_attribute58 , @lc_attribute59 , @lc_attribute60 , @lc_attribute61 , @lc_attribute62 , @lc_attribute63 , @lc_attribute64 , @lc_attribute65 , @lc_attribute66 , @lc_attribute67 , @lc_attribute68 , @lc_attribute69 , @lc_attribute70 , @lc_attribute71 , @lc_attribute72 , @lc_attribute73 , @lc_attribute74 , @lc_attribute75 , @lc_attribute76 , @lc_attribute77 , @lc_attribute78 , @lc_attribute79 , @lc_attribute80 , @lc_attribute81 , @lc_attribute82 , @lc_attribute83 , @lc_attribute84 , @lc_attribute85 , @lc_attribute86 , @lc_attribute87 , @lc_attribute88 , @lc_attribute89 , @lc_attribute90 , @lc_attribute91 , @lc_attribute92 , @lc_attribute93 , @lc_attribute94 , @lc_attribute95 , @lc_attribute96 , @lc_attribute97 , @lc_attribute98 , @lc_attribute99 , @lc_attribute100 , @lc_attribute101 , @lc_attribute102 , @lc_attribute103 , @lc_attribute104 , @lc_attribute105 , @lc_attribute106 , @lc_attribute107 , @lc_attribute108 , @lc_attribute109 , @lc_attribute110 , @lc_attribute111 , @lc_attribute112 , @lc_attribute113 , @lc_attribute114 , @lc_attribute115 , @lc_attribute116 , @lc_attribute117 , @lc_attribute118 , @lc_attribute119 , @lc_attribute120 , @lc_attribute121 , @lc_attribute122 , @lc_attribute123 , @lc_attribute124 , @lc_attribute125 , @lc_attribute126 , @lc_attribute127 , @lc_attribute128 , @lc_attribute129 , @lc_attribute130 , @lc_attribute131 , @lc_attribute132 , @lc_attribute133 , @lc_attribute134 , @lc_attribute135 , @lc_attribute136 , @lc_attribute137 , @lc_attribute138 , @lc_attribute139 , @lc_attribute140 , @lc_attribute141 , @lc_attribute142 , @lc_attribute143 , @lc_attribute144 , @lc_attribute145 , @lc_attribute146 , @lc_attribute147 , @lc_attribute148 , @lc_attribute149 , @lc_attribute150
                    END  -- END WHILE

                    CLOSE cur_suppliers_source
                    DEALLOCATE cur_suppliers_source
					 
					 
					SET @ln_count_rec =(   SELECT count(*)
											FROM hiscox_poz_suppliers_source
											WHERE batch_id      =@p_batch
											AND process_flag    ='S'
										)
                    
					IF @ln_count_rec< 1
						BEGIN
							SET @x_error_code       = 2
							SET @x_error_msg        = 'No validated Records to process further'
							 EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'EXCEPTION'
													,@p_err_column     = 'ALL'  
													,@p_err_value      = 'EXCEPTION'  
													,@p_err_desc       = @x_error_msg
													,@p_rect_action    = NULL; 
						END
					ELSE
						BEGIN
							SET @x_error_code       = 0
							SET @x_error_msg        = ''
							SET @lc_error_msg = 'UP_TRANS_P Procedure End Success Reords -'+CAST(@ln_count_rec AS VARCHAR) 
							EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'DEBUG'
													,@p_err_column     = 'DEBUG'  
													,@p_err_value      = 'DEBUG'  
													,@p_err_desc       = @lc_error_msg
													,@p_rect_action    = NULL; 
						END
				END
					ELSE
                        BEGIN
                             SET @x_error_code       = 2
							 SET @x_error_msg        = 'Invalid File Geo'
							 EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'EXCEPTION'
													,@p_err_column     = 'ALL'  
													,@p_err_value      = 'EXCEPTION'  
													,@p_err_desc       = @x_error_msg
													,@p_rect_action    = NULL; 
							END
               END
                
   
        
    END
    END TRY
    
    BEGIN CATCH
        Print 'Error Occured in Supplier Validation Procedure:'  
        Print  Error_Message() 
        SET @x_error_code       = 2
        SET @x_error_msg        = CONCAT('Error Occured - ',Error_Message())
		 EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
									,@p_batch_id       = @p_batch
									,@p_process_id     = 0          
									,@p_source         = @lc_source
									,@p_pri_record_id  = @p_batch
									,@p_sec_record_id  = @p_file_geo
									,@p_ter_record_id  = NULL
									,@p_err_code       = 'EXCEPTION'
									,@p_err_column     = 'ALL'  
									,@p_err_value      = 'EXCEPTION'  
									,@p_err_desc       = @x_error_msg
									,@p_rect_action    = NULL; 
    END CATCH
    
END
GO
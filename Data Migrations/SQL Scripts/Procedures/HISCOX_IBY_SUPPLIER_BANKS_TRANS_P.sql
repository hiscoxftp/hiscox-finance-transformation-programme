CREATE OR ALTER PROCEDURE dbo.HISCOX_IBY_SUPPLIER_BANKS_TRANS_P
                                @p_batch INT
                                ,@p_file_geo VARCHAR(100)
                                ,@x_error_code INT OUTPUT
                                ,@x_error_msg VARCHAR(4000) OUTPUT
AS

/*****************************************************************
OBJECT NAME: HISCOX_POZ_SUPPLIER_BANKS_TRANS_P
DESCRIPTION: Procedure to transform the source data from Suppliers Banks staging table to the target FBDI table further validation
Version     Name                Date                Description
----------------------------------------------------------------------------
1.0         Srinivasan          07-MAR-2019         Initial version
1.1			Srinivasan			09-APR-2019			Changes for Guernsey and Bermuda
1.2			Srinivasan			08-MAY-2019			Changes for UK file formatted like the other file
*****************************************************************/

BEGIN
    BEGIN TRY
-- Load FBDI Table with source table data and perform Transformations
        
		-- Declare Source Supplier Bank Source Table Variables
        
        DECLARE @ln_Record_ID int,@lc_Process_Flag nvarchar(10),@lc_Error_Message nvarchar(4000),@ln_Batch_ID int,@ln_Load_Batch_ID int,@lc_code nvarchar(4000),@lc_supplier_code nvarchar(4000),@lc_name nvarchar(4000),@lc_business_unit nvarchar(4000),@lc_address_name nvarchar(4000),@lc_site_name nvarchar(4000),@lc_payment_method nvarchar(4000),@lc_delivery_channel_code nvarchar(4000),@lc_settlement_priority nvarchar(4000),@lc_remit_delivery_method nvarchar(4000),@lc_remit_advice_email nvarchar(4000),@lc_remit_advice_fax nvarchar(4000),@lc_bank_instructions_1 nvarchar(4000),@lc_bank_instructions_2 nvarchar(4000),@lc_bank_instruction_details nvarchar(4000),@lc_bank_name nvarchar(4000),@lc_bank_branch nvarchar(4000), @lc_bank_sorting_code nvarchar(4000),@lc_bank_account_name nvarchar(4000),@lc_iban_bank_account_number nvarchar(4000),@lc_iban nvarchar(4000),@lc_bank_account_number nvarchar(4000),@lc_country_code nvarchar(4000),@lc_account_currency_code nvarchar(4000),@lc_allow_international_payments nvarchar(4000),@lc_account_start_date nvarchar(4000),@lc_account_end_date nvarchar(4000),@lc_account_alternate_name nvarchar(4000),@lc_account_type_code nvarchar(4000),@lc_primary_flag nvarchar(4000),@lc_account_assignment_start_date nvarchar(4000),@lc_account_assignment_end_date nvarchar(4000),@lc_comment nvarchar(4000),@lc_status nvarchar(4000),@lc_migrate nvarchar(4000),@lc_attribute1 nvarchar(4000),@lc_attribute2 nvarchar(4000),@lc_attribute3 nvarchar(4000),@lc_attribute4 nvarchar(4000),@lc_attribute5 nvarchar(4000),@lc_attribute6 nvarchar(4000),@lc_attribute7 nvarchar(4000),@lc_attribute8 nvarchar(4000),@lc_attribute9 nvarchar(4000),@lc_attribute10 nvarchar(4000),@lc_attribute11 nvarchar(4000),@lc_attribute12 nvarchar(4000),@lc_attribute13 nvarchar(4000),@lc_attribute14 nvarchar(4000),@lc_attribute15 nvarchar(4000),@lc_attribute16 nvarchar(4000),@lc_attribute17 nvarchar(4000),@lc_attribute18 nvarchar(4000),@lc_attribute19 nvarchar(4000),@lc_attribute20 nvarchar(4000),@lc_attribute21 nvarchar(4000),@lc_attribute22 nvarchar(4000),@lc_attribute23 nvarchar(4000),@lc_attribute24 nvarchar(4000),@lc_attribute25 nvarchar(4000),@lc_attribute26 nvarchar(4000),@lc_attribute27 nvarchar(4000),@lc_attribute28 nvarchar(4000),@lc_attribute29 nvarchar(4000),@lc_attribute30 nvarchar(4000),@lc_attribute31 nvarchar(4000),@lc_attribute32 nvarchar(4000),@lc_attribute33 nvarchar(4000),@lc_attribute34 nvarchar(4000),@lc_attribute35 nvarchar(4000),@lc_attribute36 nvarchar(4000),@lc_attribute37 nvarchar(4000),@lc_attribute38 nvarchar(4000),@lc_attribute39 nvarchar(4000),@lc_attribute40 nvarchar(4000),@lc_attribute41 nvarchar(4000),@lc_attribute42 nvarchar(4000),@lc_attribute43 nvarchar(4000),@lc_attribute44 nvarchar(4000),@lc_attribute45 nvarchar(4000),@lc_attribute46 nvarchar(4000),@lc_attribute47 nvarchar(4000),@lc_attribute48 nvarchar(4000),@lc_attribute49 nvarchar(4000),@lc_attribute50 nvarchar(4000),@lc_attribute51 nvarchar(4000),@lc_attribute52 nvarchar(4000),@lc_attribute53 nvarchar(4000),@lc_attribute54 nvarchar(4000),@lc_attribute55 nvarchar(4000),@lc_attribute56 nvarchar(4000),@lc_attribute57 nvarchar(4000),@lc_attribute58 nvarchar(4000),@lc_attribute59 nvarchar(4000),@lc_attribute60 nvarchar(4000),@lc_attribute61 nvarchar(4000),@lc_attribute62 nvarchar(4000),@lc_attribute63 nvarchar(4000),@lc_attribute64 nvarchar(4000),@lc_attribute65 nvarchar(4000),@lc_attribute66 nvarchar(4000),@lc_attribute67 nvarchar(4000),@lc_attribute68 nvarchar(4000),@lc_attribute69 nvarchar(4000),@lc_attribute70 nvarchar(4000),@lc_attribute71 nvarchar(4000),@lc_attribute72 nvarchar(4000),@lc_attribute73 nvarchar(4000),@lc_attribute74 nvarchar(4000),@lc_attribute75 nvarchar(4000),@lc_attribute76 nvarchar(4000),@lc_attribute77 nvarchar(4000),@lc_attribute78 nvarchar(4000),@lc_attribute79 nvarchar(4000),@lc_attribute80 nvarchar(4000),@lc_attribute81 nvarchar(4000),@lc_attribute82 nvarchar(4000),@lc_attribute83 nvarchar(4000),@lc_attribute84 nvarchar(4000),@lc_attribute85 nvarchar(4000),@lc_attribute86 nvarchar(4000),@lc_attribute87 nvarchar(4000),@lc_attribute88 nvarchar(4000),@lc_attribute89 nvarchar(4000),@lc_attribute90 nvarchar(4000),@lc_attribute91 nvarchar(4000),@lc_attribute92 nvarchar(4000),@lc_attribute93 nvarchar(4000),@lc_attribute94 nvarchar(4000),@lc_attribute95 nvarchar(4000),@lc_attribute96 nvarchar(4000),@lc_attribute97 nvarchar(4000),@lc_attribute98 nvarchar(4000),@lc_attribute99 nvarchar(4000),@lc_attribute100 nvarchar(4000),@lc_file_geo nvarchar(1000),@lc_Filename nvarchar(4000),@ld_Creation_Date datetime,@ld_Last_Update_Date datetime,@lc_Created_By nvarchar(4000),@lc_Updated_By nvarchar(4000)
        
        --Declare common variables
        DECLARE @lc_error_msg  NVARCHAR(4000), @ln_count_rec INT,@lc_party_number NVARCHAR(1000),@lc_party_site_number NVARCHAR(1000),@lc_party_name NVARCHAR(1000),@lc_payment_method_code NVARCHAR(1000),@lc_p_business_unit NVARCHAR(1000),@ln_check INT,@ln_bank_acct_length INT,@ln_sort_code_length INT,@lc_party_site_name NVARCHAR(1000),@lc_iban_country NVARCHAR(1000),@lc_concat_variable NVARCHAR(1000)
        
		-- Variables for the Table HISCOX_IBY_TEMP_EXT_PAYEES
        DECLARE @ln_ep_Record_ID int,@lc_ep_Process_Flag nvarchar(10),@lc_ep_Error_Message nvarchar(4000),@ln_ep_Batch_ID int,@ln_ep_Load_Batch_ID int,@ln_ep_Source_Record_ID int,@ln_ep_Import_Batch_Identifier int,@ln_ep_Payee_Identifier int,@lc_ep_Business_Unit_Name nvarchar(240),@lc_ep_Supplier_Number nvarchar(30),@lc_ep_Supplier_Site nvarchar(100),@lc_ep_Pay_Each_Document_Alone nvarchar(100),@lc_ep_Payment_Method_Code nvarchar(30),@lc_ep_Delivery_Channel_Code nvarchar(30),@lc_ep_Settlement_Priority nvarchar(30),@lc_ep_Remit_Delivery_Method nvarchar(30),@lc_ep_Remit_Advice_Email nvarchar(255),@lc_ep_Remit_Advice_Fax nvarchar(100),@lc_ep_Bank_Instructions_1 nvarchar(30),@lc_ep_Bank_Instructions_2 nvarchar(30),@lc_ep_Bank_Instruction_Details nvarchar(255),@lc_ep_Payment_Reason_Code nvarchar(30),@lc_ep_Payment_Reason_Comments nvarchar(240),@lc_ep_Payment_Message1 nvarchar(150),@lc_ep_Payment_Message2 nvarchar(150),@lc_ep_Payment_Message3 nvarchar(150),@lc_ep_Bank_Charge_Bearer_Code nvarchar(30),@lc_ep_cnv_attribute1 nvarchar(1000),@lc_ep_cnv_attribute2 nvarchar(1000),@lc_ep_cnv_attribute3 nvarchar(1000),@lc_ep_cnv_attribute4 nvarchar(1000),@lc_ep_cnv_attribute5 nvarchar(1000),@lc_ep_cnv_attribute6 nvarchar(1000),@lc_ep_cnv_attribute7 nvarchar(1000),@lc_ep_cnv_attribute8 nvarchar(1000),@lc_ep_cnv_attribute9 nvarchar(1000),@lc_ep_cnv_attribute10 nvarchar(1000),@lc_ep_cnv_attribute11 nvarchar(1000),@lc_ep_cnv_attribute12 nvarchar(1000),@lc_ep_cnv_attribute13 nvarchar(1000),@lc_ep_cnv_attribute14 nvarchar(1000),@lc_ep_cnv_attribute15 nvarchar(1000),@lc_ep_cnv_attribute16 nvarchar(1000),@lc_ep_cnv_attribute17 nvarchar(1000),@lc_ep_cnv_attribute18 nvarchar(1000),@lc_ep_cnv_attribute19 nvarchar(1000),@lc_ep_cnv_attribute20 nvarchar(1000),@lc_ep_cnv_attribute21 nvarchar(1000),@lc_ep_cnv_attribute22 nvarchar(1000),@lc_ep_cnv_attribute23 nvarchar(1000),@lc_ep_cnv_attribute24 nvarchar(1000),@lc_ep_cnv_attribute25 nvarchar(1000),@lc_ep_cnv_attribute26 nvarchar(1000),@lc_ep_cnv_attribute27 nvarchar(1000),@lc_ep_cnv_attribute28 nvarchar(1000),@lc_ep_cnv_attribute29 nvarchar(1000),@lc_ep_cnv_attribute30 nvarchar(1000),@lc_ep_cnv_attribute31 nvarchar(1000),@lc_ep_cnv_attribute32 nvarchar(1000),@lc_ep_cnv_attribute33 nvarchar(1000),@lc_ep_cnv_attribute34 nvarchar(1000),@lc_ep_cnv_attribute35 nvarchar(1000),@lc_ep_cnv_attribute36 nvarchar(1000),@lc_ep_cnv_attribute37 nvarchar(1000),@lc_ep_cnv_attribute38 nvarchar(1000),@lc_ep_cnv_attribute39 nvarchar(1000),@lc_ep_cnv_attribute40 nvarchar(1000),@lc_ep_cnv_attribute41 nvarchar(1000),@lc_ep_cnv_attribute42 nvarchar(1000),@lc_ep_cnv_attribute43 nvarchar(1000),@lc_ep_cnv_attribute44 nvarchar(1000),@lc_ep_cnv_attribute45 nvarchar(1000),@lc_ep_cnv_attribute46 nvarchar(1000),@lc_ep_cnv_attribute47 nvarchar(1000),@lc_ep_cnv_attribute48 nvarchar(1000),@lc_ep_cnv_attribute49 nvarchar(1000),@lc_ep_cnv_attribute50 nvarchar(1000),@lc_ep_file_geo nvarchar(1000),@lc_ep_Filename nvarchar(4000),@ld_ep_Creation_Date datetime,@ld_ep_Last_Update_Date datetime,@lc_ep_Created_By nvarchar(4000),@lc_ep_Updated_By nvarchar(4000)
		--- Variables for the table HISCOX_IBY_TEMP_EXT_BANK_ACCTS
		DECLARE @ln_eba_Record_ID int,@lc_eba_Process_Flag nvarchar(10),@lc_eba_Error_Message nvarchar(4000),@ln_eba_Batch_ID int,@ln_eba_Load_Batch_ID int,@ln_eba_Source_Record_ID int,@ln_eba_Import_Batch_Identifier int,@ln_eba_Payee_Identifier int,@ln_eba_Payee_Bank_Account_Identifier int,@lc_eba_Bank_Name nvarchar(200),@lc_eba_Branch_Name nvarchar(200),@lc_eba_Account_Country_Code nvarchar(100),@lc_eba_Account_Name nvarchar(400),@lc_eba_Account_Number nvarchar(400),@lc_eba_Account_Currency_Code nvarchar(100),@lc_eba_Allow_International_Payments nvarchar(100),@lc_eba_Account_Start_Date nvarchar(100),@lc_eba_Account_End_Date nvarchar(100),@lc_eba_IBAN nvarchar(400),@lc_eba_Check_Digits nvarchar(300),@lc_eba_Account_Alternate_Name nvarchar(500),@lc_eba_Account_Type_Code nvarchar(100),@lc_eba_Account_Suffix nvarchar(300),@lc_eba_Account_Description nvarchar(500),@lc_eba_Agency_Location_Code nvarchar(100),@lc_eba_Exchange_Rate_Agreement_Number nvarchar(500),@lc_eba_Exchange_Rate_Agreement_Type nvarchar(500),@ln_eba_Exchange_Rate int,@lc_eba_Secondary_Account_Reference nvarchar(30),@lc_eba_Attribute_Category nvarchar(150),@lc_eba_Attribute_1 nvarchar(150),@lc_eba_Attribute_2 nvarchar(150),@lc_eba_Attribute_3 nvarchar(150),@lc_eba_Attribute_4 nvarchar(150),@lc_eba_Attribute_5 nvarchar(150),@lc_eba_Attribute_6 nvarchar(150),@lc_eba_Attribute_7 nvarchar(150),@lc_eba_Attribute_8 nvarchar(150),@lc_eba_Attribute_9 nvarchar(150),@lc_eba_Attribute_10 nvarchar(150),@lc_eba_Attribute_11 nvarchar(150),@lc_eba_Attribute_12 nvarchar(150),@lc_eba_Attribute_13 nvarchar(150),@lc_eba_Attribute_14 nvarchar(150),@lc_eba_Attribute_15 nvarchar(150),@lc_eba_cnv_attribute1 nvarchar(1000),@lc_eba_cnv_attribute2 nvarchar(1000),@lc_eba_cnv_attribute3 nvarchar(1000),@lc_eba_cnv_attribute4 nvarchar(1000),@lc_eba_cnv_attribute5 nvarchar(1000),@lc_eba_cnv_attribute6 nvarchar(1000),@lc_eba_cnv_attribute7 nvarchar(1000),@lc_eba_cnv_attribute8 nvarchar(1000),@lc_eba_cnv_attribute9 nvarchar(1000),@lc_eba_cnv_attribute10 nvarchar(1000),@lc_eba_cnv_attribute11 nvarchar(1000),@lc_eba_cnv_attribute12 nvarchar(1000),@lc_eba_cnv_attribute13 nvarchar(1000),@lc_eba_cnv_attribute14 nvarchar(1000),@lc_eba_cnv_attribute15 nvarchar(1000),@lc_eba_cnv_attribute16 nvarchar(1000),@lc_eba_cnv_attribute17 nvarchar(1000),@lc_eba_cnv_attribute18 nvarchar(1000),@lc_eba_cnv_attribute19 nvarchar(1000),@lc_eba_cnv_attribute20 nvarchar(1000),@lc_eba_cnv_attribute21 nvarchar(1000),@lc_eba_cnv_attribute22 nvarchar(1000),@lc_eba_cnv_attribute23 nvarchar(1000),@lc_eba_cnv_attribute24 nvarchar(1000),@lc_eba_cnv_attribute25 nvarchar(1000),@lc_eba_cnv_attribute26 nvarchar(1000),@lc_eba_cnv_attribute27 nvarchar(1000),@lc_eba_cnv_attribute28 nvarchar(1000),@lc_eba_cnv_attribute29 nvarchar(1000),@lc_eba_cnv_attribute30 nvarchar(1000),@lc_eba_cnv_attribute31 nvarchar(1000),@lc_eba_cnv_attribute32 nvarchar(1000),@lc_eba_cnv_attribute33 nvarchar(1000),@lc_eba_cnv_attribute34 nvarchar(1000),@lc_eba_cnv_attribute35 nvarchar(1000),@lc_eba_cnv_attribute36 nvarchar(1000),@lc_eba_cnv_attribute37 nvarchar(1000),@lc_eba_cnv_attribute38 nvarchar(1000),@lc_eba_cnv_attribute39 nvarchar(1000),@lc_eba_cnv_attribute40 nvarchar(1000),@lc_eba_cnv_attribute41 nvarchar(1000),@lc_eba_cnv_attribute42 nvarchar(1000),@lc_eba_cnv_attribute43 nvarchar(1000),@lc_eba_cnv_attribute44 nvarchar(1000),@lc_eba_cnv_attribute45 nvarchar(1000),@lc_eba_cnv_attribute46 nvarchar(1000),@lc_eba_cnv_attribute47 nvarchar(1000),@lc_eba_cnv_attribute48 nvarchar(1000),@lc_eba_cnv_attribute49 nvarchar(1000),@lc_eba_cnv_attribute50 nvarchar(1000),@lc_eba_file_geo nvarchar(1000),@lc_eba_Filename nvarchar(4000),@ld_eba_Creation_Date datetime,@ld_eba_Last_Update_Date datetime,@lc_eba_Created_By nvarchar(4000),@lc_eba_Updated_By nvarchar(4000)
		--- Variables for the Table HISCOX_IBY_TEMP_PMT_INSTR_USES
		DECLARE @ln_epu_Record_ID int,@lc_epu_Process_Flag nvarchar(10),@lc_epu_Error_Message nvarchar(4000),@ln_epu_Batch_ID int,@ln_epu_Load_Batch_ID int,@ln_epu_Source_Record_ID int,@ln_epu_Import_Batch_Identifier int,@ln_epu_Payee_Identifier int,@ln_epu_Payee_Bank_Account_Identifier int,@ln_epu_Payee_Bank_Account_Assignment_Identifier int,@lc_epu_Primary_Flag nvarchar(1000),@lc_epu_Account_Assignment_Start_Date nvarchar(100),@lc_epu_Account_Assignment_End_Date nvarchar(100),@lc_epu_cnv_attribute1 nvarchar(1000),@lc_epu_cnv_attribute2 nvarchar(1000),@lc_epu_cnv_attribute3 nvarchar(1000),@lc_epu_cnv_attribute4 nvarchar(1000),@lc_epu_cnv_attribute5 nvarchar(1000),@lc_epu_cnv_attribute6 nvarchar(1000),@lc_epu_cnv_attribute7 nvarchar(1000),@lc_epu_cnv_attribute8 nvarchar(1000),@lc_epu_cnv_attribute9 nvarchar(1000),@lc_epu_cnv_attribute10 nvarchar(1000),@lc_epu_cnv_attribute11 nvarchar(1000),@lc_epu_cnv_attribute12 nvarchar(1000),@lc_epu_cnv_attribute13 nvarchar(1000),@lc_epu_cnv_attribute14 nvarchar(1000),@lc_epu_cnv_attribute15 nvarchar(1000),@lc_epu_cnv_attribute16 nvarchar(1000),@lc_epu_cnv_attribute17 nvarchar(1000),@lc_epu_cnv_attribute18 nvarchar(1000),@lc_epu_cnv_attribute19 nvarchar(1000),@lc_epu_cnv_attribute20 nvarchar(1000),@lc_epu_cnv_attribute21 nvarchar(1000),@lc_epu_cnv_attribute22 nvarchar(1000),@lc_epu_cnv_attribute23 nvarchar(1000),@lc_epu_cnv_attribute24 nvarchar(1000),@lc_epu_cnv_attribute25 nvarchar(1000),@lc_epu_cnv_attribute26 nvarchar(1000),@lc_epu_cnv_attribute27 nvarchar(1000),@lc_epu_cnv_attribute28 nvarchar(1000),@lc_epu_cnv_attribute29 nvarchar(1000),@lc_epu_cnv_attribute30 nvarchar(1000),@lc_epu_cnv_attribute31 nvarchar(1000),@lc_epu_cnv_attribute32 nvarchar(1000),@lc_epu_cnv_attribute33 nvarchar(1000),@lc_epu_cnv_attribute34 nvarchar(1000),@lc_epu_cnv_attribute35 nvarchar(1000),@lc_epu_cnv_attribute36 nvarchar(1000),@lc_epu_cnv_attribute37 nvarchar(1000),@lc_epu_cnv_attribute38 nvarchar(1000),@lc_epu_cnv_attribute39 nvarchar(1000),@lc_epu_cnv_attribute40 nvarchar(1000),@lc_epu_cnv_attribute41 nvarchar(1000),@lc_epu_cnv_attribute42 nvarchar(1000),@lc_epu_cnv_attribute43 nvarchar(1000),@lc_epu_cnv_attribute44 nvarchar(1000),@lc_epu_cnv_attribute45 nvarchar(1000),@lc_epu_cnv_attribute46 nvarchar(1000),@lc_epu_cnv_attribute47 nvarchar(1000),@lc_epu_cnv_attribute48 nvarchar(1000),@lc_epu_cnv_attribute49 nvarchar(1000),@lc_epu_cnv_attribute50 nvarchar(1000),@lc_epu_file_geo nvarchar(1000),@lc_epu_Filename nvarchar(4000),@ld_epu_Creation_Date datetime,@ld_epu_Last_Update_Date datetime,@lc_epu_Created_By nvarchar(4000),@lc_epu_Updated_By nvarchar(4000)
		

		-- Variables for Issue tracking
		DECLARE @lc_rice_id VARCHAR(100) = 'P2P-SP-CNV008'
		,@lc_source VARCHAR(100) = 'HISCOX_POZ_SUPPLIER_BANKS_TRANS_P'
		,@lc_calling_object VARCHAR(100) = 'HISCOX Supplier Bank Accounts Data Migration SSIS Package'
                
		
                    BEGIN
						DELETE FROM hiscox_iby_source WHERE code='Code' AND supplier_code ='Supplier Code' and name='Name' and batch_id=@p_batch
						
								IF @p_file_geo  IN ('UK','BERMUDA','HIG','HISL','EU','US','EMPLOYEES')
										BEGIN
											--SET @lc_p_business_unit='Hiscox Underwriting Group Services Ltd'
											
					
						-- DELETE the header Row
						--DELETE FROM hiscox_iby_source WHERE Supplier_Name ='Name' AND Supplier_Code ='Supplier Code' AND add1_postal_code='Postal Code' AND process_flag='N' AND batch_id=@p_batch
						
                        DECLARE cur_sup_bank_source CURSOR LOCAL FOR 
                        SELECT Record_ID,Process_Flag,Error_Message,Batch_ID,Load_Batch_ID,code,supplier_code,name,business_unit,address_name,site_name,payment_method,delivery_channel_code,settlement_priority,remit_delivery_method,remit_advice_email,remit_advice_fax,bank_instructions_1,bank_instructions_2,bank_instruction_details,bank_name,bank_branch,bank_sorting_code,bank_account_name,iban_bank_account_number,iban,bank_account_number,country_code,account_currency_code,allow_international_payments,account_start_date,account_end_date,account_alternate_name,account_type_code,primary_flag,account_assignment_start_date,account_assignment_end_date,comment,status,migrate,attribute1,attribute2,attribute3,attribute4,attribute5,attribute6,attribute7,attribute8,attribute9,attribute10,attribute11,attribute12,attribute13,attribute14,attribute15,attribute16,attribute17,attribute18,attribute19,attribute20,attribute21,attribute22,attribute23,attribute24,attribute25,attribute26,attribute27,attribute28,attribute29,attribute30,attribute31,attribute32,attribute33,attribute34,attribute35,attribute36,attribute37,attribute38,attribute39,attribute40,attribute41,attribute42,attribute43,attribute44,attribute45,attribute46,attribute47,attribute48,attribute49,attribute50,attribute51,attribute52,attribute53,attribute54,attribute55,attribute56,attribute57,attribute58,attribute59,attribute60,attribute61,attribute62,attribute63,attribute64,attribute65,attribute66,attribute67,attribute68,attribute69,attribute70,attribute71,attribute72,attribute73,attribute74,attribute75,attribute76,attribute77,attribute78,attribute79,attribute80,attribute81,attribute82,attribute83,attribute84,attribute85,attribute86,attribute87,attribute88,attribute89,attribute90,attribute91,attribute92,attribute93,attribute94,attribute95,attribute96,attribute97,attribute98,attribute99,attribute100,file_geo,Filename,Creation_Date,Last_Update_Date,Created_By,Updated_By
                        FROM hiscox_iby_source
                        WHERE process_flag = 'N'
                        AND batch_id=@p_batch
						
						
						EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'DEBUG'
													,@p_err_column     = 'DEBUG'  
													,@p_err_value      = 'DEBUG'  
													,@p_err_desc       = 'HISCOX_POZ_SUPPLIER_BANKS_TRANS_P Procedure Start'   
													,@p_rect_action    = NULL; 
							

                        OPEN cur_sup_bank_source  
                        FETCH NEXT FROM cur_sup_bank_source INTO 
                        @ln_Record_ID,@lc_Process_Flag,@lc_Error_Message,@ln_Batch_ID,@ln_Load_Batch_ID,@lc_code,@lc_supplier_code,@lc_name,@lc_business_unit,@lc_address_name,@lc_site_name,@lc_payment_method,@lc_delivery_channel_code,@lc_settlement_priority,@lc_remit_delivery_method,@lc_remit_advice_email,@lc_remit_advice_fax,@lc_bank_instructions_1,@lc_bank_instructions_2,@lc_bank_instruction_details,@lc_bank_name,@lc_bank_branch,@lc_bank_sorting_code,@lc_bank_account_name,@lc_iban_bank_account_number,@lc_iban,@lc_bank_account_number,@lc_country_code,@lc_account_currency_code,@lc_allow_international_payments,@lc_account_start_date,@lc_account_end_date,@lc_account_alternate_name,@lc_account_type_code,@lc_primary_flag,@lc_account_assignment_start_date,@lc_account_assignment_end_date,@lc_comment,@lc_status,@lc_migrate,@lc_attribute1,@lc_attribute2,@lc_attribute3,@lc_attribute4,@lc_attribute5,@lc_attribute6,@lc_attribute7,@lc_attribute8,@lc_attribute9,@lc_attribute10,@lc_attribute11,@lc_attribute12,@lc_attribute13,@lc_attribute14,@lc_attribute15,@lc_attribute16,@lc_attribute17,@lc_attribute18,@lc_attribute19,@lc_attribute20,@lc_attribute21,@lc_attribute22,@lc_attribute23,@lc_attribute24,@lc_attribute25,@lc_attribute26,@lc_attribute27,@lc_attribute28,@lc_attribute29,@lc_attribute30,@lc_attribute31,@lc_attribute32,@lc_attribute33,@lc_attribute34,@lc_attribute35,@lc_attribute36,@lc_attribute37,@lc_attribute38,@lc_attribute39,@lc_attribute40,@lc_attribute41,@lc_attribute42,@lc_attribute43,@lc_attribute44,@lc_attribute45,@lc_attribute46,@lc_attribute47,@lc_attribute48,@lc_attribute49,@lc_attribute50,@lc_attribute51,@lc_attribute52,@lc_attribute53,@lc_attribute54,@lc_attribute55,@lc_attribute56,@lc_attribute57,@lc_attribute58,@lc_attribute59,@lc_attribute60,@lc_attribute61,@lc_attribute62,@lc_attribute63,@lc_attribute64,@lc_attribute65,@lc_attribute66,@lc_attribute67,@lc_attribute68,@lc_attribute69,@lc_attribute70,@lc_attribute71,@lc_attribute72,@lc_attribute73,@lc_attribute74,@lc_attribute75,@lc_attribute76,@lc_attribute77,@lc_attribute78,@lc_attribute79,@lc_attribute80,@lc_attribute81,@lc_attribute82,@lc_attribute83,@lc_attribute84,@lc_attribute85,@lc_attribute86,@lc_attribute87,@lc_attribute88,@lc_attribute89,@lc_attribute90,@lc_attribute91,@lc_attribute92,@lc_attribute93,@lc_attribute94,@lc_attribute95,@lc_attribute96,@lc_attribute97,@lc_attribute98,@lc_attribute99,@lc_attribute100,@lc_file_geo,@lc_Filename,@ld_Creation_Date,@ld_Last_Update_Date,@lc_Created_By,@lc_Updated_By
                        
                        WHILE @@FETCH_STATUS = 0  
                            BEGIN 
								BEGIN TRY	
										SET @lc_error_msg ='';
										SET @lc_party_number ='';
										SET @lc_party_site_number ='';
										SET @lc_party_name ='';
										SET @lc_party_site_name='';
										SET @lc_payment_method_code='';

										SET @ln_ep_Import_Batch_Identifier				=''
										SET @ln_ep_Payee_Identifier						=''
										SET @lc_ep_Supplier_Number						=''
										SET @lc_ep_Business_Unit_Name					=''
										SET @lc_ep_Supplier_Site						=''
										SET @lc_ep_Pay_Each_Document_Alone				=''
										SET @lc_ep_Payment_Method_Code					=''
										
										
										SET @ln_eba_Import_Batch_Identifier				=''
										SET @ln_eba_Payee_Identifier					=''
										SET @ln_eba_Payee_Bank_Account_Identifier		=''
										SET @lc_eba_Bank_Name							=''
										SET @lc_eba_Branch_Name							=''
										SET @lc_eba_Account_Country_Code				=''
										SET @lc_eba_Account_Name						=''
										SET @lc_eba_Account_Number						=''
										SET @lc_eba_Allow_International_Payments		=''
										SET @lc_eba_Account_Start_Date					=''
										SET @lc_eba_IBAN								=''
										
										SET @ln_epu_Import_Batch_Identifier						=''
										SET @ln_epu_Payee_Identifier								=''
										SET @ln_epu_Payee_Bank_Account_Identifier					=''
										SET @ln_epu_Payee_Bank_Account_Assignment_Identifier		=''
										SET @lc_epu_Account_Assignment_Start_Date					=''
										SET @lc_epu_Primary_Flag									=''

										/*
										IF (@p_file_geo = 'UK')
										BEGIN
										SET @lc_p_business_unit='HUGUK BU'
                                        -- Set values for Supplier Header Interface
										
										--EXEC @lc_party_number        = hiscox_get_lookup_meaning_f 'HISCOX_POZ_EXISTING_SUPPLIERS',CAST(@lc_supplier_code AS VARCHAR)
										--SET @lc_party_name		 	 = (
											--							)
										-- Get Supplier Name from Alias Name
										SET @ln_check =(SELECT   count(*)
														FROM hiscox_lookup_ref_tbl
														WHERE 1					= 1
														AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
														AND UPPER(description)	= UPPER(@lc_code)
														)
										IF @ln_check <> 1
										BEGIN
											SET @lc_error_msg =CONCAT('Unique Supplier not found. No of Supplier found for the given Alias - ',@ln_check,'. ');
											THROW 60000,@lc_Error_Msg,5
										END
										ELSE
										BEGIN
										SELECT @lc_party_name=ISNULL(lookup_code,''),@lc_party_number=ISNULL(meaning,'')
										FROM hiscox_lookup_ref_tbl
										WHERE 1					= 1
										AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
										AND UPPER(description)	= UPPER(@lc_code)
										END
										
										IF @lc_party_number =''
										BEGIN
											SET @lc_error_msg =CONCAT('Supplier Not found for the given Alias Name. ',@lc_error_msg);
											THROW 60000,@lc_Error_Msg,5
										END
										
										-- Get Supplier Site from the given Supplier Number and the Business Unit
										SELECT @ln_check = count(*)
										FROM hiscox_lookup_ref_tbl
										WHERE 1					= 1
										AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITES'
										AND lookup_code 		LIKE @lc_party_number+'%'+@lc_p_business_unit

										IF @ln_check <> 1
										BEGIN
											SET @lc_error_msg =CONCAT('Unique Supplier Site not found. No of Supplier Sites found - ',@ln_check,'. ');
											THROW 60000,@lc_Error_Msg,5
										END
										ELSE
										BEGIN
											SELECT @lc_party_site_name=ISNULL(attribute4,''),@lc_payment_method_code=ISNULL(attribute5,'')
											FROM hiscox_lookup_ref_tbl
											WHERE 1					= 1
											AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITES'
											AND lookup_code 		LIKE @lc_party_number+'%'+@lc_p_business_unit
											--SET @ln_party_site_number = (
											--								SELECT  count(*) 
											--								FROM hiscox_lookup_ref_tbl
											--								WHERE 1					= 1
											--								AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
											--								AND UPPER(lookup_code)	= UPPER(@lc_sup_Supplier_Name)
											--							)
											
											IF @lc_party_site_name =''
											BEGIN
												SET @lc_error_msg =CONCAT('Supplier Site Not found. ',@lc_error_msg);
												THROW 60000,@lc_Error_Msg,5
											END
										END
										SET @ln_bank_acct_length				=LEN(LTRIM(RTRIM(@lc_iban_bank_account_number)))
										SET @ln_sort_code_length				=LEN(LTRIM(RTRIM(@lc_bank_sorting_code)))
										
										-- The Account is valid if the Account Number length is 8 and the Sort Code length in 6
										IF (@ln_sort_code_length      = 6 AND @ln_bank_acct_length   =8)
											BEGIN
												
												SET @ln_ep_Import_Batch_Identifier				=@p_batch
												SET @ln_ep_Payee_Identifier						=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
												SET @lc_ep_Supplier_Number						=@lc_party_number
												SET @lc_ep_Business_Unit_Name					=@lc_p_business_unit
												SET @lc_ep_Supplier_Site						=@lc_party_site_name
												SET @lc_ep_Pay_Each_Document_Alone				='N'
												SET @lc_ep_Payment_Method_Code					=@lc_payment_method_code
												
												SET @ln_eba_Import_Batch_Identifier				=@p_batch
												SET @ln_eba_Payee_Identifier					=@ln_ep_Payee_Identifier
												SET @ln_eba_Payee_Bank_Account_Identifier		=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
												SET @lc_eba_Bank_Name							='All UK banks'
												SET @lc_eba_Branch_Name							=LTRIM(RTRIM(@lc_bank_sorting_code))
												SET @lc_eba_Account_Country_Code				='GB'
												SET @lc_eba_Account_Name						=LTRIM(RTRIM(@lc_bank_account_name))
												SET @lc_eba_Account_Number						=LTRIM(RTRIM(@lc_iban_bank_account_number))
												SET @lc_eba_Allow_International_Payments		='Y'
												SET @lc_eba_Account_Start_Date					='2015/01/01'
												SET @lc_eba_IBAN								=NULL
												
												SET @ln_epu_Import_Batch_Identifier							=@p_batch
												SET @ln_epu_Payee_Identifier								=@ln_ep_Payee_Identifier
												SET @ln_epu_Payee_Bank_Account_Identifier					=@ln_eba_Payee_Bank_Account_Identifier
												SET @ln_epu_Payee_Bank_Account_Assignment_Identifier		=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
												SET @lc_epu_Account_Assignment_Start_Date					='2015/01/01'
												SET @lc_epu_Primary_Flag									='Y'
												
											END
										ELSE
											-- Check if the given data is for IBAN
											-- IBAN starts with 2 character country code.
											BEGIN
												SET @lc_iban_country	=SUBSTRING(@lc_iban_bank_account_number,1,2)
												EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_COUNTRY_CODE' , @lc_iban_country
												
												IF @ln_check = 0
													BEGIN
														SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Account No/Sorting code/IBAN is not valid ',1,4000)
													END
												ELSE
													-- Check if IBAN to account substring mapping is available
													BEGIN
														EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_IBY_IBAN_MAP' , @lc_iban_country
														IF @ln_check = 0
															BEGIN
																SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'IBAN Mapping not found. ',1,4000)
															END
														ELSE
															BEGIN
																EXEC @lc_concat_variable = hiscox_get_lookup_meaning_f 'HISCOX_IBY_IBAN_MAP' , @lc_iban_country

																SET @ln_check	=	CAST(@lc_concat_variable AS INT)
																
																SET @ln_ep_Import_Batch_Identifier				=@p_batch
																SET @ln_ep_Payee_Identifier						=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
																SET @lc_ep_Supplier_Number						=@lc_party_number
																SET @lc_ep_Business_Unit_Name					=@lc_p_business_unit
																SET @lc_ep_Supplier_Site						=@lc_party_site_name
																SET @lc_ep_Pay_Each_Document_Alone				='N'
																SET @lc_ep_Payment_Method_Code					=@lc_payment_method_code
																
																
																SET @ln_eba_Import_Batch_Identifier				=@p_batch
																SET @ln_eba_Payee_Identifier					=@ln_ep_Payee_Identifier
																SET @ln_eba_Payee_Bank_Account_Identifier		=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
																SET @lc_eba_Bank_Name							=NULL
																SET @lc_eba_Branch_Name							=NULL
																SET @lc_eba_Account_Country_Code				=SUBSTRING(@lc_iban_bank_account_number,1,2)
																SET @lc_eba_Account_Name						=LTRIM(RTRIM(@lc_bank_account_name))
																SET @lc_eba_Account_Number						=RIGHT(LTRIM(RTRIM(@lc_iban_bank_account_number)),@ln_check)
																SET @lc_eba_Allow_International_Payments		='N'
																SET @lc_eba_Account_Start_Date					='2015/01/01'
																SET @lc_eba_IBAN								=LTRIM(RTRIM(@lc_iban_bank_account_number))
																
																SET @ln_epu_Import_Batch_Identifier						=@p_batch
																SET @ln_epu_Payee_Identifier								=@ln_ep_Payee_Identifier
																SET @ln_epu_Payee_Bank_Account_Identifier					=@ln_eba_Payee_Bank_Account_Identifier
																SET @ln_epu_Payee_Bank_Account_Assignment_Identifier		=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
																SET @lc_epu_Account_Assignment_Start_Date					='2015/01/01'
																SET @lc_epu_Primary_Flag									='Y'
																
															END
													END
													
											END
										END											
										*/
									IF @p_file_geo IN ('UK','BERMUDA','HIG','HISL','EU','US','EMPLOYEES')
										BEGIN
											
											--Get Supplier Number for the given Supplier
											BEGIN
												SELECT @lc_party_name=ISNULL(lookup_code,''),@lc_party_number=ISNULL(meaning,'')
												FROM hiscox_lookup_ref_tbl
												WHERE 1					= 1
												AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
												AND UPPER(lookup_code)	= UPPER(LTRIM(RTRIM(@lc_name)))
											END
										
											IF @lc_party_number =''
											BEGIN
												SET @lc_error_msg =CONCAT('Supplier Not found in Oracle. ',@lc_error_msg);
												THROW 60000,@lc_Error_Msg,5
											END
											
											-- Get Supplier Site from the given Supplier Number and the Business Unit
											SET @lc_concat_variable	=CONCAT(@lc_party_number,'-',UPPER(LTRIM(RTRIM(SUBSTRING(LTRIM(RTRIM(@lc_site_name)),1,15)))),'-',@lc_business_unit)
											SET @ln_check = (SELECT count(*)
													FROM hiscox_lookup_ref_tbl
													WHERE 1					= 1
													AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITES'
													AND UPPER(lookup_code) 	= @lc_concat_variable)
													
													 EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
									,@p_batch_id       = @p_batch
									,@p_process_id     = 0          
									,@p_source         = @lc_source
									,@p_pri_record_id  = @p_batch
									,@p_sec_record_id  = @p_file_geo
									,@p_ter_record_id  = NULL
									,@p_err_code       = 'ERRO_SUP_SITE'
									,@p_err_column     = @lc_concat_variable  
									,@p_err_value      = 'EXCEPTION'  
									,@p_err_desc       = @ln_check 
									,@p_rect_action    = NULL; 
	
											IF @ln_check <> 1
												BEGIN
													SET @lc_error_msg =CONCAT('Unique Supplier Site not found. Site Found - ',@ln_check,'. ');
													THROW 60000,@lc_Error_Msg,5
												END
											ELSE
												BEGIN
													SELECT @lc_party_site_name=ISNULL(attribute4,''),@lc_payment_method_code=ISNULL(attribute5,'')
													FROM hiscox_lookup_ref_tbl
													WHERE 1					= 1
													AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITES'
													AND UPPER(lookup_code) 	= @lc_concat_variable
													
													IF @lc_party_site_name =''
													BEGIN
														SET @lc_error_msg =CONCAT('Supplier Site Not found. ',@lc_error_msg);
														THROW 60000,@lc_Error_Msg,5
													END
												END
												
											-- Check if the Country Code is given
											IF @lc_country_code IS NULL 
												BEGIN
													SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Account Country cannot be NULL. ',1,4000);
													THROW 60000,@lc_Error_Msg,5
												END
											ELSE
												BEGIN
													EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_COUNTRY_CODE' , @lc_country_code
													
													IF @ln_check = 0
														BEGIN
															SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Country is Invalid. ',1,4000);
															THROW 60000,@lc_Error_Msg,5
														END 
												END
											
											-- Check if Account Number is NULL
											IF ISNULL(@lc_iban_bank_account_number,'') = ''
												BEGIN
													IF ISNULL(@lc_iban_bank_account_number,'') = ''
														BEGIN
															SET @lc_iban_country	=SUBSTRING(@lc_iban,1,2)
															EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_COUNTRY_CODE' , @lc_iban_country
														
															IF @ln_check = 0
																BEGIN
																	SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Invalid IBAN. ',1,4000);
																	THROW 60000,@lc_Error_Msg,5
																END
															ELSE
																-- Check if IBAN to account substring mapping is available
																BEGIN
																	EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_IBY_IBAN_MAP' , @lc_iban_country
																	IF @ln_check = 0
																		BEGIN
																			SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'IBAN Mapping not found. ',1,4000);
																			THROW 60000,@lc_Error_Msg,5
																		END
																	ELSE
																		BEGIN
																			EXEC @lc_concat_variable = hiscox_get_lookup_meaning_f 'HISCOX_IBY_IBAN_MAP' , @lc_iban_country
																			SET @ln_check	=	CAST(@lc_concat_variable AS INT)
																		END
																END
														END
													ELSE
														BEGIN
															SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Both Account Number and IBAN is NULL ',1,4000);
															THROW 60000,@lc_Error_Msg,5
														END
												END
												
												
											SET @ln_ep_Import_Batch_Identifier				=@p_batch
											SET @ln_ep_Payee_Identifier						=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
											SET @lc_ep_Supplier_Number						=@lc_party_number
											SET @lc_ep_Business_Unit_Name					=@lc_business_unit
											SET @lc_ep_Supplier_Site						=@lc_party_site_name
											SET @lc_ep_Pay_Each_Document_Alone				='N'
											SET @lc_ep_Payment_Method_Code					=@lc_payment_method_code
											
											SET @ln_eba_Import_Batch_Identifier				=@p_batch
											SET @ln_eba_Payee_Identifier					=@ln_ep_Payee_Identifier
											SET @ln_eba_Payee_Bank_Account_Identifier		=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
											SET @lc_eba_Bank_Name							=LTRIM(RTRIM(@lc_bank_name))
											--SET @lc_eba_Branch_Name							=ISNULL(LTRIM(RTRIM(@lc_bank_sorting_code)),LTRIM(RTRIM(@lc_attribute11)))
											SET @lc_eba_Branch_Name							=LTRIM(RTRIM(@lc_bank_branch))
											SET @lc_eba_Account_Country_Code				=@lc_country_code
											SET @lc_eba_Account_Name						=LTRIM(RTRIM(@lc_bank_account_name))
											SET @lc_eba_Account_Number						=(CASE WHEN ISNULL(@lc_iban_bank_account_number,'')='' THEN
																									RIGHT(LTRIM(RTRIM(@lc_iban)),@ln_check)
																								 ElSE
																									LTRIM(RTRIM(@lc_iban_bank_account_number))
																								END
																							 )
																									
											SET @lc_eba_Allow_International_Payments		='Y'
											SET @lc_eba_Account_Start_Date					='2015/01/01'
											SET @lc_eba_IBAN								=LTRIM(RTRIM(@lc_iban))
											
											SET @ln_epu_Import_Batch_Identifier							=@p_batch
											SET @ln_epu_Payee_Identifier								=@ln_ep_Payee_Identifier
											SET @ln_epu_Payee_Bank_Account_Identifier					=@ln_eba_Payee_Bank_Account_Identifier
											SET @ln_epu_Payee_Bank_Account_Assignment_Identifier		=(NEXT VALUE FOR [HISCOX_IBY_TEMP_EXT_SEQ])
											SET @lc_epu_Account_Assignment_Start_Date					='2015/01/01'
											SET @lc_epu_Primary_Flag									='Y'
												
										END
									ELSE
										BEGIN
											SET @lc_Error_Msg ='Not a valid FileGeo';
											THROW 60000,@lc_Error_Msg,5;
										END
										
									IF @lc_error_msg <> '' 
										BEGIN
											THROW 60000,@lc_Error_Msg,5
										END
											
									
									-- Insert into Interface Table  hiscox_iby_temp_ext_payees
									BEGIN TRY
                                
										INSERT INTO hiscox_iby_temp_ext_payees
										(
										load_batch_id,
										batch_id,
										Import_Batch_Identifier,
										Payee_Identifier,
										Supplier_Number,
										Business_Unit_Name,
										Supplier_Site,
										Pay_Each_Document_Alone,
										Payment_Method_Code,
										file_geo,
										source_record_id,
										filename
										)
										VALUES
										(
										@p_batch,
										@p_batch,
										@ln_ep_Import_Batch_Identifier,
										@ln_ep_Payee_Identifier,
										@lc_ep_Supplier_Number,
										@lc_ep_Business_Unit_Name,
										@lc_ep_Supplier_Site,
										@lc_ep_Pay_Each_Document_Alone,
										@lc_ep_Payment_Method_Code,
										@p_file_geo,
										@ln_Record_Id,
										@lc_Filename
										)
                                    
									END TRY
									
									BEGIN CATCH
										SET @lc_error_msg      = SUBSTRING(@lc_error_msg  +'Insertion Failed while inserting into HISCOX_IBY_TEMP_EXT_PAYEES. ',1,4000)
									END CATCH
									
									-- Insert into Interface Table  hiscox_iby_temp_ext_bank_accts
									BEGIN TRY
                                
										INSERT INTO hiscox_iby_temp_ext_bank_accts
										(
										load_batch_id,
										batch_id,
										Import_Batch_Identifier,
										Payee_Identifier,
										Payee_Bank_Account_Identifier,
										Bank_Name,
										Branch_Name,
										Account_Country_Code,
										Account_Name,
										Account_Number,
										Allow_International_Payments,
										Account_Start_Date,
										IBAN,
										file_geo,
										source_record_id,
										filename,
										cnv_attribute1,
										cnv_attribute2,
										cnv_attribute3,
										cnv_attribute4,
										cnv_attribute5,
										cnv_attribute6,
										cnv_attribute7,
										cnv_attribute8
										)
										VALUES
										(
										@p_batch,
										@p_batch,
										@ln_eba_Import_Batch_Identifier,
										@ln_eba_Payee_Identifier,
										@ln_eba_Payee_Bank_Account_Identifier,
										@lc_eba_Bank_Name,
										@lc_eba_Branch_Name,
										@lc_eba_Account_Country_Code,
										@lc_eba_Account_Name,
										@lc_eba_Account_Number,
										@lc_eba_Allow_International_Payments,
										@lc_eba_Account_Start_Date,
										@lc_eba_IBAN,
										@p_file_geo,
										@ln_Record_Id,
										@lc_Filename,
										@lc_party_name,
										@lc_party_number,
										@lc_party_site_name,
										@lc_p_business_unit,
										@lc_payment_method_code,
										LTRIM(RTRIM(@lc_bank_sorting_code)),
										LTRIM(RTRIM(@lc_attribute11)),
										LTRIM(RTRIM(@lc_attribute12))
										)
                                    
									END TRY
									
									BEGIN CATCH
										SET @lc_error_msg      = SUBSTRING(@lc_error_msg  +'Insertion Failed while inserting into HISCOX_IBY_TEMP_EXT_BANK_ACCTS. ',1,4000)
									END CATCH
									
									-- Insert into Interface Table  hiscox_iby_temp_pmt_instr_uses
									BEGIN TRY
                                
										INSERT INTO hiscox_iby_temp_pmt_instr_uses
										(
										load_batch_id,
										batch_id,
										Import_Batch_Identifier,
										Payee_Identifier,
										Payee_Bank_Account_Identifier,
										Payee_Bank_Account_Assignment_Identifier,
										Account_Assignment_Start_Date,
										Primary_Flag,
										file_geo,
										source_record_id,
										filename
										)
										VALUES
										(
										@p_batch,
										@p_batch,
										@ln_epu_Import_Batch_Identifier,
										@ln_epu_Payee_Identifier,
										@ln_epu_Payee_Bank_Account_Identifier,
										@ln_epu_Payee_Bank_Account_Assignment_Identifier,
										@lc_epu_Account_Assignment_Start_Date,
										@lc_epu_Primary_Flag,
										@p_file_geo,
										@ln_Record_Id,
										@lc_Filename
										)
                                    
									END TRY
									
									BEGIN CATCH
										SET @lc_error_msg      = SUBSTRING(@lc_error_msg  +'Insertion Failed while inserting into HISCOX_IBY_TEMP_PMT_INSTR_USES. ',1,4000)
									END CATCH
                                
									IF @lc_error_msg = '' 
										BEGIN

											UPDATE hiscox_iby_source
											SET process_flag = 'S',
											attribute51		 = @lc_party_name,
											attribute52		 = @lc_party_number,
											attribute53		 = @lc_party_site_name,
											attribute54		 = @lc_p_business_unit,
											attribute55		 = @lc_payment_method_code,
											attribute56		 = @lc_eba_Bank_Name,
											attribute57		 = @lc_eba_Branch_Name,
											attribute58		 = @lc_eba_Account_Number,
											attribute59		 = @lc_eba_IBAN
											WHERE record_id = @ln_Record_Id

										END
									ELSE
										BEGIN
											THROW 60000,@lc_Error_Msg,5
										END
                            
								
								END TRY
								BEGIN CATCH
									SET @lc_error_msg = ERROR_MESSAGE()

									UPDATE hiscox_iby_source
									SET process_flag = 'E',
									error_message	 = @lc_error_msg,
									attribute51		 = @lc_party_name,
									attribute52		 = @lc_party_number,
									attribute53		 = @lc_party_site_name,
									attribute54		 = @lc_p_business_unit,
									attribute55		 = @lc_payment_method_code,
									attribute56		 = @lc_eba_Bank_Name,
									attribute57		 = @lc_eba_Branch_Name,
									attribute58		 = @lc_eba_Account_Number,
									attribute59		 = @lc_eba_IBAN
									WHERE record_id  = @ln_Record_Id
								END CATCH
							FETCH NEXT FROM cur_sup_bank_source INTO 
							@ln_Record_ID,@lc_Process_Flag,@lc_Error_Message,@ln_Batch_ID,@ln_Load_Batch_ID,@lc_code,@lc_supplier_code,@lc_name,@lc_business_unit,@lc_address_name,@lc_site_name,@lc_payment_method,@lc_delivery_channel_code,@lc_settlement_priority,@lc_remit_delivery_method,@lc_remit_advice_email,@lc_remit_advice_fax,@lc_bank_instructions_1,@lc_bank_instructions_2,@lc_bank_instruction_details,@lc_bank_name,@lc_bank_branch,@lc_bank_sorting_code,@lc_bank_account_name,@lc_iban_bank_account_number,@lc_iban,@lc_bank_account_number,@lc_country_code,@lc_account_currency_code,@lc_allow_international_payments,@lc_account_start_date,@lc_account_end_date,@lc_account_alternate_name,@lc_account_type_code,@lc_primary_flag,@lc_account_assignment_start_date,@lc_account_assignment_end_date,@lc_comment,@lc_status,@lc_migrate,@lc_attribute1,@lc_attribute2,@lc_attribute3,@lc_attribute4,@lc_attribute5,@lc_attribute6,@lc_attribute7,@lc_attribute8,@lc_attribute9,@lc_attribute10,@lc_attribute11,@lc_attribute12,@lc_attribute13,@lc_attribute14,@lc_attribute15,@lc_attribute16,@lc_attribute17,@lc_attribute18,@lc_attribute19,@lc_attribute20,@lc_attribute21,@lc_attribute22,@lc_attribute23,@lc_attribute24,@lc_attribute25,@lc_attribute26,@lc_attribute27,@lc_attribute28,@lc_attribute29,@lc_attribute30,@lc_attribute31,@lc_attribute32,@lc_attribute33,@lc_attribute34,@lc_attribute35,@lc_attribute36,@lc_attribute37,@lc_attribute38,@lc_attribute39,@lc_attribute40,@lc_attribute41,@lc_attribute42,@lc_attribute43,@lc_attribute44,@lc_attribute45,@lc_attribute46,@lc_attribute47,@lc_attribute48,@lc_attribute49,@lc_attribute50,@lc_attribute51,@lc_attribute52,@lc_attribute53,@lc_attribute54,@lc_attribute55,@lc_attribute56,@lc_attribute57,@lc_attribute58,@lc_attribute59,@lc_attribute60,@lc_attribute61,@lc_attribute62,@lc_attribute63,@lc_attribute64,@lc_attribute65,@lc_attribute66,@lc_attribute67,@lc_attribute68,@lc_attribute69,@lc_attribute70,@lc_attribute71,@lc_attribute72,@lc_attribute73,@lc_attribute74,@lc_attribute75,@lc_attribute76,@lc_attribute77,@lc_attribute78,@lc_attribute79,@lc_attribute80,@lc_attribute81,@lc_attribute82,@lc_attribute83,@lc_attribute84,@lc_attribute85,@lc_attribute86,@lc_attribute87,@lc_attribute88,@lc_attribute89,@lc_attribute90,@lc_attribute91,@lc_attribute92,@lc_attribute93,@lc_attribute94,@lc_attribute95,@lc_attribute96,@lc_attribute97,@lc_attribute98,@lc_attribute99,@lc_attribute100,@lc_file_geo,@lc_Filename,@ld_Creation_Date,@ld_Last_Update_Date,@lc_Created_By,@lc_Updated_By
						END  -- END WHILE

                    CLOSE cur_sup_bank_source
                    DEALLOCATE cur_sup_bank_source
					END
					ELSE
					BEGIN
						SET @x_error_code 	=2
						SET @x_error_msg	='Invalid File Geo Code. '
					END
					---Start the validation procedure on the 
					DECLARE cur_sup_iby_ext_payess CURSOR LOCAL FOR 
                    SELECT Record_ID,Process_Flag,Error_Message,Batch_ID,Load_Batch_ID,Source_Record_ID,Import_Batch_Identifier,Payee_Identifier,Business_Unit_Name,Supplier_Number,Supplier_Site,Pay_Each_Document_Alone,Payment_Method_Code,Delivery_Channel_Code,Settlement_Priority,Remit_Delivery_Method,Remit_Advice_Email,Remit_Advice_Fax,Bank_Instructions_1,Bank_Instructions_2,Bank_Instruction_Details,Payment_Reason_Code,Payment_Reason_Comments,Payment_Message1,Payment_Message2,Payment_Message3,Bank_Charge_Bearer_Code,cnv_attribute1,cnv_attribute2,cnv_attribute3,cnv_attribute4,cnv_attribute5,cnv_attribute6,cnv_attribute7,cnv_attribute8,cnv_attribute9,cnv_attribute10,cnv_attribute11,cnv_attribute12,cnv_attribute13,cnv_attribute14,cnv_attribute15,cnv_attribute16,cnv_attribute17,cnv_attribute18,cnv_attribute19,cnv_attribute20,cnv_attribute21,cnv_attribute22,cnv_attribute23,cnv_attribute24,cnv_attribute25,cnv_attribute26,cnv_attribute27,cnv_attribute28,cnv_attribute29,cnv_attribute30,cnv_attribute31,cnv_attribute32,cnv_attribute33,cnv_attribute34,cnv_attribute35,cnv_attribute36,cnv_attribute37,cnv_attribute38,cnv_attribute39,cnv_attribute40,cnv_attribute41,cnv_attribute42,cnv_attribute43,cnv_attribute44,cnv_attribute45,cnv_attribute46,cnv_attribute47,cnv_attribute48,cnv_attribute49,cnv_attribute50,file_geo,Filename,Creation_Date,Last_Update_Date,Created_By,Updated_By
					FROM hiscox_iby_temp_ext_payees
                    WHERE process_flag = 'N'
                    AND batch_id=@p_batch
					
					OPEN cur_sup_iby_ext_payess  
                        FETCH NEXT FROM cur_sup_iby_ext_payess INTO 
                        @ln_ep_Record_ID,@lc_ep_Process_Flag,@lc_ep_Error_Message,@ln_ep_Batch_ID,@ln_ep_Load_Batch_ID,@ln_ep_Source_Record_ID,@ln_ep_Import_Batch_Identifier,@ln_ep_Payee_Identifier,@lc_ep_Business_Unit_Name,@lc_ep_Supplier_Number,@lc_ep_Supplier_Site,@lc_ep_Pay_Each_Document_Alone,@lc_ep_Payment_Method_Code,@lc_ep_Delivery_Channel_Code,@lc_ep_Settlement_Priority,@lc_ep_Remit_Delivery_Method,@lc_ep_Remit_Advice_Email,@lc_ep_Remit_Advice_Fax,@lc_ep_Bank_Instructions_1,@lc_ep_Bank_Instructions_2,@lc_ep_Bank_Instruction_Details,@lc_ep_Payment_Reason_Code,@lc_ep_Payment_Reason_Comments,@lc_ep_Payment_Message1,@lc_ep_Payment_Message2,@lc_ep_Payment_Message3,@lc_ep_Bank_Charge_Bearer_Code,@lc_ep_cnv_attribute1,@lc_ep_cnv_attribute2,@lc_ep_cnv_attribute3,@lc_ep_cnv_attribute4,@lc_ep_cnv_attribute5,@lc_ep_cnv_attribute6,@lc_ep_cnv_attribute7,@lc_ep_cnv_attribute8,@lc_ep_cnv_attribute9,@lc_ep_cnv_attribute10,@lc_ep_cnv_attribute11,@lc_ep_cnv_attribute12,@lc_ep_cnv_attribute13,@lc_ep_cnv_attribute14,@lc_ep_cnv_attribute15,@lc_ep_cnv_attribute16,@lc_ep_cnv_attribute17,@lc_ep_cnv_attribute18,@lc_ep_cnv_attribute19,@lc_ep_cnv_attribute20,@lc_ep_cnv_attribute21,@lc_ep_cnv_attribute22,@lc_ep_cnv_attribute23,@lc_ep_cnv_attribute24,@lc_ep_cnv_attribute25,@lc_ep_cnv_attribute26,@lc_ep_cnv_attribute27,@lc_ep_cnv_attribute28,@lc_ep_cnv_attribute29,@lc_ep_cnv_attribute30,@lc_ep_cnv_attribute31,@lc_ep_cnv_attribute32,@lc_ep_cnv_attribute33,@lc_ep_cnv_attribute34,@lc_ep_cnv_attribute35,@lc_ep_cnv_attribute36,@lc_ep_cnv_attribute37,@lc_ep_cnv_attribute38,@lc_ep_cnv_attribute39,@lc_ep_cnv_attribute40,@lc_ep_cnv_attribute41,@lc_ep_cnv_attribute42,@lc_ep_cnv_attribute43,@lc_ep_cnv_attribute44,@lc_ep_cnv_attribute45,@lc_ep_cnv_attribute46,@lc_ep_cnv_attribute47,@lc_ep_cnv_attribute48,@lc_ep_cnv_attribute49,@lc_ep_cnv_attribute50,@lc_ep_file_geo,@lc_ep_Filename,@ld_ep_Creation_Date,@ld_ep_Last_Update_Date,@lc_ep_Created_By,@lc_ep_Updated_By
                        
                        WHILE @@FETCH_STATUS = 0  
                            BEGIN 
								BEGIN TRY
									SET @lc_error_msg	= '';
									

									-- Check if the Business unit is valid
									EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_BUSINESS_UNIT' , @lc_ep_Business_Unit_Name
									
									IF @ln_check < 1
									BEGIN
										SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Business Unit is invalid. ',1,4000)
									END
									
									-- Check if the Payment Method is valid
									IF  (@lc_ep_Payment_Method_Code IS NOT NULL AND @lc_ep_Payment_Method_Code <>'')
											BEGIN
												
												EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_VALID_PAY_METHOD' , @lc_ep_Payment_Method_Code
												
												IF @ln_check = 0
													BEGIN
														SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Payment Method is invalid. ',1,4000)
													END
											END
									

									-- Validate Supplier Number if it exists
									EXEC @ln_check = hiscox_does_meaning_exist 'HISCOX_POZ_EXISTING_SUPPLIERS' , @lc_ep_Supplier_Number
									
									IF @ln_check < 1
									BEGIN
										SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Supplier Number is invalid. ',1,4000)
									END
									
									-- Validate if the Supplier Site is valid
									SET @lc_concat_variable =(@lc_ep_Supplier_Number+'-'+@lc_ep_Supplier_Site+'-'+@lc_ep_Business_Unit_Name)
									EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_SUPPLIER_SITES' , @lc_concat_variable
									
									IF @ln_check < 1
									BEGIN
										SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Supplier Site is invalid. ',1,4000)
									END
									
									
									IF @lc_error_msg <> ''
										BEGIN
											THROW 60000,@lc_error_msg,5
										END
										
									UPDATE hiscox_iby_temp_ext_payees
									SET process_flag   	 	='S',
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_ep_Record_ID
									
								END TRY
								
								BEGIN CATCH
									SET @lc_error_msg = ERROR_MESSAGE()
										
									UPDATE hiscox_iby_temp_ext_payees
									SET process_flag 		= 'E',
									Error_Message    		= @lc_error_msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_ep_Record_ID
									
								END CATCH
								
								FETCH NEXT FROM cur_sup_iby_ext_payess INTO 
								@ln_ep_Record_ID,@lc_ep_Process_Flag,@lc_ep_Error_Message,@ln_ep_Batch_ID,@ln_ep_Load_Batch_ID,@ln_ep_Source_Record_ID,@ln_ep_Import_Batch_Identifier,@ln_ep_Payee_Identifier,@lc_ep_Business_Unit_Name,@lc_ep_Supplier_Number,@lc_ep_Supplier_Site,@lc_ep_Pay_Each_Document_Alone,@lc_ep_Payment_Method_Code,@lc_ep_Delivery_Channel_Code,@lc_ep_Settlement_Priority,@lc_ep_Remit_Delivery_Method,@lc_ep_Remit_Advice_Email,@lc_ep_Remit_Advice_Fax,@lc_ep_Bank_Instructions_1,@lc_ep_Bank_Instructions_2,@lc_ep_Bank_Instruction_Details,@lc_ep_Payment_Reason_Code,@lc_ep_Payment_Reason_Comments,@lc_ep_Payment_Message1,@lc_ep_Payment_Message2,@lc_ep_Payment_Message3,@lc_ep_Bank_Charge_Bearer_Code,@lc_ep_cnv_attribute1,@lc_ep_cnv_attribute2,@lc_ep_cnv_attribute3,@lc_ep_cnv_attribute4,@lc_ep_cnv_attribute5,@lc_ep_cnv_attribute6,@lc_ep_cnv_attribute7,@lc_ep_cnv_attribute8,@lc_ep_cnv_attribute9,@lc_ep_cnv_attribute10,@lc_ep_cnv_attribute11,@lc_ep_cnv_attribute12,@lc_ep_cnv_attribute13,@lc_ep_cnv_attribute14,@lc_ep_cnv_attribute15,@lc_ep_cnv_attribute16,@lc_ep_cnv_attribute17,@lc_ep_cnv_attribute18,@lc_ep_cnv_attribute19,@lc_ep_cnv_attribute20,@lc_ep_cnv_attribute21,@lc_ep_cnv_attribute22,@lc_ep_cnv_attribute23,@lc_ep_cnv_attribute24,@lc_ep_cnv_attribute25,@lc_ep_cnv_attribute26,@lc_ep_cnv_attribute27,@lc_ep_cnv_attribute28,@lc_ep_cnv_attribute29,@lc_ep_cnv_attribute30,@lc_ep_cnv_attribute31,@lc_ep_cnv_attribute32,@lc_ep_cnv_attribute33,@lc_ep_cnv_attribute34,@lc_ep_cnv_attribute35,@lc_ep_cnv_attribute36,@lc_ep_cnv_attribute37,@lc_ep_cnv_attribute38,@lc_ep_cnv_attribute39,@lc_ep_cnv_attribute40,@lc_ep_cnv_attribute41,@lc_ep_cnv_attribute42,@lc_ep_cnv_attribute43,@lc_ep_cnv_attribute44,@lc_ep_cnv_attribute45,@lc_ep_cnv_attribute46,@lc_ep_cnv_attribute47,@lc_ep_cnv_attribute48,@lc_ep_cnv_attribute49,@lc_ep_cnv_attribute50,@lc_ep_file_geo,@lc_ep_Filename,@ld_ep_Creation_Date,@ld_ep_Last_Update_Date,@lc_ep_Created_By,@lc_ep_Updated_By
							END -- END WHILE
					CLOSE cur_sup_iby_ext_payess
                    DEALLOCATE cur_sup_iby_ext_payess
					
					DECLARE cur_sup_iby_bank_accts CURSOR LOCAL FOR 
                    SELECT Record_ID,Process_Flag,Error_Message,Batch_ID,Load_Batch_ID,Source_Record_ID,Import_Batch_Identifier,Payee_Identifier,Payee_Bank_Account_Identifier,Bank_Name,Branch_Name,Account_Country_Code,Account_Name,Account_Number,Account_Currency_Code,Allow_International_Payments,Account_Start_Date,Account_End_Date,IBAN,Check_Digits,Account_Alternate_Name,Account_Type_Code,Account_Suffix,Account_Description,Agency_Location_Code,Exchange_Rate_Agreement_Number,Exchange_Rate_Agreement_Type,Exchange_Rate,Secondary_Account_Reference,Attribute_Category,Attribute_1,Attribute_2,Attribute_3,Attribute_4,Attribute_5,Attribute_6,Attribute_7,Attribute_8,Attribute_9,Attribute_10,Attribute_11,Attribute_12,Attribute_13,Attribute_14,Attribute_15,cnv_attribute1,cnv_attribute2,cnv_attribute3,cnv_attribute4,cnv_attribute5,cnv_attribute6,cnv_attribute7,cnv_attribute8,cnv_attribute9,cnv_attribute10,cnv_attribute11,cnv_attribute12,cnv_attribute13,cnv_attribute14,cnv_attribute15,cnv_attribute16,cnv_attribute17,cnv_attribute18,cnv_attribute19,cnv_attribute20,cnv_attribute21,cnv_attribute22,cnv_attribute23,cnv_attribute24,cnv_attribute25,cnv_attribute26,cnv_attribute27,cnv_attribute28,cnv_attribute29,cnv_attribute30,cnv_attribute31,cnv_attribute32,cnv_attribute33,cnv_attribute34,cnv_attribute35,cnv_attribute36,cnv_attribute37,cnv_attribute38,cnv_attribute39,cnv_attribute40,cnv_attribute41,cnv_attribute42,cnv_attribute43,cnv_attribute44,cnv_attribute45,cnv_attribute46,cnv_attribute47,cnv_attribute48,cnv_attribute49,cnv_attribute50,file_geo,Filename,Creation_Date,Last_Update_Date,Created_By,Updated_By
					FROM hiscox_iby_temp_ext_bank_accts
                    WHERE process_flag = 'N'
                    AND batch_id=@p_batch
					
					OPEN cur_sup_iby_bank_accts  
                        FETCH NEXT FROM cur_sup_iby_bank_accts INTO 
                        @ln_eba_Record_ID,@lc_eba_Process_Flag,@lc_eba_Error_Message,@ln_eba_Batch_ID,@ln_eba_Load_Batch_ID,@ln_eba_Source_Record_ID,@ln_eba_Import_Batch_Identifier,@ln_eba_Payee_Identifier,@ln_eba_Payee_Bank_Account_Identifier,@lc_eba_Bank_Name,@lc_eba_Branch_Name,@lc_eba_Account_Country_Code,@lc_eba_Account_Name,@lc_eba_Account_Number,@lc_eba_Account_Currency_Code,@lc_eba_Allow_International_Payments,@lc_eba_Account_Start_Date,@lc_eba_Account_End_Date,@lc_eba_IBAN,@lc_eba_Check_Digits,@lc_eba_Account_Alternate_Name,@lc_eba_Account_Type_Code,@lc_eba_Account_Suffix,@lc_eba_Account_Description,@lc_eba_Agency_Location_Code,@lc_eba_Exchange_Rate_Agreement_Number,@lc_eba_Exchange_Rate_Agreement_Type,@ln_eba_Exchange_Rate,@lc_eba_Secondary_Account_Reference,@lc_eba_Attribute_Category,@lc_eba_Attribute_1,@lc_eba_Attribute_2,@lc_eba_Attribute_3,@lc_eba_Attribute_4,@lc_eba_Attribute_5,@lc_eba_Attribute_6,@lc_eba_Attribute_7,@lc_eba_Attribute_8,@lc_eba_Attribute_9,@lc_eba_Attribute_10,@lc_eba_Attribute_11,@lc_eba_Attribute_12,@lc_eba_Attribute_13,@lc_eba_Attribute_14,@lc_eba_Attribute_15,@lc_eba_cnv_attribute1,@lc_eba_cnv_attribute2,@lc_eba_cnv_attribute3,@lc_eba_cnv_attribute4,@lc_eba_cnv_attribute5,@lc_eba_cnv_attribute6,@lc_eba_cnv_attribute7,@lc_eba_cnv_attribute8,@lc_eba_cnv_attribute9,@lc_eba_cnv_attribute10,@lc_eba_cnv_attribute11,@lc_eba_cnv_attribute12,@lc_eba_cnv_attribute13,@lc_eba_cnv_attribute14,@lc_eba_cnv_attribute15,@lc_eba_cnv_attribute16,@lc_eba_cnv_attribute17,@lc_eba_cnv_attribute18,@lc_eba_cnv_attribute19,@lc_eba_cnv_attribute20,@lc_eba_cnv_attribute21,@lc_eba_cnv_attribute22,@lc_eba_cnv_attribute23,@lc_eba_cnv_attribute24,@lc_eba_cnv_attribute25,@lc_eba_cnv_attribute26,@lc_eba_cnv_attribute27,@lc_eba_cnv_attribute28,@lc_eba_cnv_attribute29,@lc_eba_cnv_attribute30,@lc_eba_cnv_attribute31,@lc_eba_cnv_attribute32,@lc_eba_cnv_attribute33,@lc_eba_cnv_attribute34,@lc_eba_cnv_attribute35,@lc_eba_cnv_attribute36,@lc_eba_cnv_attribute37,@lc_eba_cnv_attribute38,@lc_eba_cnv_attribute39,@lc_eba_cnv_attribute40,@lc_eba_cnv_attribute41,@lc_eba_cnv_attribute42,@lc_eba_cnv_attribute43,@lc_eba_cnv_attribute44,@lc_eba_cnv_attribute45,@lc_eba_cnv_attribute46,@lc_eba_cnv_attribute47,@lc_eba_cnv_attribute48,@lc_eba_cnv_attribute49,@lc_eba_cnv_attribute50,@lc_eba_file_geo,@lc_eba_Filename,@ld_eba_Creation_Date,@ld_eba_Last_Update_Date,@lc_eba_Created_By,@lc_eba_Updated_By
                        
                        WHILE @@FETCH_STATUS = 0  
                            BEGIN 
								BEGIN TRY
									
									SET @lc_error_msg			=''
									SET @lc_party_number 		=''
									SET @lc_party_site_name		=''
									SET @lc_p_business_unit 	=''

									-- Validate if Bank is valid
									IF  (@lc_eba_Bank_Name IS NOT NULL AND @lc_eba_Bank_Name <>'')
										BEGIN
											SET @lc_concat_variable =(@lc_eba_Bank_Name+'-'+@lc_eba_Account_Country_Code)
											SET @ln_check =(	SELECT count(*) 
																FROM hiscox_lookup_ref_tbl
																WHERE 1=1
																AND lookup_type				=	'HISCOX_IBY_BANKS'
																AND UPPER(lookup_code)		=	UPPER(@lc_concat_variable)
																AND ISNULL(attribute1,'')	=   ISNULL(@lc_eba_cnv_attribute8,'')
																)
											---EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_IBY_BANKS' , @lc_concat_variable
											
											IF @ln_check = 0
												BEGIN
													SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Bank is invalid. ',1,4000)
												END
										END
										

									-- Validate if Bank Branch is valid																		
									IF  (@lc_eba_Branch_Name IS NOT NULL AND @lc_eba_Branch_Name <>'')
										BEGIN
											SET @lc_concat_variable = (@lc_eba_Bank_Name+'-'+@lc_eba_Account_Country_Code+'-'+@lc_eba_Branch_Name)
											SET @ln_check =(	SELECT count(*) 
																FROM hiscox_lookup_ref_tbl
																WHERE 1=1
																AND lookup_type				=	'HISCOX_IBY_BANK_BRANCHES'
																AND UPPER(lookup_code)		=	UPPER(@lc_concat_variable)
																AND ISNULL(attribute1,'')	=   ISNULL(@lc_eba_cnv_attribute6,'')
																AND ISNULL(attribute5,'')	=   ISNULL(@lc_eba_cnv_attribute7,'')
																)
											--EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_IBY_BANK_BRANCHES' , @lc_concat_variable
											
											IF @ln_check = 0
												BEGIN
													SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Bank Branch is invalid. ',1,4000)
												END
										END
										
									-- Check if Country is not null
									IF @lc_eba_Account_Country_Code IS NULL 
										BEGIN
											SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Account Country cannot be NULL. ',1,4000)
										END
									ELSE
										BEGIN
											EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_COUNTRY_CODE' , @lc_eba_Account_Country_Code
											
											IF @ln_check = 0
												BEGIN
													SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Country is Invalid. ',1,4000)
												END 
										END
										
									-- Check combination of Allow International Payments flag
									IF (@lc_eba_Allow_International_Payments ='N' AND (@lc_eba_Bank_Name IS NULL OR @lc_eba_Bank_Name ='') AND (@lc_eba_Branch_Name IS NULL OR @lc_eba_Branch_Name =''))
										BEGIN
											SET @ln_check = 0
											-- Valid Case
										END
									ELSE IF (@lc_eba_Allow_International_Payments ='Y' AND (@lc_eba_Bank_Name IS NOT NULL AND @lc_eba_Bank_Name <>'') AND (@lc_eba_Branch_Name IS NOT NULL AND @lc_eba_Branch_Name <>''))
										BEGIN
											SET @ln_check = 0
											-- Valid Case
										END 
									ELSE
										BEGIN
											SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'International Payments Flag and Bank Details Combination is invalid. ',1,4000)
										END
										
									-- Check if the Supplier Details are fed properly
									SET @ln_check = (	SELECT count(*)
														FROM hiscox_iby_temp_ext_payees
														WHERE 1					= 1
														AND batch_id            = @p_batch
														AND process_flag    	= 'S'
														AND Payee_Identifier	= @ln_eba_Payee_Identifier
													)
									
									IF @ln_check < 1
									BEGIN
										SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Supplier Details is invalid for this Bank Account . ',1,4000)
									END

									SELECT @lc_party_number =Supplier_Number,@lc_party_site_name=Supplier_Site,@lc_p_business_unit =Business_Unit_Name
									FROM hiscox_iby_temp_ext_payees
									WHERE 1=1
									AND batch_id            = @p_batch
									AND process_flag    	= 'S'
									AND Payee_Identifier	= @ln_eba_Payee_Identifier

									/*SET @ln_check = ( 	SELECT count(*)
														FROM HISCOX_LOOKUP_REF_TBL
														WHERE 1=1
														AND lookup_type				= 'HISCOX_IBY_SUP_SITE_BANK_ACCOUNTS'
														AND UPPER(lookup_code)		like UPPER(CONCAT('%-'+@lc_party_number,'-',@lc_party_site_name,'-',@lc_p_business_unit))
														AND description				= CONCAT(@lc_eba_Bank_Name,'-',@lc_eba_Branch_Name)
														AND meaning					= @lc_eba_Account_Number
														AND ISNULL(attribute1,'')	= ISNULL(@lc_eba_IBAN,'')
													)
	
									IF @ln_check > 0
									BEGIN
										SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Supplier Bank Account already loaded for this Supplier Site . ',1,4000)
									END
									*/
									IF @lc_error_msg <> ''
										BEGIN
											THROW 60000,@lc_error_msg,5
										END
										
									UPDATE hiscox_iby_temp_ext_bank_accts
									SET process_flag   	 	='S',
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_eba_Record_ID
									
								END TRY
								
								BEGIN CATCH
									SET @lc_error_msg = ERROR_MESSAGE()
										
									UPDATE hiscox_iby_temp_ext_bank_accts
									SET process_flag 		= 'E',
									Error_Message    		= @lc_error_msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_eba_Record_ID

									UPDATE hiscox_iby_temp_ext_payees
									SET process_flag 		= 'E',
									Error_Message    		= 'Error in Bank Accounts child Record. '+@lc_error_msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE 1					= 1
									AND batch_id            = @p_batch
									AND process_flag    	= 'S'
									AND Payee_Identifier	= @ln_eba_Payee_Identifier
									
								END CATCH
								
								FETCH NEXT FROM cur_sup_iby_bank_accts INTO 
								@ln_eba_Record_ID,@lc_eba_Process_Flag,@lc_eba_Error_Message,@ln_eba_Batch_ID,@ln_eba_Load_Batch_ID,@ln_eba_Source_Record_ID,@ln_eba_Import_Batch_Identifier,@ln_eba_Payee_Identifier,@ln_eba_Payee_Bank_Account_Identifier,@lc_eba_Bank_Name,@lc_eba_Branch_Name,@lc_eba_Account_Country_Code,@lc_eba_Account_Name,@lc_eba_Account_Number,@lc_eba_Account_Currency_Code,@lc_eba_Allow_International_Payments,@lc_eba_Account_Start_Date,@lc_eba_Account_End_Date,@lc_eba_IBAN,@lc_eba_Check_Digits,@lc_eba_Account_Alternate_Name,@lc_eba_Account_Type_Code,@lc_eba_Account_Suffix,@lc_eba_Account_Description,@lc_eba_Agency_Location_Code,@lc_eba_Exchange_Rate_Agreement_Number,@lc_eba_Exchange_Rate_Agreement_Type,@ln_eba_Exchange_Rate,@lc_eba_Secondary_Account_Reference,@lc_eba_Attribute_Category,@lc_eba_Attribute_1,@lc_eba_Attribute_2,@lc_eba_Attribute_3,@lc_eba_Attribute_4,@lc_eba_Attribute_5,@lc_eba_Attribute_6,@lc_eba_Attribute_7,@lc_eba_Attribute_8,@lc_eba_Attribute_9,@lc_eba_Attribute_10,@lc_eba_Attribute_11,@lc_eba_Attribute_12,@lc_eba_Attribute_13,@lc_eba_Attribute_14,@lc_eba_Attribute_15,@lc_eba_cnv_attribute1,@lc_eba_cnv_attribute2,@lc_eba_cnv_attribute3,@lc_eba_cnv_attribute4,@lc_eba_cnv_attribute5,@lc_eba_cnv_attribute6,@lc_eba_cnv_attribute7,@lc_eba_cnv_attribute8,@lc_eba_cnv_attribute9,@lc_eba_cnv_attribute10,@lc_eba_cnv_attribute11,@lc_eba_cnv_attribute12,@lc_eba_cnv_attribute13,@lc_eba_cnv_attribute14,@lc_eba_cnv_attribute15,@lc_eba_cnv_attribute16,@lc_eba_cnv_attribute17,@lc_eba_cnv_attribute18,@lc_eba_cnv_attribute19,@lc_eba_cnv_attribute20,@lc_eba_cnv_attribute21,@lc_eba_cnv_attribute22,@lc_eba_cnv_attribute23,@lc_eba_cnv_attribute24,@lc_eba_cnv_attribute25,@lc_eba_cnv_attribute26,@lc_eba_cnv_attribute27,@lc_eba_cnv_attribute28,@lc_eba_cnv_attribute29,@lc_eba_cnv_attribute30,@lc_eba_cnv_attribute31,@lc_eba_cnv_attribute32,@lc_eba_cnv_attribute33,@lc_eba_cnv_attribute34,@lc_eba_cnv_attribute35,@lc_eba_cnv_attribute36,@lc_eba_cnv_attribute37,@lc_eba_cnv_attribute38,@lc_eba_cnv_attribute39,@lc_eba_cnv_attribute40,@lc_eba_cnv_attribute41,@lc_eba_cnv_attribute42,@lc_eba_cnv_attribute43,@lc_eba_cnv_attribute44,@lc_eba_cnv_attribute45,@lc_eba_cnv_attribute46,@lc_eba_cnv_attribute47,@lc_eba_cnv_attribute48,@lc_eba_cnv_attribute49,@lc_eba_cnv_attribute50,@lc_eba_file_geo,@lc_eba_Filename,@ld_eba_Creation_Date,@ld_eba_Last_Update_Date,@lc_eba_Created_By,@lc_eba_Updated_By
							END -- END WHILE
					CLOSE cur_sup_iby_bank_accts
                    DEALLOCATE cur_sup_iby_bank_accts
					
					
					DECLARE cur_sup_iby_bank_accts_uses CURSOR LOCAL FOR 
                    SELECT Record_ID,Process_Flag,Error_Message,Batch_ID,Load_Batch_ID,Source_Record_ID,Import_Batch_Identifier,Payee_Identifier,Payee_Bank_Account_Identifier,Payee_Bank_Account_Assignment_Identifier,Primary_Flag,Account_Assignment_Start_Date,Account_Assignment_End_Date,cnv_attribute1,cnv_attribute2,cnv_attribute3,cnv_attribute4,cnv_attribute5,cnv_attribute6,cnv_attribute7,cnv_attribute8,cnv_attribute9,cnv_attribute10,cnv_attribute11,cnv_attribute12,cnv_attribute13,cnv_attribute14,cnv_attribute15,cnv_attribute16,cnv_attribute17,cnv_attribute18,cnv_attribute19,cnv_attribute20,cnv_attribute21,cnv_attribute22,cnv_attribute23,cnv_attribute24,cnv_attribute25,cnv_attribute26,cnv_attribute27,cnv_attribute28,cnv_attribute29,cnv_attribute30,cnv_attribute31,cnv_attribute32,cnv_attribute33,cnv_attribute34,cnv_attribute35,cnv_attribute36,cnv_attribute37,cnv_attribute38,cnv_attribute39,cnv_attribute40,cnv_attribute41,cnv_attribute42,cnv_attribute43,cnv_attribute44,cnv_attribute45,cnv_attribute46,cnv_attribute47,cnv_attribute48,cnv_attribute49,cnv_attribute50,file_geo,Filename,Creation_Date,Last_Update_Date,Created_By,Updated_By
					FROM hiscox_iby_temp_pmt_instr_uses
                    WHERE process_flag = 'N'
                    AND batch_id=@p_batch
					
					OPEN cur_sup_iby_bank_accts_uses  
                        FETCH NEXT FROM cur_sup_iby_bank_accts_uses INTO 
                        @ln_epu_Record_ID,@lc_epu_Process_Flag,@lc_epu_Error_Message,@ln_epu_Batch_ID,@ln_epu_Load_Batch_ID,@ln_epu_Source_Record_ID,@ln_epu_Import_Batch_Identifier,@ln_epu_Payee_Identifier,@ln_epu_Payee_Bank_Account_Identifier,@ln_epu_Payee_Bank_Account_Assignment_Identifier,@lc_epu_Primary_Flag,@lc_epu_Account_Assignment_Start_Date,@lc_epu_Account_Assignment_End_Date,@lc_epu_cnv_attribute1,@lc_epu_cnv_attribute2,@lc_epu_cnv_attribute3,@lc_epu_cnv_attribute4,@lc_epu_cnv_attribute5,@lc_epu_cnv_attribute6,@lc_epu_cnv_attribute7,@lc_epu_cnv_attribute8,@lc_epu_cnv_attribute9,@lc_epu_cnv_attribute10,@lc_epu_cnv_attribute11,@lc_epu_cnv_attribute12,@lc_epu_cnv_attribute13,@lc_epu_cnv_attribute14,@lc_epu_cnv_attribute15,@lc_epu_cnv_attribute16,@lc_epu_cnv_attribute17,@lc_epu_cnv_attribute18,@lc_epu_cnv_attribute19,@lc_epu_cnv_attribute20,@lc_epu_cnv_attribute21,@lc_epu_cnv_attribute22,@lc_epu_cnv_attribute23,@lc_epu_cnv_attribute24,@lc_epu_cnv_attribute25,@lc_epu_cnv_attribute26,@lc_epu_cnv_attribute27,@lc_epu_cnv_attribute28,@lc_epu_cnv_attribute29,@lc_epu_cnv_attribute30,@lc_epu_cnv_attribute31,@lc_epu_cnv_attribute32,@lc_epu_cnv_attribute33,@lc_epu_cnv_attribute34,@lc_epu_cnv_attribute35,@lc_epu_cnv_attribute36,@lc_epu_cnv_attribute37,@lc_epu_cnv_attribute38,@lc_epu_cnv_attribute39,@lc_epu_cnv_attribute40,@lc_epu_cnv_attribute41,@lc_epu_cnv_attribute42,@lc_epu_cnv_attribute43,@lc_epu_cnv_attribute44,@lc_epu_cnv_attribute45,@lc_epu_cnv_attribute46,@lc_epu_cnv_attribute47,@lc_epu_cnv_attribute48,@lc_epu_cnv_attribute49,@lc_epu_cnv_attribute50,@lc_epu_file_geo,@lc_epu_Filename,@ld_epu_Creation_Date,@ld_epu_Last_Update_Date,@lc_epu_Created_By,@lc_epu_Updated_By
                        
                        WHILE @@FETCH_STATUS = 0  
                            BEGIN 
								BEGIN TRY
									SET @lc_error_msg	= '';
									
									-- Check if the Supplier Details are fed properly
									SET @ln_check = (	SELECT count(*)
														FROM hiscox_iby_temp_ext_bank_accts
														WHERE 1								= 1
														AND batch_id            			= @p_batch
														AND process_flag    				= 'S'
														AND Payee_Bank_Account_Identifier	= @ln_epu_Payee_Bank_Account_Identifier
													)
									IF @ln_check < 1
									BEGIN
										SET @lc_error_msg = SUBSTRING(@lc_error_msg  +'Supplier Bank Details is invalid for this Assignment . ',1,4000)
									END
									
									/*
									SET @ln_check = 0
						
									IF @lc_epu_Primary_Flag = 'Y'
										BEGIN
											SET @ln_check =	(
																SELECT count(*)
																FROM hiscox_iby_temp_pmt_instr_uses hscheck,
																hiscox_iby_temp_ext_payees hspayescur,
																hiscox_iby_temp_ext_payees hspayescheck
																WHERE 1								= 1
																AND hspayescur.process_flag    		= 'S'
																AND hspayescur.Payee_Identifier		= @ln_epu_Payee_Identifier
																AND hspayescur.Batch_ID				= @ln_epu_Batch_ID
																AND hspayescheck.Batch_ID			= @ln_epu_Batch_ID
																AND hspayescheck.Supplier_Number	= hspayescur.Supplier_Number
																AND hspayescheck.Supplier_Site		= hspayescur.Supplier_Site
																AND hspayescheck.Business_Unit_Name	= hspayescur.Business_Unit_Name										
																AND hspayescheck.process_flag		= 'S'
																AND hspayescheck.Payee_Identifier	= hscheck.Payee_Identifier
																AND hscheck.primary_flag			= 'Y'
																AND hscheck.record_id				<> @ln_epu_Record_ID
															)
										END
									*/
									IF @lc_error_msg <> ''
										BEGIN
											THROW 60000,@lc_error_msg,5
										END
										
									UPDATE hiscox_iby_temp_pmt_instr_uses
									SET process_flag   	 	='S',
									--primary_flag			= (CASE WHEN @ln_check>0 THEN 'N' ELSE primary_flag END),
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_epu_Record_ID
									
								END TRY
								
								BEGIN CATCH
									SET @lc_error_msg = ERROR_MESSAGE()
										
									UPDATE hiscox_iby_temp_pmt_instr_uses
									SET process_flag 		= 'E',
									Error_Message    		= @lc_error_msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_epu_Record_ID


									UPDATE hiscox_iby_temp_ext_bank_accts
									SET process_flag 					= 'E',
									Error_Message    					= 'Error in Bank Accounts Assignments child Record. '+@lc_error_msg,
									last_update_date					= GETDATE(),
									updated_by							= USER_NAME()
									WHERE 1								= 1
									AND batch_id            			= @p_batch
									AND process_flag    				= 'S'
									AND Payee_Bank_Account_Identifier	= @ln_epu_Payee_Bank_Account_Identifier

									UPDATE hiscox_iby_temp_ext_payees
									SET process_flag 		= 'E',
									Error_Message    		= 'Error in Bank Accounts Assignments child Record. '+@lc_error_msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE 1					= 1
									AND batch_id            = @p_batch
									AND process_flag    	= 'S'
									AND Payee_Identifier	= @ln_epu_Payee_Identifier
									
								END CATCH
								
								FETCH NEXT FROM cur_sup_iby_bank_accts_uses INTO 
								@ln_epu_Record_ID,@lc_epu_Process_Flag,@lc_epu_Error_Message,@ln_epu_Batch_ID,@ln_epu_Load_Batch_ID,@ln_epu_Source_Record_ID,@ln_epu_Import_Batch_Identifier,@ln_epu_Payee_Identifier,@ln_epu_Payee_Bank_Account_Identifier,@ln_epu_Payee_Bank_Account_Assignment_Identifier,@lc_epu_Primary_Flag,@lc_epu_Account_Assignment_Start_Date,@lc_epu_Account_Assignment_End_Date,@lc_epu_cnv_attribute1,@lc_epu_cnv_attribute2,@lc_epu_cnv_attribute3,@lc_epu_cnv_attribute4,@lc_epu_cnv_attribute5,@lc_epu_cnv_attribute6,@lc_epu_cnv_attribute7,@lc_epu_cnv_attribute8,@lc_epu_cnv_attribute9,@lc_epu_cnv_attribute10,@lc_epu_cnv_attribute11,@lc_epu_cnv_attribute12,@lc_epu_cnv_attribute13,@lc_epu_cnv_attribute14,@lc_epu_cnv_attribute15,@lc_epu_cnv_attribute16,@lc_epu_cnv_attribute17,@lc_epu_cnv_attribute18,@lc_epu_cnv_attribute19,@lc_epu_cnv_attribute20,@lc_epu_cnv_attribute21,@lc_epu_cnv_attribute22,@lc_epu_cnv_attribute23,@lc_epu_cnv_attribute24,@lc_epu_cnv_attribute25,@lc_epu_cnv_attribute26,@lc_epu_cnv_attribute27,@lc_epu_cnv_attribute28,@lc_epu_cnv_attribute29,@lc_epu_cnv_attribute30,@lc_epu_cnv_attribute31,@lc_epu_cnv_attribute32,@lc_epu_cnv_attribute33,@lc_epu_cnv_attribute34,@lc_epu_cnv_attribute35,@lc_epu_cnv_attribute36,@lc_epu_cnv_attribute37,@lc_epu_cnv_attribute38,@lc_epu_cnv_attribute39,@lc_epu_cnv_attribute40,@lc_epu_cnv_attribute41,@lc_epu_cnv_attribute42,@lc_epu_cnv_attribute43,@lc_epu_cnv_attribute44,@lc_epu_cnv_attribute45,@lc_epu_cnv_attribute46,@lc_epu_cnv_attribute47,@lc_epu_cnv_attribute48,@lc_epu_cnv_attribute49,@lc_epu_cnv_attribute50,@lc_epu_file_geo,@lc_epu_Filename,@ld_epu_Creation_Date,@ld_epu_Last_Update_Date,@lc_epu_Created_By,@lc_epu_Updated_By
							END -- END WHILE
					CLOSE cur_sup_iby_bank_accts_uses
                    DEALLOCATE cur_sup_iby_bank_accts_uses
					
					
					
					
					SET @ln_count_rec =(   SELECT count(*)
											FROM hiscox_iby_temp_pmt_instr_uses
											WHERE batch_id      =@p_batch
											AND process_flag    ='S'
										)
                    
					IF @ln_count_rec< 1
						BEGIN
							SET @x_error_code       = 2
							SET @x_error_msg        = 'No validated Records to process further'
							 EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'EXCEPTION'
													,@p_err_column     = 'ALL'  
													,@p_err_value      = 'EXCEPTION'  
													,@p_err_desc       = @x_error_msg
													,@p_rect_action    = NULL; 
						END
					ELSE
						BEGIN
							SET @x_error_code       = 0
							SET @x_error_msg        = ''
							SET @lc_error_msg 		= CONCAT('HISCOX_POZ_SUPPLIER_BANKS_TRANS_P Procedure End Success Reords -',CAST(@ln_count_rec AS VARCHAR))
							EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_batch
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_batch
													,@p_sec_record_id  = @p_file_geo
													,@p_ter_record_id  = NULL
													,@p_err_code       = 'DEBUG'
													,@p_err_column     = 'DEBUG'  
													,@p_err_value      = 'DEBUG'  
													,@p_err_desc       = @lc_error_msg
													,@p_rect_action    = NULL; 
						END
			
					
   
        
					END
    END TRY
    
    BEGIN CATCH
        Print 'Error Occured in Supplier Validation Procedure:'  
        Print  Error_Message() 
        SET @x_error_code       = 2
        SET @x_error_msg        = CONCAT('Error Occured - ',Error_Message())
		 EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
									,@p_batch_id       = @p_batch
									,@p_process_id     = 0          
									,@p_source         = @lc_source
									,@p_pri_record_id  = @p_batch
									,@p_sec_record_id  = @p_file_geo
									,@p_ter_record_id  = NULL
									,@p_err_code       = 'EXCEPTION'
									,@p_err_column     = 'ALL'  
									,@p_err_value      = 'EXCEPTION'  
									,@p_err_desc       = @x_error_msg
									,@p_rect_action    = NULL; 
    END CATCH
    
END
GO
CREATE OR ALTER PROCEDURE [dbo].HISCOX_POZ_SUPPLIER_ENT_VAL_P
                                @p_new_batch_id NVARCHAR(100) 
								,@p_new_file NVARCHAR(10)
								,@p_batch_id NVARCHAR(100) = NULL
								,@p_load_batch_id NVARCHAR(100) = NULL
								,@p_reprocess_all NVARCHAR(10)
                                ,@p_file_geo NVARCHAR(100) = NULL
                                ,@x_error_code INT OUTPUT
                                ,@x_error_msg NVARCHAR(4000) OUTPUT
                                ,@x_supplier INT OUTPUT
                                ,@x_supplier_add INT OUTPUT
                                ,@x_supplier_site INT OUTPUT
                                ,@x_supplier_site_ass INT OUTPUT
                                ,@x_supplier_cnt INT OUTPUT
AS

/*****************************************************************
OBJECT NAME: HISCOX_POZ_SUPPLIER_ENT_VAL_P
DESCRIPTION: Procedure to  validate the data in Suppliers Interface table 
Version     Name                Date                Description
----------------------------------------------------------------------------
1.0         Srinivasan          18-DEC-2018         Initial version
1.1         Srinivasan			22-FEB-2019			Added Common Error Log Calls
1.2			Srinivasan			20-MAR-2019			Added validation for existing Supplier Site and Supplier Site Assignment
1.3			Srinivasan			22-APR-2019			Updated DataType to NVARCHAR for String Variables
1.4			Srinivasan			22-APR-2019			Added condition on Mandatory check for Liability Distribution
1.5			Srinivasan			22-APR-2019			Updated record id variable while checking duplicate records
*****************************************************************/

BEGIN
    BEGIN TRY
-- Load FBDI Table with source table data and perform Transformations
					
		BEGIN
                    -- For Common Variables
                    DECLARE @ln_check INT,@lc_Err_Msg   NVARCHAR(4000),@lc_country   NVARCHAR(30),@ln_count_rec INT , @lc_batch_id NVARCHAR(240), @ln_erp INT, @ln_warning	INT,@lc_war_msg NVARCHAR(4000),@lc_party_number NVARCHAR(4000), @lc_concatenated_string NVARCHAR(4000)
                    
                    -- For Supplier Header
                    DECLARE @ln_sup_Record_ID INT, @lc_sup_Process_Flag NVARCHAR(10) , @lc_sup_Error_Message NVARCHAR(4000), @lc_sup_Oracle_Process_Flag NVARCHAR(10), @lc_sup_Oracle_Error_Message NVARCHAR(4000), @lc_sup_Batch_ID NVARCHAR(200), @lc_sup_load_Batch_ID NVARCHAR(200), @ln_sup_Source_Record_ID INT, @lc_sup_Import_Action NVARCHAR(10), @lc_sup_Supplier_Name NVARCHAR(360), @lc_sup_Supplier_Name_New NVARCHAR(360), @lc_sup_Supplier_Number NVARCHAR(500), @lc_sup_Alternate_Name NVARCHAR(360), @lc_sup_Tax_Organization_Type NVARCHAR(500), @lc_sup_Supplier_Type NVARCHAR(500), @lc_sup_Inactive_Date DATETIME, @lc_sup_Business_Relationship NVARCHAR(500), @lc_sup_Parent_Supplier NVARCHAR(360), @lc_sup_Alias NVARCHAR(360), @lc_sup_D_U_N_S_Number NVARCHAR(30), @lc_sup_One_time_supplier NVARCHAR(1), @lc_sup_Customer_Number NVARCHAR(25), @lc_sup_SIC NVARCHAR(25), @lc_sup_National_Insurance_Number NVARCHAR(30), @lc_sup_Corporate_Web_Site NVARCHAR(150), @lc_sup_Chief_Executive_Title NVARCHAR(240), @lc_sup_Chief_Executive_Name NVARCHAR(240), @lc_sup_Business_Classifications_Not_Applicable NVARCHAR(1), @lc_sup_Taxpayer_Country NVARCHAR(500), @lc_sup_Taxpayer_ID NVARCHAR(500), @lc_sup_Federal_reportable NVARCHAR(500), @lc_sup_Federal_Income_Tax_Type NVARCHAR(500), @lc_sup_State_reportable NVARCHAR(500), @lc_sup_Tax_Reporting_Name NVARCHAR(500), @lc_sup_Name_Control NVARCHAR(4), @lc_sup_Tax_Verification_Date DATETIME, @lc_sup_Use_withholding_tax NVARCHAR(500), @lc_sup_Withholding_Tax_Group NVARCHAR(500), @lc_sup_Vat_Code NVARCHAR(500), @lc_sup_Tax_Registration_Number NVARCHAR(500), @lc_sup_Auto_Tax_Calc_Override NVARCHAR(1), @lc_sup_Payment_Method NVARCHAR(500), @lc_sup_Delivery_Channel NVARCHAR(30), @lc_sup_Bank_Instruction_1 NVARCHAR(30), @lc_sup_Bank_Instruction_2 NVARCHAR(30), @lc_sup_Bank_Instruction NVARCHAR(255), @lc_sup_Settlement_Priority NVARCHAR(500), @lc_sup_Payment_Text_Message_1 NVARCHAR(150), @lc_sup_Payment_Text_Message_2 NVARCHAR(150), @lc_sup_Payment_Text_Message_3 NVARCHAR(150), @lc_sup_Bank_Charge_Bearer NVARCHAR(30), @lc_sup_Payment_Reason NVARCHAR(30), @lc_sup_Payment_Reason_Comments NVARCHAR(240), @lc_sup_Payment_Format NVARCHAR(30), @lc_sup_ATTRIBUTE_CATEGORY NVARCHAR(30), @lc_sup_ATTRIBUTE1 NVARCHAR(150), @lc_sup_ATTRIBUTE2 NVARCHAR(150), @lc_sup_ATTRIBUTE3 NVARCHAR(150), @lc_sup_ATTRIBUTE4 NVARCHAR(150), @lc_sup_ATTRIBUTE5 NVARCHAR(150), @lc_sup_ATTRIBUTE6 NVARCHAR(150), @lc_sup_ATTRIBUTE7 NVARCHAR(150), @lc_sup_ATTRIBUTE8 NVARCHAR(150), @lc_sup_ATTRIBUTE9 NVARCHAR(150), @lc_sup_ATTRIBUTE10 NVARCHAR(150), @lc_sup_ATTRIBUTE11 NVARCHAR(150), @lc_sup_ATTRIBUTE12 NVARCHAR(150), @lc_sup_ATTRIBUTE13 NVARCHAR(150), @lc_sup_ATTRIBUTE14 NVARCHAR(150), @lc_sup_ATTRIBUTE15 NVARCHAR(150), @lc_sup_ATTRIBUTE16 NVARCHAR(150), @lc_sup_ATTRIBUTE17 NVARCHAR(150), @lc_sup_ATTRIBUTE18 NVARCHAR(150), @lc_sup_ATTRIBUTE19 NVARCHAR(150), @lc_sup_ATTRIBUTE20 NVARCHAR(150), @lc_sup_ATTRIBUTE_DATE1 DATETIME, @lc_sup_ATTRIBUTE_DATE2 DATETIME, @lc_sup_ATTRIBUTE_DATE3 DATETIME, @lc_sup_ATTRIBUTE_DATE4 DATETIME, @lc_sup_ATTRIBUTE_DATE5 DATETIME, @lc_sup_ATTRIBUTE_DATE6 DATETIME, @lc_sup_ATTRIBUTE_DATE7 DATETIME, @lc_sup_ATTRIBUTE_DATE8 DATETIME, @lc_sup_ATTRIBUTE_DATE9 DATETIME, @lc_sup_ATTRIBUTE_DATE10 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP1 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP2 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP3 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP4 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP5 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP6 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP7 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP8 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP9 DATETIME, @ld_sup_ATTRIBUTE_TIMESTAMP10 DATETIME, @ln_sup_ATTRIBUTE_NUMBER1 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER2 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER3 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER4 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER5 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER6 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER7 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER8 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER9 DECIMAL(25,8), @ln_sup_ATTRIBUTE_NUMBER10 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_CATEGORY NVARCHAR(30), @lc_sup_GLOBAL_ATTRIBUTE1 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE2 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE3 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE4 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE5 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE6 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE7 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE8 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE9 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE10 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE11 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE12 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE13 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE14 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE15 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE16 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE17 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE18 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE19 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE20 NVARCHAR(150), @lc_sup_GLOBAL_ATTRIBUTE_DATE1 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE2 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE3 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE4 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE5 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE6 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE7 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE8 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE9 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_DATE10 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP1 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP2 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP3 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP4 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP5 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP6 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP7 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP8 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP9 DATETIME, @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP10 DATETIME, @lc_sup_GLOBAL_ATTRIBUTE_NUMBER1 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER2 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER3 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER4 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER5 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER6 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER7 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER8 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER9 DECIMAL(25,8), @lc_sup_GLOBAL_ATTRIBUTE_NUMBER10 DECIMAL(25,8), @lc_sup_Registry_ID NVARCHAR(500), @lc_sup_Payee_Service_Level NVARCHAR(500), @lc_sup_Pay_Each_Document_Alone NVARCHAR(500), @lc_sup_Delivery_Method NVARCHAR(30), @lc_sup_Remittance_E_mail NVARCHAR(255), @lc_sup_Remittance_Fax NVARCHAR(100), @lc_sup_Creation_Date DATETIME, @lc_sup_Last_Update_Date DATETIME, @lc_sup_Created_By NVARCHAR(4000) , @lc_sup_Updated_By NVARCHAR(4000), @lc_sup_file_geo VARCHAR (1000)
                    -- For Supplier Address
                    DECLARE @ln_sa_Record_ID INT, @lc_sa_Process_Flag NVARCHAR(10), @lc_sa_Error_Message NVARCHAR(4000), @lc_sa_Oracle_Process_Flag NVARCHAR(10), @lc_sa_Oracle_Error_Message NVARCHAR(4000), @lc_sa_Batch_ID NVARCHAR(200), @lc_sa_load_Batch_ID NVARCHAR(200), @ln_sa_Source_Record_ID INT, @lc_sa_Import_Action NVARCHAR(10), @lc_sa_Supplier_Name NVARCHAR(360), @lc_sa_Address_Name NVARCHAR(500), @lc_sa_Address_Name_New NVARCHAR(500), @lc_sa_Country NVARCHAR(60), @lc_sa_Address_Line_1 NVARCHAR(500), @lc_sa_Address_Line_2 NVARCHAR(500), @lc_sa_Address_Line_3 NVARCHAR(500), @lc_sa_Address_Line_4 NVARCHAR(500), @lc_sa_Phonetic_Address_Line NVARCHAR(560), @lc_sa_Address_Element_Attribute_1 NVARCHAR(150), @lc_sa_Address_Element_Attribute_2 NVARCHAR(150), @lc_sa_Address_Element_Attribute_3 NVARCHAR(150), @lc_sa_Address_Element_Attribute_4 NVARCHAR(150), @lc_sa_Address_Element_Attribute_5 NVARCHAR(150), @lc_sa_Building NVARCHAR(240), @lc_sa_Floor_Number NVARCHAR(40), @lc_sa_City NVARCHAR(500), @lc_sa_State NVARCHAR(500), @lc_sa_Province NVARCHAR(500), @lc_sa_County NVARCHAR(500), @lc_sa_Postal_code NVARCHAR(500), @lc_sa_Postal_Plus_4_code NVARCHAR(500), @lc_sa_Addressee NVARCHAR(360), @lc_sa_Global_Location_Number NVARCHAR(40), @lc_sa_Language NVARCHAR(4), @lc_sa_Inactive_Date DATETIME, @lc_sa_Phone_Country_Code NVARCHAR(10), @lc_sa_Phone_Area_Code NVARCHAR(10), @lc_sa_Phone NVARCHAR(500), @lc_sa_Phone_Extension NVARCHAR(20), @lc_sa_Fax_Country_Code NVARCHAR(10), @lc_sa_Fax_Area_Code NVARCHAR(10), @lc_sa_Fax NVARCHAR(15), @lc_sa_RFQ_Or_Bidding NVARCHAR(500), @lc_sa_Ordering NVARCHAR(500), @lc_sa_Pay NVARCHAR(1), @lc_sa_ATTRIBUTE_CATEGORY NVARCHAR(30), @lc_sa_ATTRIBUTE1 NVARCHAR(150), @lc_sa_ATTRIBUTE2 NVARCHAR(150), @lc_sa_ATTRIBUTE3 NVARCHAR(150), @lc_sa_ATTRIBUTE4 NVARCHAR(150), @lc_sa_ATTRIBUTE5 NVARCHAR(150), @lc_sa_ATTRIBUTE6 NVARCHAR(150), @lc_sa_ATTRIBUTE7 NVARCHAR(150), @lc_sa_ATTRIBUTE8 NVARCHAR(150), @lc_sa_ATTRIBUTE9 NVARCHAR(150), @lc_sa_ATTRIBUTE10 NVARCHAR(150), @lc_sa_ATTRIBUTE11 NVARCHAR(150), @lc_sa_ATTRIBUTE12 NVARCHAR(150), @lc_sa_ATTRIBUTE13 NVARCHAR(150), @lc_sa_ATTRIBUTE14 NVARCHAR(150), @lc_sa_ATTRIBUTE15 NVARCHAR(150), @lc_sa_ATTRIBUTE16 NVARCHAR(150), @lc_sa_ATTRIBUTE17 NVARCHAR(150), @lc_sa_ATTRIBUTE18 NVARCHAR(150), @lc_sa_ATTRIBUTE19 NVARCHAR(150), @lc_sa_ATTRIBUTE20 NVARCHAR(150), @lc_sa_ATTRIBUTE21 NVARCHAR(150), @lc_sa_ATTRIBUTE22 NVARCHAR(150), @lc_sa_ATTRIBUTE23 NVARCHAR(150), @lc_sa_ATTRIBUTE24 NVARCHAR(150), @lc_sa_ATTRIBUTE25 NVARCHAR(150), @lc_sa_ATTRIBUTE26 NVARCHAR(150), @lc_sa_ATTRIBUTE27 NVARCHAR(150), @lc_sa_ATTRIBUTE28 NVARCHAR(150), @lc_sa_ATTRIBUTE29 NVARCHAR(150), @lc_sa_ATTRIBUTE30 NVARCHAR(255), @ln_sa_ATTRIBUTE_NUMBER1 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER2 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER3 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER4 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER5 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER6 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER7 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER8 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER9 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER10 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER11 DECIMAL(25,8), @ln_sa_ATTRIBUTE_NUMBER12 DECIMAL(25,8), @ld_sa_ATTRIBUTE_DATE1 DATETIME, @ld_sa_ATTRIBUTE_DATE2 DATETIME, @ld_sa_ATTRIBUTE_DATE3 DATETIME, @ld_sa_ATTRIBUTE_DATE4 DATETIME, @ld_sa_ATTRIBUTE_DATE5 DATETIME, @ld_sa_ATTRIBUTE_DATE6 DATETIME, @ld_sa_ATTRIBUTE_DATE7 DATETIME, @ld_sa_ATTRIBUTE_DATE8 DATETIME, @ld_sa_ATTRIBUTE_DATE9 DATETIME, @ld_sa_ATTRIBUTE_DATE10 DATETIME, @ld_sa_ATTRIBUTE_DATE11 DATETIME, @ld_sa_ATTRIBUTE_DATE12 DATETIME, @lc_sa_E_Mail NVARCHAR(320), @lc_sa_Delivery_Channel NVARCHAR(30), @lc_sa_Bank_Instruction_1 NVARCHAR(30), @lc_sa_Bank_Instruction_2 NVARCHAR(30), @lc_sa_Bank_Instruction NVARCHAR(255), @lc_sa_Settlement_Priority NVARCHAR(30), @lc_sa_Payment_Text_Message_1 NVARCHAR(150), @lc_sa_Payment_Text_Message_2 NVARCHAR(150), @lc_sa_Payment_Text_Message_3 NVARCHAR(150), @lc_sa_Payee_Service_Level NVARCHAR(30), @lc_sa_Pay_Each_Document_Alone NVARCHAR(500), @lc_sa_Bank_Charge_Bearer NVARCHAR(30), @lc_sa_Payment_Reason NVARCHAR(30), @lc_sa_Payment_Reason_Comments NVARCHAR(240), @lc_sa_Delivery_Method NVARCHAR(30), @lc_sa_Remittance_E_Mail NVARCHAR(255), @lc_sa_Remittance_Fax NVARCHAR(15), @lc_sa_Creation_Date DATETIME, @lc_sa_Last_Update_Date DATETIME, @lc_sa_Created_By VARCHAR (4000), @lc_sa_Updated_By VARCHAR (4000), @lc_sa_file_geo VARCHAR (1000)
                    -- For Supplier Site
                    DECLARE @ln_ss_Record_ID INT, @lc_ss_Process_Flag NVARCHAR(10), @lc_ss_Error_Message NVARCHAR(4000), @lc_ss_Oracle_Process_Flag NVARCHAR(10), @lc_ss_Oracle_Error_Message NVARCHAR(4000), @lc_ss_Batch_ID NVARCHAR(200), @lc_ss_load_Batch_ID NVARCHAR(200), @ln_ss_Source_Record_ID INT, @lc_ss_Import_Action NVARCHAR(10), @lc_ss_Supplier_Name NVARCHAR(360), @lc_ss_Procurement_BU NVARCHAR(500), @lc_ss_Address_Name NVARCHAR(500), @lc_ss_Supplier_Site NVARCHAR(500), @lc_ss_Supplier_Site_New NVARCHAR(500), @lc_ss_Inactive_Date DATETIME, @lc_ss_Sourcing_only NVARCHAR(500), @lc_ss_Purchasing NVARCHAR(1), @lc_ss_Procurement_card NVARCHAR(1), @lc_ss_Pay NVARCHAR(500), @lc_ss_Primary_Pay NVARCHAR(500), @lc_ss_Income_tax_reporting_site NVARCHAR(500), @lc_ss_Alternate_Site_Name NVARCHAR(320), @lc_ss_Customer_Number NVARCHAR(25), @lc_ss_B2B_Communication_Method NVARCHAR(500), @lc_ss_B2B_Supplier_Site_Code NVARCHAR(256), @lc_ss_Communication_Method NVARCHAR(500), @lc_ss_E_Mail NVARCHAR(2000), @lc_ss_Fax_Country_Code NVARCHAR(10), @lc_ss_Fax_Area_Code NVARCHAR(10), @lc_ss_Fax NVARCHAR(15), @lc_ss_Hold_all_new_purchasing_documents NVARCHAR(1), @lc_ss_Hold_Reason NVARCHAR(240), @lc_ss_Carrier NVARCHAR(360), @lc_ss_Mode_of_Transport NVARCHAR(30), @lc_ss_Service_Level NVARCHAR(30), @lc_ss_Freight_Terms NVARCHAR(25), @lc_ss_Pay_on_receipt NVARCHAR(25), @lc_ss_FOB NVARCHAR(25), @lc_ss_Country_of_Origin NVARCHAR(2), @lc_ss_Buyer_Managed_Transportation NVARCHAR(1), @lc_ss_Pay_on_use NVARCHAR(1), @lc_ss_Aging_Onset_Point NVARCHAR(30), @lc_ss_Aging_Period_Days DECIMAL(25,8), @lc_ss_Consumption_Advice_Frequency NVARCHAR(30), @lc_ss_Consumption_Advice_Summary NVARCHAR(30), @lc_ss_Default_Pay_Site NVARCHAR(15), @lc_ss_Invoice_Summary_Level NVARCHAR(25), @lc_ss_Gapless_invoice_numbering NVARCHAR(1), @lc_ss_Selling_Company_Identifier NVARCHAR(10), @lc_ss_Create_debit_memo_from_return NVARCHAR(25), @lc_ss_Ship_to_Exception_Action NVARCHAR(25), @lc_ss_Receipt_Routing DECIMAL(25,8), @lc_ss_Over_receipt_Tolerance DECIMAL(25,8), @lc_ss_Over_receipt_Action NVARCHAR(25), @lc_ss_Early_Receipt_Tolerance_in_Days DECIMAL(25,8), @lc_ss_Late_Receipt_Tolerance_in_Days DECIMAL(25,8), @lc_ss_Allow_Substitute_Receipts NVARCHAR(1), @lc_ss_Allow_unordered_receipts NVARCHAR(1), @lc_ss_Receipt_Date_Exception NVARCHAR(25), @lc_ss_Invoice_Currency NVARCHAR(500), @lc_ss_Invoice_Amount_Limit DECIMAL(25,8), @lc_ss_Invoice_Match_Option NVARCHAR(25), @lc_ss_Match_Approval_Level NVARCHAR(1), @lc_ss_Payment_Currency NVARCHAR(500), @lc_ss_Payment_Priority DECIMAL(25,8), @lc_ss_Pay_Group NVARCHAR(25), @lc_ss_Quantity_Tolerances NVARCHAR(255), @lc_ss_Amount_Tolerance NVARCHAR(255), @lc_ss_Hold_All_Invoices NVARCHAR(1), @lc_ss_Hold_Unmatched_Invoices NVARCHAR(1), @lc_ss_Hold_Unvalidated_Invoices NVARCHAR(1), @lc_ss_Payment_Hold_By DECIMAL(25,8), @lc_ss_Payment_Hold_Date DATETIME, @lc_ss_Payment_Hold_Reason NVARCHAR(240), @lc_ss_Payment_Terms NVARCHAR(500), @lc_ss_Terms_Date_Basis NVARCHAR(500), @lc_ss_Pay_Date_Basis NVARCHAR(25), @lc_ss_Bank_Charge_Deduction_Type NVARCHAR(25), @lc_ss_Always_Take_Discount NVARCHAR(500), @lc_ss_Exclude_Freight_From_Discount NVARCHAR(1), @lc_ss_Exclude_Tax_From_Discount NVARCHAR(1), @lc_ss_Create_Interest_Invoices NVARCHAR(1), @lc_ss_Vat_Code NVARCHAR(30), @lc_ss_Tax_Registration_Number NVARCHAR(20), @lc_ss_Payment_Method NVARCHAR(500), @lc_ss_Delivery_Channel NVARCHAR(500), @lc_ss_Bank_Instruction_1 NVARCHAR(500), @lc_ss_Bank_Instruction_2 NVARCHAR(500), @lc_ss_Bank_Instruction NVARCHAR(500), @lc_ss_Settlement_Priority NVARCHAR(500), @lc_ss_Payment_Text_Message_1 NVARCHAR(150), @lc_ss_Payment_Text_Message_2 NVARCHAR(150), @lc_ss_Payment_Text_Message_3 NVARCHAR(150), @lc_ss_Bank_Charge_Bearer NVARCHAR(500), @lc_ss_Payment_Reason NVARCHAR(500), @lc_ss_Payment_Reason_Comments NVARCHAR(240), @lc_ss_Delivery_Method NVARCHAR(500), @lc_ss_Remittance_E_Mail NVARCHAR(255), @lc_ss_Remittance_Fax NVARCHAR(15), @lc_ss_ATTRIBUTE_CATEGORY NVARCHAR(30), @lc_ss_ATTRIBUTE1 NVARCHAR(150), @lc_ss_ATTRIBUTE2 NVARCHAR(150), @lc_ss_ATTRIBUTE3 NVARCHAR(150), @lc_ss_ATTRIBUTE4 NVARCHAR(150), @lc_ss_ATTRIBUTE5 NVARCHAR(150), @lc_ss_ATTRIBUTE6 NVARCHAR(150), @lc_ss_ATTRIBUTE7 NVARCHAR(150), @lc_ss_ATTRIBUTE8 NVARCHAR(150), @lc_ss_ATTRIBUTE9 NVARCHAR(150), @lc_ss_ATTRIBUTE10 NVARCHAR(150), @lc_ss_ATTRIBUTE11 NVARCHAR(150), @lc_ss_ATTRIBUTE12 NVARCHAR(150), @lc_ss_ATTRIBUTE13 NVARCHAR(150), @lc_ss_ATTRIBUTE14 NVARCHAR(150), @lc_ss_ATTRIBUTE15 NVARCHAR(150), @lc_ss_ATTRIBUTE16 NVARCHAR(150), @lc_ss_ATTRIBUTE17 NVARCHAR(150), @lc_ss_ATTRIBUTE18 NVARCHAR(150), @lc_ss_ATTRIBUTE19 NVARCHAR(150), @lc_ss_ATTRIBUTE20 NVARCHAR(150), @lc_ss_ATTRIBUTE_DATE1 DATETIME, @lc_ss_ATTRIBUTE_DATE2 DATETIME, @lc_ss_ATTRIBUTE_DATE3 DATETIME, @lc_ss_ATTRIBUTE_DATE4 DATETIME, @lc_ss_ATTRIBUTE_DATE5 DATETIME, @lc_ss_ATTRIBUTE_DATE6 DATETIME, @lc_ss_ATTRIBUTE_DATE7 DATETIME, @lc_ss_ATTRIBUTE_DATE8 DATETIME, @lc_ss_ATTRIBUTE_DATE9 DATETIME, @lc_ss_ATTRIBUTE_DATE10 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP1 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP2 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP3 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP4 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP5 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP6 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP7 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP8 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP9 DATETIME, @lc_ss_ATTRIBUTE_TIMESTAMP10 DATETIME, @ln_ss_ATTRIBUTE_NUMBER1 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER2 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER3 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER4 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER5 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER6 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER7 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER8 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER9 DECIMAL(25,8), @ln_ss_ATTRIBUTE_NUMBER10 DECIMAL(25,8), @lc_ss_GLOBAL_ATTRIBUTE_CATEGORY NVARCHAR(30), @lc_ss_GLOBAL_ATTRIBUTE1 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE2 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE3 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE4 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE5 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE6 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE7 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE8 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE9 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE10 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE11 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE12 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE13 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE14 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE15 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE16 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE17 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE18 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE19 NVARCHAR(150), @lc_ss_GLOBAL_ATTRIBUTE20 NVARCHAR(150), @ld_ss_GLOBAL_ATTRIBUTE_DATE1 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE2 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE3 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE4 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE5 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE6 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE7 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE8 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE9 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_DATE10 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP1 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP2 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP3 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP4 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP5 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP6 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP7 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP8 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP9 DATETIME, @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP10 DATETIME, @ln_ss_GLOBAL_ATTRIBUTE_NUMBER1 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER2 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER3 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER4 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER5 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER6 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER7 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER8 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER9 DECIMAL(25,8), @ln_ss_GLOBAL_ATTRIBUTE_NUMBER10 DECIMAL(25,8), @lc_ss_Required_Acknowledgement NVARCHAR(30) , @ln_ss_Acknowledge_Within_Days INT , @lc_ss_Invoice_Channel NVARCHAR(30) , @lc_ss_Payee_Service_Level NVARCHAR(30), @lc_ss_Pay_Each_Document_Alone NVARCHAR(500), @lc_ss_Creation_Date DATETIME, @lc_ss_Last_Update_Date DATETIME, @lc_ss_Created_By VARCHAR (4000), @lc_ss_Updated_By VARCHAR (4000), @lc_ss_file_geo VARCHAR (1000)
                    -- For Supplier Site Assignment
                    DECLARE @ln_ssa_Record_ID INT, @lc_ssa_Process_Flag NVARCHAR(10), @lc_ssa_Error_Message NVARCHAR(4000), @lc_ssa_Batch_ID NVARCHAR(200), @ln_ssa_Source_Record_ID INT, @lc_ssa_Import_Action NVARCHAR(500), @lc_ssa_Supplier_Name NVARCHAR(360), @lc_ssa_Supplier_Site NVARCHAR(500), @lc_ssa_Procurement_BU NVARCHAR(240), @lc_ssa_Client_BU NVARCHAR(240), @lc_ssa_Bill_to_BU NVARCHAR(240), @lc_ssa_Ship_to_Location NVARCHAR(500), @lc_ssa_Bill_to_Location NVARCHAR(500), @lc_ssa_Use_Withholding_Tax NVARCHAR(500), @lc_ssa_Withholding_Tax_Group NVARCHAR(500), @lc_ssa_Liability_Distribution NVARCHAR(800), @lc_ssa_Prepayment_Distribution NVARCHAR(800), @lc_ssa_Bills_Payable_Distribution NVARCHAR(800), @lc_ssa_Distribution_Set NVARCHAR(50), @lc_ssa_Inactive_Date DATETIME, @lc_ssa_Creation_Date DATETIME, @lc_ssa_Last_Update_Date DATETIME, @lc_ssa_Created_By VARCHAR (4000), @lc_ssa_Updated_By VARCHAR (4000)
                    -- For Supplier Contacts
                    DECLARE @ln_sc_Record_ID INT, @lc_sc_Process_Flag NVARCHAR(10), @lc_sc_Error_Message NVARCHAR(4000), @lc_sc_Batch_ID NVARCHAR(200), @ln_sc_Source_Record_ID INT, @lc_sc_Import_Action NVARCHAR(10), @lc_sc_Supplier_Name NVARCHAR(30), @lc_sc_Prefix NVARCHAR(60), @lc_sc_First_Name NVARCHAR(150), @lc_sc_First_Name_New NVARCHAR(150), @lc_sc_Middle_Name NVARCHAR(60), @lc_sc_Last_Name NVARCHAR(150), @lc_sc_Last_Name_New NVARCHAR(150), @lc_sc_Job_Title NVARCHAR(100), @lc_sc_Administrative_Contact NVARCHAR(1), @lc_sc_E_Mail NVARCHAR(2000), @lc_sc_E_Mail_New NVARCHAR(2000), @lc_sc_Phone_Country_Code NVARCHAR(10), @lc_sc_Phone_Area_Code NVARCHAR(10), @lc_sc_Phone NVARCHAR(40), @lc_sc_Phone_Extension NVARCHAR(20), @lc_sc_Fax_Country_Code NVARCHAR(20), @lc_sc_Fax_Area_Code NVARCHAR(10), @lc_sc_Fax NVARCHAR(40), @lc_sc_Mobile_Country_Code NVARCHAR(20), @lc_sc_Mobile_Area_Code NVARCHAR(10), @lc_sc_Mobile NVARCHAR(40), @lc_sc_Inactive_Date DATETIME, @lc_sc_ATTRIBUTE_CATEGORY NVARCHAR(30), @lc_sc_ATTRIBUTE1 NVARCHAR(150), @lc_sc_ATTRIBUTE2 NVARCHAR(150), @lc_sc_ATTRIBUTE3 NVARCHAR(150), @lc_sc_ATTRIBUTE4 NVARCHAR(150), @lc_sc_ATTRIBUTE5 NVARCHAR(150), @lc_sc_ATTRIBUTE6 NVARCHAR(150), @lc_sc_ATTRIBUTE7 NVARCHAR(150), @lc_sc_ATTRIBUTE8 NVARCHAR(150), @lc_sc_ATTRIBUTE9 NVARCHAR(150), @lc_sc_ATTRIBUTE10 NVARCHAR(150), @lc_sc_ATTRIBUTE11 NVARCHAR(150), @lc_sc_ATTRIBUTE12 NVARCHAR(150), @lc_sc_ATTRIBUTE13 NVARCHAR(150), @lc_sc_ATTRIBUTE14 NVARCHAR(150), @lc_sc_ATTRIBUTE15 NVARCHAR(150), @lc_sc_ATTRIBUTE16 NVARCHAR(150), @lc_sc_ATTRIBUTE17 NVARCHAR(150), @lc_sc_ATTRIBUTE18 NVARCHAR(150), @lc_sc_ATTRIBUTE19 NVARCHAR(150), @lc_sc_ATTRIBUTE20 NVARCHAR(150), @lc_sc_ATTRIBUTE21 NVARCHAR(150), @lc_sc_ATTRIBUTE22 NVARCHAR(150), @lc_sc_ATTRIBUTE23 NVARCHAR(150), @lc_sc_ATTRIBUTE24 NVARCHAR(150), @lc_sc_ATTRIBUTE25 NVARCHAR(150), @lc_sc_ATTRIBUTE26 NVARCHAR(150), @lc_sc_ATTRIBUTE27 NVARCHAR(150), @lc_sc_ATTRIBUTE28 NVARCHAR(150), @lc_sc_ATTRIBUTE29 NVARCHAR(150), @lc_sc_ATTRIBUTE30 NVARCHAR(255), @ln_sc_ATTRIBUTE_NUMBER1 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER2 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER3 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER4 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER5 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER6 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER7 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER8 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER9 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER10 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER11 DECIMAL(25,8), @ln_sc_ATTRIBUTE_NUMBER12 DECIMAL(25,8), @ld_sc_ATTRIBUTE_DATE1 DATETIME, @ld_sc_ATTRIBUTE_DATE2 DATETIME, @ld_sc_ATTRIBUTE_DATE3 DATETIME, @ld_sc_ATTRIBUTE_DATE4 DATETIME, @ld_sc_ATTRIBUTE_DATE5 DATETIME, @ld_sc_ATTRIBUTE_DATE6 DATETIME, @ld_sc_ATTRIBUTE_DATE7 DATETIME, @ld_sc_ATTRIBUTE_DATE8 DATETIME, @ld_sc_ATTRIBUTE_DATE9 DATETIME, @ld_sc_ATTRIBUTE_DATE10 DATETIME, @ld_sc_ATTRIBUTE_DATE11 DATETIME, @ld_sc_ATTRIBUTE_DATE12 DATETIME, @lc_sc_Creation_Date DATETIME, @lc_sc_Last_Update_Date DATETIME, @lc_sc_Created_By NVARCHAR(4000), @lc_sc_Updated_By NVARCHAR(4000)
                    -- For Supplier Contact Address
                    DECLARE @ln_sca_Record_ID INT , @lc_sca_Process_Flag NVARCHAR(1), @lc_sca_Error_Message NVARCHAR(4000), @lc_sca_Batch_ID NVARCHAR(200), @ln_sca_Source_Record_ID INT, @lc_sca_Import_Action NVARCHAR(10), @lc_sca_Supplier_Name NVARCHAR(360), @lc_sca_Address_Name NVARCHAR(15), @lc_sca_First_Name NVARCHAR(150), @lc_sca_Last_Name NVARCHAR(150), @lc_sca_E_Mail NVARCHAR(2000), @lc_sca_Creation_Date DATETIME, @lc_sca_Last_Update_Date DATETIME, @lc_sca_Created_By VARCHAR (4000), @lc_sca_Updated_By VARCHAR (4000) 
                    
					DECLARE @lc_rice_id NVARCHAR(100) = 'P2P-SP-CNV003,005,006,007',@lc_source NVARCHAR(100) = 'HISCOX_POZ_SUPPLIER_ENT_VAL_P',@lc_calling_object NVARCHAR(100) = 'HISCOX Supplier Data Migration SSIS Package'
					
					PRINT 'p_new_batch_id  '+ ISNULL(CAST(@p_new_batch_id AS VARCHAR),'')
					PRINT 'p_reprocess_all  '+ ISNULL(CAST(@p_reprocess_all AS VARCHAR),'')
					PRINT 'p_new_file  '+ ISNULL(CAST(@p_new_file AS VARCHAR),'')
					PRINT 'p_batch_id  '+ ISNULL(CAST(@p_batch_id AS VARCHAR),'')
					PRINT 'p_load_batch_id  '+ ISNULL(CAST(@p_load_batch_id AS VARCHAR),'')
					
					IF ISNULL(@p_new_batch_id,'') <> '' 
					BEGIN
					
						EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_new_batch_id
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_new_batch_id
													,@p_sec_record_id  = @p_load_batch_id
													,@p_ter_record_id  = @p_batch_id
													,@p_err_code       = 'DEBUG'
													,@p_err_column     = 'DEBUG'  
													,@p_err_value      = 'DEBUG'  
													,@p_err_desc       = 'ENT_VAL_P Procedure Start'   
													,@p_rect_action    = NULL; 
						
						
						UPDATE hiscox_poz_suppliers_int_stg
						SET batch_id		=@p_new_batch_id,
						last_update_date	=GETDATE(),
						updated_by			=USER_NAME()
						WHERE 1=1
						AND process_flag	=	(CASE @p_new_file WHEN 'Y' THEN 'N' ELSE (CASE @p_reprocess_all WHEN 'Y' THEN process_flag ELSE 'E' END) END )
						AND (batch_id		=	(CASE @p_batch_id WHEN '' THEN batch_id ELSE @p_batch_id END) OR @p_batch_id ='')
						AND load_batch_id	= 	(CASE @p_new_file WHEN 'Y' THEN @p_new_batch_id ELSE (CASE @p_load_batch_id WHEN '' THEN load_batch_id ELSE @p_load_batch_id END) END)
						
						UPDATE hiscox_poz_supplier_addresses_int_stg
						SET batch_id		=@p_new_batch_id,
						last_update_date	=GETDATE(),
						updated_by			=USER_NAME()
						WHERE 1=1
						AND process_flag	=	(CASE @p_new_file WHEN 'Y' THEN 'N' ELSE (CASE @p_reprocess_all WHEN 'Y' THEN process_flag ELSE 'E' END) END )
						AND (batch_id		=	(CASE @p_batch_id WHEN '' THEN batch_id ELSE @p_batch_id END) OR @p_batch_id ='')
						AND load_batch_id	= 	(CASE @p_new_file WHEN 'Y' THEN @p_new_batch_id ELSE (CASE @p_load_batch_id WHEN '' THEN load_batch_id ELSE @p_load_batch_id END) END)
						
						UPDATE hiscox_poz_supplier_sites_int_stg
						SET batch_id		=@p_new_batch_id,
						last_update_date	=GETDATE(),
						updated_by			=USER_NAME()
						WHERE 1=1
						AND process_flag	=	(CASE @p_new_file WHEN 'Y' THEN 'N' ELSE (CASE @p_reprocess_all WHEN 'Y' THEN process_flag ELSE 'E' END) END )
						AND (batch_id		=	(CASE @p_batch_id WHEN '' THEN batch_id ELSE @p_batch_id END) OR @p_batch_id ='')
						AND load_batch_id	= 	(CASE @p_new_file WHEN 'Y' THEN @p_new_batch_id ELSE (CASE @p_load_batch_id WHEN '' THEN load_batch_id ELSE @p_load_batch_id END) END)
						
						
						UPDATE hiscox_poz_site_assignments_int_stg
						SET batch_id		=@p_new_batch_id,
						last_update_date	=GETDATE(),
						updated_by			=USER_NAME()
						WHERE 1=1
						AND process_flag	=	(CASE @p_new_file WHEN 'Y' THEN 'N' ELSE (CASE @p_reprocess_all WHEN 'Y' THEN process_flag ELSE 'E' END) END )
						AND (batch_id		=	(CASE @p_batch_id WHEN '' THEN batch_id ELSE @p_batch_id END) OR @p_batch_id ='')
						AND load_batch_id	= 	(CASE @p_new_file WHEN 'Y' THEN @p_new_batch_id ELSE (CASE @p_load_batch_id WHEN '' THEN load_batch_id ELSE @p_load_batch_id END) END)
						
						/*
						UPDATE hiscox_poz_sup_contacts_int_stg
						SET batch_id		=@p_new_batch_id,
						last_update_date	=GETDATE(),
						updated_by			=USER_NAME()
						WHERE 1=1
						AND process_flag	=	(CASE @p_new_file WHEN 'Y' THEN 'N' ELSE (CASE @p_reprocess_all WHEN 'Y' THEN process_flag ELSE 'E' END) END )
						AND (batch_id		=	(CASE @p_batch_id WHEN '' THEN batch_id ELSE @p_batch_id END) OR @p_batch_id ='')
						AND load_batch_id	= 	(CASE @p_new_file WHEN 'Y' THEN @p_new_batch_id ELSE (CASE @p_load_batch_id WHEN '' THEN load_batch_id ELSE @p_load_batch_id END) END)
						
						UPDATE hiscox_poz_supp_contact_addresses_int_stg
						SET batch_id		=@p_new_batch_id,
						last_update_date	=GETDATE(),
						updated_by			=USER_NAME()
						WHERE 1=1
						AND process_flag	=	(CASE @p_new_file WHEN 'Y' THEN 'N' ELSE (CASE @p_reprocess_all WHEN 'Y' THEN process_flag ELSE 'E' END) END )
						AND (batch_id		=	(CASE @p_batch_id WHEN '' THEN batch_id ELSE @p_batch_id END) OR @p_batch_id ='')
						AND load_batch_id	= 	(CASE @p_new_file WHEN 'Y' THEN @p_new_batch_id ELSE (CASE @p_load_batch_id WHEN '' THEN load_batch_id ELSE @p_load_batch_id END) END)
						
						*/
						
						DECLARE cur_suppliers_FBDI CURSOR LOCAL FOR 
						SELECT Record_ID ,Process_Flag ,Error_Message,Oracle_Process_Flag,Oracle_Error_Message,Batch_ID,Load_Batch_id,Source_Record_ID ,Import_Action,Supplier_Name,Supplier_Name_New,Supplier_Number,Alternate_Name,Tax_Organization_Type,Supplier_Type,Inactive_Date ,Business_Relationship,Parent_Supplier,Alias,D_U_N_S_Number,One_time_supplier,Customer_Number,SIC,National_Insurance_Number,Corporate_Web_Site,Chief_Executive_Title,Chief_Executive_Name,Business_Classifications_Not_Applicable,Taxpayer_Country,Taxpayer_ID,Federal_reportable,Federal_Income_Tax_Type,State_reportable,Tax_Reporting_Name,Name_Control,Tax_Verification_Date ,Use_withholding_tax,Withholding_Tax_Group,Vat_Code,Tax_Registration_Number ,Auto_Tax_Calc_Override,Payment_Method,Delivery_Channel,Bank_Instruction_1,Bank_Instruction_2,Bank_Instruction ,Settlement_Priority,Payment_Text_Message_1,Payment_Text_Message_2,Payment_Text_Message_3,Bank_Charge_Bearer,Payment_Reason,Payment_Reason_Comments,Payment_Format,ATTRIBUTE_CATEGORY,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5,ATTRIBUTE6,ATTRIBUTE7,ATTRIBUTE8,ATTRIBUTE9,ATTRIBUTE10,ATTRIBUTE11,ATTRIBUTE12,ATTRIBUTE13,ATTRIBUTE14,ATTRIBUTE15,ATTRIBUTE16,ATTRIBUTE17,ATTRIBUTE18,ATTRIBUTE19,ATTRIBUTE20,ATTRIBUTE_DATE1 ,ATTRIBUTE_DATE2 ,ATTRIBUTE_DATE3 ,ATTRIBUTE_DATE4 ,ATTRIBUTE_DATE5 ,ATTRIBUTE_DATE6 ,ATTRIBUTE_DATE7 ,ATTRIBUTE_DATE8 ,ATTRIBUTE_DATE9 ,ATTRIBUTE_DATE10 ,ATTRIBUTE_TIMESTAMP1 ,ATTRIBUTE_TIMESTAMP2 ,ATTRIBUTE_TIMESTAMP3 ,ATTRIBUTE_TIMESTAMP4 ,ATTRIBUTE_TIMESTAMP5 ,ATTRIBUTE_TIMESTAMP6 ,ATTRIBUTE_TIMESTAMP7 ,ATTRIBUTE_TIMESTAMP8 ,ATTRIBUTE_TIMESTAMP9 ,ATTRIBUTE_TIMESTAMP10 ,ATTRIBUTE_NUMBER1 ,ATTRIBUTE_NUMBER2 ,ATTRIBUTE_NUMBER3 ,ATTRIBUTE_NUMBER4 ,ATTRIBUTE_NUMBER5 ,ATTRIBUTE_NUMBER6 ,ATTRIBUTE_NUMBER7 ,ATTRIBUTE_NUMBER8 ,ATTRIBUTE_NUMBER9 ,ATTRIBUTE_NUMBER10 ,GLOBAL_ATTRIBUTE_CATEGORY,GLOBAL_ATTRIBUTE1,GLOBAL_ATTRIBUTE2,GLOBAL_ATTRIBUTE3,GLOBAL_ATTRIBUTE4,GLOBAL_ATTRIBUTE5,GLOBAL_ATTRIBUTE6,GLOBAL_ATTRIBUTE7,GLOBAL_ATTRIBUTE8,GLOBAL_ATTRIBUTE9,GLOBAL_ATTRIBUTE10,GLOBAL_ATTRIBUTE11,GLOBAL_ATTRIBUTE12,GLOBAL_ATTRIBUTE13,GLOBAL_ATTRIBUTE14,GLOBAL_ATTRIBUTE15,GLOBAL_ATTRIBUTE16,GLOBAL_ATTRIBUTE17,GLOBAL_ATTRIBUTE18,GLOBAL_ATTRIBUTE19,GLOBAL_ATTRIBUTE20,GLOBAL_ATTRIBUTE_DATE1 ,GLOBAL_ATTRIBUTE_DATE2 ,GLOBAL_ATTRIBUTE_DATE3 ,GLOBAL_ATTRIBUTE_DATE4 ,GLOBAL_ATTRIBUTE_DATE5 ,GLOBAL_ATTRIBUTE_DATE6 ,GLOBAL_ATTRIBUTE_DATE7 ,GLOBAL_ATTRIBUTE_DATE8 ,GLOBAL_ATTRIBUTE_DATE9 ,GLOBAL_ATTRIBUTE_DATE10 ,GLOBAL_ATTRIBUTE_TIMESTAMP1 ,GLOBAL_ATTRIBUTE_TIMESTAMP2 ,GLOBAL_ATTRIBUTE_TIMESTAMP3 ,GLOBAL_ATTRIBUTE_TIMESTAMP4 ,GLOBAL_ATTRIBUTE_TIMESTAMP5 ,GLOBAL_ATTRIBUTE_TIMESTAMP6 ,GLOBAL_ATTRIBUTE_TIMESTAMP7 ,GLOBAL_ATTRIBUTE_TIMESTAMP8 ,GLOBAL_ATTRIBUTE_TIMESTAMP9 ,GLOBAL_ATTRIBUTE_TIMESTAMP10 ,GLOBAL_ATTRIBUTE_NUMBER1 ,GLOBAL_ATTRIBUTE_NUMBER2 ,GLOBAL_ATTRIBUTE_NUMBER3 ,GLOBAL_ATTRIBUTE_NUMBER4 ,GLOBAL_ATTRIBUTE_NUMBER5 ,GLOBAL_ATTRIBUTE_NUMBER6 ,GLOBAL_ATTRIBUTE_NUMBER7 ,GLOBAL_ATTRIBUTE_NUMBER8 ,GLOBAL_ATTRIBUTE_NUMBER9 ,GLOBAL_ATTRIBUTE_NUMBER10 ,Registry_ID,Payee_Service_Level,Pay_Each_Document_Alone,Delivery_Method,Remittance_E_mail ,Remittance_Fax,Creation_Date ,Last_Update_Date ,Created_By ,Updated_By
							FROM hiscox_poz_suppliers_int_stg
							WHERE 1=1
							--AND process_flag      = 'N'
							AND batch_id            = @p_new_batch_id
							ORDER BY (CASE WHEN ISNULL(Taxpayer_id,'') ='' THEN 2 ELSE 1 END)
						
						
	
							BEGIN
								OPEN cur_suppliers_FBDI  
								FETCH NEXT FROM cur_suppliers_FBDI INTO @ln_sup_Record_ID , @lc_sup_Process_Flag , @lc_sup_Error_Message, @lc_sup_Oracle_Process_Flag, @lc_sup_Oracle_Error_Message, @lc_sup_Batch_ID, @lc_sup_load_Batch_ID, @ln_sup_Source_Record_ID , @lc_sup_Import_Action, @lc_sup_Supplier_Name, @lc_sup_Supplier_Name_New, @lc_sup_Supplier_Number, @lc_sup_Alternate_Name, @lc_sup_Tax_Organization_Type, @lc_sup_Supplier_Type, @lc_sup_Inactive_Date , @lc_sup_Business_Relationship, @lc_sup_Parent_Supplier, @lc_sup_Alias, @lc_sup_D_U_N_S_Number, @lc_sup_One_time_supplier, @lc_sup_Customer_Number, @lc_sup_SIC, @lc_sup_National_Insurance_Number, @lc_sup_Corporate_Web_Site, @lc_sup_Chief_Executive_Title, @lc_sup_Chief_Executive_Name, @lc_sup_Business_Classifications_Not_Applicable, @lc_sup_Taxpayer_Country, @lc_sup_Taxpayer_ID, @lc_sup_Federal_reportable, @lc_sup_Federal_Income_Tax_Type, @lc_sup_State_reportable, @lc_sup_Tax_Reporting_Name, @lc_sup_Name_Control, @lc_sup_Tax_Verification_Date , @lc_sup_Use_withholding_tax, @lc_sup_Withholding_Tax_Group, @lc_sup_Vat_Code, @lc_sup_Tax_Registration_Number , @lc_sup_Auto_Tax_Calc_Override, @lc_sup_Payment_Method, @lc_sup_Delivery_Channel, @lc_sup_Bank_Instruction_1, @lc_sup_Bank_Instruction_2, @lc_sup_Bank_Instruction , @lc_sup_Settlement_Priority, @lc_sup_Payment_Text_Message_1, @lc_sup_Payment_Text_Message_2, @lc_sup_Payment_Text_Message_3, @lc_sup_Bank_Charge_Bearer, @lc_sup_Payment_Reason, @lc_sup_Payment_Reason_Comments, @lc_sup_Payment_Format, @lc_sup_ATTRIBUTE_CATEGORY, @lc_sup_ATTRIBUTE1, @lc_sup_ATTRIBUTE2, @lc_sup_ATTRIBUTE3, @lc_sup_ATTRIBUTE4, @lc_sup_ATTRIBUTE5, @lc_sup_ATTRIBUTE6, @lc_sup_ATTRIBUTE7, @lc_sup_ATTRIBUTE8, @lc_sup_ATTRIBUTE9, @lc_sup_ATTRIBUTE10, @lc_sup_ATTRIBUTE11, @lc_sup_ATTRIBUTE12, @lc_sup_ATTRIBUTE13, @lc_sup_ATTRIBUTE14, @lc_sup_ATTRIBUTE15, @lc_sup_ATTRIBUTE16, @lc_sup_ATTRIBUTE17, @lc_sup_ATTRIBUTE18, @lc_sup_ATTRIBUTE19, @lc_sup_ATTRIBUTE20, @lc_sup_ATTRIBUTE_DATE1 , @lc_sup_ATTRIBUTE_DATE2 , @lc_sup_ATTRIBUTE_DATE3 , @lc_sup_ATTRIBUTE_DATE4 , @lc_sup_ATTRIBUTE_DATE5 , @lc_sup_ATTRIBUTE_DATE6 , @lc_sup_ATTRIBUTE_DATE7 , @lc_sup_ATTRIBUTE_DATE8 , @lc_sup_ATTRIBUTE_DATE9 , @lc_sup_ATTRIBUTE_DATE10 , @ld_sup_ATTRIBUTE_TIMESTAMP1 , @ld_sup_ATTRIBUTE_TIMESTAMP2 , @ld_sup_ATTRIBUTE_TIMESTAMP3 , @ld_sup_ATTRIBUTE_TIMESTAMP4 , @ld_sup_ATTRIBUTE_TIMESTAMP5 , @ld_sup_ATTRIBUTE_TIMESTAMP6 , @ld_sup_ATTRIBUTE_TIMESTAMP7 , @ld_sup_ATTRIBUTE_TIMESTAMP8 , @ld_sup_ATTRIBUTE_TIMESTAMP9 , @ld_sup_ATTRIBUTE_TIMESTAMP10 , @ln_sup_ATTRIBUTE_NUMBER1 , @ln_sup_ATTRIBUTE_NUMBER2 , @ln_sup_ATTRIBUTE_NUMBER3 , @ln_sup_ATTRIBUTE_NUMBER4 , @ln_sup_ATTRIBUTE_NUMBER5 , @ln_sup_ATTRIBUTE_NUMBER6 , @ln_sup_ATTRIBUTE_NUMBER7 , @ln_sup_ATTRIBUTE_NUMBER8 , @ln_sup_ATTRIBUTE_NUMBER9 , @ln_sup_ATTRIBUTE_NUMBER10 , @lc_sup_GLOBAL_ATTRIBUTE_CATEGORY, @lc_sup_GLOBAL_ATTRIBUTE1, @lc_sup_GLOBAL_ATTRIBUTE2, @lc_sup_GLOBAL_ATTRIBUTE3, @lc_sup_GLOBAL_ATTRIBUTE4, @lc_sup_GLOBAL_ATTRIBUTE5, @lc_sup_GLOBAL_ATTRIBUTE6, @lc_sup_GLOBAL_ATTRIBUTE7, @lc_sup_GLOBAL_ATTRIBUTE8, @lc_sup_GLOBAL_ATTRIBUTE9, @lc_sup_GLOBAL_ATTRIBUTE10, @lc_sup_GLOBAL_ATTRIBUTE11, @lc_sup_GLOBAL_ATTRIBUTE12, @lc_sup_GLOBAL_ATTRIBUTE13, @lc_sup_GLOBAL_ATTRIBUTE14, @lc_sup_GLOBAL_ATTRIBUTE15, @lc_sup_GLOBAL_ATTRIBUTE16, @lc_sup_GLOBAL_ATTRIBUTE17, @lc_sup_GLOBAL_ATTRIBUTE18, @lc_sup_GLOBAL_ATTRIBUTE19, @lc_sup_GLOBAL_ATTRIBUTE20, @lc_sup_GLOBAL_ATTRIBUTE_DATE1 , @lc_sup_GLOBAL_ATTRIBUTE_DATE2 , @lc_sup_GLOBAL_ATTRIBUTE_DATE3 , @lc_sup_GLOBAL_ATTRIBUTE_DATE4 , @lc_sup_GLOBAL_ATTRIBUTE_DATE5 , @lc_sup_GLOBAL_ATTRIBUTE_DATE6 , @lc_sup_GLOBAL_ATTRIBUTE_DATE7 , @lc_sup_GLOBAL_ATTRIBUTE_DATE8 , @lc_sup_GLOBAL_ATTRIBUTE_DATE9 , @lc_sup_GLOBAL_ATTRIBUTE_DATE10 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP1 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP2 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP3 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP4 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP5 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP6 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP7 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP8 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP9 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP10 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER1 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER2 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER3 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER4 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER5 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER6 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER7 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER8 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER9 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER10 , @lc_sup_Registry_ID, @lc_sup_Payee_Service_Level, @lc_sup_Pay_Each_Document_Alone, @lc_sup_Delivery_Method, @lc_sup_Remittance_E_mail , @lc_sup_Remittance_Fax, @lc_sup_Creation_Date , @lc_sup_Last_Update_Date , @lc_sup_Created_By , @lc_sup_Updated_By
								WHILE @@FETCH_STATUS = 0 
								BEGIN
								BEGIN TRY   -- Try Catch for Supplier FBDI
									SET @ln_erp 		= 0
									SET @lc_Err_Msg     = ''
									SET @ln_warning		= 0
									SET @lc_war_msg		= ''
									PRINT '--Supplier - ' + CAST(@lc_sup_Supplier_Name AS VARCHAR);
										-- Check if Supplier Name is not null
										
										IF NOT(@lc_sup_Import_Action IN ('CREATE','UPDATE'))
											BEGIN 
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Import Action can only be either CREATE or UPDATE. ',1,4000)
											END
											
										IF @lc_sup_supplier_name IS NOT NULL 
											BEGIN
												-- Check if Supplier is already created
												--EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_EXISTING_SUPPLIERS' , @lc_sup_Supplier_Name
												
												SET @ln_check = (
																SELECT  count(*) 
																FROM hiscox_lookup_ref_tbl
																WHERE 1					= 1
																AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
																AND UPPER(lookup_code)	= UPPER(@lc_sup_Supplier_Name)
																)
												
												
												IF @ln_check <> 0
												
													BEGIN
														SET @ln_erp = 1
													END
													
												ELSE
												
													BEGIN
														-- Check if Supplier Name is already available in current load
														SET @ln_check = (
																		SELECT  count(*) 
																		FROM hiscox_poz_suppliers_int_stg
																		WHERE 1				= 1
																		AND batch_id		=	@p_new_batch_id
																		AND record_id		<>	@ln_sup_Record_ID
																		AND process_flag	=	'S'
																		AND supplier_name	=	@lc_sup_Supplier_Name
																		)
														
														IF @ln_check <> 0 
															BEGIN
																--SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier '+@lc_sup_Supplier_Name+' already exists in the current run. ',1,4000)
																SET @ln_warning	= 1
																SET @lc_war_msg	= SUBSTRING(@lc_war_msg  +'Supplier already exists in the current run. ',1,4000)
															END
														ELSE
															BEGIN
																-- Check if Supplier Name is within the Oracle Column Length
																SET @ln_check = LEN(@lc_sup_Supplier_Name)
																IF @ln_check > 360 
																	BEGIN
																		SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Name length cannot exceed 360. ',1,4000)
																	END
															END
													END
												
											END
										ELSE
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Name Cannot be NULL. ',1,4000)
											END
										
										-- Check if Supplier Number is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Supplier_Number)
											IF @ln_check > 30 
												BEGIN
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Number length cannot exceed 30. ',1,4000)
												END
												
										-- Check for double quotes in the data
										IF CHARINDEX('"',@lc_sup_Supplier_Name,1)>0
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Name cannot have double quotes["]. ',1,4000)
											END
										
										IF CHARINDEX('"',@lc_sup_Alias,1)>0
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Name cannot have double quotes["]. ',1,4000)
											END
												
										-- Check if Tax Organization Type is Valid
										IF @lc_sup_Tax_Organization_Type IS NOT NULL 
											BEGIN
												EXEC @ln_check = hiscox_does_meaning_exist 'HISCOX_POZ_TAX_ORGANIZATION_TYPE' , @lc_sup_Tax_Organization_Type
														
												IF @ln_check = 0
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Tax Organization Type is Invalid. ',1,4000)
													END
											END
											
										-- Check if Tax Payer Country is valid
										IF @lc_sup_Taxpayer_Country IS NOT NULL 
											BEGIN
												EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_COUNTRY_CODE' , @lc_sup_Taxpayer_Country
														
												IF @ln_check = 0
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Tax Payer Country is Invalid. ',1,4000)
													END
											END
										
										-- Check if the Business Relationship is valid
										IF @lc_sup_Business_Relationship NOT IN ('PROSPECTIVE','SPEND_AUTHORIZED')
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Business Relationship is Invalid. ',1,4000)
											END
										
										-- Check if Taxpayer ID is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Taxpayer_ID)
											IF @ln_check > 30 
												BEGIN
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Taxpayer ID length cannot exceed 30. ',1,4000)
												END
										
										-- Check if the Federal Reportable Flag is valid
										IF NOT(@lc_sup_Federal_reportable IS NULL OR @lc_sup_Federal_reportable IN ('N','Y'))
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Federal reportable should either be N or Y. ',1,4000)
											END
										
										-- Check on Income Tax Type when the Federal Reportable Flag is Y
										IF @lc_sup_Federal_reportable = 'Y'
											BEGIN
												IF @lc_sup_Federal_Income_Tax_Type IS NOT NULL
													BEGIN
														--SET @ln_check = LEN(@lc_sup_Federal_Income_Tax_Type)
														--IF @ln_check > 10 
														--BEGIN
														--	SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Taxpayer ID '+@lc_sup_Taxpayer_ID+' length cannot exceed 10. ',1,4000)
														--END
														EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_INCOME_TAX_TYPES' , @lc_sup_Federal_Income_Tax_Type
												
														IF @ln_check = 0
												
														BEGIN
															SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Federal Income Tax Type is invalid. ',1,4000)
														END
													END
												ELSE
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Federal reportable is Y, hence  Federal Income Tax Type cannot be null. ',1,4000)
													END
											END
										ELSE
											
										-- Check on Income Tax Type if its null when the Federal Reportable Flag is N
										IF @lc_sup_Federal_reportable = 'N' OR @lc_sup_Federal_reportable IS NULL
											BEGIN
												IF @lc_sup_Federal_Income_Tax_Type IS NOT NULL
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Federal reportable is N, hence Federal Income Tax Type should be null. ',1,4000)
											END
											
										-- Check on State Reportable Flag
										IF NOT(@lc_sup_State_reportable IS NULL OR @lc_sup_State_reportable IN ('N','Y'))
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'State reportable should either be N or Y. ',1,4000)
											END
										
										
										-- Check on Tax Reporting Name when the State Reportable Flag is Y
										IF @lc_sup_State_reportable = 'Y'
											BEGIN
												IF @lc_sup_Tax_Reporting_Name IS NOT NULL
													BEGIN
														SET @ln_check = LEN(@lc_sup_Tax_Reporting_Name)
														IF @ln_check > 80 
														BEGIN
															SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Taxpayer ID length cannot exceed 80. ',1,4000)
														END
													END
												ELSE
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'State reportable is Y, hence  Tax Reporting Name cannot be null. ',1,4000)
													END
											END
										ELSE
											
										-- Check on Tax Reporting Name when the State Reportable Flag is N									
										IF @lc_sup_State_reportable = 'N' OR @lc_sup_State_reportable IS NULL
											BEGIN
												IF @lc_sup_Tax_Reporting_Name IS NOT NULL
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'State reportable is N, hence Tax Reporting Name should be null. ',1,4000)
											END
										
										-- Check on the Use Withholding Tax Flag
										IF NOT(@lc_sup_Use_withholding_tax IS NULL OR @lc_sup_Use_withholding_tax IN ('N','Y'))
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Withholding Tax should either be N or Y. ',1,4000)
											END
										
										
										-- Check on the Withholding Tax Group when the Use Withholding Tax Flag ='Y'
										IF @lc_sup_Use_withholding_tax = 'Y'
											BEGIN
												IF @lc_sup_Withholding_Tax_Group IS NOT NULL
													BEGIN
														SET @ln_check = LEN(@lc_sup_Withholding_Tax_Group)
														IF @ln_check > 25 
														BEGIN
															SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Withholding Tax group length cannot exceed 25. ',1,4000)
														END
													END
												ELSE
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Withholdiong Tax is Y, hence Withholding Tax Group cannot be null. ',1,4000)
													END
											END
										
										-- Check on the Withholding Tax Group when the Use Withholding Tax Flag ='N'
										IF @lc_sup_Use_withholding_tax = 'N' OR @lc_sup_Use_withholding_tax IS NULL 
											BEGIN
												IF @lc_sup_Withholding_Tax_Group IS NOT NULL
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Use withholding Tax Falg is N, hence Withholding Tax Group should be null. ',1,4000)
											END
										
										-- Check if Vat Code is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Vat_Code)
										IF @ln_check > 30 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Vat Code length cannot exceed 30. ',1,4000)
											END
										
										-- Check if Tax Registration Number is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Tax_Registration_Number)
										IF @ln_check > 20 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Tax Registration Number length cannot exceed 20. ',1,4000)
											END
										
										-- Check if Payment Method is valid
										
										IF  (@lc_sup_Payment_Method IS NOT NULL AND @lc_sup_Payment_Method <>'')
											BEGIN
												
												EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_VALID_PAY_METHOD' , @lc_sup_Payment_Method
												
												IF @ln_check = 0
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Payment Method is invalid. ',1,4000)
													END
											END
											
										-- Check if Settlement Priority is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Settlement_Priority)
										IF @ln_check > 20 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Settlement Priority length cannot exceed 20. ',1,4000)
											END
										
										
										-- Check if Registry ID is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Registry_ID)
										IF @ln_check > 30 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Registry Id length cannot exceed 30. ',1,4000)
											END
											
										-- Check if Payee Service Level is within the Oracle Column Length
										SET @ln_check = LEN(@lc_sup_Payee_Service_Level)
										IF @ln_check > 30 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Payee Service Level length cannot exceed 30. ',1,4000)
											END
										
										-- Check if Pay Each Document Alone flag has a valid value
										IF NOT(@lc_sup_Pay_Each_Document_Alone IS NULL OR @lc_sup_Pay_Each_Document_Alone IN ('N','Y'))
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Pay Each Document Alone should either be N or Y. ',1,4000)
											END
											
										-- Check if the validation has failed
										IF @ln_erp = 1
											BEGIN
												UPDATE hiscox_poz_suppliers_int_stg
												SET process_flag    	= 'S'
												,oracle_process_flag	= 'S'
												,error_message			= 'Supplier already exists in ERP Cloud'
												,last_update_date		= GETDATE()
												,updated_by				= USER_NAME()
												WHERE record_id     	= @ln_sup_Record_ID
											END
										ELSE
											IF @ln_warning = 1 
												BEGIN
											
													UPDATE hiscox_poz_suppliers_int_stg
													SET process_flag    	= 'W',
													error_message           = 'Duplicate - '+@lc_war_msg,
													last_update_date		= GETDATE(),
													updated_by				= USER_NAME()
													WHERE record_id    	 	= @ln_sup_Record_ID
													
												END 
											ELSE
											BEGIN
												IF @lc_Err_Msg <> ''
													BEGIN
														THROW 60000,@lc_Err_Msg,5
													END
										
													UPDATE hiscox_poz_suppliers_int_stg
													SET process_flag    = 'S',
													last_update_date	=GETDATE(),
													updated_by			=USER_NAME()
													WHERE record_id     = @ln_sup_Record_ID
											END
									
									
								END TRY     -- Try Catch for Supplier FBDI
								BEGIN CATCH   -- Try Catch for Supplier FBDI
									
									SET @lc_Err_Msg = ERROR_MESSAGE()
									
									UPDATE hiscox_poz_suppliers_int_stg
									SET process_flag		= 'E',
									error_message           = @lc_Err_Msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_sup_Record_ID
									
									
									
								END CATCH       -- Try Catch for Supplier FBDI
								FETCH NEXT FROM cur_suppliers_fbdi INTO @ln_sup_Record_ID , @lc_sup_Process_Flag , @lc_sup_Error_Message, @lc_sup_Oracle_Process_Flag, @lc_sup_Oracle_Error_Message, @lc_sup_Batch_ID, @lc_sup_load_Batch_ID, @ln_sup_Source_Record_ID , @lc_sup_Import_Action, @lc_sup_Supplier_Name, @lc_sup_Supplier_Name_New, @lc_sup_Supplier_Number, @lc_sup_Alternate_Name, @lc_sup_Tax_Organization_Type, @lc_sup_Supplier_Type, @lc_sup_Inactive_Date , @lc_sup_Business_Relationship, @lc_sup_Parent_Supplier, @lc_sup_Alias, @lc_sup_D_U_N_S_Number, @lc_sup_One_time_supplier, @lc_sup_Customer_Number, @lc_sup_SIC, @lc_sup_National_Insurance_Number, @lc_sup_Corporate_Web_Site, @lc_sup_Chief_Executive_Title, @lc_sup_Chief_Executive_Name, @lc_sup_Business_Classifications_Not_Applicable, @lc_sup_Taxpayer_Country, @lc_sup_Taxpayer_ID, @lc_sup_Federal_reportable, @lc_sup_Federal_Income_Tax_Type, @lc_sup_State_reportable, @lc_sup_Tax_Reporting_Name, @lc_sup_Name_Control, @lc_sup_Tax_Verification_Date , @lc_sup_Use_withholding_tax, @lc_sup_Withholding_Tax_Group, @lc_sup_Vat_Code, @lc_sup_Tax_Registration_Number , @lc_sup_Auto_Tax_Calc_Override, @lc_sup_Payment_Method, @lc_sup_Delivery_Channel, @lc_sup_Bank_Instruction_1, @lc_sup_Bank_Instruction_2, @lc_sup_Bank_Instruction , @lc_sup_Settlement_Priority, @lc_sup_Payment_Text_Message_1, @lc_sup_Payment_Text_Message_2, @lc_sup_Payment_Text_Message_3, @lc_sup_Bank_Charge_Bearer, @lc_sup_Payment_Reason, @lc_sup_Payment_Reason_Comments, @lc_sup_Payment_Format, @lc_sup_ATTRIBUTE_CATEGORY, @lc_sup_ATTRIBUTE1, @lc_sup_ATTRIBUTE2, @lc_sup_ATTRIBUTE3, @lc_sup_ATTRIBUTE4, @lc_sup_ATTRIBUTE5, @lc_sup_ATTRIBUTE6, @lc_sup_ATTRIBUTE7, @lc_sup_ATTRIBUTE8, @lc_sup_ATTRIBUTE9, @lc_sup_ATTRIBUTE10, @lc_sup_ATTRIBUTE11, @lc_sup_ATTRIBUTE12, @lc_sup_ATTRIBUTE13, @lc_sup_ATTRIBUTE14, @lc_sup_ATTRIBUTE15, @lc_sup_ATTRIBUTE16, @lc_sup_ATTRIBUTE17, @lc_sup_ATTRIBUTE18, @lc_sup_ATTRIBUTE19, @lc_sup_ATTRIBUTE20, @lc_sup_ATTRIBUTE_DATE1 , @lc_sup_ATTRIBUTE_DATE2 , @lc_sup_ATTRIBUTE_DATE3 , @lc_sup_ATTRIBUTE_DATE4 , @lc_sup_ATTRIBUTE_DATE5 , @lc_sup_ATTRIBUTE_DATE6 , @lc_sup_ATTRIBUTE_DATE7 , @lc_sup_ATTRIBUTE_DATE8 , @lc_sup_ATTRIBUTE_DATE9 , @lc_sup_ATTRIBUTE_DATE10 , @ld_sup_ATTRIBUTE_TIMESTAMP1 , @ld_sup_ATTRIBUTE_TIMESTAMP2 , @ld_sup_ATTRIBUTE_TIMESTAMP3 , @ld_sup_ATTRIBUTE_TIMESTAMP4 , @ld_sup_ATTRIBUTE_TIMESTAMP5 , @ld_sup_ATTRIBUTE_TIMESTAMP6 , @ld_sup_ATTRIBUTE_TIMESTAMP7 , @ld_sup_ATTRIBUTE_TIMESTAMP8 , @ld_sup_ATTRIBUTE_TIMESTAMP9 , @ld_sup_ATTRIBUTE_TIMESTAMP10 , @ln_sup_ATTRIBUTE_NUMBER1 , @ln_sup_ATTRIBUTE_NUMBER2 , @ln_sup_ATTRIBUTE_NUMBER3 , @ln_sup_ATTRIBUTE_NUMBER4 , @ln_sup_ATTRIBUTE_NUMBER5 , @ln_sup_ATTRIBUTE_NUMBER6 , @ln_sup_ATTRIBUTE_NUMBER7 , @ln_sup_ATTRIBUTE_NUMBER8 , @ln_sup_ATTRIBUTE_NUMBER9 , @ln_sup_ATTRIBUTE_NUMBER10 , @lc_sup_GLOBAL_ATTRIBUTE_CATEGORY, @lc_sup_GLOBAL_ATTRIBUTE1, @lc_sup_GLOBAL_ATTRIBUTE2, @lc_sup_GLOBAL_ATTRIBUTE3, @lc_sup_GLOBAL_ATTRIBUTE4, @lc_sup_GLOBAL_ATTRIBUTE5, @lc_sup_GLOBAL_ATTRIBUTE6, @lc_sup_GLOBAL_ATTRIBUTE7, @lc_sup_GLOBAL_ATTRIBUTE8, @lc_sup_GLOBAL_ATTRIBUTE9, @lc_sup_GLOBAL_ATTRIBUTE10, @lc_sup_GLOBAL_ATTRIBUTE11, @lc_sup_GLOBAL_ATTRIBUTE12, @lc_sup_GLOBAL_ATTRIBUTE13, @lc_sup_GLOBAL_ATTRIBUTE14, @lc_sup_GLOBAL_ATTRIBUTE15, @lc_sup_GLOBAL_ATTRIBUTE16, @lc_sup_GLOBAL_ATTRIBUTE17, @lc_sup_GLOBAL_ATTRIBUTE18, @lc_sup_GLOBAL_ATTRIBUTE19, @lc_sup_GLOBAL_ATTRIBUTE20, @lc_sup_GLOBAL_ATTRIBUTE_DATE1 , @lc_sup_GLOBAL_ATTRIBUTE_DATE2 , @lc_sup_GLOBAL_ATTRIBUTE_DATE3 , @lc_sup_GLOBAL_ATTRIBUTE_DATE4 , @lc_sup_GLOBAL_ATTRIBUTE_DATE5 , @lc_sup_GLOBAL_ATTRIBUTE_DATE6 , @lc_sup_GLOBAL_ATTRIBUTE_DATE7 , @lc_sup_GLOBAL_ATTRIBUTE_DATE8 , @lc_sup_GLOBAL_ATTRIBUTE_DATE9 , @lc_sup_GLOBAL_ATTRIBUTE_DATE10 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP1 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP2 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP3 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP4 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP5 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP6 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP7 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP8 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP9 , @ld_sup_GLOBAL_ATTRIBUTE_TIMESTAMP10 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER1 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER2 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER3 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER4 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER5 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER6 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER7 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER8 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER9 , @lc_sup_GLOBAL_ATTRIBUTE_NUMBER10 , @lc_sup_Registry_ID, @lc_sup_Payee_Service_Level, @lc_sup_Pay_Each_Document_Alone, @lc_sup_Delivery_Method, @lc_sup_Remittance_E_mail , @lc_sup_Remittance_Fax, @lc_sup_Creation_Date , @lc_sup_Last_Update_Date , @lc_sup_Created_By , @lc_sup_Updated_By
								
								END -- END Supplier FBDI
							CLOSE cur_suppliers_fbdi
							DEALLOCATE cur_suppliers_fbdi
							PRINT '--Supplier End-------- ';
							
							
							PRINT '--Supplier Address Start-------- ';
							
							DECLARE cur_suppliers_add CURSOR LOCAL FOR 
							SELECT Record_ID, Process_Flag, Error_Message ,Oracle_Process_Flag,Oracle_Error_Message, Batch_ID ,Load_Batch_id, Source_Record_ID , Import_Action , Supplier_Name , Address_Name , Address_Name_New , Country , Address_Line_1 , Address_Line_2 , Address_Line_3 , Address_Line_4 , Phonetic_Address_Line , Address_Element_Attribute_1 , Address_Element_Attribute_2 , Address_Element_Attribute_3 , Address_Element_Attribute_4 , Address_Element_Attribute_5 , Building , Floor_Number , City , State , Province , County , Postal_code , Postal_Plus_4_code , Addressee , Global_Location_Number , Language , Inactive_Date , Phone_Country_Code , Phone_Area_Code , Phone , Phone_Extension , Fax_Country_Code , Fax_Area_Code , Fax , RFQ_Or_Bidding , Ordering , Pay , ATTRIBUTE_CATEGORY , ATTRIBUTE1 , ATTRIBUTE2 , ATTRIBUTE3 , ATTRIBUTE4 , ATTRIBUTE5 , ATTRIBUTE6 , ATTRIBUTE7 , ATTRIBUTE8 , ATTRIBUTE9 , ATTRIBUTE10 , ATTRIBUTE11 , ATTRIBUTE12 , ATTRIBUTE13 , ATTRIBUTE14 , ATTRIBUTE15 , ATTRIBUTE16 , ATTRIBUTE17 , ATTRIBUTE18 , ATTRIBUTE19 , ATTRIBUTE20 , ATTRIBUTE21 , ATTRIBUTE22 , ATTRIBUTE23 , ATTRIBUTE24 , ATTRIBUTE25 , ATTRIBUTE26 , ATTRIBUTE27 , ATTRIBUTE28 , ATTRIBUTE29 , ATTRIBUTE30 , ATTRIBUTE_NUMBER1 , ATTRIBUTE_NUMBER2 , ATTRIBUTE_NUMBER3 , ATTRIBUTE_NUMBER4 , ATTRIBUTE_NUMBER5 , ATTRIBUTE_NUMBER6 , ATTRIBUTE_NUMBER7 , ATTRIBUTE_NUMBER8 , ATTRIBUTE_NUMBER9 , ATTRIBUTE_NUMBER10 , ATTRIBUTE_NUMBER11 , ATTRIBUTE_NUMBER12 , ATTRIBUTE_DATE1 , ATTRIBUTE_DATE2 , ATTRIBUTE_DATE3 , ATTRIBUTE_DATE4 , ATTRIBUTE_DATE5 , ATTRIBUTE_DATE6 , ATTRIBUTE_DATE7 , ATTRIBUTE_DATE8 , ATTRIBUTE_DATE9 , ATTRIBUTE_DATE10 , ATTRIBUTE_DATE11 , ATTRIBUTE_DATE12 , E_Mail  , Delivery_Channel , Bank_Instruction_1 , Bank_Instruction_2 , Bank_Instruction , Settlement_Priority , Payment_Text_Message_1 , Payment_Text_Message_2 , Payment_Text_Message_3 , Payee_Service_Level , Pay_Each_Document_Alone , Bank_Charge_Bearer , Payment_Reason , Payment_Reason_Comments , Delivery_Method , Remittance_E_Mail , Remittance_Fax , Creation_Date , Last_Update_Date , Created_By  , Updated_By  
							FROM hiscox_poz_supplier_addresses_int_stg
							WHERE 1=1
							--AND process_flag      = 'N'
							AND batch_id            = @p_new_batch_id
											
								PRINT '--Supplier Address Start-------- ';
								OPEN cur_suppliers_add  
								FETCH NEXT FROM cur_suppliers_add INTO @ln_sa_Record_ID, @lc_sa_Process_Flag, @lc_sa_Error_Message ,@lc_sa_Oracle_Process_Flag,@lc_sa_Oracle_Error_Message,@lc_sa_Batch_ID,@lc_sa_load_Batch_ID , @ln_sa_Source_Record_ID , @lc_sa_Import_Action , @lc_sa_Supplier_Name , @lc_sa_Address_Name , @lc_sa_Address_Name_New , @lc_sa_Country , @lc_sa_Address_Line_1 , @lc_sa_Address_Line_2 , @lc_sa_Address_Line_3 , @lc_sa_Address_Line_4 , @lc_sa_Phonetic_Address_Line , @lc_sa_Address_Element_Attribute_1 , @lc_sa_Address_Element_Attribute_2 , @lc_sa_Address_Element_Attribute_3 , @lc_sa_Address_Element_Attribute_4 , @lc_sa_Address_Element_Attribute_5 , @lc_sa_Building , @lc_sa_Floor_Number , @lc_sa_City , @lc_sa_State , @lc_sa_Province , @lc_sa_County , @lc_sa_Postal_code , @lc_sa_Postal_Plus_4_code , @lc_sa_Addressee , @lc_sa_Global_Location_Number , @lc_sa_Language , @lc_sa_Inactive_Date , @lc_sa_Phone_Country_Code , @lc_sa_Phone_Area_Code , @lc_sa_Phone , @lc_sa_Phone_Extension , @lc_sa_Fax_Country_Code , @lc_sa_Fax_Area_Code , @lc_sa_Fax , @lc_sa_RFQ_Or_Bidding , @lc_sa_Ordering , @lc_sa_Pay , @lc_sa_ATTRIBUTE_CATEGORY , @lc_sa_ATTRIBUTE1 , @lc_sa_ATTRIBUTE2 , @lc_sa_ATTRIBUTE3 , @lc_sa_ATTRIBUTE4 , @lc_sa_ATTRIBUTE5 , @lc_sa_ATTRIBUTE6 , @lc_sa_ATTRIBUTE7 , @lc_sa_ATTRIBUTE8 , @lc_sa_ATTRIBUTE9 , @lc_sa_ATTRIBUTE10 , @lc_sa_ATTRIBUTE11 , @lc_sa_ATTRIBUTE12 , @lc_sa_ATTRIBUTE13 , @lc_sa_ATTRIBUTE14 , @lc_sa_ATTRIBUTE15 , @lc_sa_ATTRIBUTE16 , @lc_sa_ATTRIBUTE17 , @lc_sa_ATTRIBUTE18 , @lc_sa_ATTRIBUTE19 , @lc_sa_ATTRIBUTE20 , @lc_sa_ATTRIBUTE21 , @lc_sa_ATTRIBUTE22 , @lc_sa_ATTRIBUTE23 , @lc_sa_ATTRIBUTE24 , @lc_sa_ATTRIBUTE25 , @lc_sa_ATTRIBUTE26 , @lc_sa_ATTRIBUTE27 , @lc_sa_ATTRIBUTE28 , @lc_sa_ATTRIBUTE29 , @lc_sa_ATTRIBUTE30 , @ln_sa_ATTRIBUTE_NUMBER1 , @ln_sa_ATTRIBUTE_NUMBER2 , @ln_sa_ATTRIBUTE_NUMBER3 , @ln_sa_ATTRIBUTE_NUMBER4 , @ln_sa_ATTRIBUTE_NUMBER5 , @ln_sa_ATTRIBUTE_NUMBER6 , @ln_sa_ATTRIBUTE_NUMBER7 , @ln_sa_ATTRIBUTE_NUMBER8 , @ln_sa_ATTRIBUTE_NUMBER9 , @ln_sa_ATTRIBUTE_NUMBER10 , @ln_sa_ATTRIBUTE_NUMBER11 , @ln_sa_ATTRIBUTE_NUMBER12 , @ld_sa_ATTRIBUTE_DATE1 , @ld_sa_ATTRIBUTE_DATE2 , @ld_sa_ATTRIBUTE_DATE3 , @ld_sa_ATTRIBUTE_DATE4 , @ld_sa_ATTRIBUTE_DATE5 , @ld_sa_ATTRIBUTE_DATE6 , @ld_sa_ATTRIBUTE_DATE7 , @ld_sa_ATTRIBUTE_DATE8 , @ld_sa_ATTRIBUTE_DATE9 , @ld_sa_ATTRIBUTE_DATE10 , @ld_sa_ATTRIBUTE_DATE11 , @ld_sa_ATTRIBUTE_DATE12 , @lc_sa_E_Mail  , @lc_sa_Delivery_Channel , @lc_sa_Bank_Instruction_1 , @lc_sa_Bank_Instruction_2 , @lc_sa_Bank_Instruction , @lc_sa_Settlement_Priority , @lc_sa_Payment_Text_Message_1 , @lc_sa_Payment_Text_Message_2 , @lc_sa_Payment_Text_Message_3 , @lc_sa_Payee_Service_Level , @lc_sa_Pay_Each_Document_Alone , @lc_sa_Bank_Charge_Bearer , @lc_sa_Payment_Reason , @lc_sa_Payment_Reason_Comments , @lc_sa_Delivery_Method , @lc_sa_Remittance_E_Mail , @lc_sa_Remittance_Fax , @lc_sa_Creation_Date , @lc_sa_Last_Update_Date , @lc_sa_Created_By  , @lc_sa_Updated_By  
								WHILE @@FETCH_STATUS = 0 
								BEGIN 
									BEGIN TRY   -- Try Catch for Supplier Address FBDI
										PRINT '----Supplier- ' + CAST(ISNULL(@lc_sa_Supplier_Name,'NULL') AS VARCHAR);
										PRINT '----Supplier Address- ' + CAST(ISNULL(@lc_sa_Address_Name,'NULL') AS VARCHAR);
										SET @lc_Err_Msg = ''
										SET @ln_warning	= 0
										SET @lc_war_msg	= ''

										SET @lc_party_number	=''
										SET @lc_concatenated_string =''
										SET @ln_erp = 0
												
										-- Check if Address Name is not null
										IF (@lc_sa_Address_Name IS NULL  OR @lc_sa_Address_Name='')
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Address Name cannot be NULL. ',1,4000)
											END
										ELSE
											SET @ln_check = LEN(@lc_sa_Address_Name)
											IF @ln_check > 240 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Address Name length cannot exceed 240. ',1,4000)
											END
										
										SET @ln_check = LEN(@lc_sa_Address_Name_New)
										IF @ln_check > 15 
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Address Name New length cannot exceed 15. ',1,4000)
										END
										
										-- Check if Country is not null
										IF @lc_sa_Country IS NULL 
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Address Country cannot be NULL. ',1,4000)
											END
										ELSE
											BEGIN
												EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_COUNTRY_CODE' , @lc_sa_Country
												
												IF @ln_check = 0
													BEGIN
														SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Country is Invalid. ',1,4000)
													END 
											END
											
										-- Check if the Supplier Address has its corresponding Supplier
										SET @ln_check = (	SELECT count(*)
															FROM hiscox_poz_suppliers_int_stg
															WHERE 1					= 1
															AND batch_id            = @p_new_batch_id
															AND process_flag    	= 'S'
															AND supplier_name		= @lc_sa_Supplier_Name
														)
										
										IF @ln_check < 1
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier is invalid for this supplier Address. ',1,4000)
										END 
										
										/*									
										IF @lc_sa_City IS NOT NULL
										BEGIN
											EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_ADD_US_CITY' , @lc_sa_City
												
											IF @ln_check = 0
												BEGIN
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'City '+@lc_sa_City+' is invalid. ',1,4000)
												END
										END
										
										IF @lc_sa_County IS NOT NULL
										BEGIN
											EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_ADD_US_COUNTY' , @lc_sa_County
												
											IF @ln_check = 0
												BEGIN
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'County '+@lc_sa_County+' is invalid. ',1,4000)
												END
										END
										
										IF @lc_sa_state IS NOT NULL 
										BEGIN
											EXEC @ln_check = hiscox_does_meaning_exist 'HISCOX_POZ_ADD_US_STATE' , @lc_sa_state
												
											IF @ln_check = 0
												BEGIN
													SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'State '+@lc_sa_state+' is invalid. ',1,4000)
												END
										END
										
										IF @lc_sa_state IS NOT NULL AND @lc_sa_city IS NOT NULL
										BEGIN
											SET @ln_check = (	SELECT count(*)
															FROM hiscox_lookup_ref_tbl city,
															hiscox_lookup_ref_tbl st
															WHERE 1					= 1
															AND city.lookup_type    = 'HISCOX_POZ_ADD_US_CITY'
															AND city.lookup_code    = @lc_sa_City
															AND st.lookup_code		= city.meaning
															AND st.lookup_type		= 'HISCOX_POZ_ADD_US_STATE'
															AND st.meaning			= @lc_sa_state
														)
										
											IF @ln_check < 1
											BEGIN
												SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'City/State Combination is invalid for this supplier Address. ',1,4000)
											END 
										
										END
										*/

										--EXEC @lc_party_number = hiscox_get_lookup_meaning_f 'HISCOX_POZ_EXISTING_SUPPLIERS' , @lc_sa_Supplier_Name
										SET @lc_party_number = (
																SELECT  meaning 
																FROM hiscox_lookup_ref_tbl
																WHERE 1					= 1
																AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
																AND UPPER(lookup_code)	= UPPER(@lc_sa_Supplier_Name)
																)

										IF @lc_party_number <> ''
											BEGIN
												SET @lc_concatenated_string	= UPPER(CONCAT(@lc_party_number,'-',@lc_sa_Address_Name))
												SET @ln_check = ( SELECT count(*) 
																	FROM hiscox_lookup_ref_tbl
																	WHERE lookup_type ='HISCOX_POZ_SUPPLIER_ADD'
																	AND UPPER(lookup_code) = @lc_concatenated_string)
												--EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_SUPPLIER_ADD', @lc_concatenated_string
												IF @ln_check > 0
												BEGIN
													SET @ln_erp =1
												END
											END

										SET @ln_check = (
															SELECT  count(*) 
															FROM hiscox_poz_supplier_addresses_int_stg
															WHERE 1				= 1
															AND batch_id		=	@p_new_batch_id
															AND record_id		<>	@ln_sa_Record_ID
															AND process_flag	=	'S'
															AND address_name	=   @lc_sa_address_Name
															AND supplier_name	=	@lc_sa_Supplier_Name
														)
														
										IF @ln_check <> 0 
											BEGIN
												--SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Address '+@lc_sa_address_Name+' for the supplier '+ @lc_sa_Supplier_Name +' already exists in the current run. ',1,4000)
												SET @ln_warning	= 1
												SET @lc_war_msg	= SUBSTRING(@lc_war_msg  +'Duplicate - Supplier Address for the supplier already exists in the current run. ',1,4000)
											END
									IF @ln_erp = 1
										BEGIN
											UPDATE hiscox_poz_supplier_addresses_int_stg
											SET process_flag    	= 'S'
											,oracle_process_flag	= 'S'
											,error_message			= 'Supplier Address already exists in ERP Cloud'
											,last_update_date		= GETDATE()
											,updated_by				= USER_NAME()
											WHERE record_id     	= @ln_sa_Record_ID
										END
									ELSE IF @ln_warning = 1 
									BEGIN
										
										UPDATE hiscox_poz_supplier_addresses_int_stg
										SET process_flag   	 	= 'W',
										error_message           = @lc_war_msg,
										last_update_date		= GETDATE(),
										updated_by				= USER_NAME()
										WHERE record_id         = @ln_sa_Record_ID
									END 
									ELSE
										BEGIN
											-- Check if the validation has failed
											IF @lc_Err_Msg <> ''
											BEGIN
												THROW 60000,@lc_Err_Msg,5
											END
										
											UPDATE hiscox_poz_supplier_addresses_int_stg
											SET process_flag   	 	='S',
											last_update_date		= GETDATE(),
											updated_by				= USER_NAME()
											WHERE record_id         = @ln_sa_Record_ID
										END
	
												
									END TRY     -- Try Catch for Supplier Address FBDI
									BEGIN CATCH -- Try Catch for Supplier Address FBDI
											
										SET @lc_Err_Msg = ERROR_MESSAGE()
										
										UPDATE hiscox_poz_supplier_addresses_int_stg
										SET process_flag 		= 'E',
										Error_Message    		= @lc_Err_Msg,
										last_update_date		= GETDATE(),
										updated_by				= USER_NAME()
										WHERE record_id         = @ln_sa_Record_ID
		
									END CATCH   -- Try Catch for Supplier Address FBDI
									FETCH NEXT FROM cur_suppliers_add INTO @ln_sa_Record_ID, @lc_sa_Process_Flag, @lc_sa_Error_Message ,@lc_sa_Oracle_Process_Flag,@lc_sa_Oracle_Error_Message,@lc_sa_Batch_ID,@lc_sa_load_Batch_ID , @ln_sa_Source_Record_ID , @lc_sa_Import_Action , @lc_sa_Supplier_Name , @lc_sa_Address_Name , @lc_sa_Address_Name_New , @lc_sa_Country , @lc_sa_Address_Line_1 , @lc_sa_Address_Line_2 , @lc_sa_Address_Line_3 , @lc_sa_Address_Line_4 , @lc_sa_Phonetic_Address_Line , @lc_sa_Address_Element_Attribute_1 , @lc_sa_Address_Element_Attribute_2 , @lc_sa_Address_Element_Attribute_3 , @lc_sa_Address_Element_Attribute_4 , @lc_sa_Address_Element_Attribute_5 , @lc_sa_Building , @lc_sa_Floor_Number , @lc_sa_City , @lc_sa_State , @lc_sa_Province , @lc_sa_County , @lc_sa_Postal_code , @lc_sa_Postal_Plus_4_code , @lc_sa_Addressee , @lc_sa_Global_Location_Number , @lc_sa_Language , @lc_sa_Inactive_Date , @lc_sa_Phone_Country_Code , @lc_sa_Phone_Area_Code , @lc_sa_Phone , @lc_sa_Phone_Extension , @lc_sa_Fax_Country_Code , @lc_sa_Fax_Area_Code , @lc_sa_Fax , @lc_sa_RFQ_Or_Bidding , @lc_sa_Ordering , @lc_sa_Pay , @lc_sa_ATTRIBUTE_CATEGORY , @lc_sa_ATTRIBUTE1 , @lc_sa_ATTRIBUTE2 , @lc_sa_ATTRIBUTE3 , @lc_sa_ATTRIBUTE4 , @lc_sa_ATTRIBUTE5 , @lc_sa_ATTRIBUTE6 , @lc_sa_ATTRIBUTE7 , @lc_sa_ATTRIBUTE8 , @lc_sa_ATTRIBUTE9 , @lc_sa_ATTRIBUTE10 , @lc_sa_ATTRIBUTE11 , @lc_sa_ATTRIBUTE12 , @lc_sa_ATTRIBUTE13 , @lc_sa_ATTRIBUTE14 , @lc_sa_ATTRIBUTE15 , @lc_sa_ATTRIBUTE16 , @lc_sa_ATTRIBUTE17 , @lc_sa_ATTRIBUTE18 , @lc_sa_ATTRIBUTE19 , @lc_sa_ATTRIBUTE20 , @lc_sa_ATTRIBUTE21 , @lc_sa_ATTRIBUTE22 , @lc_sa_ATTRIBUTE23 , @lc_sa_ATTRIBUTE24 , @lc_sa_ATTRIBUTE25 , @lc_sa_ATTRIBUTE26 , @lc_sa_ATTRIBUTE27 , @lc_sa_ATTRIBUTE28 , @lc_sa_ATTRIBUTE29 , @lc_sa_ATTRIBUTE30 , @ln_sa_ATTRIBUTE_NUMBER1 , @ln_sa_ATTRIBUTE_NUMBER2 , @ln_sa_ATTRIBUTE_NUMBER3 , @ln_sa_ATTRIBUTE_NUMBER4 , @ln_sa_ATTRIBUTE_NUMBER5 , @ln_sa_ATTRIBUTE_NUMBER6 , @ln_sa_ATTRIBUTE_NUMBER7 , @ln_sa_ATTRIBUTE_NUMBER8 , @ln_sa_ATTRIBUTE_NUMBER9 , @ln_sa_ATTRIBUTE_NUMBER10 , @ln_sa_ATTRIBUTE_NUMBER11 , @ln_sa_ATTRIBUTE_NUMBER12 , @ld_sa_ATTRIBUTE_DATE1 , @ld_sa_ATTRIBUTE_DATE2 , @ld_sa_ATTRIBUTE_DATE3 , @ld_sa_ATTRIBUTE_DATE4 , @ld_sa_ATTRIBUTE_DATE5 , @ld_sa_ATTRIBUTE_DATE6 , @ld_sa_ATTRIBUTE_DATE7 , @ld_sa_ATTRIBUTE_DATE8 , @ld_sa_ATTRIBUTE_DATE9 , @ld_sa_ATTRIBUTE_DATE10 , @ld_sa_ATTRIBUTE_DATE11 , @ld_sa_ATTRIBUTE_DATE12 , @lc_sa_E_Mail  , @lc_sa_Delivery_Channel , @lc_sa_Bank_Instruction_1 , @lc_sa_Bank_Instruction_2 , @lc_sa_Bank_Instruction , @lc_sa_Settlement_Priority , @lc_sa_Payment_Text_Message_1 , @lc_sa_Payment_Text_Message_2 , @lc_sa_Payment_Text_Message_3 , @lc_sa_Payee_Service_Level , @lc_sa_Pay_Each_Document_Alone , @lc_sa_Bank_Charge_Bearer , @lc_sa_Payment_Reason , @lc_sa_Payment_Reason_Comments , @lc_sa_Delivery_Method , @lc_sa_Remittance_E_Mail , @lc_sa_Remittance_Fax , @lc_sa_Creation_Date , @lc_sa_Last_Update_Date , @lc_sa_Created_By  , @lc_sa_Updated_By  
								END -- END Supplier Address FBDI
								
							CLOSE cur_suppliers_add
							DEALLOCATE cur_suppliers_add
							PRINT '--Supplier Address End-------- ';
							
							PRINT '--Supplier Sites Start-------- ';
							DECLARE cur_suppliers_sites CURSOR LOCAL FOR 
							SELECT Record_ID, Process_Flag , Error_Message, Batch_ID, Source_Record_ID, Import_Action, Supplier_Name, Procurement_BU, Address_Name, Supplier_Site, Supplier_Site_New, Inactive_Date , Sourcing_only , Purchasing , Procurement_card , Pay , Primary_Pay , Income_tax_reporting_site , Alternate_Site_Name, Customer_Number, B2B_Communication_Method, B2B_Supplier_Site_Code, Communication_Method, E_Mail, Fax_Country_Code, Fax_Area_Code, Fax, Hold_all_new_purchasing_documents , Hold_Reason, Carrier, Mode_of_Transport, Service_Level, Freight_Terms, Pay_on_receipt, FOB, Country_of_Origin, Buyer_Managed_Transportation , Pay_on_use , Aging_Onset_Point, Aging_Period_Days , Consumption_Advice_Frequency, Consumption_Advice_Summary, Default_Pay_Site, Invoice_Summary_Level, Gapless_invoice_numbering , Selling_Company_Identifier, Create_debit_memo_from_return, Ship_to_Exception_Action, Receipt_Routing , Over_receipt_Tolerance , Over_receipt_Action, Early_Receipt_Tolerance_in_Days , Late_Receipt_Tolerance_in_Days , Allow_Substitute_Receipts , Allow_unordered_receipts , Receipt_Date_Exception, Invoice_Currency, Invoice_Amount_Limit , Invoice_Match_Option, Match_Approval_Level , Payment_Currency, Payment_Priority , Pay_Group, Quantity_Tolerances, Amount_Tolerance, Hold_All_Invoices , Hold_Unmatched_Invoices , Hold_Unvalidated_Invoices , Payment_Hold_By , Payment_Hold_Date , Payment_Hold_Reason, Payment_Terms, Terms_Date_Basis, Pay_Date_Basis, Bank_Charge_Deduction_Type, Always_Take_Discount , Exclude_Freight_From_Discount , Exclude_Tax_From_Discount , Create_Interest_Invoices , Vat_Code, Tax_Registration_Number, Payment_Method, Delivery_Channel, Bank_Instruction_1, Bank_Instruction_2, Bank_Instruction, Settlement_Priority, Payment_Text_Message_1, Payment_Text_Message_2, Payment_Text_Message_3, Bank_Charge_Bearer, Payment_Reason, Payment_Reason_Comments, Delivery_Method, Remittance_E_Mail , Remittance_Fax, ATTRIBUTE_CATEGORY, ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8, ATTRIBUTE9, ATTRIBUTE10, ATTRIBUTE11, ATTRIBUTE12, ATTRIBUTE13, ATTRIBUTE14, ATTRIBUTE15, ATTRIBUTE16, ATTRIBUTE17, ATTRIBUTE18, ATTRIBUTE19, ATTRIBUTE20, ATTRIBUTE_DATE1 , ATTRIBUTE_DATE2 , ATTRIBUTE_DATE3 , ATTRIBUTE_DATE4 , ATTRIBUTE_DATE5 , ATTRIBUTE_DATE6 , ATTRIBUTE_DATE7 , ATTRIBUTE_DATE8 , ATTRIBUTE_DATE9 , ATTRIBUTE_DATE10 , ATTRIBUTE_TIMESTAMP1 , ATTRIBUTE_TIMESTAMP2 , ATTRIBUTE_TIMESTAMP3 , ATTRIBUTE_TIMESTAMP4 , ATTRIBUTE_TIMESTAMP5 , ATTRIBUTE_TIMESTAMP6 , ATTRIBUTE_TIMESTAMP7 , ATTRIBUTE_TIMESTAMP8 , ATTRIBUTE_TIMESTAMP9 , ATTRIBUTE_TIMESTAMP10 , ATTRIBUTE_NUMBER1 , ATTRIBUTE_NUMBER2 , ATTRIBUTE_NUMBER3 , ATTRIBUTE_NUMBER4 , ATTRIBUTE_NUMBER5 , ATTRIBUTE_NUMBER6 , ATTRIBUTE_NUMBER7 , ATTRIBUTE_NUMBER8 , ATTRIBUTE_NUMBER9 , ATTRIBUTE_NUMBER10 , GLOBAL_ATTRIBUTE_CATEGORY, GLOBAL_ATTRIBUTE1, GLOBAL_ATTRIBUTE2, GLOBAL_ATTRIBUTE3, GLOBAL_ATTRIBUTE4, GLOBAL_ATTRIBUTE5, GLOBAL_ATTRIBUTE6, GLOBAL_ATTRIBUTE7, GLOBAL_ATTRIBUTE8, GLOBAL_ATTRIBUTE9, GLOBAL_ATTRIBUTE10, GLOBAL_ATTRIBUTE11, GLOBAL_ATTRIBUTE12, GLOBAL_ATTRIBUTE13, GLOBAL_ATTRIBUTE14, GLOBAL_ATTRIBUTE15, GLOBAL_ATTRIBUTE16, GLOBAL_ATTRIBUTE17, GLOBAL_ATTRIBUTE18, GLOBAL_ATTRIBUTE19, GLOBAL_ATTRIBUTE20, GLOBAL_ATTRIBUTE_DATE1 , GLOBAL_ATTRIBUTE_DATE2 , GLOBAL_ATTRIBUTE_DATE3 , GLOBAL_ATTRIBUTE_DATE4 , GLOBAL_ATTRIBUTE_DATE5 , GLOBAL_ATTRIBUTE_DATE6 , GLOBAL_ATTRIBUTE_DATE7 , GLOBAL_ATTRIBUTE_DATE8 , GLOBAL_ATTRIBUTE_DATE9 , GLOBAL_ATTRIBUTE_DATE10 , GLOBAL_ATTRIBUTE_TIMESTAMP1 , GLOBAL_ATTRIBUTE_TIMESTAMP2 , GLOBAL_ATTRIBUTE_TIMESTAMP3 , GLOBAL_ATTRIBUTE_TIMESTAMP4 , GLOBAL_ATTRIBUTE_TIMESTAMP5 , GLOBAL_ATTRIBUTE_TIMESTAMP6 , GLOBAL_ATTRIBUTE_TIMESTAMP7 , GLOBAL_ATTRIBUTE_TIMESTAMP8 , GLOBAL_ATTRIBUTE_TIMESTAMP9 , GLOBAL_ATTRIBUTE_TIMESTAMP10 , GLOBAL_ATTRIBUTE_NUMBER1 , GLOBAL_ATTRIBUTE_NUMBER2 , GLOBAL_ATTRIBUTE_NUMBER3 , GLOBAL_ATTRIBUTE_NUMBER4 , GLOBAL_ATTRIBUTE_NUMBER5 , GLOBAL_ATTRIBUTE_NUMBER6 , GLOBAL_ATTRIBUTE_NUMBER7 , GLOBAL_ATTRIBUTE_NUMBER8 , GLOBAL_ATTRIBUTE_NUMBER9 , GLOBAL_ATTRIBUTE_NUMBER10 , Required_Acknowledgement , Acknowledge_Within_Days , Invoice_Channel , Payee_Service_Level, Pay_Each_Document_Alone , Creation_Date , Last_Update_Date , Created_By, Updated_By
							FROM hiscox_poz_supplier_sites_int_stg
							WHERE 1=1
							--AND process_flag      = 'N'
							AND batch_id            = @p_new_batch_id
												
							OPEN cur_suppliers_sites  
							FETCH NEXT FROM cur_suppliers_sites INTO @ln_ss_Record_ID, @lc_ss_Process_Flag , @lc_ss_Error_Message, @lc_ss_Batch_ID, @ln_ss_Source_Record_ID, @lc_ss_Import_Action, @lc_ss_Supplier_Name, @lc_ss_Procurement_BU, @lc_ss_Address_Name, @lc_ss_Supplier_Site, @lc_ss_Supplier_Site_New, @lc_ss_Inactive_Date , @lc_ss_Sourcing_only , @lc_ss_Purchasing , @lc_ss_Procurement_card , @lc_ss_Pay , @lc_ss_Primary_Pay , @lc_ss_Income_tax_reporting_site , @lc_ss_Alternate_Site_Name, @lc_ss_Customer_Number, @lc_ss_B2B_Communication_Method, @lc_ss_B2B_Supplier_Site_Code, @lc_ss_Communication_Method, @lc_ss_E_Mail, @lc_ss_Fax_Country_Code, @lc_ss_Fax_Area_Code, @lc_ss_Fax, @lc_ss_Hold_all_new_purchasing_documents , @lc_ss_Hold_Reason, @lc_ss_Carrier, @lc_ss_Mode_of_Transport, @lc_ss_Service_Level, @lc_ss_Freight_Terms, @lc_ss_Pay_on_receipt, @lc_ss_FOB, @lc_ss_Country_of_Origin, @lc_ss_Buyer_Managed_Transportation , @lc_ss_Pay_on_use , @lc_ss_Aging_Onset_Point, @lc_ss_Aging_Period_Days , @lc_ss_Consumption_Advice_Frequency, @lc_ss_Consumption_Advice_Summary, @lc_ss_Default_Pay_Site, @lc_ss_Invoice_Summary_Level, @lc_ss_Gapless_invoice_numbering , @lc_ss_Selling_Company_Identifier, @lc_ss_Create_debit_memo_from_return, @lc_ss_Ship_to_Exception_Action, @lc_ss_Receipt_Routing , @lc_ss_Over_receipt_Tolerance , @lc_ss_Over_receipt_Action, @lc_ss_Early_Receipt_Tolerance_in_Days , @lc_ss_Late_Receipt_Tolerance_in_Days , @lc_ss_Allow_Substitute_Receipts , @lc_ss_Allow_unordered_receipts , @lc_ss_Receipt_Date_Exception, @lc_ss_Invoice_Currency, @lc_ss_Invoice_Amount_Limit , @lc_ss_Invoice_Match_Option, @lc_ss_Match_Approval_Level , @lc_ss_Payment_Currency, @lc_ss_Payment_Priority , @lc_ss_Pay_Group, @lc_ss_Quantity_Tolerances, @lc_ss_Amount_Tolerance, @lc_ss_Hold_All_Invoices , @lc_ss_Hold_Unmatched_Invoices , @lc_ss_Hold_Unvalidated_Invoices , @lc_ss_Payment_Hold_By , @lc_ss_Payment_Hold_Date , @lc_ss_Payment_Hold_Reason, @lc_ss_Payment_Terms, @lc_ss_Terms_Date_Basis, @lc_ss_Pay_Date_Basis, @lc_ss_Bank_Charge_Deduction_Type, @lc_ss_Always_Take_Discount , @lc_ss_Exclude_Freight_From_Discount , @lc_ss_Exclude_Tax_From_Discount , @lc_ss_Create_Interest_Invoices , @lc_ss_Vat_Code, @lc_ss_Tax_Registration_Number, @lc_ss_Payment_Method, @lc_ss_Delivery_Channel, @lc_ss_Bank_Instruction_1, @lc_ss_Bank_Instruction_2, @lc_ss_Bank_Instruction, @lc_ss_Settlement_Priority, @lc_ss_Payment_Text_Message_1, @lc_ss_Payment_Text_Message_2, @lc_ss_Payment_Text_Message_3, @lc_ss_Bank_Charge_Bearer, @lc_ss_Payment_Reason, @lc_ss_Payment_Reason_Comments, @lc_ss_Delivery_Method, @lc_ss_Remittance_E_Mail, @lc_ss_Remittance_Fax, @lc_ss_ATTRIBUTE_CATEGORY, @lc_ss_ATTRIBUTE1, @lc_ss_ATTRIBUTE2, @lc_ss_ATTRIBUTE3, @lc_ss_ATTRIBUTE4, @lc_ss_ATTRIBUTE5, @lc_ss_ATTRIBUTE6, @lc_ss_ATTRIBUTE7, @lc_ss_ATTRIBUTE8, @lc_ss_ATTRIBUTE9, @lc_ss_ATTRIBUTE10, @lc_ss_ATTRIBUTE11, @lc_ss_ATTRIBUTE12, @lc_ss_ATTRIBUTE13, @lc_ss_ATTRIBUTE14, @lc_ss_ATTRIBUTE15, @lc_ss_ATTRIBUTE16, @lc_ss_ATTRIBUTE17, @lc_ss_ATTRIBUTE18, @lc_ss_ATTRIBUTE19, @lc_ss_ATTRIBUTE20, @lc_ss_ATTRIBUTE_DATE1 , @lc_ss_ATTRIBUTE_DATE2 , @lc_ss_ATTRIBUTE_DATE3 , @lc_ss_ATTRIBUTE_DATE4 , @lc_ss_ATTRIBUTE_DATE5 , @lc_ss_ATTRIBUTE_DATE6 , @lc_ss_ATTRIBUTE_DATE7 , @lc_ss_ATTRIBUTE_DATE8 , @lc_ss_ATTRIBUTE_DATE9 , @lc_ss_ATTRIBUTE_DATE10 , @lc_ss_ATTRIBUTE_TIMESTAMP1 , @lc_ss_ATTRIBUTE_TIMESTAMP2 , @lc_ss_ATTRIBUTE_TIMESTAMP3 , @lc_ss_ATTRIBUTE_TIMESTAMP4 , @lc_ss_ATTRIBUTE_TIMESTAMP5 , @lc_ss_ATTRIBUTE_TIMESTAMP6 , @lc_ss_ATTRIBUTE_TIMESTAMP7 , @lc_ss_ATTRIBUTE_TIMESTAMP8 , @lc_ss_ATTRIBUTE_TIMESTAMP9 , @lc_ss_ATTRIBUTE_TIMESTAMP10 , @ln_ss_ATTRIBUTE_NUMBER1 , @ln_ss_ATTRIBUTE_NUMBER2 , @ln_ss_ATTRIBUTE_NUMBER3 , @ln_ss_ATTRIBUTE_NUMBER4 , @ln_ss_ATTRIBUTE_NUMBER5 , @ln_ss_ATTRIBUTE_NUMBER6 , @ln_ss_ATTRIBUTE_NUMBER7 , @ln_ss_ATTRIBUTE_NUMBER8 , @ln_ss_ATTRIBUTE_NUMBER9 , @ln_ss_ATTRIBUTE_NUMBER10 , @lc_ss_GLOBAL_ATTRIBUTE_CATEGORY, @lc_ss_GLOBAL_ATTRIBUTE1, @lc_ss_GLOBAL_ATTRIBUTE2, @lc_ss_GLOBAL_ATTRIBUTE3, @lc_ss_GLOBAL_ATTRIBUTE4, @lc_ss_GLOBAL_ATTRIBUTE5, @lc_ss_GLOBAL_ATTRIBUTE6, @lc_ss_GLOBAL_ATTRIBUTE7, @lc_ss_GLOBAL_ATTRIBUTE8, @lc_ss_GLOBAL_ATTRIBUTE9, @lc_ss_GLOBAL_ATTRIBUTE10, @lc_ss_GLOBAL_ATTRIBUTE11, @lc_ss_GLOBAL_ATTRIBUTE12, @lc_ss_GLOBAL_ATTRIBUTE13, @lc_ss_GLOBAL_ATTRIBUTE14, @lc_ss_GLOBAL_ATTRIBUTE15, @lc_ss_GLOBAL_ATTRIBUTE16, @lc_ss_GLOBAL_ATTRIBUTE17, @lc_ss_GLOBAL_ATTRIBUTE18, @lc_ss_GLOBAL_ATTRIBUTE19, @lc_ss_GLOBAL_ATTRIBUTE20, @ld_ss_GLOBAL_ATTRIBUTE_DATE1 , @ld_ss_GLOBAL_ATTRIBUTE_DATE2 , @ld_ss_GLOBAL_ATTRIBUTE_DATE3 , @ld_ss_GLOBAL_ATTRIBUTE_DATE4 , @ld_ss_GLOBAL_ATTRIBUTE_DATE5 , @ld_ss_GLOBAL_ATTRIBUTE_DATE6 , @ld_ss_GLOBAL_ATTRIBUTE_DATE7 , @ld_ss_GLOBAL_ATTRIBUTE_DATE8 , @ld_ss_GLOBAL_ATTRIBUTE_DATE9 , @ld_ss_GLOBAL_ATTRIBUTE_DATE10 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP1 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP2 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP3 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP4 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP5 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP6 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP7 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP8 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP9 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP10 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER1 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER2 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER3 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER4 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER5 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER6 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER7 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER8 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER9 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER10 , @lc_ss_Required_Acknowledgement , @ln_ss_Acknowledge_Within_Days , @lc_ss_Invoice_Channel , @lc_ss_Payee_Service_Level, @lc_ss_Pay_Each_Document_Alone , @lc_ss_Creation_Date , @lc_ss_Last_Update_Date , @lc_ss_Created_By, @lc_ss_Updated_By
							WHILE @@FETCH_STATUS = 0 
							BEGIN 
								BEGIN TRY  -- Try Catch for Supplier Sites FBDI
								PRINT '------Supplier Site- ' + CAST(ISNULL(@lc_ss_Supplier_Site,'NULL') AS NVARCHAR(255));
								SET @lc_Err_Msg 	= '';
								SET @ln_warning	= 0
								SET @lc_war_msg	= ''

								SET @lc_party_number	=''
								SET @lc_concatenated_string =''
								SET @ln_erp = 0
								
								
								
								IF (ISNULL(@lc_ss_Supplier_Site,'') = '')
									BEGIN
										PRINT '------Supplier Site NULL ';
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Site cannot be NULL. ',1,4000)
									END
									
									
								SET @ln_check = LEN(@lc_ss_Supplier_Site)
								IF @ln_check > 15 
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Site length cannot exceed 15. ',1,4000)
									END
									
									
								-- Check if BU is not null
								IF (@lc_ss_Procurement_BU IS NULL OR @lc_ss_Procurement_BU = '')
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Procurement BU cannot be NULL. ',1,4000)
									END
								ELSE
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_BUSINESS_UNIT' , @lc_ss_Procurement_BU
										
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Procurement BU is Invalid. ',1,4000)
										END
								END
								
								
								-- Check if the Invoice Currency is Valid
								IF (@lc_ss_Invoice_Currency IS NOT NULL AND @lc_ss_Invoice_Currency <>'')
									
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_CURRENCY_CODE' , @lc_ss_Invoice_Currency
										
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Invoice Currency is Invalid. ',1,4000)
										END
									END
								
								-- Check if the Payment Currency is Valid
								IF (@lc_ss_Payment_Currency IS NOT NULL AND @lc_ss_Payment_Currency <> '')
									
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_CURRENCY_CODE' , @lc_ss_Payment_Currency
										
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Payment Currency is Invalid. ',1,4000)
										END
									END
									
								-- Check if the Payment Terms is Valid
								IF (@lc_ss_Payment_Terms IS NOT NULL AND @lc_ss_Payment_Terms <> '')
									
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_AP_TERMS' , @lc_ss_Payment_Terms
										
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Payment Terms is Invalid. ',1,4000)
										END
									END
									
								-- Check if the Term Date Basis is Valid
								IF (@lc_ss_Terms_Date_Basis IS NOT NULL AND @lc_ss_Terms_Date_Basis<> '')
									
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_TERMS_DATE_BASIS' , @lc_ss_Terms_Date_Basis
										
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Term Date Basis  is Invalid. ',1,4000)
										END
									END
								
								-- Check if the Term Date Basis is Valid
								IF (@lc_ss_Payment_Method IS NOT NULL AND @lc_ss_Payment_Method <>'')
									
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_VALID_PAY_METHOD' , @lc_ss_Payment_Method
										
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Payment Method is Invalid. ',1,4000)
										END
									END
								
								
								-- Check if the Supplier Sites has its corresponding Supplier
								SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_suppliers_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND supplier_name		= @lc_ss_Supplier_Name
												)
										
								IF @ln_check < 1
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier is invalid for this supplier Site. ',1,4000)
								END
								
								-- Check if the Supplier Sites has its corresponding Supplier Address
								SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_supplier_addresses_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND address_name		= @lc_ss_Address_Name
													AND supplier_name		= @lc_ss_Supplier_Name
												)
										
								IF @ln_check < 1
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Address is invalid for this supplier Site. ',1,4000)
								END

								--EXEC @lc_party_number = hiscox_get_lookup_meaning_f 'HISCOX_POZ_EXISTING_SUPPLIERS' , @lc_ss_Supplier_Name
									SET @lc_party_number = (
																SELECT  meaning 
																FROM hiscox_lookup_ref_tbl
																WHERE 1					= 1
																AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
																AND UPPER(lookup_code)	= UPPER(@lc_ss_Supplier_Name)
																)
									
								/*EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_new_batch_id
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_new_batch_id
													,@p_sec_record_id  = @p_load_batch_id
													,@p_ter_record_id  = @p_batch_id
													,@p_err_code       = 'DEBUG'
													,@p_err_column     = 'DEBUG'  
													,@p_err_value      = 'Party Number'  
													,@p_err_desc       = @lc_party_number
													,@p_rect_action    = NULL;   */

								IF @lc_party_number <> ''
									BEGIN
										SET @lc_concatenated_string	= UPPER(CONCAT(@lc_party_number,'-',@lc_ss_Supplier_Site,'-',@lc_ss_Procurement_BU))
										SET @ln_check				= (
																		SELECT count(*)
																		FROM hiscox_lookup_ref_tbl
																		WHERE 1=1
																		AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITES'
																		AND UPPER(lookup_code)  = @lc_concatenated_string										
																		)
										--EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_SUPPLIER_SITES', @lc_concatenated_string

										/*EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
													,@p_batch_id       = @p_new_batch_id
													,@p_process_id     = 0          
													,@p_source         = @lc_source
													,@p_pri_record_id  = @p_new_batch_id
													,@p_sec_record_id  = @p_load_batch_id
													,@p_ter_record_id  = @p_batch_id
													,@p_err_code       = 'SUPPLIER_SITE'
													,@p_err_column     = @lc_ss_Supplier_Name
													,@p_err_value      = @ln_check  
													,@p_err_desc       = @lc_concatenated_string
													,@p_rect_action    = NULL;*/
										IF @ln_check > 0
											BEGIN
												SET @ln_erp =1
											END
										ELSE
											BEGIN
												-- Check if the there is any Primary Pay Site in Oracle Cloud and if yes, update the Primary Pay Flag current record to N
												SET @ln_check = (
													SELECT  count(*) 
													FROM hiscox_lookup_ref_tbl
													WHERE 1				= 1
													AND lookup_type		= 'HISCOX_POZ_SUPPLIER_SITES'
													AND lookup_code		LIKE CONCAT(@lc_party_number,'-%-',@lc_ss_Procurement_BU)
													AND attribute3		= 'Y'
													)
												IF @ln_check > 0 
												BEGIN
													UPDATE hiscox_poz_supplier_sites_int_stg
													SET primary_pay			= 'N'
													WHERE record_id         = @ln_ss_Record_ID;
												END
											END
									END
								
								-- If there exists a primary pay site in the current run update the primary pay site to N
								SET @ln_check = (	
													SELECT  count(*) 
													FROM hiscox_poz_supplier_sites_int_stg
													WHERE 1				= 1
													AND batch_id		=	@p_new_batch_id
													AND record_id		<>	@ln_ss_Record_ID
													AND procurement_bu	=   @lc_ss_Procurement_BU
													AND process_flag	=	'S'
													--AND supplier_site	=   @lc_ss_supplier_site
													AND supplier_name	=	@lc_ss_Supplier_Name
													AND primary_pay		=   'Y'
												)

								IF @ln_check > 0 
												BEGIN
													UPDATE hiscox_poz_supplier_sites_int_stg
													SET primary_pay			= 'N'
													WHERE record_id         = @ln_ss_Record_ID;
												END
							
								SET @ln_check = (
													SELECT  count(*) 
													FROM hiscox_poz_supplier_sites_int_stg
													WHERE 1				= 1
													AND batch_id		=	@p_new_batch_id
													AND record_id		<>	@ln_ss_Record_ID
													AND procurement_bu	=   @lc_ss_Procurement_BU
													AND process_flag	=	'S'
													AND supplier_site	=   @lc_ss_supplier_site
													AND supplier_name	=	@lc_ss_Supplier_Name
												)
												
								IF @ln_check <> 0 
									BEGIN
										--SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Site '+@lc_ss_supplier_site+' for the supplier '+ @lc_ss_Supplier_Name +' already exists in the current run. ',1,4000)
										SET @ln_warning	= 1
										SET @lc_war_msg	= SUBSTRING(@lc_war_msg  +'Supplier Site for this supplier already exists in the current run. ',1,4000)
									END
											
								IF @ln_erp = 1
										BEGIN
											UPDATE hiscox_poz_supplier_sites_int_stg
											SET process_flag    	= 'S'
											,oracle_process_flag	= 'S'
											,error_message			= 'Supplier Site already exists in ERP Cloud'
											,last_update_date		= GETDATE()
											,updated_by				= USER_NAME()
											WHERE record_id     	= @ln_ss_Record_ID
										END
								ELSE IF @ln_warning = 1 
									BEGIN
										UPDATE hiscox_poz_supplier_sites_int_stg
										SET process_flag   	 	='W',
										error_message           ='Duplicate - '+@lc_war_msg,
										last_update_date		= GETDATE(),
										updated_by				= USER_NAME()
										WHERE record_id         = @ln_ss_Record_ID;
									END 
								ELSE
									BEGIN
										-- Check if the validation has failed
										IF @lc_Err_Msg <> ''
										BEGIN
											THROW 60000,@lc_Err_Msg,5
										END
								
										UPDATE hiscox_poz_supplier_sites_int_stg
										SET process_flag   	 	='S',
										last_update_date		= GETDATE(),
										updated_by				= USER_NAME()
										WHERE record_id         = @ln_ss_Record_ID;
									END
								
								
								END TRY   -- Try Catch for Supplier Sites FBDI
													
								BEGIN CATCH  -- Try Catch for Supplier Sites FBDI
									
									SET @lc_Err_Msg = ERROR_MESSAGE()
									
									UPDATE hiscox_poz_supplier_sites_int_Stg
									SET process_flag 		= 'E',
									Error_Message    		= @lc_Err_Msg,
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         = @ln_ss_Record_ID;			
									
														
								END CATCH   -- Try Catch for Supplier Sites FBDI
								FETCH NEXT FROM cur_suppliers_sites INTO @ln_ss_Record_ID, @lc_ss_Process_Flag , @lc_ss_Error_Message, @lc_ss_Batch_ID, @ln_ss_Source_Record_ID, @lc_ss_Import_Action, @lc_ss_Supplier_Name, @lc_ss_Procurement_BU, @lc_ss_Address_Name, @lc_ss_Supplier_Site, @lc_ss_Supplier_Site_New, @lc_ss_Inactive_Date , @lc_ss_Sourcing_only , @lc_ss_Purchasing , @lc_ss_Procurement_card , @lc_ss_Pay , @lc_ss_Primary_Pay , @lc_ss_Income_tax_reporting_site , @lc_ss_Alternate_Site_Name, @lc_ss_Customer_Number, @lc_ss_B2B_Communication_Method, @lc_ss_B2B_Supplier_Site_Code, @lc_ss_Communication_Method, @lc_ss_E_Mail, @lc_ss_Fax_Country_Code, @lc_ss_Fax_Area_Code, @lc_ss_Fax, @lc_ss_Hold_all_new_purchasing_documents , @lc_ss_Hold_Reason, @lc_ss_Carrier, @lc_ss_Mode_of_Transport, @lc_ss_Service_Level, @lc_ss_Freight_Terms, @lc_ss_Pay_on_receipt, @lc_ss_FOB, @lc_ss_Country_of_Origin, @lc_ss_Buyer_Managed_Transportation , @lc_ss_Pay_on_use , @lc_ss_Aging_Onset_Point, @lc_ss_Aging_Period_Days , @lc_ss_Consumption_Advice_Frequency, @lc_ss_Consumption_Advice_Summary, @lc_ss_Default_Pay_Site, @lc_ss_Invoice_Summary_Level, @lc_ss_Gapless_invoice_numbering , @lc_ss_Selling_Company_Identifier, @lc_ss_Create_debit_memo_from_return, @lc_ss_Ship_to_Exception_Action, @lc_ss_Receipt_Routing , @lc_ss_Over_receipt_Tolerance , @lc_ss_Over_receipt_Action, @lc_ss_Early_Receipt_Tolerance_in_Days , @lc_ss_Late_Receipt_Tolerance_in_Days , @lc_ss_Allow_Substitute_Receipts , @lc_ss_Allow_unordered_receipts , @lc_ss_Receipt_Date_Exception, @lc_ss_Invoice_Currency, @lc_ss_Invoice_Amount_Limit , @lc_ss_Invoice_Match_Option, @lc_ss_Match_Approval_Level , @lc_ss_Payment_Currency, @lc_ss_Payment_Priority , @lc_ss_Pay_Group, @lc_ss_Quantity_Tolerances, @lc_ss_Amount_Tolerance, @lc_ss_Hold_All_Invoices , @lc_ss_Hold_Unmatched_Invoices , @lc_ss_Hold_Unvalidated_Invoices , @lc_ss_Payment_Hold_By , @lc_ss_Payment_Hold_Date , @lc_ss_Payment_Hold_Reason, @lc_ss_Payment_Terms, @lc_ss_Terms_Date_Basis, @lc_ss_Pay_Date_Basis, @lc_ss_Bank_Charge_Deduction_Type, @lc_ss_Always_Take_Discount , @lc_ss_Exclude_Freight_From_Discount , @lc_ss_Exclude_Tax_From_Discount , @lc_ss_Create_Interest_Invoices , @lc_ss_Vat_Code, @lc_ss_Tax_Registration_Number, @lc_ss_Payment_Method, @lc_ss_Delivery_Channel, @lc_ss_Bank_Instruction_1, @lc_ss_Bank_Instruction_2, @lc_ss_Bank_Instruction, @lc_ss_Settlement_Priority, @lc_ss_Payment_Text_Message_1, @lc_ss_Payment_Text_Message_2, @lc_ss_Payment_Text_Message_3, @lc_ss_Bank_Charge_Bearer, @lc_ss_Payment_Reason, @lc_ss_Payment_Reason_Comments, @lc_ss_Delivery_Method, @lc_ss_Remittance_E_Mail, @lc_ss_Remittance_Fax, @lc_ss_ATTRIBUTE_CATEGORY, @lc_ss_ATTRIBUTE1, @lc_ss_ATTRIBUTE2, @lc_ss_ATTRIBUTE3, @lc_ss_ATTRIBUTE4, @lc_ss_ATTRIBUTE5, @lc_ss_ATTRIBUTE6, @lc_ss_ATTRIBUTE7, @lc_ss_ATTRIBUTE8, @lc_ss_ATTRIBUTE9, @lc_ss_ATTRIBUTE10, @lc_ss_ATTRIBUTE11, @lc_ss_ATTRIBUTE12, @lc_ss_ATTRIBUTE13, @lc_ss_ATTRIBUTE14, @lc_ss_ATTRIBUTE15, @lc_ss_ATTRIBUTE16, @lc_ss_ATTRIBUTE17, @lc_ss_ATTRIBUTE18, @lc_ss_ATTRIBUTE19, @lc_ss_ATTRIBUTE20, @lc_ss_ATTRIBUTE_DATE1 , @lc_ss_ATTRIBUTE_DATE2 , @lc_ss_ATTRIBUTE_DATE3 , @lc_ss_ATTRIBUTE_DATE4 , @lc_ss_ATTRIBUTE_DATE5 , @lc_ss_ATTRIBUTE_DATE6 , @lc_ss_ATTRIBUTE_DATE7 , @lc_ss_ATTRIBUTE_DATE8 , @lc_ss_ATTRIBUTE_DATE9 , @lc_ss_ATTRIBUTE_DATE10 , @lc_ss_ATTRIBUTE_TIMESTAMP1 , @lc_ss_ATTRIBUTE_TIMESTAMP2 , @lc_ss_ATTRIBUTE_TIMESTAMP3 , @lc_ss_ATTRIBUTE_TIMESTAMP4 , @lc_ss_ATTRIBUTE_TIMESTAMP5 , @lc_ss_ATTRIBUTE_TIMESTAMP6 , @lc_ss_ATTRIBUTE_TIMESTAMP7 , @lc_ss_ATTRIBUTE_TIMESTAMP8 , @lc_ss_ATTRIBUTE_TIMESTAMP9 , @lc_ss_ATTRIBUTE_TIMESTAMP10 , @ln_ss_ATTRIBUTE_NUMBER1 , @ln_ss_ATTRIBUTE_NUMBER2 , @ln_ss_ATTRIBUTE_NUMBER3 , @ln_ss_ATTRIBUTE_NUMBER4 , @ln_ss_ATTRIBUTE_NUMBER5 , @ln_ss_ATTRIBUTE_NUMBER6 , @ln_ss_ATTRIBUTE_NUMBER7 , @ln_ss_ATTRIBUTE_NUMBER8 , @ln_ss_ATTRIBUTE_NUMBER9 , @ln_ss_ATTRIBUTE_NUMBER10 , @lc_ss_GLOBAL_ATTRIBUTE_CATEGORY, @lc_ss_GLOBAL_ATTRIBUTE1, @lc_ss_GLOBAL_ATTRIBUTE2, @lc_ss_GLOBAL_ATTRIBUTE3, @lc_ss_GLOBAL_ATTRIBUTE4, @lc_ss_GLOBAL_ATTRIBUTE5, @lc_ss_GLOBAL_ATTRIBUTE6, @lc_ss_GLOBAL_ATTRIBUTE7, @lc_ss_GLOBAL_ATTRIBUTE8, @lc_ss_GLOBAL_ATTRIBUTE9, @lc_ss_GLOBAL_ATTRIBUTE10, @lc_ss_GLOBAL_ATTRIBUTE11, @lc_ss_GLOBAL_ATTRIBUTE12, @lc_ss_GLOBAL_ATTRIBUTE13, @lc_ss_GLOBAL_ATTRIBUTE14, @lc_ss_GLOBAL_ATTRIBUTE15, @lc_ss_GLOBAL_ATTRIBUTE16, @lc_ss_GLOBAL_ATTRIBUTE17, @lc_ss_GLOBAL_ATTRIBUTE18, @lc_ss_GLOBAL_ATTRIBUTE19, @lc_ss_GLOBAL_ATTRIBUTE20, @ld_ss_GLOBAL_ATTRIBUTE_DATE1 , @ld_ss_GLOBAL_ATTRIBUTE_DATE2 , @ld_ss_GLOBAL_ATTRIBUTE_DATE3 , @ld_ss_GLOBAL_ATTRIBUTE_DATE4 , @ld_ss_GLOBAL_ATTRIBUTE_DATE5 , @ld_ss_GLOBAL_ATTRIBUTE_DATE6 , @ld_ss_GLOBAL_ATTRIBUTE_DATE7 , @ld_ss_GLOBAL_ATTRIBUTE_DATE8 , @ld_ss_GLOBAL_ATTRIBUTE_DATE9 , @ld_ss_GLOBAL_ATTRIBUTE_DATE10 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP1 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP2 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP3 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP4 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP5 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP6 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP7 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP8 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP9 , @ld_ss_GLOBAL_ATTRIBUTE_TIMESTAMP10 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER1 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER2 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER3 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER4 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER5 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER6 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER7 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER8 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER9 , @ln_ss_GLOBAL_ATTRIBUTE_NUMBER10 , @lc_ss_Required_Acknowledgement , @ln_ss_Acknowledge_Within_Days , @lc_ss_Invoice_Channel , @lc_ss_Payee_Service_Level, @lc_ss_Pay_Each_Document_Alone , @lc_ss_Creation_Date , @lc_ss_Last_Update_Date , @lc_ss_Created_By, @lc_ss_Updated_By
							END  -- END Supplier Sites FBDI
							CLOSE cur_suppliers_sites
							DEALLOCATE cur_suppliers_sites
							
						END
						PRINT '--Supplier Sites END-------- ';
						
						
						PRINT '--Supplier Sites Assignments Start-------- ';
						DECLARE cur_suppliers_site_assign CURSOR LOCAL FOR 
						SELECT Record_ID,Process_Flag, Error_Message,Batch_ID,Source_Record_ID,Import_Action,Supplier_Name,Supplier_Site,Procurement_BU,Client_BU,Bill_to_BU,Ship_to_Location,Bill_to_Location,Use_Withholding_Tax,Withholding_Tax_Group,Liability_Distribution,Prepayment_Distribution,Bills_Payable_Distribution,Distribution_Set,Inactive_Date ,Creation_Date ,Last_Update_Date ,Created_By ,Updated_By 
						FROM hiscox_poz_site_assignments_int_stg
						WHERE 1=1
						--AND process_flag      = 'N'
						AND batch_id            = @p_new_batch_id
						
						
						OPEN cur_suppliers_site_assign  
						FETCH NEXT FROM cur_suppliers_site_assign INTO @ln_ssa_Record_ID,@lc_ssa_Process_Flag, @lc_ssa_Error_Message,@lc_ssa_Batch_ID,@ln_ssa_Source_Record_ID,@lc_ssa_Import_Action,@lc_ssa_Supplier_Name,@lc_ssa_Supplier_Site,@lc_ssa_Procurement_BU,@lc_ssa_Client_BU,@lc_ssa_Bill_to_BU,@lc_ssa_Ship_to_Location,@lc_ssa_Bill_to_Location,@lc_ssa_Use_Withholding_Tax,@lc_ssa_Withholding_Tax_Group,@lc_ssa_Liability_Distribution,@lc_ssa_Prepayment_Distribution,@lc_ssa_Bills_Payable_Distribution,@lc_ssa_Distribution_Set,@lc_ssa_Inactive_Date ,@lc_ssa_Creation_Date ,@lc_ssa_Last_Update_Date ,@lc_ssa_Created_By ,@lc_ssa_Updated_By 
						WHILE @@FETCH_STATUS = 0 
						BEGIN 
							BEGIN TRY
								PRINT '--------Supplier Site Assign - ' + CAST(ISNULL(@lc_ssa_Supplier_Site,'NULL') AS VARCHAR) + 'Proc BU -'+CAST(ISNULL(@lc_ssa_Procurement_BU,'NULL') AS VARCHAR);
								SET @lc_Err_Msg = ''
								SET @ln_warning	= 0
								SET @lc_war_msg	= ''
								SET @lc_party_number	=''
								SET @lc_concatenated_string =''
								SET @ln_erp = 0
								
								
								SET @ln_check = len(@lc_ssa_Supplier_Site)
								
								IF (ISNULL(@lc_ssa_Supplier_Site,'') = '')
									BEGIN
										PRINT '------Supplier Site NULL ';
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Site cannot be NULL. ',1,4000)
									END
								ELSE IF @ln_check > 15 
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Site length cannot exceed 15. ',1,4000)
									END
															
								-- Check if its valid Procurement BU
								IF (ISNULL(@lc_ssa_Procurement_BU,'') = '')
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Procurement BU cannot be NULL. ',1,4000)
									END
								ELSE
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_BUSINESS_UNIT' , @lc_ssa_Procurement_BU
								
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Procurement BU is Invalid. ',1,4000)
										END
								END
															
								-- Check if its valid Client BU
								IF (@lc_ssa_Client_BU IS NULL OR @lc_ssa_Client_BU = '')
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Procurement BU cannot be NULL. ',1,4000)
									END
								ELSE
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_BUSINESS_UNIT' , @lc_ssa_Client_BU
								
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Client BU is Invalid. ',1,4000)
										END
								END
															
															
								-- Check if its valid Bill to BU
								IF @lc_ssa_Bill_to_BU IS NOT NULL
									BEGIN
										EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_BUSINESS_UNIT' , @lc_ssa_Bill_to_BU
								
										IF @ln_check = 0
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +' Bill BU is Invalid. ',1,4000)
										END
									END
									
									
								-- Check if Liability Distribution is given.
								IF (@lc_ssa_Liability_Distribution IS NULL OR @lc_ssa_Liability_Distribution = '')
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Liability Distribution cannot be NULL. ',1,4000)
									END
								
								
								-- Check if the Supplier Site Assignment has its corresponding Supplier
								SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_suppliers_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND supplier_name		= @lc_ssa_Supplier_Name
												)
										
								IF @ln_check < 1
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier is invalid for this supplier Site Assignment. ',1,4000)
								END
								
								-- Check if the Supplier Site Assignment has its corresponding Supplier Site
								SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_supplier_sites_int_Stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND supplier_site		= @lc_ssa_supplier_site
													AND supplier_name		= @lc_ssa_Supplier_Name
												)
										
								IF @ln_check < 1
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Site is invalid for this supplier Site Assignment. ',1,4000)
								END
								
								SET @ln_check = (
															SELECT  count(*) 
															FROM hiscox_poz_site_assignments_int_stg
															WHERE 1				= 1
															AND batch_id		=	@p_new_batch_id
															AND record_id		<>	@ln_ssa_Record_ID
															AND process_flag	=	'S'
															AND supplier_site	=   @lc_ssa_supplier_site
															AND supplier_name	=	@lc_ssa_Supplier_Name
															AND client_bu		=   @lc_ssa_client_bu
															AND Procurement_BU  =   @lc_ssa_Procurement_BU
														)
										
										
										IF @ln_check <> 0 
											BEGIN
											--SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'This Supplier Site Assignment for BU-'+@lc_ssa_Procurement_BU +' Site-'+@lc_ssa_supplier_site+' for the supplier '+ @lc_ss_Supplier_Name +' already exists in the current run. ',1,4000)
												SET @ln_warning	= 1
												SET @lc_war_msg	= SUBSTRING(@lc_war_msg  +'This Supplier Site Assignment for this BU already exists in the current run. ',1,4000)
											END
								/*-- Check if the Supplier Sites has its corresponding Supplier Address
								SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_supplier_addresses_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND address_name		= @lc_ssa_Address_Name
													AND supplier_name		= @lc_ssa_Supplier_Name
												)
										
								IF @ln_check <> 1
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Address -'+@lc_ssa_Address_Name+' is invalid for this supplier Site. ',1,4000)
								END*/
								
								--EXEC @lc_party_number = hiscox_get_lookup_meaning_f 'HISCOX_POZ_EXISTING_SUPPLIERS' , @lc_ssa_Supplier_Name
								
								
								
								
								SET @lc_party_number =	''
								SET @lc_party_number = (
																SELECT  meaning 
																FROM hiscox_lookup_ref_tbl
																WHERE 1					= 1
																AND lookup_type			= 'HISCOX_POZ_EXISTING_SUPPLIERS'
																AND UPPER(lookup_code)	= UPPER(@lc_ssa_Supplier_Name)
																)
								
								
								IF @lc_party_number <> ''
									BEGIN
										SET @lc_concatenated_string	= UPPER(CONCAT(@lc_party_number,'-',@lc_ssa_supplier_site,'-',@lc_ssa_Procurement_BU))
										SET @lc_party_number =''
										SET @lc_party_number =(
																				SELECT meaning
																				FROM hiscox_lookup_ref_tbl
																				WHERE 1=1
																				AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITES'
																				AND UPPER(lookup_code)  = @lc_concatenated_string										
																				)
																				
										
										--EXEC @lc_party_number = hiscox_get_lookup_meaning_f 'HISCOX_POZ_SUPPLIER_SITES', @lc_concatenated_string
										IF @lc_party_number <> ''
											BEGIN
												SET @lc_concatenated_string  = UPPER(CONCAT(@lc_party_number,'-',@lc_ssa_client_bu))
												SET @ln_check				= (
																				SELECT count(*)
																				FROM hiscox_lookup_ref_tbl
																				WHERE 1=1
																				AND lookup_type			= 'HISCOX_POZ_SUPPLIER_SITE_ASS'
																				AND UPPER(lookup_code)  = @lc_concatenated_string										
																				)
												--EXEC @ln_check = hiscox_does_lookup_code_exist 'HISCOX_POZ_SUPPLIER_SITE_ASS',@lc_concatenated_string
												
												IF @ln_check > 0
												BEGIN
													SET @ln_erp =1
												END
											END
									END
								
								-- Check if the validation has failed	
								IF @ln_erp = 1
										BEGIN
											UPDATE hiscox_poz_site_assignments_int_stg
											SET process_flag    	= 'S'
											,oracle_process_flag	= 'S'
											,error_message			= 'Supplier Site Assignment already exists in ERP Cloud'
											,last_update_date		= GETDATE()
											,updated_by				= USER_NAME()
											WHERE record_id     	= @ln_ssa_Record_ID
										END
								ELSE IF @ln_warning = 1 
									BEGIN
										UPDATE hiscox_poz_site_assignments_int_stg
										SET process_flag        ='W',
										error_message           ='Duplicate - '+@lc_war_msg,
										last_update_date		= GETDATE(),
										updated_by				= USER_NAME()
										WHERE record_id         =@ln_ssa_Record_ID
									END 
								ELSE 
									BEGIN
										IF @lc_Err_Msg <> ''
											BEGIN
												THROW 60000,@lc_Err_Msg,5
											END
									
								
									UPDATE hiscox_poz_site_assignments_int_stg
									SET process_flag        ='S',
									last_update_date		= GETDATE(),
									updated_by				= USER_NAME()
									WHERE record_id         =@ln_ssa_Record_ID
								
									END	
								
							END TRY
							BEGIN CATCH
								SET @lc_Err_Msg = ERROR_MESSAGE()
															
								UPDATE hiscox_poz_site_assignments_int_stg
								SET process_flag        ='E',
								error_message           = @lc_Err_Msg,
								last_update_date		= GETDATE(),
								updated_by				= USER_NAME()
								WHERE record_id         =@ln_ssa_Record_ID
														
							END CATCH
							FETCH NEXT FROM cur_suppliers_site_assign INTO @ln_ssa_Record_ID,@lc_ssa_Process_Flag, @lc_ssa_Error_Message,@lc_ssa_Batch_ID,@ln_ssa_Source_Record_ID,@lc_ssa_Import_Action,@lc_ssa_Supplier_Name,@lc_ssa_Supplier_Site,@lc_ssa_Procurement_BU,@lc_ssa_Client_BU,@lc_ssa_Bill_to_BU,@lc_ssa_Ship_to_Location,@lc_ssa_Bill_to_Location,@lc_ssa_Use_Withholding_Tax,@lc_ssa_Withholding_Tax_Group,@lc_ssa_Liability_Distribution,@lc_ssa_Prepayment_Distribution,@lc_ssa_Bills_Payable_Distribution,@lc_ssa_Distribution_Set,@lc_ssa_Inactive_Date ,@lc_ssa_Creation_Date ,@lc_ssa_Last_Update_Date ,@lc_ssa_Created_By ,@lc_ssa_Updated_By 
						END  -- END Supplier Site Assignment FBDI
						CLOSE cur_suppliers_site_assign
						DEALLOCATE cur_suppliers_site_assign
						PRINT '--Supplier Sites Assignments END-------- ';
						
						/*
						DECLARE cur_suppliers_contacts CURSOR LOCAL FOR 
						SELECT Record_ID, Process_Flag  , Error_Message , Batch_ID , Source_Record_ID , Import_Action , Supplier_Name , Prefix , First_Name, First_Name_New , Middle_Name, Last_Name  , Last_Name_New  , Job_Title , Administrative_Contact  , E_Mail , E_Mail_New , Phone_Country_Code , Phone_Area_Code , Phone , Phone_Extension , Fax_Country_Code , Fax_Area_Code , Fax , Mobile_Country_Code , Mobile_Area_Code , Mobile , Inactive_Date  , ATTRIBUTE_CATEGORY , ATTRIBUTE1  , ATTRIBUTE2  , ATTRIBUTE3  , ATTRIBUTE4  , ATTRIBUTE5  , ATTRIBUTE6  , ATTRIBUTE7  , ATTRIBUTE8  , ATTRIBUTE9  , ATTRIBUTE10  , ATTRIBUTE11  , ATTRIBUTE12  , ATTRIBUTE13  , ATTRIBUTE14  , ATTRIBUTE15  , ATTRIBUTE16  , ATTRIBUTE17  , ATTRIBUTE18  , ATTRIBUTE19  , ATTRIBUTE20  , ATTRIBUTE21  , ATTRIBUTE22  , ATTRIBUTE23  , ATTRIBUTE24  , ATTRIBUTE25  , ATTRIBUTE26  , ATTRIBUTE27  , ATTRIBUTE28  , ATTRIBUTE29  , ATTRIBUTE30, ATTRIBUTE_NUMBER1 , ATTRIBUTE_NUMBER2 , ATTRIBUTE_NUMBER3 , ATTRIBUTE_NUMBER4 , ATTRIBUTE_NUMBER5 , ATTRIBUTE_NUMBER6 , ATTRIBUTE_NUMBER7 , ATTRIBUTE_NUMBER8 , ATTRIBUTE_NUMBER9 , ATTRIBUTE_NUMBER10 , ATTRIBUTE_NUMBER11 , ATTRIBUTE_NUMBER12 , ATTRIBUTE_DATE1  , ATTRIBUTE_DATE2  , ATTRIBUTE_DATE3  , ATTRIBUTE_DATE4  , ATTRIBUTE_DATE5  , ATTRIBUTE_DATE6  , ATTRIBUTE_DATE7  , ATTRIBUTE_DATE8  , ATTRIBUTE_DATE9  , ATTRIBUTE_DATE10  , ATTRIBUTE_DATE11  , ATTRIBUTE_DATE12  , Creation_Date   , Last_Update_Date  , Created_By , Updated_By 
						FROM hiscox_poz_sup_contacts_int_stg
						WHERE 1=1
						AND batch_id            = @p_new_batch_id
						
						OPEN cur_suppliers_contacts  
						FETCH NEXT FROM cur_suppliers_contacts INTO @ln_sc_Record_ID, @lc_sc_Process_Flag  , @lc_sc_Error_Message , @lc_sc_Batch_ID , @ln_sc_Source_Record_ID , @lc_sc_Import_Action , @lc_sc_Supplier_Name , @lc_sc_Prefix , @lc_sc_First_Name, @lc_sc_First_Name_New , @lc_sc_Middle_Name, @lc_sc_Last_Name  , @lc_sc_Last_Name_New  , @lc_sc_Job_Title , @lc_sc_Administrative_Contact  , @lc_sc_E_Mail , @lc_sc_E_Mail_New , @lc_sc_Phone_Country_Code , @lc_sc_Phone_Area_Code , @lc_sc_Phone , @lc_sc_Phone_Extension , @lc_sc_Fax_Country_Code , @lc_sc_Fax_Area_Code , @lc_sc_Fax , @lc_sc_Mobile_Country_Code , @lc_sc_Mobile_Area_Code , @lc_sc_Mobile , @lc_sc_Inactive_Date  , @lc_sc_ATTRIBUTE_CATEGORY , @lc_sc_ATTRIBUTE1  , @lc_sc_ATTRIBUTE2  , @lc_sc_ATTRIBUTE3  , @lc_sc_ATTRIBUTE4  , @lc_sc_ATTRIBUTE5  , @lc_sc_ATTRIBUTE6  , @lc_sc_ATTRIBUTE7  , @lc_sc_ATTRIBUTE8  , @lc_sc_ATTRIBUTE9  , @lc_sc_ATTRIBUTE10  , @lc_sc_ATTRIBUTE11  , @lc_sc_ATTRIBUTE12  , @lc_sc_ATTRIBUTE13  , @lc_sc_ATTRIBUTE14  , @lc_sc_ATTRIBUTE15  , @lc_sc_ATTRIBUTE16  , @lc_sc_ATTRIBUTE17  , @lc_sc_ATTRIBUTE18  , @lc_sc_ATTRIBUTE19  , @lc_sc_ATTRIBUTE20  , @lc_sc_ATTRIBUTE21  , @lc_sc_ATTRIBUTE22  , @lc_sc_ATTRIBUTE23  , @lc_sc_ATTRIBUTE24  , @lc_sc_ATTRIBUTE25  , @lc_sc_ATTRIBUTE26  , @lc_sc_ATTRIBUTE27  , @lc_sc_ATTRIBUTE28  , @lc_sc_ATTRIBUTE29  , @lc_sc_ATTRIBUTE30, @ln_sc_ATTRIBUTE_NUMBER1 , @ln_sc_ATTRIBUTE_NUMBER2 , @ln_sc_ATTRIBUTE_NUMBER3 , @ln_sc_ATTRIBUTE_NUMBER4 , @ln_sc_ATTRIBUTE_NUMBER5 , @ln_sc_ATTRIBUTE_NUMBER6 , @ln_sc_ATTRIBUTE_NUMBER7 , @ln_sc_ATTRIBUTE_NUMBER8 , @ln_sc_ATTRIBUTE_NUMBER9 , @ln_sc_ATTRIBUTE_NUMBER10 , @ln_sc_ATTRIBUTE_NUMBER11 , @ln_sc_ATTRIBUTE_NUMBER12 , @ld_sc_ATTRIBUTE_DATE1  , @ld_sc_ATTRIBUTE_DATE2  , @ld_sc_ATTRIBUTE_DATE3  , @ld_sc_ATTRIBUTE_DATE4  , @ld_sc_ATTRIBUTE_DATE5  , @ld_sc_ATTRIBUTE_DATE6  , @ld_sc_ATTRIBUTE_DATE7  , @ld_sc_ATTRIBUTE_DATE8  , @ld_sc_ATTRIBUTE_DATE9  , @ld_sc_ATTRIBUTE_DATE10  , @ld_sc_ATTRIBUTE_DATE11  , @ld_sc_ATTRIBUTE_DATE12  , @lc_sc_Creation_Date   , @lc_sc_Last_Update_Date  , @lc_sc_Created_By , @lc_sc_Updated_By 
						WHILE @@FETCH_STATUS = 0 
						BEGIN 
							BEGIN TRY   -- Try Catch for Supplier Contacts FBDI
								PRINT '----Supplier Contact- ' + CAST(@lc_sc_First_Name AS VARCHAR);
										
								SET @lc_Err_Msg		= 	'';
								
								IF @lc_sc_First_Name IS NULL 
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'First Name cannot be NULL for Supplier Contact. ',1,4000)
									END
									
								IF @lc_sc_Last_Name IS NULL 
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'First Name cannot be NULL for Supplier Contact. ',1,4000)
									END
									
								-- Check if the Supplier Sites has its corresponding Supplier
								SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_suppliers_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND supplier_name		= @lc_sc_Supplier_Name
												)
										
								IF @ln_check < 1
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier is invalid for this supplier Contact. ',1,4000)
								END
									
								-- Check if the validation has failed
								IF @lc_Err_Msg <> ''
									BEGIN
										THROW 60000,@lc_Err_Msg,5
									END
								
								
								UPDATE hiscox_poz_sup_contacts_int_stg
								SET process_flag		='S',
								last_update_date		= GETDATE(),
								updated_by				= USER_NAME()
								WHERE record_id =@ln_sc_Record_ID
								
							END TRY     -- Try Catch for Supplier Contacts FBDI
							BEGIN CATCH -- Try Catch for Supplier Contacts FBDI
								
								SET @lc_Err_Msg 		= ERROR_MESSAGE()
								
								UPDATE hiscox_poz_sup_contacts_int_stg
								SET process_flag		='E',
								error_message   		='Exception -' + @lc_Err_Msg,
								last_update_date		= GETDATE(),
								updated_by				= USER_NAME()
								WHERE record_id =@ln_sc_Record_ID
								
							END CATCH   -- Try Catch for Supplier Contacts FBDI
							FETCH NEXT FROM cur_suppliers_contacts INTO @ln_sc_Record_ID, @lc_sc_Process_Flag  , @lc_sc_Error_Message , @lc_sc_Batch_ID , @ln_sc_Source_Record_ID , @lc_sc_Import_Action , @lc_sc_Supplier_Name , @lc_sc_Prefix , @lc_sc_First_Name, @lc_sc_First_Name_New , @lc_sc_Middle_Name, @lc_sc_Last_Name  , @lc_sc_Last_Name_New  , @lc_sc_Job_Title , @lc_sc_Administrative_Contact  , @lc_sc_E_Mail , @lc_sc_E_Mail_New , @lc_sc_Phone_Country_Code , @lc_sc_Phone_Area_Code , @lc_sc_Phone , @lc_sc_Phone_Extension , @lc_sc_Fax_Country_Code , @lc_sc_Fax_Area_Code , @lc_sc_Fax , @lc_sc_Mobile_Country_Code , @lc_sc_Mobile_Area_Code , @lc_sc_Mobile , @lc_sc_Inactive_Date  , @lc_sc_ATTRIBUTE_CATEGORY , @lc_sc_ATTRIBUTE1  , @lc_sc_ATTRIBUTE2  , @lc_sc_ATTRIBUTE3  , @lc_sc_ATTRIBUTE4  , @lc_sc_ATTRIBUTE5  , @lc_sc_ATTRIBUTE6  , @lc_sc_ATTRIBUTE7  , @lc_sc_ATTRIBUTE8  , @lc_sc_ATTRIBUTE9  , @lc_sc_ATTRIBUTE10  , @lc_sc_ATTRIBUTE11  , @lc_sc_ATTRIBUTE12  , @lc_sc_ATTRIBUTE13  , @lc_sc_ATTRIBUTE14  , @lc_sc_ATTRIBUTE15  , @lc_sc_ATTRIBUTE16  , @lc_sc_ATTRIBUTE17  , @lc_sc_ATTRIBUTE18  , @lc_sc_ATTRIBUTE19  , @lc_sc_ATTRIBUTE20  , @lc_sc_ATTRIBUTE21  , @lc_sc_ATTRIBUTE22  , @lc_sc_ATTRIBUTE23  , @lc_sc_ATTRIBUTE24  , @lc_sc_ATTRIBUTE25  , @lc_sc_ATTRIBUTE26  , @lc_sc_ATTRIBUTE27  , @lc_sc_ATTRIBUTE28  , @lc_sc_ATTRIBUTE29  , @lc_sc_ATTRIBUTE30, @ln_sc_ATTRIBUTE_NUMBER1 , @ln_sc_ATTRIBUTE_NUMBER2 , @ln_sc_ATTRIBUTE_NUMBER3 , @ln_sc_ATTRIBUTE_NUMBER4 , @ln_sc_ATTRIBUTE_NUMBER5 , @ln_sc_ATTRIBUTE_NUMBER6 , @ln_sc_ATTRIBUTE_NUMBER7 , @ln_sc_ATTRIBUTE_NUMBER8 , @ln_sc_ATTRIBUTE_NUMBER9 , @ln_sc_ATTRIBUTE_NUMBER10 , @ln_sc_ATTRIBUTE_NUMBER11 , @ln_sc_ATTRIBUTE_NUMBER12 , @ld_sc_ATTRIBUTE_DATE1  , @ld_sc_ATTRIBUTE_DATE2  , @ld_sc_ATTRIBUTE_DATE3  , @ld_sc_ATTRIBUTE_DATE4  , @ld_sc_ATTRIBUTE_DATE5  , @ld_sc_ATTRIBUTE_DATE6  , @ld_sc_ATTRIBUTE_DATE7  , @ld_sc_ATTRIBUTE_DATE8  , @ld_sc_ATTRIBUTE_DATE9  , @ld_sc_ATTRIBUTE_DATE10  , @ld_sc_ATTRIBUTE_DATE11  , @ld_sc_ATTRIBUTE_DATE12  , @lc_sc_Creation_Date   , @lc_sc_Last_Update_Date  , @lc_sc_Created_By , @lc_sc_Updated_By 
						END
						CLOSE cur_suppliers_contacts
						DEALLOCATE cur_suppliers_contacts
			
						DECLARE cur_suppliers_contacts_addr CURSOR LOCAL FOR 
						SELECT Record_ID, Process_Flag, Error_Message, Batch_ID, Source_Record_ID, Import_Action, Supplier_Name, Address_Name, First_Name, Last_Name, E_Mail, Creation_Date  , Last_Update_Date  , Created_By  , Updated_By   
						FROM hiscox_poz_supp_contact_addresses_int_stg
						WHERE 1=1
						--AND process_flag        = 'N'
						AND batch_id            = @p_new_batch_id
						--AND source_record_id    = @ln_sc_Source_Record_ID
						--AND supplier_name       = @lc_sc_Supplier_Name
						--AND first_name          = @lc_sc_first_name
						--AND ISNULL(first_name,'X')    = ISNULL(@lc_sc_First_Name,'X') -- To be modified at later point
						
						OPEN cur_suppliers_contacts_addr  
						FETCH NEXT FROM cur_suppliers_contacts_addr INTO @ln_sca_Record_ID, @lc_sca_Process_Flag, @lc_sca_Error_Message, @lc_sca_Batch_ID, @ln_sca_Source_Record_ID, @lc_sca_Import_Action, @lc_sca_Supplier_Name, @lc_sca_Address_Name, @lc_sca_First_Name, @lc_sca_Last_Name, @lc_sca_E_Mail, @lc_sca_Creation_Date  , @lc_sca_Last_Update_Date  , @lc_sca_Created_By  , @lc_sca_Updated_By   
						WHILE @@FETCH_STATUS = 0 
						BEGIN 
							BEGIN TRY   -- Try Catch for Supplier Contact Addresses FBDI
								PRINT '----Supplier Contact Address- ' + CAST(@lc_sca_Address_Name AS VARCHAR);
							
								IF @lc_sca_First_Name IS NULL OR @lc_sca_Last_Name IS NULL
									BEGIN
										SET @lc_Err_Msg = @lc_Err_Msg  +'First Name/Last Name cannot be NULL for Supplier Contact Address. '
									END
								ELSE
									BEGIN
										SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_sup_contacts_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND supplier_name		= @lc_sca_Supplier_Name
													AND first_name			= @lc_sca_First_Name
													AND last_name			= @lc_sca_Last_Name
												)
										
										IF @ln_check < 1
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Contact is invalid for this Supplier Contact Address. ',1,4000)
										END
									END
								
								IF @lc_sca_Address_Name IS NULL 
									BEGIN
										SET @lc_Err_Msg = @lc_Err_Msg  +'Address Name cannot be NULL for Supplier Contact Address. '
									END
								ELSE
									BEGIN
										SET @ln_check = (	SELECT count(*)
															FROM hiscox_poz_supplier_addresses_int_stg
															WHERE 1					= 1
															AND batch_id            = @p_new_batch_id
															AND process_flag    	= 'S'
															AND address_name		= @lc_sca_Address_Name
															AND supplier_name		= @lc_sca_Supplier_Name
														)
										
										IF @ln_check < 1
										BEGIN
											SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Address is invalid for this Supplier Contact Address. ',1,4000)
										END
									END
								
								
								IF @lc_sca_Supplier_Name IS NULL 
								BEGIN
									SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier Name cannot be NULL for Supplier Contact Address. ',1,4000)
								END
								ELSE
								BEGIN
									-- Check if the Supplier Sites has its corresponding Supplier
									SET @ln_check = (	SELECT count(*)
													FROM hiscox_poz_suppliers_int_stg
													WHERE 1					= 1
													AND batch_id            = @p_new_batch_id
													AND process_flag    	= 'S'
													AND supplier_name		= @lc_sca_Supplier_Name
												)
										
									IF @ln_check <> 1
									BEGIN
										SET @lc_Err_Msg = SUBSTRING(@lc_Err_Msg  +'Supplier -'+@lc_sca_Supplier_Name+' is invalid for this supplier Contact Address. ',1,4000)
									END
								END
								
								-- Check if the validation has failed
								IF @lc_Err_Msg <> ''
								BEGIN
									THROW 60000,@lc_Err_Msg,5
								END
								
								UPDATE hiscox_poz_supp_contact_addresses_int_stg
								SET process_flag    	='S',
								last_update_date		= GETDATE(),
								updated_by				= USER_NAME()
								WHERE record_id     	=@ln_sca_Record_ID
														
							END TRY     -- Try Catch for Supplier Contact Addresses FBDI
							BEGIN CATCH -- Try Catch for Supplier Contact Addresses FBDI
								SET @lc_Err_Msg = ERROR_MESSAGE()
								
								UPDATE hiscox_poz_supp_contact_addresses_int_stg
								SET process_flag    	= 'E'
								,error_message      	= @lc_Err_Msg
								,last_update_date		= GETDATE()
								,updated_by				= USER_NAME()
								WHERE record_id     	=@ln_sca_Record_ID
							END CATCH   -- Try Catch for Supplier Contact Addresses FBDI
							FETCH NEXT FROM cur_suppliers_contacts_addr INTO @ln_sca_Record_ID, @lc_sca_Process_Flag, @lc_sca_Error_Message, @lc_sca_Batch_ID, @ln_sca_Source_Record_ID, @lc_sca_Import_Action, @lc_sca_Supplier_Name, @lc_sca_Address_Name, @lc_sca_First_Name, @lc_sca_Last_Name, @lc_sca_E_Mail, @lc_sca_Creation_Date  , @lc_sca_Last_Update_Date  , @lc_sca_Created_By  , @lc_sca_Updated_By   
						END 
						CLOSE cur_suppliers_contacts_addr
						DEALLOCATE cur_suppliers_contacts_addr
					*/
					
		
					PRINT('Out of the Loop ')
					
			
					SET @x_supplier =(  SELECT count(*)
										FROM hiscox_poz_suppliers_int_stg
										WHERE batch_id      =@p_new_batch_id
										AND process_flag    ='S'
										AND oracle_process_flag	<>'S'
									)
					SET @x_supplier_add =(  SELECT count(*)
										FROM hiscox_poz_supplier_addresses_int_stg
										WHERE batch_id      =@p_new_batch_id
										AND process_flag    ='S'
										AND oracle_process_flag	<>'S'
									)
					SET @x_supplier_site =( SELECT count(*)
										FROM hiscox_poz_supplier_sites_int_stg
										WHERE batch_id      =@p_new_batch_id
										AND process_flag    ='S'
										AND oracle_process_flag	<>'S'
									)
									
					SET @x_supplier_site_ass =( SELECT count(*)
										FROM hiscox_poz_site_assignments_int_stg
										WHERE batch_id      =@p_new_batch_id
										AND process_flag    ='S'
										AND oracle_process_flag	<>'S'
									)
					/*SET @x_supplier_cnt =(  SELECT count(*)
										FROM hiscox_poz_sup_contacts_int_stg
										WHERE batch_id      =@p_new_batch_id
										AND process_flag    ='S'
									) +
									(   SELECT count(*)
										FROM hiscox_poz_supp_contact_addresses_int_stg
										WHERE batch_id      =@p_new_batch_id
										AND process_flag    ='S'
									) */
					SET @x_supplier_cnt	= 0
					SET @lc_Err_Msg='Supplier-'+CAST(@x_supplier AS VARCHAR)+'SupplierAdd-'+CAST(@x_supplier_add AS VARCHAR)+'SupplierSite-'+CAST(@x_supplier_site AS VARCHAR)+'SupplierSite-'+CAST(@x_supplier_site_ass AS VARCHAR)+'SupplierCnt+CntAdd-'+CAST(@x_supplier_cnt AS VARCHAR)
					EXEC hiscox_cnv_log_errors_p @p_rice_id        = @lc_rice_id      
															,@p_batch_id       = @p_new_batch_id
															,@p_process_id     = 0          
															,@p_source         = @lc_source
															,@p_pri_record_id  = @p_new_batch_id
															,@p_sec_record_id  = NULL
															,@p_ter_record_id  = NULL
															,@p_err_code       = 'DEBUG'
															,@p_err_column     = 'DEBUG'  
															,@p_err_value      = 'DEBUG'  
															,@p_err_desc       = @lc_Err_Msg
															,@p_rect_action    = NULL; 
									
					SET @ln_count_rec            	= @x_supplier+@x_supplier_add+@x_supplier_site+@x_supplier_site_ass+@x_supplier_cnt
					IF @ln_count_rec< 1
						BEGIN
							SET @x_error_code       = 2
							SET @x_error_msg        = 'No validated Records to process further'
						END
					ELSE
						BEGIN
							SET @x_error_code       = 0
							SET @x_error_msg        = ''
						END
				END
			ELSE 
				BEGIN
					SET @x_error_code       = 2
					SET @x_error_msg        = 'Batch cannot be NULL'
					
					SET @lc_Err_Msg='p_new_batch_id-'+ISNULL(CAST(@p_new_batch_id AS VARCHAR),'')+'p_reprocess_all-'+ISNULL(@p_reprocess_all,'')+'p_new_file-'+ISNULL(@p_new_file,'')+'p_batch_id-'+ISNULL(@p_batch_id,'')+'p_load_batch_id-'+ISNULL(@p_load_batch_id,'')
					EXEC hiscox_cnv_log_errors_p 		@p_rice_id         = @lc_rice_id      
												,@p_batch_id       = 'NULL'
												,@p_process_id     = 0          
												,@p_source         = @lc_source
												,@p_pri_record_id  = 'NULL'
												,@p_sec_record_id  = NULL
												,@p_ter_record_id  = NULL
												,@p_err_code       = 'EXCEPTION'
												,@p_err_column     = 'ENT_VAL_P INVALID PARAM'  
												,@p_err_value      = 'EXCEPTION'  
												,@p_err_desc       = @lc_Err_Msg
												,@p_rect_action    = NULL; 
				END
        END
		
		EXEC hiscox_cnv_log_errors_p 		@p_rice_id         	   = @lc_rice_id      
												,@p_batch_id       = @p_new_batch_id
												,@p_process_id     = 0          
												,@p_source         = @lc_source
												,@p_pri_record_id  = @p_new_batch_id
												,@p_sec_record_id  = NULL
												,@p_ter_record_id  = NULL
												,@p_err_code       = 'DEBUG'
												,@p_err_column     = 'ENT_VAL_P END'
												,@p_err_value      = 'DEBUG'  
												,@p_err_desc       = @x_error_msg
												,@p_rect_action    = NULL; 
		
		
        END TRY
        
        BEGIN CATCH
			SET @lc_Err_Msg = ERROR_MESSAGE()
            Print 'Error Occured in Supplier Validation Procedure:'  
            Print  ERROR_MESSAGE() 
			EXEC hiscox_cnv_log_errors_p 		@p_rice_id         = @lc_rice_id      
												,@p_batch_id       = @p_new_batch_id
												,@p_process_id     = 0          
												,@p_source         = @lc_source
												,@p_pri_record_id  = @p_new_batch_id
												,@p_sec_record_id  = NULL
												,@p_ter_record_id  = NULL
												,@p_err_code       = 'EXCEPTION'
												,@p_err_column     = 'ENT_VAL_P Overall Procedure Error'  
												,@p_err_value      = 'EXCEPTION'  
												,@p_err_desc       = @lc_Err_Msg
												,@p_rect_action    = NULL; 
        END CATCH
        
    END
    GO
